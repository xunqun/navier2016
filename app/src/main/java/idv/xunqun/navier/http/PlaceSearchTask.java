package idv.xunqun.navier.http;

import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.v2.content.Place;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

public class PlaceSearchTask extends AsyncTask<Void, Void, List> {

	private String mKey;
	private LatLng mLocation;
	private Context mContext;

	private OnPlaceSearchListener mListener;

	public PlaceSearchTask(Context context,String key, LatLng location, OnPlaceSearchListener listener) {
		mContext = context;
		mKey = key;
		mLocation = location;
		mListener = listener;
	}

	public interface OnPlaceSearchListener {
		void onPostExecute(List result);
	}

	@Override
	protected void onPostExecute(List result) {
		if (mListener != null)
			mListener.onPostExecute(result);
		super.onPostExecute(result);
	}

	@Override
	protected List doInBackground(Void... params) {
		
		PlacesSearchRequester requester = new PlacesSearchRequester(mKey, mLocation);
		try {

			String result = requester.sentHttpRequest();
			if (result != null) {
				List list = parseResult(result);
				if(list!=null && list.size()>0){
					return list;
				}else{
					result = new KeywordSearchRequester(mKey).sentHttpRequest();
					if(result!=null){
						list = parseResult(result);
						return list;
					}
				}
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return null;
	}

	

	private List parseResult(String result) throws JSONException {
		ArrayList<Place> list = new ArrayList<Place>();
		JSONObject jsonObject = new JSONObject(result);
		if (jsonObject.getString("status").equals("OK")) {
			JSONArray array = jsonObject.getJSONArray("results");
			for (int i = 0; i < array.length(); i++) {
				JSONObject item = array.getJSONObject(i);

				Place place = new Place();
				place.address = item.has("vicinity") ? item.getString("vicinity") : "";
				place.iconUri = item.getString("icon");
				
				double lat = item.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
				double lng = item.getJSONObject("geometry").getJSONObject("location").getDouble("lng");
				place.latitude = lat;
				place.longitude = lng;
				place.name = item.getString("name");
//				place.rating = item.has("rating") ? item.getDouble("rating") : 0;
//				place.reference = item.getString("reference");
				list.add(place);
			}
			return list;
		} else {
			return null;
		}

	}

}
