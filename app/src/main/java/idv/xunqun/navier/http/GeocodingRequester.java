package idv.xunqun.navier.http;

import idv.xunqun.navier.content.Latlng;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class GeocodingRequester {
	
	public static final String GEOCODING_URL = "http://maps.google.com/maps/api/geocode/json?";
	
	private double _latitude;
	private double _longitude;
	private String _parameter;
	
	Latlng destination;
	HttpClient client;
	HttpGet request;
	
	
	public GeocodingRequester(double lat, double lng){
		_latitude = lat;
		_longitude = lng;	
		_parameter = "latlng="+lat+","+lng+
					"&sensor=true";
		
		
		//request.addHeader("Accept-Language", Locale.getDefault().toString());
	}
	
	public String sentHttpRequest() throws Exception{
		BufferedReader in=null;
		try {

			
			client = new DefaultHttpClient();
			request = new HttpGet(this.GEOCODING_URL+_parameter);
			request.addHeader("Accept-Language", Locale.getDefault().toString());
			
			Log.d("mine",this.GEOCODING_URL+_parameter);
			
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line="";
			String NL =System.getProperty("line.separator");
			while((line=in.readLine())!= null){
				sb.append(line+NL);				
			}
			in.close();
			
			String result=sb.toString();		
			
						
			return result;
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine",e.toString());
			e.printStackTrace();
			
		}finally{
			
			if(in!=null){
				try{
					in.close();
				}catch(IOException e){
					e.printStackTrace();
				}
				
			}
		}
		return null;
	}
	
	public static JSONObject string2Object(String str) throws JSONException{
		
		return new JSONObject(str);
		
	}
}
