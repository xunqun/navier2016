package idv.xunqun.navier.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class MylayoutCheckRequester {

	public static final String GEOCODING_URL = "http://navierlab.appspot.com/synclayout?";
	
	private HttpClient client;
	private HttpGet request;
	private String _parameter;
	private Context _context;
	
	public MylayoutCheckRequester(String user, Context context){
		
		_parameter = "user="+user;
		_context = context;
	}
	
	public String sentHttpRequest() throws Exception{
		BufferedReader in=null;
		try {

			
			client = new DefaultHttpClient();
			request = new HttpGet(this.GEOCODING_URL+_parameter);
			request.addHeader("Accept-Language", Locale.getDefault().toString());
			
			Log.d("GAE","GET: "+this.GEOCODING_URL+_parameter);
			
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line="";
			String NL =System.getProperty("line.separator");
			while((line=in.readLine())!= null){
				sb.append(line+NL);				
			}
			in.close();
			
			String result=sb.toString();		
			
						
			return result;
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(_context, "Data sync fail, Please try again later", Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine",e.toString());
			e.printStackTrace();
			Toast.makeText(_context, "Data sync fail, Please try again later", Toast.LENGTH_LONG).show();
			
		}finally{
			
			if(in!=null){
				try{
					in.close();
				}catch(IOException e){
					e.printStackTrace();
				}
				
			}
		}
		return null;
	}
}
