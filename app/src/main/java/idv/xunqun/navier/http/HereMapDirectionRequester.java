package idv.xunqun.navier.http;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.content.Latlng;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class HereMapDirectionRequester {

	public static final String TAG = "HereMapDirectionRequester";
	public static final int AVOID_TOLLS = 1;
	public static final int AVOID_HIGHWAY = 2;

	// destination location

	HttpClient mClient;
	HttpGet mRequest;

	// Google API
	private String DIRECTIONS_API = "http://route.cit.api.here.com/routing/7.2/calculateroute.json?";

	private Object _uri;
	private PreferencesHandler mPref;
	private Context mContext;

	public HereMapDirectionRequester(Context context, Latlng from, Latlng to) {

		mContext = context;
		mPref = PreferencesHandler.get(context);
		String mode = mPref.getPREF_TRANS_TYPE();
		if(mode.equals(PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_BICYCLE)){
			mode = "pedestrian";
		}else if(mode.equals(PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_PEDESTRIAN)){
			mode="pedestrian";
		}else{
			mode = "car";
		}
		
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http");
		builder.authority("route.api.here.com");
		builder.path("/routing/7.2/calculateroute.json");
		builder.appendQueryParameter("app_id", mContext.getString(R.string.here_app_id));
		builder.appendQueryParameter("app_code", mContext.getString(R.string.here_app_code));
		builder.appendQueryParameter("waypoint0", from.getLat() + "," + from.getLng());
		builder.appendQueryParameter("waypoint1", to.getLat() + "," + to.getLng());
		builder.appendQueryParameter("mode", "fastest;"+mode+";traffic:disabled");
		builder.appendQueryParameter("routeattributes", "all");
		builder.appendQueryParameter("language", Locale.getDefault().getLanguage() + "-" + Locale.getDefault().getCountry());
		builder.appendQueryParameter("metricSystem", mPref.getPREF_SPEED_UNIT() == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH?"metric":"imperial");
		builder.appendQueryParameter("alternatives", "3");
		builder.appendQueryParameter("maneuverAttributes", "rn,nr,sp,po,tt,le,no,ac");
		
		_uri = builder.build();
	}

	private String LatLngToText(LatLng latLng) {
		return String.valueOf(latLng.latitude) + "," + String.valueOf(latLng.longitude);
	}

	public String sentHttpRequest() throws Exception {

		BufferedReader in = null;
		try {

			mClient = new DefaultHttpClient();

			Log.i(TAG, "[SEND]" + _uri.toString());

			mRequest = new HttpGet(_uri.toString());
			// request = new HttpGet(DIRECTIONS_API+parameter);
			mRequest.addHeader("Accept-Language", Locale.getDefault().toString());

			HttpResponse response = mClient.execute(mRequest);

			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();

			String result = sb.toString();
			Log.i(TAG, "[RECEIVE]" + result);
			return result;

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		return null;

	}

}
