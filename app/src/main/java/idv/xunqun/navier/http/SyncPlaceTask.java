package idv.xunqun.navier.http;

import idv.xunqun.navier.MyDBOpenHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

public class SyncPlaceTask extends AsyncTask<Void, Void, Boolean> {

	//private static final String SYNCPLACE_URL = "http://navierlab.appspot.com/syncplace";
	private static final String SYNCPLACE_URL = "http://navierlab.appspot.com/syncplace";
	private static final String POST_KEY = "synctable";

	public interface SyncPlaceHandler {
		void onSyncPlaceComplete(boolean ok);
	}

	private Context mContext;
	private SyncPlaceHandler mHandler;
	private String mAccount;

	public SyncPlaceTask(Context context, SyncPlaceHandler handler,
			String account) {
		mContext = context;
		mHandler = handler;
		mAccount = account;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

	}

	@Override
	protected void onPostExecute(Boolean ok) {
		// TODO Auto-generated method stub
		super.onPostExecute(ok);

		if(ok){
			MyDBOpenHelper.getDb(mContext).delete(MyDBOpenHelper.SYNC_TABLE_NAME, "", new String[]{});
		}
		mHandler.onSyncPlaceComplete(ok);
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		try {
			doMyPlacesSync();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}

	private void doMyPlacesSync() throws Exception {

		ArrayList<HashMap> upstreamList = new ArrayList<HashMap>();
		// organize the params

		// myplace data

		String[] columns = { MyDBOpenHelper.SYNC_COL_ID,
				MyDBOpenHelper.SYNC_COL_PLACEID,
				MyDBOpenHelper.SYNC_COL_SYNCSTATE,
				MyDBOpenHelper.SYNC_COL_FINGERPRINT };

		String selection = "";
		String[] selectionArgs = {};
		String groupBy = "";
		String having = "";
		String orderBy = MyDBOpenHelper.SYNC_COL_ID + " DESC";

		Cursor c = MyDBOpenHelper.getDb(mContext).query(MyDBOpenHelper.SYNC_TABLE_NAME, columns,
				selection, selectionArgs, groupBy, having, orderBy);
		if (c.getCount() > 0) { // have something to update

			while (c.moveToNext()) {

				HashMap map = new HashMap();

				map.put(MyDBOpenHelper.SYNC_COL_SYNCSTATE,
						c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_COL_SYNCSTATE)));
				map.put(MyDBOpenHelper.COL_FINGERPRINT,
						c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_COL_FINGERPRINT)));

				//
				if (!c.getString(
						c.getColumnIndex(MyDBOpenHelper.SYNC_COL_SYNCSTATE))
						.equalsIgnoreCase(MyDBOpenHelper.STATE_KILLED)) {

					String[] columns1 = { MyDBOpenHelper.COL_ID,
							MyDBOpenHelper.COL_PLACE_NAME,
							MyDBOpenHelper.COL_ADDRESS,
							MyDBOpenHelper.COL_DESCRIPTION,
							MyDBOpenHelper.COL_FAVERITE,
							MyDBOpenHelper.COL_LATITUDE,
							MyDBOpenHelper.COL_LONGITUDE,
							MyDBOpenHelper.COL_TIMESTAMP,
							MyDBOpenHelper.COL_FINGERPRINT };

					String selection1 = MyDBOpenHelper.COL_ID + "=?";
					String[] selectionArgs1 = { c
							.getString(c
									.getColumnIndex(MyDBOpenHelper.SYNC_COL_PLACEID)) };
					String groupBy1 = "";
					String having1 = "";
					String orderBy1 = MyDBOpenHelper.COL_TIMESTAMP
							+ " DESC";
					Cursor c1 = MyDBOpenHelper.getDb(mContext).query(MyDBOpenHelper.TABLE_NAME,
							columns1, selection1, selectionArgs1, groupBy1,
							having1, orderBy1);
					if (c1.getCount() > 0) {

						c1.moveToFirst();

						map.put(MyDBOpenHelper.COL_ID, c1.getString(c1
								.getColumnIndex(MyDBOpenHelper.COL_ID)));
						map.put(MyDBOpenHelper.COL_PLACE_NAME,
								c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_PLACE_NAME)));
						map.put(MyDBOpenHelper.COL_ADDRESS,
								c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_ADDRESS)));
						map.put(MyDBOpenHelper.COL_DESCRIPTION,
								c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_DESCRIPTION)));
						map.put(MyDBOpenHelper.COL_LATITUDE,
								c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_LATITUDE)));
						map.put(MyDBOpenHelper.COL_LONGITUDE,
								c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_LONGITUDE)));
						map.put(MyDBOpenHelper.COL_FAVERITE,
								c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_FAVERITE)));
						map.put(MyDBOpenHelper.COL_TIMESTAMP,
								c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_TIMESTAMP)));

					}

					c1.close();

				} else {
					// if killed only have id, fingerprint and state
					map.put(MyDBOpenHelper.COL_ID,
							c.getString(c
									.getColumnIndex(MyDBOpenHelper.SYNC_COL_PLACEID)));

				}
				upstreamList.add(map);

			}

			try {

				String params = postMyplaceParamOrganize(upstreamList);

				Log.d("GAE", "POST: " + params);
				String results = sentMyplaceHttpPost(params);
				Log.d("GAE", "RECEIVED: " + results);
				localSync(results);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {

				// db.close();

			}
		} else {

			String results;
			try {
				results = sentMyplaceHttpGet();
				Log.d("GAE", "RECEIVED: " + results);
				localSync(results);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			} finally {
				// db.close();

			}

		}
		c.close();

	}

	private String postMyplaceParamOrganize(ArrayList<HashMap> myplaceList)
			throws JSONException {

		String param = "";
		JSONObject jsonRoot = new JSONObject();

		jsonRoot.put("user", mAccount);

		if (myplaceList.size() > 0) {

			JSONArray jsonArray = new JSONArray();
			for (int i = 0; i < myplaceList.size(); i++) {
				JSONObject jsonPlace = new JSONObject();

				String state = (String) myplaceList.get(i).get(
						MyDBOpenHelper.SYNC_COL_SYNCSTATE);

				jsonPlace.put(MyDBOpenHelper.SYNC_COL_SYNCSTATE, state);

				int id = myplaceList.get(i).get(MyDBOpenHelper.COL_ID)==null ? 0 : Integer.parseInt((String) myplaceList.get(i).get(MyDBOpenHelper.COL_ID));
				jsonPlace.put("id",id);
				jsonPlace.put(
						MyDBOpenHelper.COL_FINGERPRINT,
						String.valueOf(myplaceList.get(i).get(
								MyDBOpenHelper.COL_FINGERPRINT)));

				if (!state.equalsIgnoreCase(MyDBOpenHelper.STATE_KILLED)) {

					jsonPlace.put(
							MyDBOpenHelper.COL_PLACE_NAME,
							myplaceList.get(i).get(
									MyDBOpenHelper.COL_PLACE_NAME));
					jsonPlace.put(MyDBOpenHelper.COL_ADDRESS, myplaceList
							.get(i).get(MyDBOpenHelper.COL_ADDRESS));
					jsonPlace.put(
							MyDBOpenHelper.COL_DESCRIPTION,
							myplaceList.get(i).get(
									MyDBOpenHelper.COL_DESCRIPTION));
					jsonPlace.put(
							MyDBOpenHelper.COL_LATITUDE,
							Float.parseFloat((String) myplaceList.get(i).get(
									MyDBOpenHelper.COL_LATITUDE)));
					jsonPlace.put(
							MyDBOpenHelper.COL_LONGITUDE,
							Float.parseFloat((String) myplaceList.get(i).get(
									MyDBOpenHelper.COL_LONGITUDE)));
					jsonPlace.put(
							MyDBOpenHelper.COL_FAVERITE,
							Integer.parseInt((String) myplaceList.get(i).get(
									MyDBOpenHelper.COL_FAVERITE)));
					jsonPlace.put(
							MyDBOpenHelper.COL_TIMESTAMP,
							String.valueOf(myplaceList.get(i).get(
									MyDBOpenHelper.COL_TIMESTAMP)));

				} else {

					jsonPlace.put(MyDBOpenHelper.COL_PLACE_NAME, "");
					jsonPlace.put(MyDBOpenHelper.COL_ADDRESS, "");
					jsonPlace.put(MyDBOpenHelper.COL_DESCRIPTION, "");
					jsonPlace.put(MyDBOpenHelper.COL_LATITUDE, "");
					jsonPlace.put(MyDBOpenHelper.COL_LONGITUDE, "");
					jsonPlace.put(MyDBOpenHelper.COL_FAVERITE, "");
					jsonPlace.put(MyDBOpenHelper.COL_TIMESTAMP, "");
				}

				jsonArray.put(jsonPlace);
			}
			jsonRoot.put("myplaces", jsonArray);
		}

		return jsonRoot.toString();
	}

	private String sentMyplaceHttpGet() throws Exception {
		BufferedReader in = null;
		String parameter = "?user=" + mAccount;
		try {

			DefaultHttpClient client = GlobalHttpClient.getHttpClient();
			HttpGet request = new HttpGet(SYNCPLACE_URL + parameter);
			request.addHeader("Accept-Language", Locale.getDefault().toString());
			Log.d("GAE", "GET: " + SYNCPLACE_URL + parameter);

			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();

			String result = sb.toString();

			return result;

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine", e.toString());
			e.printStackTrace();

		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		return null;

	}

	public String sentMyplaceHttpPost(String strParam) throws Exception {

		List<NameValuePair> paramList = new ArrayList<NameValuePair>();
		paramList.add(new BasicNameValuePair(POST_KEY, strParam));

		BasicHttpParams params = new BasicHttpParams();
		params.setParameter(POST_KEY, URLEncoder.encode(strParam, "UTF-8"));

		try {

			DefaultHttpClient client = GlobalHttpClient.getHttpClient();
			HttpPost request = new HttpPost(this.SYNCPLACE_URL);

			/*
			 * request.addHeader("Accept-Language",
			 * Locale.getDefault().toString());
			 * request.addHeader("Content-Type", "text/plain");
			 */
			request.setEntity(new UrlEncodedFormEntity(paramList, HTTP.UTF_8));

			HttpResponse response = client.execute(request);
			Log.d("GAE", response.getStatusLine().getStatusCode() + "");
			if (response.getStatusLine().getStatusCode() == 200) {
				String result = EntityUtils.toString(response.getEntity());

				Log.d("GAE", result);
				MyDBOpenHelper.getDb(mContext).delete(MyDBOpenHelper.SYNC_TABLE_NAME, "", null);

				return result;
			}

			// clearSyncTable();

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine", e.toString());
			e.printStackTrace();

		} finally {

		}
		return null;
	}

	public void localSync(String str) throws JSONException {

		if (str != null) {

			JSONObject jsonRoot = new JSONObject(str);

			JSONArray myplaces = jsonRoot.getJSONArray("myplaces");

			MyDBOpenHelper.getDb(mContext).delete(MyDBOpenHelper.TABLE_NAME, "", new String[] {});
			MyDBOpenHelper.getDb(mContext).delete(MyDBOpenHelper.SYNC_TABLE_NAME, "", new String[] {});

			for (int i = 0; i < myplaces.length(); i++) {

				JSONObject place = (JSONObject) myplaces.get(i);
				int id = Integer.parseInt(place.getString("id"));
				String fingerprint = place
						.getString(MyDBOpenHelper.COL_FINGERPRINT);
				HashMap map = new HashMap();

				map.put(MyDBOpenHelper.COL_ID, place.getString("id"));
				map.put(MyDBOpenHelper.COL_PLACE_NAME,
						place.getString(MyDBOpenHelper.COL_PLACE_NAME));
				map.put(MyDBOpenHelper.COL_ADDRESS,
						place.getString(MyDBOpenHelper.COL_ADDRESS));
				map.put(MyDBOpenHelper.COL_DESCRIPTION,
						place.getString(MyDBOpenHelper.COL_DESCRIPTION));
				map.put(MyDBOpenHelper.COL_LATITUDE,
						place.getString(MyDBOpenHelper.COL_LATITUDE));
				map.put(MyDBOpenHelper.COL_LONGITUDE,
						place.getString(MyDBOpenHelper.COL_LONGITUDE));
				map.put(MyDBOpenHelper.COL_FAVERITE,
						place.getString(MyDBOpenHelper.COL_FAVERITE));
				map.put(MyDBOpenHelper.COL_TIMESTAMP,
						place.getString(MyDBOpenHelper.COL_TIMESTAMP));
				map.put(MyDBOpenHelper.COL_FINGERPRINT,
						place.getString(MyDBOpenHelper.COL_FINGERPRINT));

				if (checkMyplaceDataAvailable(fingerprint)) {
					// update data

					updateMyplace2DB(map);

				} else {

					addMyplace2DB(map);
				}

			}

		}

	}

	private boolean checkMyplaceDataAvailable(String fingerprint) {

		String[] columns = { MyDBOpenHelper.COL_ID };
		String selection = MyDBOpenHelper.COL_FINGERPRINT + "=?";
		String[] selectionArgs = { fingerprint };
		String groupBy = "";
		String having = "";
		String orderBy = MyDBOpenHelper.COL_TIMESTAMP + " DESC";

		Cursor c = MyDBOpenHelper.getDb(mContext).query(MyDBOpenHelper.TABLE_NAME, columns,
				selection, selectionArgs, groupBy, having, orderBy);

		if (c.getCount() > 0) {
			c.close();

			return true;
		} else {
			c.close();

			return false;
		}

	}

	private void updateMyplace2DB(HashMap map) {

		ContentValues values = new ContentValues();

		values.put(MyDBOpenHelper.COL_PLACE_NAME,
				(String) map.get(MyDBOpenHelper.COL_PLACE_NAME));
		values.put(MyDBOpenHelper.COL_ADDRESS,
				(String) map.get(MyDBOpenHelper.COL_ADDRESS));
		values.put(MyDBOpenHelper.COL_DESCRIPTION,
				(String) map.get(MyDBOpenHelper.COL_DESCRIPTION));
		values.put(MyDBOpenHelper.COL_LATITUDE,
				(String) map.get(MyDBOpenHelper.COL_LATITUDE));
		values.put(MyDBOpenHelper.COL_LONGITUDE,
				(String) map.get(MyDBOpenHelper.COL_LONGITUDE));
		values.put(MyDBOpenHelper.COL_FAVERITE,
				(String) map.get(MyDBOpenHelper.COL_FAVERITE));
		values.put(MyDBOpenHelper.COL_TIMESTAMP,
				(String) map.get(MyDBOpenHelper.COL_TIMESTAMP));

		String[] args = { (String) map
				.get(MyDBOpenHelper.COL_FINGERPRINT) };

		MyDBOpenHelper.getDb(mContext).update(MyDBOpenHelper.TABLE_NAME, values,
				MyDBOpenHelper.COL_FINGERPRINT + "=?", args);

	}

	private void addMyplace2DB(HashMap map) {

		ContentValues values = new ContentValues();
		// values.put(MyPlacesDBOpenHelper.COL_ID, (String)
		// map.get(MyPlacesDBOpenHelper.COL_ID));
		values.put(MyDBOpenHelper.COL_PLACE_NAME,
				(String) map.get(MyDBOpenHelper.COL_PLACE_NAME));
		values.put(MyDBOpenHelper.COL_ADDRESS,
				(String) map.get(MyDBOpenHelper.COL_ADDRESS));
		values.put(MyDBOpenHelper.COL_DESCRIPTION,
				(String) map.get(MyDBOpenHelper.COL_DESCRIPTION));
		values.put(MyDBOpenHelper.COL_LATITUDE,
				(String) map.get(MyDBOpenHelper.COL_LATITUDE));
		values.put(MyDBOpenHelper.COL_LONGITUDE,
				(String) map.get(MyDBOpenHelper.COL_LONGITUDE));
		values.put(MyDBOpenHelper.COL_FAVERITE,
				(String) map.get(MyDBOpenHelper.COL_FAVERITE));
		values.put(MyDBOpenHelper.COL_TIMESTAMP,
				(String) map.get(MyDBOpenHelper.COL_TIMESTAMP));
		values.put(MyDBOpenHelper.COL_FINGERPRINT,
				(String) map.get(MyDBOpenHelper.COL_FINGERPRINT));

		MyDBOpenHelper.getDb(mContext).insert(MyDBOpenHelper.TABLE_NAME, "", values);

	}

}
