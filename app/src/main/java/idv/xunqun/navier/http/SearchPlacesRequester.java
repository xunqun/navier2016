package idv.xunqun.navier.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.net.Uri;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class SearchPlacesRequester {

//	public static final String URI = "https://maps.googleapis.com/maps/api/place/textsearch/json";
	public static final String URI = "https://maps.googleapis.com/maps/api/place/textsearch/json";
	public static final String API_KEY = "AIzaSyA8U6uVnA8xfoxRSXtJIhpHYnBnGAK6Gbc";
	private Uri _uri;
	
	
	public SearchPlacesRequester(String queryString, LatLng latlng){
		_uri = new Uri.Builder()
		.scheme("https")
	    .authority("maps.googleapis.com")
	    .path("/maps/api/place/nearbysearch/json")
	    .appendQueryParameter("keyword", queryString)
	    .appendQueryParameter("key", API_KEY)
	    .appendQueryParameter("radius", "50000")
	    .appendQueryParameter("location", latlng.latitude + "," + latlng.longitude)
	    .appendQueryParameter("sensor", "true")
	    .appendQueryParameter("language", Locale.getDefault().toString())
	    .build();
	}
	
	public String sentHttpRequest() throws Exception {
		BufferedReader in = null;
		try {

			DefaultHttpClient client = new DefaultHttpClient();
			
			Log.i("http",_uri.toString());
			
			HttpGet request = new HttpGet(_uri.toString());
			request.addHeader("Accept-Language", Locale.getDefault().toString());
			
			HttpResponse response = client.execute(request);
			
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();

			String result = sb.toString();

			return result;

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine", e.toString());
			e.printStackTrace();

		} finally {

			
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		
		
		return null;
		
	}
}
