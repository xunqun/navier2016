package idv.xunqun.navier.http;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.v2.content.Place;

import java.util.ArrayList;

import org.json.JSONObject;

import android.R.integer;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class DirectionTask extends AsyncTask<Void, Float, Boolean> {

	DirectionTaskListener _handler;
	PreferencesHandler _settings;
	private Context _context;
	private Latlng _current;
	private ArrayList<Latlng> _list;
	private String _reply_str;
	private String _status;
	private int _curLeg;

	// For direction result call back
	public interface DirectionTaskListener {
		void onDirectionTaskComplete(Boolean ok, String jsonResult,
									 String result);
	}

	/**
	 * Constructure for one destination
	 * 
	 * @param context
	 * @param current
	 *            Place object of current position
	 * @param destination
	 *            Place object of destination
	 */
	public DirectionTask(Context context, DirectionTaskListener handler,
			Latlng current, Latlng destination) {
		
		_handler = handler;
		_context = context;
		_settings = new PreferencesHandler(_context);
		_current = current;
		_list = new ArrayList<Latlng>();
		_list.add(destination);
	}

	/**
	 * Constructure for multiple destinations
	 * 
	 * @param context
	 * @param current
	 *            Place object of current position
	 * @param curLeg
	 *            Used to skip the leg which passed
	 * @param destination
	 *            Place objects in ArrayList of destination
	 */
	public DirectionTask(Context context, DirectionTaskListener handler,
			Latlng current, int curLeg, ArrayList<Latlng> destination) {
		// TODO Auto-generated constructor stub
		_handler = handler;
		_context = context;
		_settings = new PreferencesHandler(_context);
		_current = current;
		_curLeg = curLeg;
		_list = destination;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		_handler.onDirectionTaskComplete(result, _reply_str, _status);
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		String transMode = _settings.getPREF_TRANS_TYPE();
		int avoid = 0;
		if (_settings.getPREF_AVOID_TOLLS())
			avoid += DirectionRequester.AVOID_TOLLS;
		if (_settings.getPREF_AVOID_HIGHWAY())
			avoid += DirectionRequester.AVOID_HIGHWAY;

		DirectionRequester rq;
		if (_list.size() == 1) {
			// TODO Single destination
			rq = new DirectionRequester(_current, _list.get(0), transMode,
					avoid, _settings.getPREF_SPEED_UNIT() == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH?"metric":"imperial");
		} else {
			Latlng[] allLatlngs = new Latlng[_list.size() - _curLeg];
			for (int i = 0; i < _list.size() - _curLeg; i++) {
				allLatlngs[i] = _list.get(i + _curLeg);
			}

			
			rq = new DirectionRequester(_current, allLatlngs, transMode, avoid,
					false, _settings.getPREF_SPEED_UNIT() == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH?"metric":"imperial");
		}

		try {
			_reply_str = rq.sentHttpRequest();

			Log.i("http", _reply_str);

			JSONObject json = rq.string2Object(_reply_str);
			_status = json.getString("status");
			if (_status.equalsIgnoreCase("ok")) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;

	}

}
