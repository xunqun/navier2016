package idv.xunqun.navier.http;

import idv.xunqun.navier.v2.content.Place;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

public class FourSquarePlaceSearchTask extends AsyncTask<Void, Void, List> {

	private String mKey;
	private LatLng mLocation;
	private OnFourSquarePlaceSearchListener mListener;
	private Context mContext;

	public FourSquarePlaceSearchTask(Context context, String key, LatLng location, OnFourSquarePlaceSearchListener listener) {
		mContext = context;
		mKey = key;
		mLocation = location;
		mListener = listener;
	}

	public interface OnFourSquarePlaceSearchListener {
		void onPostExecute(List result);
	}

	@Override
	protected void onPostExecute(List result) {
		if (mListener != null)
			mListener.onPostExecute(result);
		super.onPostExecute(result);
	}

	@Override
	protected List doInBackground(Void... params) {
		FourSquarePlacesExplorerRequester requester = new FourSquarePlacesExplorerRequester(mContext, mKey, mLocation, "browse");
		try {

			String result = requester.sentHttpRequest();
			if (result != null) {
				List list = parseExploreResult(result);
				if (list != null && list.size() > 0) {
					return list;
				} else {
					FourSquarePlacesSearchRequester s_requester = new FourSquarePlacesSearchRequester(mContext, mKey, mLocation, "global");
					result = s_requester.sentHttpRequest();
					if (result != null) {
						list = parseResult(result);
						return list;
					}
				}

			}

		} catch (Exception e) {

			e.printStackTrace();
		}
		return null;
	}

	private List parseExploreResult(String result) throws JSONException {
		ArrayList<Place> list = new ArrayList<Place>();
		JSONObject jsonObject = new JSONObject(result);
		if (jsonObject.has("response")) {
			JSONArray array = jsonObject.getJSONObject("response").getJSONArray("groups");
			for (int i = 0; i < array.length(); i++) {

				JSONArray items = ((JSONObject) array.get(i)).getJSONArray("items");
				for (int j = 0; j < items.length(); j++) {
					JSONObject venue = items.getJSONObject(j).getJSONObject("venue");

					Place place = new Place();
					place.address = venue.getJSONObject("location").has("address") ? venue.getJSONObject("location").getString("address") : "";
					place.iconUri = "";

					double lat = venue.getJSONObject("location").getDouble("lat");
					double lng = venue.getJSONObject("location").getDouble("lng");
					place.latitude = lat;
					place.longitude = lng;
					place.name = venue.getString("name");
					// place.rating = item.has("rating") ?
					// item.getDouble("rating")
					// : 0;
					// place.reference = item.getString("reference");
					list.add(place);
				}
			}
		}
		return list;
	}

	private List parseResult(String result) throws JSONException {
		ArrayList<Place> list = new ArrayList<Place>();
		JSONObject jsonObject = new JSONObject(result);
		if (jsonObject.has("response")) {
			JSONArray array = jsonObject.getJSONObject("response").getJSONArray("venues");
			for (int i = 0; i < array.length(); i++) {
				JSONObject item = array.getJSONObject(i);

				Place place = new Place();
				place.address = item.getJSONObject("location").has("address") ? item.getJSONObject("location").getString("address") : "";
				place.iconUri = "";

				double lat = item.getJSONObject("location").getDouble("lat");
				double lng = item.getJSONObject("location").getDouble("lng");
				place.latitude = lat;
				place.longitude = lng;
				place.name = item.getString("name");
				// place.rating = item.has("rating") ? item.getDouble("rating")
				// : 0;
				// place.reference = item.getString("reference");
				list.add(place);
			}
			return list;
		} else {
			return null;
		}

	}

}
