package idv.xunqun.navier.http;

import idv.xunqun.navier.content.Latlng;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.animation.AnimatorSet.Builder;
import android.net.Uri;
import android.util.Log;

public class DirectionRequester {

	// destination location

	HttpClient client;
	HttpGet request;

	// Google API
	// private String DIRECTIONS_API =
	// "http://maps.google.com/maps/api/directions/json?";
	private String DIRECTIONS_API = "http://maps.googleapis.com/maps/api/directions/json?";

	private String parameter;
	public static final int AVOID_TOLLS = 1;
	public static final int AVOID_HIGHWAY = 2;
	// HTTP components
	private String _transMode;
	private Object _uri;

	/**
	 * For single destination
	 * 
	 * @param s
	 *            Departure location
	 * @param d
	 *            Destination location
	 * @param transMode
	 *            Vehicle type
	 * @param avoid
	 *            AVOID_TOLLS and AVOID_HIGHWAY;
	 */
	public DirectionRequester(Latlng s, Latlng d, String transMode, int avoid, String unit) {

		_transMode = transMode;

		// initiat parameter
		String strAvoid = getAvoidParamText(avoid);

		parameter = "origin=" + s.printString() + "&destination="
				+ d.printString() + "&mode=" + _transMode + strAvoid
				+ "&sensor=true" + "&language=" + Locale.getDefault();

		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http");
		builder.authority("maps.google.com");
		builder.path("/maps/api/directions/json");
		builder.appendQueryParameter("origin", s.printString());
		builder.appendQueryParameter("destination", d.printString());
		builder = addAvoidParam(avoid, builder);
		builder.appendQueryParameter("units", unit);
		builder.appendQueryParameter("mode", _transMode);
		builder.appendQueryParameter("sensor", "true");
		builder.appendQueryParameter("alternatives", "true");
		builder.appendQueryParameter("language", Locale.getDefault().toString());
		_uri = builder.build();
	}

	private Uri.Builder addAvoidParam(int avoid, Uri.Builder builder) {
		// TODO Auto-generated method stub
		switch (avoid) {
		case 0:

			break;
		case AVOID_TOLLS:
			builder.appendQueryParameter("avoid", "tolls");

			break;
		case AVOID_HIGHWAY:
			builder.appendQueryParameter("avoid", "highways");

			break;
		case AVOID_TOLLS + AVOID_HIGHWAY:
			builder.appendQueryParameter("avoid", "tolls");
			builder.appendQueryParameter("avoid", "highways");
			break;
		}
		return builder;
	}

	/**
	 * For multiple destination
	 * 
	 * @param s
	 * @param d
	 * @param transMode
	 * @param avoid
	 */
	public DirectionRequester(Latlng s, Latlng[] d, String transMode, int avoid, String unit) {
		this(s, d, transMode, avoid, false, unit);

	}

	public DirectionRequester(Latlng s, Latlng[] d, String transMode, int avoid, boolean optimize, String unit) {
		_transMode = transMode;

		// initiat parameter
		String strAvoid = getAvoidParamText(avoid);

		parameter = "origin=" + s.printString() +
				"&destination=" + d[d.length - 1].printString() +
				getWayPointsParam(d, optimize) +
				"&mode=" + _transMode + strAvoid +
				"&sensor=true" + "&language=" + Locale.getDefault();

		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http");
		builder.authority("maps.google.com");
		builder.path("/maps/api/directions/json");
		builder.appendQueryParameter("origin", s.printString());
		builder.appendQueryParameter("destination", d[d.length - 1].printString());
		builder.appendQueryParameter("waypoints", getWayPointsParam(d, optimize));
		builder = addAvoidParam(avoid, builder);
		builder.appendQueryParameter("units", unit);
		builder.appendQueryParameter("mode", _transMode);
		builder.appendQueryParameter("alternatives", "true");
		builder.appendQueryParameter("sensor", "true");
		builder.appendQueryParameter("language", Locale.getDefault().toString());
		
		_uri = builder.build();
	}

	private String getWayPointsParam(Latlng[] d, boolean optimaize) {
		// TODO Auto-generated method stub
		String paramString = "";

		if (optimaize) {
			paramString += "optimize:true|";
		}

		for (int i = 0; i < d.length-1; i++) {
			paramString += d[i].printString();
			if (i < d.length - 1) {
				paramString += "|";
			}
		}
		return paramString;
	}

	private String getAvoidParamText(int avoid) {
		// TODO Auto-generated method stub
		String text = "";

		switch (avoid) {
		case 0:
			text = "";
			break;
		case AVOID_TOLLS:
			text = "&avoid=tolls";
			break;
		case AVOID_HIGHWAY:
			text = "&avoid=highways";
			break;
		case AVOID_TOLLS + AVOID_HIGHWAY:
			text = "&avoid=highways&avoid=tolls";
			break;
		}
		return text;
	}

	public String sentHttpRequest() throws Exception {

		BufferedReader in = null;
		try {

			client = new DefaultHttpClient();

			Log.i("http", _uri.toString());

			request = new HttpGet(_uri.toString());
			// request = new HttpGet(DIRECTIONS_API+parameter);
			request.addHeader("Accept-Language", Locale.getDefault().toString());

			HttpResponse response = client.execute(request);

			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();

			String result = sb.toString();

			return cn2tw(result);

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine", e.toString());
			e.printStackTrace();

		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		return null;

	}

	public static JSONObject string2Object(String str) throws JSONException {

		return new JSONObject(str);
	}

	private String cn2tw(String s) {

		if (Locale.getDefault().equals(Locale.TAIWAN)) {
			s = s.replace('从', '從');
			s = s.replace('东', '東');
			s = s.replace('转', '轉');
			s = s.replace('进', '進');
			s = s.replace('继', '繼');
			s = s.replace('续', '續');
			s = s.replace('侧', '側');
		}
		return s;
	}

}
