package idv.xunqun.navier.http;

import idv.xunqun.navier.R;
import idv.xunqun.navier.utils.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;


public class FourSquarePlacesExplorerRequester {

	public static final String URI = "https://api.foursquare.com/v2/venues/search";

	private Uri _uri;
/**
 * 
 * @param context
 * @param queryString
 * @param latlng
 * @param intent  //global, match
 */
	public FourSquarePlacesExplorerRequester(Context context, String queryString, LatLng latlng, String intent) {
		Calendar cal = Calendar.getInstance();
		_uri = new Uri.Builder().scheme("https").authority("api.foursquare.com").path("/v2/venues/explore")
				.appendQueryParameter("client_id", context.getString(R.string.foursquare_client_id))
				.appendQueryParameter("client_secret", context.getString(R.string.foursquare_client_secret))
				.appendQueryParameter("ll", latlng.latitude + "," + latlng.longitude)
				.appendQueryParameter("query", queryString)
				.appendQueryParameter("radius", "100000")
				.appendQueryParameter("v", "20130815")
				.appendQueryParameter("intent", intent)
				.appendQueryParameter("limit", "25").build();
	}

	public String sentHttpRequest() throws Exception {
		BufferedReader in = null;
		try {

			DefaultHttpClient client = new DefaultHttpClient();

			Log.i("http", _uri.toString());

			HttpGet request = new HttpGet(_uri.toString());
			request.addHeader("Accept-Language", Locale.getDefault().toString());

			HttpResponse response = client.execute(request);

			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();

			String result = sb.toString();
			Log.i("http", result);
			return result;

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine", e.toString());
			e.printStackTrace();

		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}

		return null;

	}
}
