package idv.xunqun.navier.http;

import idv.xunqun.navier.MyDBOpenHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

public class PushPanelTask extends AsyncTask<Void, Void, Boolean> {

	//private static final String SYNCLAYOUT_URL = "http://navierlab.appspot.com/synclayout";
	private static final String SYNCLAYOUT_URL = "http://navierlab.appspot.com/synclayout";
	private static final String POST_KEY = "synctable";

	public interface OnPushPanelTaskListener {
		void onPreExecute();
		void onPostExecute();
		void onSyncPanelComplete(boolean ok);
	}

	private Context mContext;
	private OnPushPanelTaskListener mHandler;
	private String mAccount;

	public PushPanelTask(Context context, OnPushPanelTaskListener handler, String account) {
		mContext = context;
		mHandler = handler;
		mAccount = account;

	}

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		try {
			doPushPanel();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	@Override
	protected void onPostExecute(Boolean ok) {
		// TODO Auto-generated method stub
		super.onPostExecute(ok);
		if(mHandler!= null){
			if(ok){
				MyDBOpenHelper.getDb(mContext).delete(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME, "", new String[]{});
			}
			mHandler.onSyncPanelComplete(ok);
			mHandler.onPostExecute();
		}
		
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		if(mHandler!= null){
			mHandler.onPreExecute();
		}
	}

	private void doPushPanel() throws Exception {

		ArrayList<HashMap> upstreamList = new ArrayList<HashMap>();

		String[] columns = { MyDBOpenHelper.SYNC_LAYOUT_COL_ID,
				MyDBOpenHelper.SYNC_LAYOUT_LAYOUTID,
				MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE,
				MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT
		};

		String selection = ""; // if press faverite button
		String[] selectionArgs = {};
		String groupBy = "";
		String having = "";
		String orderBy = MyDBOpenHelper.SYNC_LAYOUT_COL_ID + " DESC";

		// Cursor c = db.query(MyPlacesDBOpenHelper.SYNC_LAYOUT_TABLE_NAME,
		// columns, selection, selectionArgs, groupBy, having, orderBy);
		Cursor c = MyDBOpenHelper.getDb(mContext).query(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);
		if (c.getCount() > 0) { // have something to update
			while (c.moveToNext()) {

				HashMap map = new HashMap();

				map.put(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE, c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE)));
				map.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT, c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT)));

				//
				if (!c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_COL_SYNCSTATE)).equalsIgnoreCase(MyDBOpenHelper.STATE_KILLED)) {

					String[] columns1 = { MyDBOpenHelper.LAYOUT_COL_ID,
							MyDBOpenHelper.LAYOUT_COL_LAYOUT,
							MyDBOpenHelper.LAYOUT_COL_TIMESTAMP,

					};

					// String selection1 = MyPlacesDBOpenHelper.LAYOUT_COL_ID +
					// "=?";
					String selection1 = MyDBOpenHelper.LAYOUT_COL_FINGERPRINT + "=?";
					String[] selectionArgs1 = { c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT)) };
					String groupBy1 = "";
					String having1 = "";
					String orderBy1 = MyDBOpenHelper.LAYOUT_COL_TIMESTAMP + " DESC";
					Cursor c1 =
							MyDBOpenHelper.getDb(mContext).query(MyDBOpenHelper.LAYOUT_TABLE_NAME,
									columns1, selection1, selectionArgs1, groupBy1, having1,
									orderBy1);
					// Cursor c1 =
					// mContext.getContentResolver().query(LayoutTableMetadata.CONTENT_URI,
					// columns1, selection1, selectionArgs1, orderBy1);
					if (c1.getCount() > 0) {

						c1.moveToFirst();

						map.put(MyDBOpenHelper.LAYOUT_COL_ID, c1.getString(c1.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_ID)));
						map.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, c1.getString(c1.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_LAYOUT)));
						map.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, c1.getString(c1.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP)));

					}

					c1.close();

				} else {
					// if killed only have id, fingerprint and state
					map.put(MyDBOpenHelper.LAYOUT_COL_ID, c.getString(c.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_ID)));

				}
				upstreamList.add(map);

			}

			try {

				String mParams = postMyPanelParamOrganize(upstreamList);
				Log.d("GAE", "POST: " + mParams);
				String results = sentMyPanelHttpPost(mParams);
				Log.d("GAE", "RECEIVED: " + results);
				//localSync(results);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			c.close();
		}
//		} else {
//			String results;
//			try {
//				results = sentMyPanelHttpGet();
//				Log.d("GAE", "RECEIVED: " + results);
//				localSync(results);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//
//			}
//		}
		c.close();

	}

	private String postMyPanelParamOrganize(ArrayList<HashMap> mylayoutList) throws JSONException {

		String param = "";
		JSONObject jsonRoot = new JSONObject();

		jsonRoot.put("user", mAccount);

		if (mylayoutList.size() > 0) {
			JSONArray jsonArray = new JSONArray();
			for (int i = 0; i < mylayoutList.size(); i++) {

				try {

					JSONObject jsonLayout = new JSONObject();

					String state = (String) mylayoutList.get(i).get(MyDBOpenHelper.SYNC_COL_SYNCSTATE);

					jsonLayout.put(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE, state);
					jsonLayout.put("id", Integer.parseInt((String) mylayoutList.get(i).get(MyDBOpenHelper.LAYOUT_COL_ID)));
					jsonLayout.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT, (String) mylayoutList.get(i).get(MyDBOpenHelper.SYNC_COL_FINGERPRINT));

					if (!state.equalsIgnoreCase(MyDBOpenHelper.STATE_KILLED)) {

						jsonLayout.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, mylayoutList.get(i).get(MyDBOpenHelper.LAYOUT_COL_LAYOUT));
						jsonLayout.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, mylayoutList.get(i).get(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));

					} else {

					}

					jsonArray.put(jsonLayout);
				} catch (Exception e) {

				}
			}

			jsonRoot.put("mylayouts", jsonArray);
		}

		return jsonRoot.toString();
	}

	public String sentMyPanelHttpPost(String strParam) throws Exception {
		List<NameValuePair> paramList = new ArrayList<NameValuePair>();
		paramList.add(new BasicNameValuePair(POST_KEY, strParam));

		BasicHttpParams params = new BasicHttpParams();
		params.setParameter(POST_KEY, URLEncoder.encode(strParam, "UTF-8"));
		DefaultHttpClient client = GlobalHttpClient.getHttpClient();
		try {

			HttpPost request = new HttpPost(this.SYNCLAYOUT_URL);

			/*
			 * request.addHeader("Accept-Language",
			 * Locale.getDefault().toString());
			 * request.addHeader("Content-Type", "text/plain");
			 */
			request.setEntity(new UrlEncodedFormEntity(paramList, HTTP.UTF_8));

			HttpResponse response = client.execute(request);
			
			if (response.getStatusLine().getStatusCode() == 200) {
				String result = EntityUtils.toString(response.getEntity());

				
				MyDBOpenHelper.getDb(mContext).delete(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME, "", null);

				return result;
			}

			// clearSyncTable();

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine", e.toString());
			e.printStackTrace();

		} finally {

		}
		return null;
	}

	private String sentMyPanelHttpGet() throws Exception {
		BufferedReader in = null;
		String parameter = "?user=" + mAccount;
		try {

			DefaultHttpClient client = GlobalHttpClient.getHttpClient();
			HttpGet request = new HttpGet(SYNCLAYOUT_URL + parameter);
			request.addHeader("Accept-Language", Locale.getDefault().toString());
			Log.d("GAE", "GET: " + SYNCLAYOUT_URL + parameter);

			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);

			}
			in.close();

			String result = sb.toString();
			Log.d("GAE", "RECEIVED: " + result);
			return result;

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine", e.toString());
			e.printStackTrace();

		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		return null;
	}

	public void localSync(String str) throws JSONException {


		
		if (str != null) {

			JSONObject jsonRoot = new JSONObject(str);

			JSONArray mylayouts = jsonRoot.getJSONArray("mylayouts");
			if (mylayouts.length() > 0) {
				// MyDBOpenHelper.getDb(mContext).delete(MyDBOpenHelper.LAYOUT_TABLE_NAME,
				// "", new String[] {});
				for (int i = 0; i < mylayouts.length(); i++) {

					JSONObject layout = (JSONObject) mylayouts.get(i);
					int id = Integer.parseInt(layout.getString("id"));
					String fingerprint = layout.getString(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT);
					HashMap map = new HashMap();

					map.put(MyDBOpenHelper.LAYOUT_COL_ID, layout.getString("id"));
					map.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, layout.getString(MyDBOpenHelper.LAYOUT_COL_LAYOUT));
					map.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, layout.getString(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));
					map.put(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT, fingerprint);


					updateMylayout2DB(map);
					

				}
			} else {

			}

		}

	}

	private boolean checkMylayoutDataAvailable(String fingerprint) {
		// TODO Auto-generated method stub
		return false;
	}

	private void updateMylayout2DB(HashMap map) {

		ContentValues values = new ContentValues();
		// values.put(MyPlacesDBOpenHelper.COL_ID, (String)
		// map.get(MyPlacesDBOpenHelper.COL_ID));
		values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, (String) map.get(MyDBOpenHelper.LAYOUT_COL_LAYOUT));
		values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, (String) map.get(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));
		values.put(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT, (String) map.get(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT));

		MyDBOpenHelper.getDb(mContext).insert(MyDBOpenHelper.LAYOUT_TABLE_NAME, "", values);
		// mContext.getContentResolver().insert(LayoutTableMetadata.CONTENT_URI,
		// values);

	}
}
