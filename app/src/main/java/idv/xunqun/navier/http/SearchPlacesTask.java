package idv.xunqun.navier.http;

import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.v2.content.Place;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.R.integer;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

public class SearchPlacesTask extends AsyncTask<Void, Float, JSONObject> {

	SearchPlacesTaskHandler _handler;
	String _queryString;
	private LatLng _latlng;
	
	public interface SearchPlacesTaskHandler{
		void onSearchPlacesResult(Boolean ok, ArrayList<Place> list);
		
	}
	
	public SearchPlacesTask(SearchPlacesTaskHandler handler, String queryString, LatLng latlng){
		_queryString = queryString;
		_handler = handler;
		_latlng = latlng;
	}
	
	@Override
	protected void onPostExecute(JSONObject result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		ArrayList<Place> searchResult = new ArrayList<Place>();
		
		
		try {
			JSONArray array  = result.getJSONArray("results");
			int count = array.length();
			for(int i=0; i<count; i++){
				JSONObject object = array.getJSONObject(i);
				
				Place place = new Place();
				place.name = object.getString("name");
				place.address = object.getString("vicinity");
				place.iconUri = object.getString("icon");
				JSONObject location = object.getJSONObject("geometry").getJSONObject("location");
				place.latitude = location.getDouble("lat");
				place.longitude = location.getDouble("lng");
				
				searchResult.add(place);
				
			}
			
			_handler.onSearchPlacesResult(true, searchResult);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			_handler.onSearchPlacesResult(false, null);
		}
		
		
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected JSONObject doInBackground(Void... params) {
		// TODO Auto-generated method stub
		
		try {
			
			SearchPlacesRequester requester = new SearchPlacesRequester(_queryString,_latlng);
			String result = requester.sentHttpRequest();
			
			if(result != null){
				return new JSONObject(result);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
