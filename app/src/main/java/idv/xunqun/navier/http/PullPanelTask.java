package idv.xunqun.navier.http;

import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.v2.content.PanelStore;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

public class PullPanelTask extends AsyncTask<Void, Void, Integer> {

	public static final int RESULT_OK = 0;
	public static final int RESULT_TIMEOUT = 1;
	public static final int RESULT_FAIL = 2;

	//private static final String SYNCLAYOUT_URL = "http://navierlab.appspot.com/synclayout";
	private static final String SYNCLAYOUT_URL = "http://navierlab.appspot.com/synclayout";
	private Context mContext;
	private SyncPanelHandler mHandler;
	private String mAccount;
	private OnPullPanelListener mListener;

	public interface OnPullPanelListener {
		void onPostExecute(int result);

		void onPreExecute();
	}

	public void setOnPullPanelListener(OnPullPanelListener listener) {
		mListener = listener;
	}

	public interface SyncPanelHandler {
		void onSyncPanelComplete(boolean ok);
	}

	public PullPanelTask(Context context, String account) {
		mContext = context;
		mAccount = account;

	}

	public void setSyncPanelHandler(SyncPanelHandler handler) {
		mHandler = handler;
	}

	@Override
	protected Integer doInBackground(Void... params) {
		try {
			return doPullPanels();

		} catch (Exception e) {
			e.printStackTrace();
			return RESULT_FAIL;
		}

	}

	private int doPullPanels() throws Exception {
		String result = null;
		// retry times
		for (int i = 0; i < 3; i++) {
			Log.d("GAE", "POST: doPullPanels()");
			result = sentMyPanelHttpGet();
			Log.d("GAE", "RECEIVE: "+result);
			if (result != null) {
				break;
			}

		}

		if (result != null) {
			return updateDb(result);
		}
		return RESULT_TIMEOUT;

	}

	private int updateDb(String str) throws JSONException {

		HashMap<String, TmpLayout> fpMap = new HashMap<String, TmpLayout>();

		String[] columns = new String[] { MyDBOpenHelper.LAYOUT_COL_LAYOUT, MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, MyDBOpenHelper.LAYOUT_COL_FINGERPRINT };
		String selection = "";
		String[] selectionArgs = new String[] {};

		Cursor cursor = MyDBOpenHelper.getDb(mContext).query(MyDBOpenHelper.LAYOUT_TABLE_NAME, columns, selection, selectionArgs, null, null, null);
		while (cursor.moveToNext()) {
			TmpLayout layout = new TmpLayout();

			layout.layout = cursor.getString(cursor.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_LAYOUT));
			layout.timestamp = cursor.getString(cursor.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));
			layout.fingerPrint = cursor.getString(cursor.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT));

			fpMap.put((String) layout.fingerPrint, layout);
		}

		if (str != null) {

			MyDBOpenHelper.getDb(mContext).delete(MyDBOpenHelper.LAYOUT_TABLE_NAME,
					"", new String[] {});
			
			JSONObject jsonRoot = new JSONObject(str);

			JSONArray mylayouts = jsonRoot.getJSONArray("mylayouts");
			if (mylayouts.length() > 0) {
				
				for (int i = 0; i < mylayouts.length(); i++) {

					JSONObject layoutJson = (JSONObject) mylayouts.get(i);
					int id = Integer.parseInt(layoutJson.getString("id"));
					String fingerprint = layoutJson.getString(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT);

					TmpLayout layout = new TmpLayout();
					layout.fingerPrint = fingerprint;
					layout.layout = layoutJson.getString(MyDBOpenHelper.LAYOUT_COL_LAYOUT);
					layout.timestamp = layoutJson.getString(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP);
					layout.deleted = false;

					fpMap.put(fingerprint, layout);

				}

				// update db
				Iterator it = fpMap.entrySet().iterator();
				while (it.hasNext()) {
					HashMap.Entry pairs = (HashMap.Entry) it.next();
					TmpLayout layout = (TmpLayout) pairs.getValue();
					if (layout.deleted) {
						// kill it from db
						MyDBOpenHelper.getDb(mContext).delete(MyDBOpenHelper.LAYOUT_TABLE_NAME, MyDBOpenHelper.LAYOUT_COL_FINGERPRINT + "=?", new String[] { layout.layout });

					} else {

						ContentValues values = new ContentValues();

						values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, layout.layout);
						values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, layout.timestamp);
						values.put(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT, layout.fingerPrint);

						Cursor c = MyDBOpenHelper.getDb(mContext).query(MyDBOpenHelper.LAYOUT_TABLE_NAME, columns, MyDBOpenHelper.LAYOUT_COL_FINGERPRINT + "=?", new String[] { layout.layout }, null, null, null);
						if (c.getCount() > 0) {
							// update db
							MyDBOpenHelper.getDb(mContext).update(MyDBOpenHelper.LAYOUT_TABLE_NAME, values, MyDBOpenHelper.LAYOUT_COL_FINGERPRINT + "=?", new String[] { layout.layout });
						} else {

							// insert to db
							MyDBOpenHelper.getDb(mContext).insert(MyDBOpenHelper.LAYOUT_TABLE_NAME, null, values);
						}

					}
					it.remove();
				}
			}else{  //length==0
				
			}
		}
		return RESULT_OK;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		if (mListener != null) {
			mListener.onPreExecute();
		}
	}

	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		if (mContext != null && mListener != null) {
			mListener.onPostExecute(result);
		}
	}

	private String sentMyPanelHttpGet() throws Exception {
		BufferedReader in = null;
		String parameter = "?user=" + mAccount;
		try {

			DefaultHttpClient client = GlobalHttpClient.getHttpClient();
			HttpGet request = new HttpGet(SYNCLAYOUT_URL + parameter);
			request.addHeader("Accept-Language", Locale.getDefault().toString());
			Log.d("GAE", "GET: " + SYNCLAYOUT_URL + parameter);

			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);

			}
			in.close();

			String result = sb.toString();

			return result;

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.d("mine", e.toString());
			e.printStackTrace();

		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		return null;
	}

	public class TmpLayout {
		String layout;
		String fingerPrint;
		String timestamp;
		boolean deleted = true;
	}

}
