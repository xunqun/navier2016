package idv.xunqun.navier.http;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.v2.content.Place;

import java.util.ArrayList;

import org.json.JSONObject;

import android.R.integer;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class MapQuestDirectionTask extends AsyncTask<Void, Float, Boolean> {

	MapQuestDirectionTaskListener _handler;
	PreferencesHandler _settings;
	private Context _context;
	private Latlng _current;
	private ArrayList<Latlng> _list;
	private String _reply_str;
	private String _status;
	private int _curLeg;
	private Context mContext;

	// For direction result call back
	public interface MapQuestDirectionTaskListener {
		void onDirectionTaskComplete(Boolean ok, String jsonResult,
									 String result);
	}

	/**
	 * Constructure for one destination
	 * 
	 * @param context
	 * @param current
	 *            Place object of current position
	 * @param destination
	 *            Place object of destination
	 */
	public MapQuestDirectionTask(Context context, MapQuestDirectionTaskListener handler,
			Latlng current, Latlng destination) {
		mContext = context;
		_handler = handler;
		_context = context;
		_settings = new PreferencesHandler(_context);
		_current = current;
		_list = new ArrayList<Latlng>();
		_list.add(destination);
	}

	/**
	 * Constructure for multiple destinations
	 * 
	 * @param context
	 * @param current
	 *            Place object of current position
	 * @param curLeg
	 *            Used to skip the leg which passed
	 * @param destination
	 *            Place objects in ArrayList of destination
	 */
	public MapQuestDirectionTask(Context context, MapQuestDirectionTaskListener handler,
			Latlng current, int curLeg, ArrayList<Latlng> destination) {
		mContext = context;
		_handler = handler;
		_context = context;
		_settings = new PreferencesHandler(_context);
		_current = current;
		_curLeg = curLeg;
		_list = destination;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		_handler.onDirectionTaskComplete(result, _reply_str, _status);
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		String transMode = _settings.getPREF_TRANS_TYPE();
		int avoid = 0;
		if (_settings.getPREF_AVOID_TOLLS())
			avoid += DirectionRequester.AVOID_TOLLS;
//		if (_settings.getPREF_AVOID_HIGHWAY())
//			avoid += DirectionRequester.AVOID_HIGHWAY;

		MapQuestDirectionRequester rq = new MapQuestDirectionRequester(mContext, _current, _list.get(0), transMode, avoid);

		try {
			_reply_str = rq.sentHttpRequest();

			Log.i("http", _reply_str);

			JSONObject json = new JSONObject(_reply_str);

			return true;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;

	}

}
