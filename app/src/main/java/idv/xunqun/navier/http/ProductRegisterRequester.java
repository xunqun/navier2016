package idv.xunqun.navier.http;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import android.net.Uri;
import android.util.Log;

public class ProductRegisterRequester {
	// destination location

		HttpClient client;
		HttpGet request;
		
		private String API_DOMAIN = "9-dot-navierlab.appspot.com";
		private String parameter;
		private Uri mUrl;
		
		public ProductRegisterRequester(String account, String product, String key){
			
			Uri.Builder builder = new Uri.Builder();
			builder.scheme("http");
			builder.authority(API_DOMAIN);
			builder.path("/register");
			builder.appendQueryParameter("account", account);
			builder.appendQueryParameter("product", product);
			builder.appendQueryParameter("key", key);
			mUrl = builder.build();
		}
		
		public String sentHttpRequest() throws Exception {
			BufferedReader in = null;
			
			try {
				client = initHttpClient();
				Log.i("http", mUrl.toString());

				request = new HttpGet(mUrl.toString());
				request.addHeader("Accept-Language", Locale.getDefault().toString());
				HttpResponse response = client.execute(request);
				in = new BufferedReader(new InputStreamReader(response.getEntity()
						.getContent()));
				StringBuffer sb = new StringBuffer("");
				String line = "";
				String NL = System.getProperty("line.separator");
				while ((line = in.readLine()) != null) {
					sb.append(line + NL);
				}
				
				String result = sb.toString();
				return result;
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				in.close();
			}
			return API_DOMAIN;
		}

		private HttpClient initHttpClient() {
			HttpParams httpParameters = new BasicHttpParams();
            HttpProtocolParams.setVersion(httpParameters, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(httpParameters,
                    HTTP.DEFAULT_CONTENT_CHARSET);
            HttpProtocolParams.setUseExpectContinue(httpParameters, true);

            // Set the timeout in milliseconds until a connection is
            // established.
            // The default value is zero, that means the timeout is not used.
            int timeoutConnection = 5 * 1000;
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT)
            // in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 5 * 1000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

			DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
			
			return httpClient;
		}
		
}
