package idv.xunqun.navier.http;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.content.Latlng;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.animation.AnimatorSet.Builder;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

public class MapQuestDirectionRequester {

	// destination location

	HttpClient client;
	HttpGet request;

	// Google API
	// private String DIRECTIONS_API =
	// "http://maps.google.com/maps/api/directions/json?";

	private String parameter;
	public static final int AVOID_TOLLS = 1;
	public static final int AVOID_HIGHWAY = 2;
	private static final String MAPQUEST_KEY = "Fmjtd%7Cluur2q62l9%2Ca5%3Do5-9a2llu";
	// HTTP components
	private String _transMode;
	private Object _uri;
	private PreferencesHandler mPref;

	/**
	 * For single destination
	 * 
	 * @param s
	 *            Departure location
	 * @param d
	 *            Destination location
	 * @param transMode
	 *            Vehicle type
	 * @param avoid
	 *            AVOID_TOLLS and AVOID_HIGHWAY;
	 */
	public MapQuestDirectionRequester(Context context, Latlng s, Latlng d,
			String transMode, int avoidToll) {
		mPref = new PreferencesHandler(context);
		_transMode = transMode;

		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http");
		builder.authority("www.mapquestapi.com");
		builder.path("/directions/v2/route");
		builder.appendQueryParameter("ambiguities", "ignore");
		builder.appendQueryParameter("avoidTimedConditions", "false");
		builder.appendQueryParameter("doReverseGeocode", "true");
		builder.appendQueryParameter("outFormat", "json");
		builder.appendQueryParameter("routeType", transMode);
		builder.appendQueryParameter("timeType", "1");
		builder.appendQueryParameter("drivingStyle", "3");
		builder.appendQueryParameter("shapeFormat", "cmp");
		builder.appendQueryParameter("generalize", "0");
		builder.appendQueryParameter("locale", Locale.getDefault().toString());
		builder.appendQueryParameter(
				"unit",
				mPref.getPREF_SPEED_UNIT() == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH ? "k"
						: "m");
		builder.appendQueryParameter("from", s.getLat() + "," + s.getLng());
		builder.appendQueryParameter("to", d.getLat() + "," + d.getLng());
		builder.appendQueryParameter("drivingStyle", "2");
		if (avoidToll == 1)
			builder.appendQueryParameter("avoids", "Toll road");
		_uri = builder.build();
		_uri = _uri + ("&key=" + MAPQUEST_KEY);
	}

	public String sentHttpRequest() throws Exception {

		BufferedReader in = null;
		try {

			client = new DefaultHttpClient();

			Log.i("http", _uri.toString());

			request = new HttpGet(_uri.toString());
			// request = new HttpGet(DIRECTIONS_API+parameter);
			request.addHeader("Accept-Language", Locale.getDefault().toString());

			HttpResponse response = client.execute(request);

			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();

			String result = sb.toString();

			return result;

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine", e.toString());
			e.printStackTrace();

		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		return null;

	}

}
