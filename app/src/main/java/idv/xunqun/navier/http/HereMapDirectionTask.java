package idv.xunqun.navier.http;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.content.Latlng;

import java.util.ArrayList;

import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

public class HereMapDirectionTask extends AsyncTask<Void, Float, Boolean> {

	private Latlng mCurrent;
	private ArrayList<Latlng> mList;
	private String mReplyStr;
	private int mCurLeg;
	private HereMapDirectionTaskListener mListener;
	private PreferencesHandler mPref;
	private Context mContext;

	// For direction result call back
	public interface HereMapDirectionTaskListener {
		void onDirectionTaskComplete(Boolean ok, String jsonResult);
	}

	/**
	 * Constructure for one destination
	 * 
	 * @param context
	 * @param current
	 *            Place object of current position
	 * @param destination
	 *            Place object of destination
	 */
	public HereMapDirectionTask(Context context, HereMapDirectionTaskListener handler, Latlng current, Latlng destination) {

		mListener = handler;
		mContext = context;
		mPref = new PreferencesHandler(context);
		mCurrent = current;
		mList = new ArrayList<Latlng>();
		mList.add(destination);
	}

	/**
	 * Constructure for multiple destinations
	 * 
	 * @param context
	 * @param current
	 *            Place object of current position
	 * @param curLeg
	 *            Used to skip the leg which passed
	 * @param destination
	 *            Place objects in ArrayList of destination
	 */
	public HereMapDirectionTask(Context context, HereMapDirectionTaskListener handler, Latlng current, int curLeg, Latlng destination) {

		mListener = handler;
		mContext = context;
		mPref = new PreferencesHandler(context);
		mCurrent = current;
		mCurLeg = curLeg;
		mList.add(destination);
	}

	@Override
	protected void onPostExecute(Boolean result) {

		mListener.onDirectionTaskComplete(result, mReplyStr);
	}

	@Override
	protected Boolean doInBackground(Void... params) {

		HereMapDirectionRequester rq = null;
		rq = new HereMapDirectionRequester(mContext, mCurrent, mList.get(0));

		try {
			mReplyStr = rq.sentHttpRequest();
			JSONObject json = new JSONObject(mReplyStr);

			return true;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;

	}

}
