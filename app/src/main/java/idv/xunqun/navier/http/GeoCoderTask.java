package idv.xunqun.navier.http;

import java.util.List;
import java.util.Locale;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import idv.xunqun.navier.utils.http.HttpRequest;

public class GeoCoderTask extends AsyncTask<Void, Void, NavierAddress> {

	public static final String URL = "http://maps.googleapis.com/maps/api/geocode/json";

	public interface GeoCoderTaskListener {
		void onFinish(NavierAddress address);
	}

	private Context mContext;
	private GeoCoderTaskListener mListener;
	private LatLng mLatLng;

	public GeoCoderTask(Context context, LatLng latLng, GeoCoderTaskListener listener) {
		mContext = context;
		mListener = listener;
		mLatLng = latLng;
	}

	@Override
	protected NavierAddress doInBackground(Void... params) {
		// Geocoder coder = new Geocoder(mContext, Locale.getDefault());
		List<NavierAddress> addressList;
		NavierAddress address = null;
		

		// try {
		// addressList = coder.getFromLocation(mLatLng.latitude,
		// mLatLng.longitude, 1);
		// if (addressList.size() > 0) {
		// resultAddress.address = addressList.get(0).getAddressLine(0);
		// resultAddress.address = addressList.get(0).get
		// }
		// } catch (IOException e) {
		// e.printStackTrace();
		// }

		
			Uri.Builder builder = new Uri.Builder();
			builder.scheme("http");
			builder.authority("maps.googleapis.com");
			builder.path("/maps/api/geocode/json");
			builder.appendQueryParameter("latlng", mLatLng.latitude + "," + mLatLng.longitude);
			builder.appendQueryParameter("sensor", "true");
			builder.appendQueryParameter("language", Locale.getDefault().toString());
			Uri uri = builder.build();
			String result = HttpRequest.sendHttpGet(uri, new DefaultHttpClient());

			try {
				JSONObject j = new JSONObject(result);
				if (j.getString("status").equals("OK")) {
					JSONArray array = j.getJSONArray("results");
					if (array.length() > 0) {
						JSONObject addrJson = array.getJSONObject(0);
						address = new NavierAddress();
						address.address = addrJson.getString("formatted_address");

						// find the route
						JSONArray components = addrJson.getJSONArray("address_components");
						for (int i = 0; i < components.length(); i++) {
							JSONObject componenet = components.getJSONObject(i);
							JSONArray types = componenet.getJSONArray("types");
							
							for(int n = 0; n<types.length();n++){
								if(types.get(n).equals("route")){
									address.route = componenet.getString("long_name");
									
									break;
								}
								
							}			
						}
					}
				}
			} catch (Exception e) {
				
			}
		

		return address;
	}

	@Override
	protected void onPostExecute(NavierAddress result) {
		if(mListener != null)
			mListener.onFinish(result);
		super.onPostExecute(result);
	}

}
