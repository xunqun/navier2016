package idv.xunqun.navier;

import idv.xunqun.navier.content.Layout;
import idv.xunqun.navier.content.LayoutItem;
import idv.xunqun.navier.content.Place;
import idv.xunqun.navier.parts.DashGridBoard;
import idv.xunqun.navier.parts.GridBoard;
import idv.xunqun.navier.parts.GridBoard.GridBoardHandler;
import idv.xunqun.navier.parts.NavigationGridBoard;
import idv.xunqun.navier.utils.Utility;
import idv.xunqun.navier.v2.content.DirectionRoute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class NavierPanel extends Activity implements OnInitListener, BaseNavierPanelContext {

	public static final String EXTRA_PLACE_DISTINATION = "dest"; // only when
																	// layout is
																	// a
																	// Navigation
																	// Panel
	public static final String EXTRA_PLACE_FROM = "curr";
	public static final String EXTRA_ROUTE = "route";
	public static final String EXTRA_LAYOUT = "layout";
	public String JSONOBJECT_DEFAULT_LAYOUT;
	private final int TTS_CHECK_CODE = 1;
	public static final float MaxStdDeviation = 1f;

	public static final String NOTIFICATION_CLEAR_INTENT = "NOTIFICATION_CLEAR_INTENT";

	//
	private PreferencesHandler _settings;
	// public SharedPreferences _settings;
	public Place _destination;
	public Place _current;
	public String _raw_route;
	private JSONObject _gridBoardLayout;
	private GridBoard _gridBoard;

	// hardware controler
	SensorManager _sensorManager;
	LocationManager _locationManager;
	PowerManager.WakeLock _wl;
	private TextToSpeech _tts;
	final int _matrix_size = 16;

	WindowManager.LayoutParams _layoutParams;

	// Sensor Listener
	public boolean _isGPSFix = false;
	public long _LastLocationMillis;
	public GPSStateListener _gpsStateListener;

	// values
	private ArrayList<HashMap<String, Object>> _colorList = new ArrayList<HashMap<String, Object>>();
	private SimpleAdapter _colorListAdapter;
	public Location _currentBestLocation;

	private boolean _dontTick = false;

	// animation settings on gridboard
	public boolean _endAnim = false;
	public boolean _stopAnim = false;

	private final SensorEventListener _sensorListener = new SensorEventListener() {
		final int matrix_size = 16;

		float[] R = new float[matrix_size];
		float[] outR = new float[matrix_size];
		float[] I = new float[matrix_size];
		float[] values = new float[3];
		boolean isReady = false;
		float[] mags = null;
		float[] accels = null;
		float[] orients = null;

		DigitalAverage[] avg = { new DigitalAverage(), new DigitalAverage(), new DigitalAverage() };

		private long lastMagsTime;
		private long lastAccelsTime;
		private long lastOrientsTime;

		public void onSensorChanged(SensorEvent event) {

			Sensor sensor = event.sensor;
			int type = sensor.getType();

			switch (type) {
			case Sensor.TYPE_MAGNETIC_FIELD:
				mags = event.values.clone();

				break;
			case Sensor.TYPE_ACCELEROMETER:

				accels = event.values.clone();
				/*
				 * if(accels[0]>100){ accels = null; }
				 */
				break;
			case Sensor.TYPE_ORIENTATION:
				orients = event.values;
				// Log.d("debug", event.values[0]+"");
				_gridBoard.onSensorChangeHandler(event);

				break;
			}

			if (mags != null && accels != null) {

				SensorManager.getRotationMatrix(R, null, accels, mags);

				SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, outR);
				SensorManager.getOrientation(outR, values);

				// Log.d("mine", "accels: " + accels[0] + " mags: " + mags[0] +
				// " angle: " + v[0]);
				// Log.d("mine", "oriention: (" + Math.toDegrees(values[0]) +
				// " , " + Math.toDegrees(values[1]) + " , " +
				// Math.toDegrees(values[2]) +" )" );
				values[0] = (getAverage((float) Math.toDegrees(values[0]))+360)%360;
				
				values[1] = ((float) Math.toDegrees(values[1])+360)%360;
				values[2] = ((float) Math.toDegrees(values[2])+360)%360;
				try {
					
					if(Math.abs(orients[2])<60){
						_gridBoard.onSensorChangeHandler(values);
					}else{
						orients[0] +=90;
						_gridBoard.onSensorChangeHandler(orients);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				accels = null;
				mags = null;
			}
		}

		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};

	private static final int _AVERAGE_NUM = 10;
	private int _averageCounting = 0;
	private int _averageIndex = 0;
	private int _preAverage = 0;
	private float[] _valuesToAverage = new float[_AVERAGE_NUM];

	private float getAverage(float value) {

		_valuesToAverage[_averageIndex] = value;

		if (_averageIndex < _AVERAGE_NUM - 1) {
			_averageIndex += 1;
		} else {
			_averageIndex = 0;
		}
		if (_averageCounting < _AVERAGE_NUM) {
			_averageCounting++;
		}

		boolean cross360 = false;

		if (_averageCounting > 1) {
			for (int i = 0; i < _averageCounting - 1; i++) {

				if (Math.abs(_valuesToAverage[i] - _valuesToAverage[i + 1]) > 180) {
					cross360 = true;

				}
			}

			if (cross360) {

				for (int i = 0; i < _averageCounting; i++) {

					if (_valuesToAverage[i] > 0) {
						_valuesToAverage[i] = _valuesToAverage[i] - 360;
					}

				}

				float sum = 0;
				for (int i = 0; i < _averageCounting; i++) {
					sum = sum + _valuesToAverage[i];
				}

				return sum / _averageCounting;

			} else {
				float sum = 0;
				for (int i = 0; i < _averageCounting; i++) {
					sum = sum + _valuesToAverage[i];
				}

				return sum / _averageCounting;
			}

		} else {
			return value;
		}

	}

	// Location Listener
	/*
	 * private final LocationListener _locationListener = new LocationListener()
	 * { public void onLocationChanged(Location location) { // Called when a new
	 * location is found by the network location provider.
	 * if(NavigationManager.isBetterLocation(location, _currentBestLocation)){
	 * _gridBoard.onLocationChangeHandler(location); _currentBestLocation =
	 * location; _LastLocationMillis = SystemClock.elapsedRealtime(); }else{
	 * //just update location //mRoute.setCurrentLocation(new
	 * Latlng(location.getLatitude(),location.getLongitude()));
	 * //mRoute.invalidate(); }
	 * 
	 * }
	 * 
	 * public void onStatusChanged(String provider, int status, Bundle extras) {
	 * 
	 * _gridBoard.onLocationStatusChangeHandle(provider, status, extras);
	 * 
	 * }
	 * 
	 * public void onProviderEnabled(String provider) {
	 * 
	 * 
	 * }
	 * 
	 * public void onProviderDisabled(String provider) {
	 * 
	 * _gridBoard.onLocationProviderDisableHandle(provider);
	 * 
	 * } };
	 */
	private BroadcastReceiver _MessageReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			if (intent.getAction().equals(LocationService.EXTRA_ONLOCATIONCHANGE)) {

				Location location = intent.getParcelableExtra("location");
				if (NavigationManager.isBetterLocation(location, _currentBestLocation)) {
					_gridBoard.onLocationChangeHandler(location);
					_currentBestLocation = location;
					_LastLocationMillis = SystemClock.elapsedRealtime();
				} else {
					// just update location
					// mRoute.setCurrentLocation(new
					// Latlng(location.getLatitude(),location.getLongitude()));
					// mRoute.invalidate();
				}
			}

			if (intent.getAction().equals(LocationService.EXTRA_ONPROVIDERDISABLEED)) {

				String provider = intent.getStringExtra("provider");
				_gridBoard.onLocationProviderDisableHandle(provider);
			}

			if (intent.getAction().equals(LocationService.EXTRA_ONPROVIDERENABLED)) {

			}

			if (intent.getAction().equals(LocationService.EXTRA_ONSTATUSCHANGED)) {

				String provider = intent.getStringExtra("provider");
				int status = intent.getIntExtra("status", -1);
				Bundle extras = intent.getBundleExtra("extras");

				_gridBoard.onLocationStatusChangeHandle(provider, status, extras);
			}

		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setIsEndAnimation(false);
		_settings = new PreferencesHandler(this);

		initDefaultLayout();
		screenBrightSetting();
		try {
			screenLockSetting();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// get extras
		Intent it = getIntent();
		if (it.hasExtra(EXTRA_PLACE_DISTINATION) && it.hasExtra(EXTRA_PLACE_FROM)) {
			_destination = (Place) it.getSerializableExtra(EXTRA_PLACE_DISTINATION);
			_current = (Place) it.getSerializableExtra(EXTRA_PLACE_FROM);

		}

		if (this.getIntent().hasExtra(EXTRA_ROUTE)) {
			_raw_route = this.getIntent().getStringExtra(EXTRA_ROUTE);

		}

		try {

			LayoutItem layout;
			if (getIntent().hasExtra(EXTRA_LAYOUT)) {
				layout = (LayoutItem) getIntent().getSerializableExtra(EXTRA_LAYOUT);
				_gridBoardLayout = new JSONObject(layout.LAYOUT_JSONSTR);
			} else {
				_gridBoardLayout = new JSONObject(JSONOBJECT_DEFAULT_LAYOUT);
			}

			if (_gridBoardLayout.getJSONObject(Layout.JSON_ROOT).getInt(Layout.JSON_TYPE) == Layout.TYPE_NAVIGATION) {

				_gridBoard = new NavigationGridBoard(this, this, _gridBoardLayout, new MyGridViewHandler());

			} else {
				_gridBoard = new DashGridBoard(this, this, _gridBoardLayout, new MyGridViewHandler());
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * try{ _tts = new TextToSpeech(this, this);
		 * 
		 * }catch(Exception e){
		 * 
		 * }
		 */
		createTextToSpeech(this, Locale.getDefault());
		log_life("NavierPanel.onCreate()");
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		// TODO Auto-generated method stub

		_gridBoard.saveInstance(outState);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		// RestoreInstances

		if (savedInstanceState != null) {
			_gridBoard.restoreInstance(savedInstanceState);
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		setIsStopAnimation(false);

		initHWSensor();
		_gridBoard.onResume();

		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver, new IntentFilter(LocationService.EXTRA_ONLOCATIONCHANGE));
		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver, new IntentFilter(LocationService.EXTRA_ONPROVIDERDISABLEED));
		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver, new IntentFilter(LocationService.EXTRA_ONPROVIDERENABLED));
		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver, new IntentFilter(LocationService.EXTRA_ONSTATUSCHANGED));
		_wl.acquire();

		if (_settings.getPREF_BRIGHTNESS_SETTING()) {

			_layoutParams.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;

			getWindow().setAttributes(_layoutParams);

		}

		// Utility.checkLanscapeOrientation(this);
		Utility.requestLocationServiceUpdate(this);
		log_life("NavierPanel.onResume()");
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// disableLocationListener();

		setIsStopAnimation(true);
		LocalBroadcastManager.getInstance(this).unregisterReceiver(_MessageReceiver);
		_gridBoard.onPause();

		if (_settings.getPREF_BRIGHTNESS_SETTING()) {
			_layoutParams.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
			getWindow().setAttributes(_layoutParams);
		}
		try {
			if (_tts != null && _tts.isSpeaking()) {

				_tts.stop();
			}
			_wl.release(); // screan bright
			log_life("NavierPanel.onPause()");

		} catch (Exception e) {

		}
		_sensorManager.unregisterListener(_sensorListener);
		Utility.requestLocationServiceStop(this);
		// showStatusBarNotify();
	}

	@Override
	protected void onDestroy() {

		setIsEndAnimation(true);

		// Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,
		// _brightness);
		try {
			_tts.shutdown();
			_tts = null;
		} catch (Exception e) {

		}
		log_life("NavierPanel.onDestroy()");
		super.onDestroy();
	}

	public void initHWSensor() {

		// sensor

		_sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

		// have been deprecated

		Sensor sensor = _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		_sensorManager.registerListener(_sensorListener, sensor, SensorManager.SENSOR_DELAY_GAME);

		sensor = _sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		_sensorManager.registerListener(_sensorListener, sensor, SensorManager.SENSOR_DELAY_GAME);

		sensor = _sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		_sensorManager.registerListener(_sensorListener, sensor, SensorManager.SENSOR_DELAY_GAME);

		
		// GPS sensor

		_locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		_gpsStateListener = new GPSStateListener();
		_locationManager.addGpsStatusListener(_gpsStateListener);

		// audio
		setVolumeControlStream(AudioManager.STREAM_MUSIC);

		// tts

	}

	private void screenLockSetting() throws Exception {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			if (_settings.getPREF_LOCK_SCREEN_ORIENTATION()) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			}

		}

	}

	public void createTextToSpeech(final Context context, final Locale locale) {

		_tts = new TextToSpeech(context, new OnInitListener()
		{
			@Override
			public void onInit(int status)
			{
				if (status == TextToSpeech.SUCCESS)
				{
					Locale defaultOrPassedIn = locale;
					if (locale == null)
					{
						defaultOrPassedIn = Locale.getDefault();
					}
					// check if language is available

					switch (_tts.isLanguageAvailable(defaultOrPassedIn))
					{
					case TextToSpeech.LANG_AVAILABLE:
					case TextToSpeech.LANG_COUNTRY_AVAILABLE:
					case TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE:

						// _tts.setLanguage(locale);
						// pass the tts back to the main
						// activity for use
						break;
					case TextToSpeech.LANG_MISSING_DATA:

						if (_gridBoard instanceof NavigationGridBoard) {
							Toast.makeText(NavierPanel.this, getString(R.string.navierpanel_ttsdatamissing), Toast.LENGTH_SHORT).show();
						}

						break;
					case TextToSpeech.LANG_NOT_SUPPORTED:

						if (_gridBoard instanceof NavigationGridBoard) {
							Toast.makeText(NavierPanel.this, getString(R.string.navierpanel_ttsnotsupport), Toast.LENGTH_SHORT).show();
						}
						break;
					}
				}
			}
		});
	}

	public void ttsSpeak(String s) {
		try {
			if (_settings.getPREF_NAVI_VOICE()) {

				_tts.speak(s, TextToSpeech.QUEUE_ADD, null);
			}
		} catch (Exception e) {

		}
	}

	public void initDefaultLayout() {

		JSONOBJECT_DEFAULT_LAYOUT = new Utility(this).getStringFromRaw(R.raw.navigation_classic);
	}

	private void screenBrightSetting() {

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		_wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "My Tag");
		// _wl.acquire();

		// ..screen will stay on during this section..
		_layoutParams = getWindow().getAttributes();
	}

	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.panel_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();

		if (id == R.id.doswitch) {

			int mode = _gridBoard._layout._mode == Layout.MODE_NORMAL ? Layout.MODE_HUD : Layout.MODE_NORMAL;
			_gridBoard.modeSwitch(mode);

		}

		if (id == R.id.speed) {

			int unit = (_gridBoard.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH) ? PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_MPH : PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH;
			_gridBoard.onSpeedUnitChangeHandler(unit);
		}

		if (id == R.id.color) {

			String[] colors = getResources().getStringArray(R.array.color_prompt);
			_colorListAdapter = new SimpleAdapter(this, _colorList, R.layout.color_list_item, new String[] { "name", "type" }, new int[] { R.id.colorname, R.id.colortype });

			setColorItem(colors);

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getString(R.string.navierpanel_color));
			// builder.setItems(colors, new onColorClick(this,colors));
			builder.setAdapter(_colorListAdapter, new onColorClick(this, colors));
			AlertDialog alert = builder.create();
			alert.show();
		}

		if (id == R.id.grid) {
			_gridBoard._showGrid = _gridBoard._showGrid == true ? false : true;

		}
		/*
		 * if(id == R.id.agps){
		 * 
		 * Bundle bundle = new Bundle();
		 * _locationManager.sendExtraCommand(LocationManager
		 * .GPS_PROVIDER,"delete_aiding_data", bundle);
		 * _locationManager.sendExtraCommand(LocationManager.GPS_PROVIDER,
		 * "force_xtra_injection", bundle);
		 * _locationManager.sendExtraCommand(LocationManager.GPS_PROVIDER,
		 * "force_time_injection", bundle); Toast.makeText(this,
		 * getString(R.string.navierpanel_agps), Toast.LENGTH_SHORT).show();
		 * 
		 * 
		 * }
		 */

		return true;

	}

	private void setColorItem(String[] colors) {

		_colorList.clear();
		HashMap map;

		// CYAN
		map = new HashMap();
		map.put("name", colors[0]);
		map.put("type", R.drawable.color_cyan);

		_colorList.add(map);

		// Light gray
		map = new HashMap();

		map.put("name", colors[1]);
		map.put("type", R.drawable.color_lightgray);

		_colorList.add(map);

		// WHITE
		map = new HashMap();
		map.put("name", colors[2]);
		map.put("type", R.drawable.color_white);

		_colorList.add(map);

		// yellow
		map = new HashMap();
		map.put("name", colors[3]);
		map.put("type", R.drawable.color_yellow);

		_colorList.add(map);

		// green
		map = new HashMap();
		map.put("name", colors[4]);
		map.put("type", R.drawable.color_green);

		_colorList.add(map);

		// blue
		map = new HashMap();
		map.put("name", colors[5]);
		map.put("type", R.drawable.color_blue);

		_colorList.add(map);

		// orange red
		map = new HashMap();
		map.put("name", colors[6]);
		map.put("type", R.drawable.color_orangered);

		_colorList.add(map);

		// sky blue
		map = new HashMap();
		map.put("name", colors[7]);
		map.put("type", R.drawable.color_skyblue);

		_colorList.add(map);
	}

	private class DigitalAverage {

		final int history_len = 5;
		double[] mLocHistory = new double[history_len];
		int mLocPos = 0;
		float preAvg = 0;

		// ------------------------------------------------------------------------------------------------------------
		float average(double d) {

			float avg = 0;
			double sum2 = 0;
			double sum3 = 0;
			double z = 0;

			mLocHistory[mLocPos] = d;

			mLocPos++;
			if (mLocPos > mLocHistory.length - 1) {
				mLocPos = 0;
			}
			for (double h : mLocHistory) {
				avg += h;
			}
			avg /= mLocHistory.length;

			for (int e = 0; e < mLocHistory.length; e++) {
				sum2 += (mLocHistory[e] - avg) * (mLocHistory[e] - avg);
			}

			sum3 = (sum2 / (mLocHistory.length));

			z = Math.sqrt(sum3);

			return avg;

		}
	}

	private class onColorClick implements DialogInterface.OnClickListener {

		Context context;
		String[] colors;

		public onColorClick(Context c, String[] colors) {
			context = c;
			this.colors = colors;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			int rgb = 0xFFFFFF;

			switch (which) {
			case 0:
				rgb = Color.CYAN;
				break;
			case 1:
				rgb = Color.LTGRAY;
				break;
			case 2:
				rgb = Color.WHITE;
				break;
			case 3:
				rgb = Color.YELLOW;
				break;
			case 4:
				rgb = Color.GREEN;
				break;

			case 5:
				rgb = Color.BLUE;
				break;

			case 6:
				rgb = 0xffff4500;
				break;
			case 7:
				rgb = 0xff87ceeb;
				break;
			default:
				rgb = Color.CYAN;
			}

			_gridBoard.GLOBAL_COLOR = rgb;
			_gridBoard.onGlobalColorChange(rgb);
		}

	}

	private class GPSStateListener implements GpsStatus.Listener {
		public void onGpsStatusChanged(int event) {
			switch (event) {
			case GpsStatus.GPS_EVENT_SATELLITE_STATUS:

				GpsStatus status = _locationManager.getGpsStatus(null);

				if (_currentBestLocation != null)
					_isGPSFix = (SystemClock.elapsedRealtime() - _LastLocationMillis) < 3000;

				if (_isGPSFix) { // A fix has been acquired.
					// Do something.
					_gridBoard.onIsGPSFix(status, true);
				} else { // The fix has been lost.
					// Do something.
					_gridBoard.onIsGPSFix(status, false);
				}

				break;
			case GpsStatus.GPS_EVENT_FIRST_FIX:
				// Do something.
				_isGPSFix = true;

				break;
			}

		}
	}

	private void log_life(String s) {
		Log.d("life", s);
	}

	// Below methods are implement of BaseNavierPanelContext

	@Override
	public boolean getIsEndAnimation() {
		// TODO Auto-generated method stub
		return _endAnim;
	}

	@Override
	public void setIsEndAnimation(boolean b) {
		// TODO Auto-generated method stub
		_endAnim = b;
	}

	@Override
	public boolean getIsStopAnimation() {
		// TODO Auto-generated method stub
		return _stopAnim;
	}

	@Override
	public void setIsStopAnimation(boolean b) {
		// TODO Auto-generated method stub
		_stopAnim = b;
	}

	@Override
	public Place getDestPlace() {
		// TODO Auto-generated method stub
		return _destination;
	}

	@Override
	public Place getCurrPlace() {
		// TODO Auto-generated method stub
		return _current;
	}

	@Override
	public String getRowRoute() {
		// TODO Auto-generated method stub
		return _raw_route;
	}

	@Override
	public void doTTS(String text) {
		// TODO Auto-generated method stub
		ttsSpeak(text);
	}

	protected class MyGridViewHandler implements GridBoardHandler {

		@Override
		public void onLayoutInflate(View v) {
			// TODO Auto-generated method stub
			setContentView(v);
		}

	}

	@Override
	public DirectionRoute getDirectionPlan() {
		// TODO Auto-generated method stub
		return null;
	}

}
