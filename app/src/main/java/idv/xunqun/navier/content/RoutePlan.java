package idv.xunqun.navier.content;


import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class RoutePlan {
	public JSONObject _json;
	public ArrayList<DirectionStep> arr_steps =new ArrayList();
	public int _current_step=0;
	public int _current_rpoint=0;
	public int _totalDistance = 0;
	
	
	
	public RoutePlan(){
		
	};
	
	
	public boolean setJson(JSONObject j) throws JSONException{
		this._json =j;
		JSONArray legs = new JSONArray();
		JSONArray steps = new JSONArray();
		
		if(_json.getString("status").equalsIgnoreCase("ok")){
			legs = _json.getJSONArray("routes").getJSONObject(0).getJSONArray("legs");
			steps = legs.getJSONObject(0).getJSONArray("steps");
		
			for(int i=0;i<steps.length();i++){
				JSONObject obj=steps.getJSONObject(i);
				DirectionStep step = new DirectionStep();
				step.distance_text = obj.getJSONObject("distance").getString("text");
				step.distance_value = obj.getJSONObject("distance").getInt("value");
				step.duration_text = obj.getJSONObject("duration").getString("text");
				step.duration_value = obj.getJSONObject("duration").getInt("value");
				step.endlocation_lat = obj.getJSONObject("end_location").getDouble("lat");
				step.endlocation_lng = obj.getJSONObject("end_location").getDouble("lng");
				step.html_instructions = obj.getString("html_instructions");
				step.setPolyline( obj.getJSONObject("polyline").getString("points"));
				step.startlocation_lat = obj.getJSONObject("start_location").getDouble("lat");
				step.startlocation_lng = obj.getJSONObject("start_location").getDouble("lng");				
				step.travel_mode = obj.getString("travel_mode");

				_totalDistance += step.distance_value;
				arr_steps.add(step);				
				Log.d("steps", step.html_instructions);
			}
		
		}
		return true;
	}
	

}
