package idv.xunqun.navier.content;

import java.io.Serializable;
import java.text.DecimalFormat;

import com.google.android.gms.maps.model.LatLng;

public class Latlng implements Serializable{
	private double lat;
	private double lng;
	private String name;
	
	public Latlng(double lat,double lng){
		this.lat = lat;
		this.lng = lng;
	}
	
	public String toString(){
		return "("+lat+","+lng+")";		
	}
	
	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String printString(){
		LatLng p = new LatLng(lat, lng);
		return p.latitude + "," + p.longitude;
	}
	
	public LatLng toLatLng(){
		return new LatLng(lat, lng);
	}
}
