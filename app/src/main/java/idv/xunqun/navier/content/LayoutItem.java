package idv.xunqun.navier.content;

import java.io.Serializable;

public class LayoutItem  implements Serializable{
	public String LAYOUT_NAME;
	public long LAYOUT_ID;
	public String LAYOUT_JSONSTR;
	public int LAYOUT_TYPE;
	public String LAYOUT_FINGERPRINT;
	public boolean _isStored;
	public boolean _isEditable;
	
}

