package idv.xunqun.navier.content;


import java.util.ArrayList;
import java.util.List;

public class DirectionStep {
	public String distance_text;
	public int distance_value;
	public String duration_text;
	public int duration_value;
	public double endlocation_lat;
	public double endlocation_lng;
	public String polyline;
	public List<Latlng> polyline_list;
	public String html_instructions;
	public double startlocation_lat;
	public double startlocation_lng;
	public String travel_mode;
	
	
	
	
	public void setPolyline(String s){
		this.polyline = s;
		this.polyline_list = decodePoly(s);
	}
	
	public static List<Latlng> decodePoly(String encoded) {

	    List<Latlng> poly = new ArrayList<Latlng>();
	    int index = 0, len = encoded.length();
	    int lat = 0, lng = 0;

	    while (index < len) {
	        int b, shift = 0, result = 0;
	        do {
	            b = encoded.charAt(index++) - 63;
	            result |= (b & 0x1f) << shift;
	            shift += 5;
	        } while (b >= 0x20);
	        int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	        lat += dlat;

	        shift = 0;
	        result = 0;
	        do {
	            b = encoded.charAt(index++) - 63;
	            result |= (b & 0x1f) << shift;
	            shift += 5;
	        } while (b >= 0x20);
	        int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	        lng += dlng;
	        
	        double dbl_lat = (double)(lat/1E5);
	        double dbl_lng = (double)(lng/1E5);
	        Latlng p = new Latlng(dbl_lat,dbl_lng);
	        poly.add(p);
	    }

	    return poly;
	}
}
