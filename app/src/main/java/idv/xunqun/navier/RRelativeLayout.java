package idv.xunqun.navier;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class RRelativeLayout extends RelativeLayout {

	public RRelativeLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	public RRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}


	@Override
	protected void dispatchDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		canvas.translate(0, this.getHeight());
		canvas.scale(1, -1, 0,0);
		super.dispatchDraw(canvas);
	}
	
	

}
