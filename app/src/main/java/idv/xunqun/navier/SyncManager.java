package idv.xunqun.navier;

import idv.xunqun.navier.provider.LayoutProviderMetadata.LayoutTableMetadata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class SyncManager extends Service{

	private DefaultHttpClient client;
	private PreferencesHandler _settings;
	private static final String SYNCPLACE_URL = "http://navierlab.appspot.com/syncplace";	
	private static final String SYNCLAYOUT_URL = "http://navierlab.appspot.com/synclayout";
	private static final String POST_KEY = "synctable";
	public static final String EXTRA_ACCOUNT = "account";
	

	//values
	private String _params; //string in JSON format
	private String _account;
	private SQLiteDatabase db = null;
	MyDBOpenHelper helper;
	
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	
	/* ---------------------here is a param sample--f
	 {
		"user":"xunqun@gmail.com",
		"myplaces":[
			{
				"id":12,
			    "place_name" : "My home",
			    "address" : "Taiwan",
			    "description" : "",
			    "latitude" : 24.2,
			    "longitude" : 120.68,
			    "favorite" : 1,
			    "timestamp" : 123456,   
			    "state" : "new"
			},
			{
				"id":14,
			    "place_name" : "My home",
			    "address" : "Taiwan",
			    "description" : "",
			    "latitude" : 24.2,
			    "longitude" : 120.68,
			    "favorite" : 1,
			    "timestamp" : 123456,   
			    "user" : "abc@gmail.com",
			    "state" : "modified"
			}
		]
	}

	---------------------------------------------------- */
	
	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		

	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		try{
			
			_settings = new PreferencesHandler(this.getApplication());
			_account = intent.getStringExtra(EXTRA_ACCOUNT);
			
			if(_settings.getPREF_SYNC() && !_account.equalsIgnoreCase("NONE")){
				
				new ServiceTask().execute(new Object());
				Toast.makeText(this.getApplicationContext(), this.getResources().getString(R.string.naviernaviconfigure_datasync), Toast.LENGTH_SHORT).show();
			}
			
		}catch(Exception e){
			
			e.printStackTrace();
			
		}
		
		stopSelf();
		return startId;
		
		
	}
	
	private String postMylayoutParamOrganize(ArrayList<HashMap> mylayoutList)throws JSONException{
		
		String param = "";
		JSONObject jsonRoot = new JSONObject();
		
		
		
		jsonRoot.put("user", _account);		
		
		if(mylayoutList.size()>0){
			JSONArray jsonArray = new JSONArray();
			for(int i=0 ; i<mylayoutList.size() ; i++){
				
				try{
				
					JSONObject jsonLayout = new JSONObject();
					
					String state = (String) mylayoutList.get(i).get(MyDBOpenHelper.SYNC_COL_SYNCSTATE);
					
					jsonLayout.put(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE, state);
					jsonLayout.put("id", Integer.parseInt((String) mylayoutList.get(i).get(MyDBOpenHelper.LAYOUT_COL_ID)));
					jsonLayout.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT,  (String) mylayoutList.get(i).get(MyDBOpenHelper.SYNC_COL_FINGERPRINT));
					
					if(!state.equalsIgnoreCase(MyDBOpenHelper.STATE_KILLED)){
					
						
						jsonLayout.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, mylayoutList.get(i).get(MyDBOpenHelper.LAYOUT_COL_LAYOUT));
						jsonLayout.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, mylayoutList.get(i).get(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));
						
						
					
					}else{
						
						
					}
					
					jsonArray.put(jsonLayout);
				}catch(Exception e){
					
				}
			}
			
			jsonRoot.put("mylayouts", jsonArray);
		}
		
		return jsonRoot.toString();
	}
	
	private String postMyplaceParamOrganize(ArrayList<HashMap> myplaceList) throws JSONException{
		
		String param = "";
		JSONObject jsonRoot = new JSONObject();
		
		
		
		jsonRoot.put("user", _account);
		
		if(myplaceList.size()>0){
			
			JSONArray jsonArray = new JSONArray();
			for(int i=0 ; i<myplaceList.size() ; i++){
				JSONObject jsonPlace = new JSONObject();
				
				String state = (String) myplaceList.get(i).get(MyDBOpenHelper.SYNC_COL_SYNCSTATE);
				
				jsonPlace.put(MyDBOpenHelper.SYNC_COL_SYNCSTATE, state);
				jsonPlace.put("id", Integer.parseInt((String) myplaceList.get(i).get(MyDBOpenHelper.COL_ID)));
				jsonPlace.put(MyDBOpenHelper.COL_FINGERPRINT, String.valueOf(myplaceList.get(i).get(MyDBOpenHelper.COL_FINGERPRINT)));
				
				if(!state.equalsIgnoreCase(MyDBOpenHelper.STATE_KILLED)){
				
					
					jsonPlace.put(MyDBOpenHelper.COL_PLACE_NAME, myplaceList.get(i).get(MyDBOpenHelper.COL_PLACE_NAME));
					jsonPlace.put(MyDBOpenHelper.COL_ADDRESS, myplaceList.get(i).get(MyDBOpenHelper.COL_ADDRESS));
					jsonPlace.put(MyDBOpenHelper.COL_DESCRIPTION, myplaceList.get(i).get(MyDBOpenHelper.COL_DESCRIPTION));
					jsonPlace.put(MyDBOpenHelper.COL_LATITUDE, Float.parseFloat((String) myplaceList.get(i).get(MyDBOpenHelper.COL_LATITUDE)));
					jsonPlace.put(MyDBOpenHelper.COL_LONGITUDE, Float.parseFloat((String)myplaceList.get(i).get(MyDBOpenHelper.COL_LONGITUDE)));
					jsonPlace.put(MyDBOpenHelper.COL_FAVERITE, Integer.parseInt((String) myplaceList.get(i).get(MyDBOpenHelper.COL_FAVERITE)));
					jsonPlace.put(MyDBOpenHelper.COL_TIMESTAMP, String.valueOf(myplaceList.get(i).get(MyDBOpenHelper.COL_TIMESTAMP)));
					
				
				}else{
					
					
					
					jsonPlace.put(MyDBOpenHelper.COL_PLACE_NAME, "");
					jsonPlace.put(MyDBOpenHelper.COL_ADDRESS, "");
					jsonPlace.put(MyDBOpenHelper.COL_DESCRIPTION, "");
					jsonPlace.put(MyDBOpenHelper.COL_LATITUDE, "");
					jsonPlace.put(MyDBOpenHelper.COL_LONGITUDE, "");
					jsonPlace.put(MyDBOpenHelper.COL_FAVERITE, "");
					jsonPlace.put(MyDBOpenHelper.COL_TIMESTAMP, "");
				}
				
				jsonArray.put(jsonPlace);
			}
			jsonRoot.put("myplaces", jsonArray);
		}
		
		
		
		
		
		return jsonRoot.toString();
	}

	private void initDB(){
		try{
			helper = new MyDBOpenHelper(this, null);
			if(db!=null&&db.isOpen())db.close();
			db = helper.getWritableDatabase();
		}catch(SQLException e){
			e.printStackTrace();			
		}
		
	}
	
	private String sentMylayoutHttpGet() throws Exception{
		BufferedReader in=null;
		String parameter = "?user="+_account;
		try {

			client = new DefaultHttpClient();
			HttpGet request = new HttpGet(SYNCLAYOUT_URL+parameter);		
			request.addHeader("Accept-Language", Locale.getDefault().toString());
			Log.d("GAE","GET: " + SYNCLAYOUT_URL+parameter);
			
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line="";
			String NL =System.getProperty("line.separator");
			while((line=in.readLine())!= null){
				sb.append(line+NL);		
				
			}
			in.close();
			
			String result=sb.toString();		
			
						
			return result;
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine",e.toString());
			e.printStackTrace();
			
		}finally{
			
			if(in!=null){
				try{
					in.close();
				}catch(IOException e){
					e.printStackTrace();
				}
				
			}
		}
		return null;
	}
	
	private String sentMyplaceHttpGet() throws Exception{
		BufferedReader in=null;
		String parameter = "?user="+_account;
		try {

			client = new DefaultHttpClient();
			HttpGet request = new HttpGet(SYNCPLACE_URL+parameter);		
			request.addHeader("Accept-Language", Locale.getDefault().toString());
			Log.d("GAE","GET: " + SYNCPLACE_URL+parameter);
			
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line="";
			String NL =System.getProperty("line.separator");
			while((line=in.readLine())!= null){
				sb.append(line+NL);				
			}
			in.close();
			
			String result=sb.toString();		
			
						
			return result;
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine",e.toString());
			e.printStackTrace();
			
		}finally{
			
			if(in!=null){
				try{
					in.close();
				}catch(IOException e){
					e.printStackTrace();
				}
				
			}
		}
		return null;
		
	}
	
	public String sentMylayoutHttpPost(String strParam) throws Exception{
		List<NameValuePair> paramList = new ArrayList<NameValuePair>();
		paramList.add(new BasicNameValuePair(POST_KEY, strParam)); 
		
		BasicHttpParams params = new BasicHttpParams();
		params.setParameter(POST_KEY, URLEncoder.encode(strParam,"UTF-8"));
		
		
		
		try {


			client = new DefaultHttpClient();
			HttpPost request = new HttpPost(this.SYNCLAYOUT_URL);
			
			/*
			request.addHeader("Accept-Language", Locale.getDefault().toString());
			request.addHeader("Content-Type", "text/plain");
			*/
			request.setEntity(new UrlEncodedFormEntity(paramList,HTTP.UTF_8));
			

			HttpResponse response = client.execute(request);
			Log.d("GAE", response.getStatusLine().getStatusCode()+"");
			if(response.getStatusLine().getStatusCode() ==200){
				String result = EntityUtils.toString(response.getEntity());		
				
				Log.d("GAE", result);
				db.delete(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME, "", null);
				db.close();
				return result;
			}
			
			
			//clearSyncTable();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine",e.toString());
			e.printStackTrace();
			
		}finally{
			
			
		}
		return null;
	}
	public String sentMyplaceHttpPost(String strParam) throws Exception{
		
		
		List<NameValuePair> paramList = new ArrayList<NameValuePair>();
		paramList.add(new BasicNameValuePair(POST_KEY, strParam)); 
		
		BasicHttpParams params = new BasicHttpParams();
		params.setParameter(POST_KEY, URLEncoder.encode(strParam,"UTF-8"));
		
		
		
		try {


			client = new DefaultHttpClient();
			HttpPost request = new HttpPost(this.SYNCPLACE_URL);
			
			/*
			request.addHeader("Accept-Language", Locale.getDefault().toString());
			request.addHeader("Content-Type", "text/plain");
			*/
			request.setEntity(new UrlEncodedFormEntity(paramList,HTTP.UTF_8));
			

			HttpResponse response = client.execute(request);
			Log.d("GAE", response.getStatusLine().getStatusCode()+"");
			if(response.getStatusLine().getStatusCode() ==200){
				String result = EntityUtils.toString(response.getEntity());		
				
				Log.d("GAE", result);
				db.delete(MyDBOpenHelper.SYNC_TABLE_NAME, "", null);
				db.close();
				return result;
			}
			
			
			//clearSyncTable();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine",e.toString());
			e.printStackTrace();
			
		}finally{
			
			
		}
		return null;
	}
	
	public void localSync(String str) throws JSONException{
		
		if(db==null || !db.isOpen()){
			initDB();
		}

		if(str != null){
		
			JSONObject jsonRoot = new JSONObject(str);
			
			
			if(jsonRoot.has("myplaces")){
				JSONArray myplaces = jsonRoot.getJSONArray("myplaces");
				
				//clearDB();
				
				
				for(int i=0; i<myplaces.length(); i++){
					
					
					JSONObject place = (JSONObject) myplaces.get(i);
					int id = Integer.parseInt(place.getString("id"));
					String fingerprint = place.getString(MyDBOpenHelper.COL_FINGERPRINT);
					HashMap map = new HashMap();
					
					map.put(MyDBOpenHelper.COL_ID, place.getString("id"));
					map.put(MyDBOpenHelper.COL_PLACE_NAME, place.getString(MyDBOpenHelper.COL_PLACE_NAME) );
					map.put(MyDBOpenHelper.COL_ADDRESS, place.getString(MyDBOpenHelper.COL_ADDRESS));
					map.put(MyDBOpenHelper.COL_DESCRIPTION, place.getString(MyDBOpenHelper.COL_DESCRIPTION));
					map.put(MyDBOpenHelper.COL_LATITUDE, place.getString(MyDBOpenHelper.COL_LATITUDE));
					map.put(MyDBOpenHelper.COL_LONGITUDE, place.getString(MyDBOpenHelper.COL_LONGITUDE));
					map.put(MyDBOpenHelper.COL_FAVERITE, place.getString(MyDBOpenHelper.COL_FAVERITE));
					map.put(MyDBOpenHelper.COL_TIMESTAMP, place.getString(MyDBOpenHelper.COL_TIMESTAMP));
					map.put(MyDBOpenHelper.COL_FINGERPRINT, place.getString(MyDBOpenHelper.COL_FINGERPRINT));
					
					if(checkMyplaceDataAvailable(fingerprint)){
						//update data
						
						updateMyplace2DB(map);
						
						
						
					}else{
					
		
			
						addMyplace2DB(map);
					}
					
				}
			}
			
			if(jsonRoot.has("mylayouts")){
				
				
				
				JSONArray mylayouts = jsonRoot.getJSONArray("mylayouts");
				if(mylayouts.length()>0){
					db.delete(MyDBOpenHelper.LAYOUT_TABLE_NAME, "", new String[]{});
					for(int i=0; i<mylayouts.length(); i++){
						
						
						JSONObject layout = (JSONObject) mylayouts.get(i);
						int id = Integer.parseInt(layout.getString("id"));
						String fingerprint = layout.getString(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT);
						HashMap map = new HashMap();
						
						map.put(MyDBOpenHelper.LAYOUT_COL_ID, layout.getString("id"));
						map.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, layout.getString(MyDBOpenHelper.LAYOUT_COL_LAYOUT) );
						map.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, layout.getString(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));
						map.put(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT, fingerprint);
						/*
						if(checkMylayoutDataAvailable(fingerprint)){
							//update data
							
							updateMylayout2DB(map);
							
							
							
						}else{
						
			
						*/
							addMylayout2DB(map);
						//}
						
					}
				}else{
					
				}
			}
			
		}
		db.close();
	}
	

	private boolean checkMylayoutDataAvailable(String fingerprint){
		if(db==null || !db.isOpen()){
			
			initDB();
		}
		
		String[] columns = {MyDBOpenHelper.LAYOUT_COL_ID};
		String selection = MyDBOpenHelper.LAYOUT_COL_FINGERPRINT+ "=?"; 
		String[] selectionArgs = {fingerprint};
		String groupBy = "";
		String having = "";
		String orderBy = MyDBOpenHelper.COL_TIMESTAMP+" DESC";
		
		Cursor c = db.query(MyDBOpenHelper.LAYOUT_TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);
		
		if(c.getCount()>0){
			c.close();
			
			return true;
		}else{
			c.close();
			
			return false;
		}
		
	}
	
	
	private boolean checkMyplaceDataAvailable(String fingerprint){
		
		if(db==null || !db.isOpen()){
			initDB();
		}
		
		String[] columns = {MyDBOpenHelper.COL_ID};
		String selection = MyDBOpenHelper.COL_FINGERPRINT + "=?"; 
		String[] selectionArgs = {fingerprint};
		String groupBy = "";
		String having = "";
		String orderBy = MyDBOpenHelper.COL_TIMESTAMP+" DESC";
		
		Cursor c = db.query(MyDBOpenHelper.TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);
		
		if(c.getCount()>0){
			c.close();
			
			return true;
		}else{
			c.close();
			
			return false;
		}
		
	}
	
	public void clearSyncTable(){
		if(db==null || !db.isOpen()){
			initDB();
		}
		
		db.delete(MyDBOpenHelper.SYNC_TABLE_NAME, "", new String[]{});
		db.close();
	}
	
	private void addMyplace2DB(HashMap map){
		
		if(db==null || !db.isOpen()){
			initDB();
		}
		
		ContentValues values = new ContentValues();
		//values.put(MyPlacesDBOpenHelper.COL_ID, (String) map.get(MyPlacesDBOpenHelper.COL_ID));
		values.put(MyDBOpenHelper.COL_PLACE_NAME, (String) map.get(MyDBOpenHelper.COL_PLACE_NAME));
		values.put(MyDBOpenHelper.COL_ADDRESS, (String) map.get(MyDBOpenHelper.COL_ADDRESS));
		values.put(MyDBOpenHelper.COL_DESCRIPTION, (String) map.get(MyDBOpenHelper.COL_DESCRIPTION));
		values.put(MyDBOpenHelper.COL_LATITUDE, (String) map.get(MyDBOpenHelper.COL_LATITUDE));
		values.put(MyDBOpenHelper.COL_LONGITUDE, (String) map.get(MyDBOpenHelper.COL_LONGITUDE));
		values.put(MyDBOpenHelper.COL_FAVERITE, (String) map.get(MyDBOpenHelper.COL_FAVERITE));
		values.put(MyDBOpenHelper.COL_TIMESTAMP, (String) map.get(MyDBOpenHelper.COL_TIMESTAMP));
		values.put(MyDBOpenHelper.COL_FINGERPRINT, (String) map.get(MyDBOpenHelper.COL_FINGERPRINT));
		
		db.insert(MyDBOpenHelper.TABLE_NAME, "", values);
		db.close();
	}

	private void addMylayout2DB(HashMap map){
		if(db==null || !db.isOpen()){
			initDB();
		}
		
		ContentValues values = new ContentValues();
		//values.put(MyPlacesDBOpenHelper.COL_ID, (String) map.get(MyPlacesDBOpenHelper.COL_ID));
		values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, (String) map.get(MyDBOpenHelper.LAYOUT_COL_LAYOUT));
		values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, (String) map.get(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));
		values.put(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT, (String) map.get(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT));
		
		//db.insert(MyPlacesDBOpenHelper.LAYOUT_TABLE_NAME, "", values);
		this.getContentResolver().insert(LayoutTableMetadata.CONTENT_URI, values);
		db.close();
	}
	
	private void updateMylayout2DB(HashMap map){
		if(db==null || !db.isOpen()){
			initDB();
		}
		
		ContentValues values = new ContentValues();
		
		
		values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, (String) map.get(MyDBOpenHelper.LAYOUT_COL_LAYOUT));
		values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, (String) map.get(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));

		
		String[] args = {(String) map.get(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT)};
		
		db.update(MyDBOpenHelper.LAYOUT_TABLE_NAME, values, MyDBOpenHelper.LAYOUT_COL_FINGERPRINT +"=?", args);
		db.close();
	}
	
	private void updateMyplace2DB(HashMap map){
		
		if(db==null || !db.isOpen()){
			initDB();
		}
		
		ContentValues values = new ContentValues();
		
		values.put(MyDBOpenHelper.COL_PLACE_NAME, (String) map.get(MyDBOpenHelper.COL_PLACE_NAME));
		values.put(MyDBOpenHelper.COL_ADDRESS, (String) map.get(MyDBOpenHelper.COL_ADDRESS));
		values.put(MyDBOpenHelper.COL_DESCRIPTION, (String) map.get(MyDBOpenHelper.COL_DESCRIPTION));
		values.put(MyDBOpenHelper.COL_LATITUDE, (String) map.get(MyDBOpenHelper.COL_LATITUDE));
		values.put(MyDBOpenHelper.COL_LONGITUDE, (String) map.get(MyDBOpenHelper.COL_LONGITUDE));
		values.put(MyDBOpenHelper.COL_FAVERITE, (String) map.get(MyDBOpenHelper.COL_FAVERITE));
		values.put(MyDBOpenHelper.COL_TIMESTAMP, (String) map.get(MyDBOpenHelper.COL_TIMESTAMP));
		
		String[] args = {(String) map.get(MyDBOpenHelper.COL_FINGERPRINT)};
		
		db.update(MyDBOpenHelper.TABLE_NAME, values, MyDBOpenHelper.COL_FINGERPRINT +"=?", args);
		db.close();
	}
	
	private class ServiceTask extends AsyncTask{

		@Override
		protected Object doInBackground(Object... params) {
			// TODO Auto-generated method stu			
			// my layout data
			try{
				doMyPlacesSync();
				doMyLayoutSync(); 
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			try{
				db.close();	
			}catch(Exception e){
				e.printStackTrace();
			}
			return null;
		}
		
	}
	
	private void doMyPlacesSync() throws Exception{
		
		if(db==null || !db.isOpen()){
			initDB();
		}
		
		ArrayList<HashMap> upstreamList = new ArrayList<HashMap>();
		//organize the params

		
		
		

		
		
		//myplace data
		
		String[] columns = {MyDBOpenHelper.SYNC_COL_ID,
				MyDBOpenHelper.SYNC_COL_PLACEID,
				MyDBOpenHelper.SYNC_COL_SYNCSTATE,
				MyDBOpenHelper.SYNC_COL_FINGERPRINT};
		
		String selection = ""; // if press faverite button
		String[] selectionArgs = {};
		String groupBy = "";
		String having = "";
		String orderBy = MyDBOpenHelper.SYNC_COL_ID+" DESC";
		
		
		Cursor c = db.query(MyDBOpenHelper.SYNC_TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);
		if(c.getCount()>0){ //have something to update
		
			while(c.moveToNext()){
			
				HashMap map = new HashMap();
	
				map.put(MyDBOpenHelper.SYNC_COL_SYNCSTATE, c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_COL_SYNCSTATE)));
				map.put(MyDBOpenHelper.COL_FINGERPRINT, c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_COL_FINGERPRINT)));
				
				//
				if(!c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_COL_SYNCSTATE)).equalsIgnoreCase(MyDBOpenHelper.STATE_KILLED)){
				
					String[] columns1 = {MyDBOpenHelper.COL_ID,
							MyDBOpenHelper.COL_PLACE_NAME,
							MyDBOpenHelper.COL_ADDRESS,
							MyDBOpenHelper.COL_DESCRIPTION,
							MyDBOpenHelper.COL_FAVERITE,
							MyDBOpenHelper.COL_LATITUDE,
							MyDBOpenHelper.COL_LONGITUDE,
							MyDBOpenHelper.COL_TIMESTAMP,
							MyDBOpenHelper.COL_FINGERPRINT};
					
					String selection1 = MyDBOpenHelper.COL_ID + "=?"; 
					String[] selectionArgs1 = {c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_COL_PLACEID))};
					String groupBy1 = "";
					String having1 = "";
					String orderBy1 = MyDBOpenHelper.COL_TIMESTAMP+" DESC";
					Cursor c1 = db.query(MyDBOpenHelper.TABLE_NAME, columns1, selection1, selectionArgs1, groupBy1, having1, orderBy1);
					if(c1.getCount()>0){
						
						c1.moveToFirst();
						
						map.put(MyDBOpenHelper.COL_ID, c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_ID)));
						map.put(MyDBOpenHelper.COL_PLACE_NAME, c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_PLACE_NAME)));
						map.put(MyDBOpenHelper.COL_ADDRESS, c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_ADDRESS)));
						map.put(MyDBOpenHelper.COL_DESCRIPTION, c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_DESCRIPTION)));
						map.put(MyDBOpenHelper.COL_LATITUDE, c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_LATITUDE)));
						map.put(MyDBOpenHelper.COL_LONGITUDE, c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_LONGITUDE)));
						map.put(MyDBOpenHelper.COL_FAVERITE, c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_FAVERITE)));
						map.put(MyDBOpenHelper.COL_TIMESTAMP, c1.getString(c1.getColumnIndex(MyDBOpenHelper.COL_TIMESTAMP)));
						
					}
					
					c1.close();
					
				}else{
					// if killed only have id, fingerprint and state
					map.put(MyDBOpenHelper.COL_ID, c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_COL_PLACEID)));
					
				}
				upstreamList.add(map);
				
			}
			
			
			
			
			
		try {
				
				_params = postMyplaceParamOrganize(upstreamList);
				
				Log.d("GAE", "POST: " + _params);
				String results = sentMyplaceHttpPost(_params);
				Log.d("GAE", "RECEIVED: " + results);
				localSync(results);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				
				
				db.close();
				
			}
		}else{
			
				String results;
				try {
					results = sentMyplaceHttpGet();
					Log.d("GAE", "RECEIVED: " +results);
					localSync(results);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				}finally{
					db.close();
					
				}
				
		}
		c.close();
		
	}
	
	private void doMyLayoutSync() throws Exception{
		
		if(db==null || !db.isOpen()){
			initDB();
		}
		
		ArrayList<HashMap> upstreamList = new ArrayList<HashMap>();
		
		String[] columns = {MyDBOpenHelper.SYNC_LAYOUT_COL_ID,
				MyDBOpenHelper.SYNC_LAYOUT_LAYOUTID,
				MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE,
				MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT
		};
		
		String selection = ""; // if press faverite button
		String[] selectionArgs = {};
		String groupBy = "";
		String having = "";
		String orderBy = MyDBOpenHelper.SYNC_LAYOUT_COL_ID+" DESC";
		
		//Cursor c = db.query(MyPlacesDBOpenHelper.SYNC_LAYOUT_TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);
		Cursor c = db.query(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);
		if(c.getCount()>0){ //have something to update
			while(c.moveToNext()){
				
				HashMap map = new HashMap();
	
				map.put(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE, c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE)));
				map.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT, c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT)));
											
				//
				if(!c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_COL_SYNCSTATE)).equalsIgnoreCase(MyDBOpenHelper.STATE_KILLED)){
				
					String[] columns1 = {MyDBOpenHelper.LAYOUT_COL_ID,
							MyDBOpenHelper.LAYOUT_COL_LAYOUT,
							MyDBOpenHelper.LAYOUT_COL_TIMESTAMP
							};
					
					//String selection1 = MyPlacesDBOpenHelper.LAYOUT_COL_ID + "=?"; 
					String selection1 = MyDBOpenHelper.LAYOUT_COL_FINGERPRINT + "=?"; 
					String[] selectionArgs1 = {c.getString(c.getColumnIndex(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT))};
					String groupBy1 = "";
					String having1 = "";
					String orderBy1 = MyDBOpenHelper.LAYOUT_COL_TIMESTAMP+" DESC";
					//Cursor c1 = db.query(MyPlacesDBOpenHelper.LAYOUT_TABLE_NAME, columns1, selection1, selectionArgs1, groupBy1, having1, orderBy1);
					Cursor c1 = getContentResolver().query(LayoutTableMetadata.CONTENT_URI, columns1, selection1, selectionArgs1, orderBy1);
					if(c1.getCount()>0){
						
						c1.moveToFirst();
						
						map.put(MyDBOpenHelper.LAYOUT_COL_ID, c1.getString(c1.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_ID)));
						map.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, c1.getString(c1.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_LAYOUT)));
						map.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, c1.getString(c1.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP)));				
						
					}
					
					c1.close();
					
				}else{
					// if killed only have id, fingerprint and state
					map.put(MyDBOpenHelper.LAYOUT_COL_ID, c.getString(c.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_ID)));
					
				}
				upstreamList.add(map);
				
			}
			
			
			
			try {
				
				_params = postMylayoutParamOrganize(upstreamList);
				
				Log.d("GAE", "POST: " + _params);
				String results = sentMylayoutHttpPost(_params);
				Log.d("GAE", "RECEIVED: " + results);
				localSync(results);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			c.close();
			db.close();
				
			
		}else{
			String results;
			try {
				results = sentMylayoutHttpGet();
				Log.d("GAE", "RECEIVED: " +results);
				localSync(results);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
		}
		c.close();
		db.close();
	}
}
