package idv.xunqun.navier;



import idv.xunqun.navier.utils.Utility;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;


public class PreferenceSettings extends PreferenceActivity {
	
	private SharedPreferences _sharedPreference;
	private SharedPreferences.Editor _editor;
	public static String PREFS_NAME = "navier.pref";
	
	
	private String PREF_ACCOUNT = "account";
	
	
	//DEFAULT VALUES
	//UI
	
	private boolean Nav_Mode = false; // NORMAL -> false or HUD -> true
	private boolean Energy_Mode = false; // NORMAL -> false or Saving -> ture
	//Speedometer
	private int Speed_Unit = 0; //Km/h or Mph
	private int Max_Speed = 160; //kph
	private int Alarm_Speed = 100; //kph
	
	//Direction
	private String Direction_API = "http://maps.google.com/maps/api/directions/json?";
	private String Driving = "driving"; //driving, walking, bicycling
	private String Avoid = ""; //highways, tolls
	private String Language = "en"; 
	private String Sensor = "true";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.preference_setting);
		

		addPreferencesFromResource(R.xml.settings);
		
		
	}


	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Utility.requestLocationServiceUpdate(this);
		//Utility.checkLanscapeOrientation(this);
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Utility.requestLocationServiceStop(this);
	}




	


	
	

}
