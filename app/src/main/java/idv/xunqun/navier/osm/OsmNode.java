package idv.xunqun.navier.osm;

import idv.xunqun.navier.content.Latlng;

public class OsmNode {
	public int _id;
	public double _lat;
	public double _lng;
	
	public boolean _visible;
	
	public double getLatOnMap(Latlng iniPos, double mapScaleRate, int screenWidth){
		double y = _lat - iniPos.getLat();
		return (screenWidth/mapScaleRate ) * y *-1;
	}
	
	public double getLngOnMap(Latlng iniPos, double mapScaleRate, int screenWidth){
		double x = _lng - iniPos.getLng();
		return (screenWidth/mapScaleRate)*x ;
	}
	

}
