package idv.xunqun.navier.osm;

import android.graphics.Path;

public class OsmWay {
	public String _id = "";
	public Path _wayPath;
	public String _type = "";
	public Boolean _oneWay;
	public String _name = "";
}
