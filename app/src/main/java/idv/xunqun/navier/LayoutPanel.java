package idv.xunqun.navier;

import idv.xunqun.navier.NavierPanelDesigner.PartNail;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

public class LayoutPanel extends View {
	
	
	public final static int WIDTH_UNIT_COUNT = 16;
	public final static int HEIGHT_UNIT_COUNT = 8;
	public final static float ELEM_ZOOMOUTRATE = .8f;
	public final static int GLOBAL_COLOR = Color.CYAN;
	

	
	
	
	
	Context _context;
	
	//layout properties
	int _unitPixel;
	int _layoutWidth;
	int _layoutHeight;
	int _screenWidthMargin;
	int _screenHeightMargin;
	int _iniTop;
	int _iniLeft;
	
	//path
	Path _gridPath;
	Paint _gridPaint;
	
	Path _bgPath;
	Paint _bgPaint;
	
	Path _shadowPath;
	Paint _shadowPaint;
	
	
	//touch
	
	float[] _posDown;
	boolean _holdFlag = false;
	
	
	
	//values
	NavierPanelDesigner.PartNail _currentPartNail=null;
	public ArrayList<NavierPanelDesigner.PartNail> _allPartNails;
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		
		
		
		
		//if(_currentPartNail!=null){
		
		
			switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				
				_posDown = new float[]{event.getX(),event.getY()};
				
				if(((NavierPanelDesigner)_context)._currentState == NavierPanelDesigner.STATE_READY){
					
					_holdFlag = true;
					
					new CountDownTimer(500, 500){

						@Override
						public void onTick(long millisUntilFinished) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onFinish() {
							// TODO Auto-generated method stub
							if(_holdFlag == true && _allPartNails.size()>0){
								((NavierPanelDesigner)_context).setState(NavierPanelDesigner.STATE_EDIT);
								_currentPartNail = getTartPartNail();
								if(_currentPartNail!=null)
									preDrawShadow(_currentPartNail);
							}
						}
						
					}.start();
					
				}
				
				
				
				break;
			case MotionEvent.ACTION_MOVE:
				
				if(((NavierPanelDesigner)_context)._currentState == NavierPanelDesigner.STATE_EDIT && _currentPartNail!=null){
					float x = event.getX();
					float y = event.getY();
					
					float deltaX = x - _posDown[0];
					float deltaY = y - _posDown[1];
					
					if(((deltaX/_unitPixel)>=1 && _currentPartNail.ELEM_PIN[0]+_currentPartNail.ELEM_DIMENSION[0]+(int)(deltaX/_unitPixel) <= WIDTH_UNIT_COUNT) ||
							((deltaX/_unitPixel)<=-1 && _currentPartNail.ELEM_PIN[0]+(int)(deltaX/_unitPixel) >= 0)){
						
						_currentPartNail.ELEM_PIN[0]+=(int)(deltaX/_unitPixel);
						_posDown[0] = x;
						
					}
					
					if(((deltaY/_unitPixel)>=1 && _currentPartNail.ELEM_PIN[1]+_currentPartNail.ELEM_DIMENSION[1]+(int)(deltaY/_unitPixel) <= HEIGHT_UNIT_COUNT) ||
							((deltaY/_unitPixel)<=-1 && _currentPartNail.ELEM_PIN[1]+(int)(deltaY/_unitPixel) >= 0)){
						
						_currentPartNail.ELEM_PIN[1]+=(int)(deltaY/_unitPixel);
						_posDown[1] = y;
						
					}
					
					preDrawShadow(_currentPartNail);
				}
				break;
			case MotionEvent.ACTION_UP:
				
				_holdFlag = false;
				
				break;
			case MotionEvent.ACTION_OUTSIDE:
				
				break;
			case MotionEvent.ACTION_CANCEL:
				
				break;
			}
		//}
		return true;
		
		
	}
	
	public NavierPanelDesigner.PartNail getTartPartNail(){
		
		ArrayList<PartNail> candidate = new ArrayList<PartNail>();
		
		for(int i=0;i<_allPartNails.size();i++){
			NavierPanelDesigner.PartNail p = _allPartNails.get(i);
			int[] rect = p.ELEM_RECT;
			if(_posDown[0]>= rect[0] && _posDown[0]<=rect[2] && _posDown[1] >= rect[1] && _posDown[1]<=rect[3])
				candidate.add(p);
		}
		
		if(candidate.size() == 0){
			
			return null;			
		}else if(candidate.size() == 1){
			
			return (PartNail) candidate.get(0);
		}else{
			
			return (PartNail) candidate.get(candidate.size()-1);
		}
		
	}
	
	@Override
	protected void onCreateContextMenu(ContextMenu menu) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu);
	}

	public void onLongClick(){
		Log.d("mine", "LongClick: " + _posDown[0] + "," + _posDown[1]);
	}

	
	
	
	public LayoutPanel(Context context, AttributeSet set) {
		super(context, set);
		// TODO Auto-generated constructor stub
		_context = context;
		initProperties();
		initPath();
		
		
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		drawBoard(canvas);
		drawStickedParts(canvas);
		
		if(((NavierPanelDesigner)_context)._currentState == NavierPanelDesigner.STATE_EDIT){
			drawShadow(canvas);
		}
		
		
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		

		
		setMeasuredDimension(_screenWidthMargin*2+_unitPixel*WIDTH_UNIT_COUNT, _screenHeightMargin*2+_unitPixel*HEIGHT_UNIT_COUNT);
		
	}

	public void initProperties(){
		
		Display display = ((WindowManager) _context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(); 
		int width = display.getWidth(); 
		int height = display.getHeight();
		
		int _layoutWidth = (int) (width * ELEM_ZOOMOUTRATE);
		int _layoutHeight = (int) (height * ELEM_ZOOMOUTRATE);
		
		_unitPixel=(int)((_layoutWidth/WIDTH_UNIT_COUNT>_layoutHeight/HEIGHT_UNIT_COUNT)?_layoutHeight/HEIGHT_UNIT_COUNT:_layoutWidth/WIDTH_UNIT_COUNT);
		
		_screenWidthMargin = (_layoutWidth - (_unitPixel*WIDTH_UNIT_COUNT))/2;
		_screenHeightMargin = (_layoutHeight - (_unitPixel*HEIGHT_UNIT_COUNT))/2;
		
		_iniTop = _screenHeightMargin;
		_iniLeft =_screenWidthMargin ;
		
		//parts list
		_allPartNails = new ArrayList<NavierPanelDesigner.PartNail>();
		
		
	}
	
	public void initPath(){
		//Paint
		_gridPaint = new Paint();
		_gridPaint.setColor(GLOBAL_COLOR);        
		_gridPaint.setStrokeWidth(1);
		_gridPaint.setStyle(Style.STROKE);
		
		
		//Path
		_gridPath = new Path();
		_gridPath.moveTo(_screenWidthMargin, _screenHeightMargin);
		float tmpX=_screenWidthMargin , tmpY=_screenHeightMargin;
		
		for(int i=0; i<=WIDTH_UNIT_COUNT ; i++){
			_gridPath.lineTo(tmpX, tmpY+HEIGHT_UNIT_COUNT*_unitPixel);
			tmpX += _unitPixel;
			tmpY = _screenHeightMargin;
			_gridPath.moveTo(tmpX, tmpY);
		}
		
		_bgPath = new Path();
		_bgPath.addRect(_iniLeft, _iniTop, _iniLeft + _unitPixel*WIDTH_UNIT_COUNT, _iniTop + _unitPixel*HEIGHT_UNIT_COUNT, Direction.CCW);
		
		_bgPaint = new Paint();
		_bgPaint.setStyle(Style.FILL);
		_bgPaint.setColor(0xD0000000);
		
		
		
		tmpX = _screenWidthMargin;
		tmpY = _screenHeightMargin;		
		_gridPath.moveTo(tmpX, tmpY);
		
		for(int i=0 ; i<=HEIGHT_UNIT_COUNT ; i++){
			_gridPath.lineTo(tmpX+WIDTH_UNIT_COUNT*_unitPixel, tmpY);
			tmpY +=  _unitPixel;
			tmpX = _screenWidthMargin;
			_gridPath.moveTo(tmpX, tmpY);
		}
		
		
		//Shadow
		_shadowPath = new Path();
		
		_shadowPaint = new Paint();
		_shadowPaint.setColor(GLOBAL_COLOR);
		_shadowPaint.setAlpha(100);
		_shadowPaint.setStyle(Style.FILL);
		
		
	}
	
	public void drawBoard(Canvas canvas){
		
		canvas.drawPath(_bgPath, _bgPaint);
		canvas.drawPath(_gridPath, _gridPaint);			
		
		
	}
	
	public void addEditablePartNail(NavierPanelDesigner.PartNail p){
		p.ELEM_PIN = new int[]{0,0};
		_currentPartNail = p;
		
		
		preDrawShadow(p);
		
		
	}

	public void stickCurrentPartNail(){
		if(_currentPartNail!=null){
			removePartNail(_currentPartNail);
			_allPartNails.add(_currentPartNail);
			invalidate();
		}
		
		
	}
	
	private boolean removePartNail(PartNail p){
		boolean have = false;
		for(int i=0;i<_allPartNails.size();i++){
			PartNail pn = _allPartNails.get(i);
			if(pn.ELEM_ID == p.ELEM_ID){
				have = true;
				_allPartNails.remove(i);
				break;
			}
		}
		
		return have;
	}
	
	public void cancelCurrentPartNail(){
		if(_currentPartNail!=null){
			removePartNail(_currentPartNail);
			invalidate();
		}
	}
	
	public void handleNewButton(){
		_allPartNails.clear();
		invalidate();
	}
	
	public void handleLoadLayout(ArrayList list){
		_allPartNails = list;
		invalidate();
	}
	
	public void handleSaveButton(){
		
	}
	
	public void preDrawShadow(NavierPanelDesigner.PartNail p){
		_shadowPath.reset();
		
		int left = p.ELEM_PIN[0]*_unitPixel + _screenWidthMargin;
		int top = p.ELEM_PIN[1]*_unitPixel + _screenHeightMargin;
		int right = left + p.ELEM_DIMENSION[0]*_unitPixel;
		int bottom = top + p.ELEM_DIMENSION[1]*_unitPixel;
		
		_currentPartNail.ELEM_RECT = new int[]{left, top, right, bottom};
		_shadowPath.addRect(left, top, right, bottom, Direction.CCW);
		
		invalidate();
	}
	
	public void drawShadow(Canvas canvas){
		canvas.save();
		canvas.drawPath(_shadowPath, _shadowPaint);
		canvas.restore();
	}
	
	public void drawStickedParts(Canvas canvas){
		
		canvas.save();
		
		for(int i=0;i<_allPartNails.size();i++){
			
			NavierPanelDesigner.PartNail p = _allPartNails.get(i);
			p.drawNail(canvas, _screenWidthMargin, _screenHeightMargin, _unitPixel);
			p.drawShadow(canvas, _unitPixel, _iniLeft, _iniTop, p.ELEM_PIN);
		}
		
		canvas.restore();
	}
}
