package idv.xunqun.navier;

import android.R.integer;
import android.R.string;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;

public class PreferencesHandler {

	private static PreferencesHandler _instance;
	private SharedPreferences _sharedPreference;
	private SharedPreferences.Editor _editor;
	private static String FILE_NAME = PreferenceSettings.PREFS_NAME;

	// values

	private Context _context;

	public PreferencesHandler(Context context) {

		FILE_NAME = Version.isLite(context) ? "idv.xunqun.navier_preferences"
				: "idv.xunqun.navier.premium_preferences";

		_context = context;
		_sharedPreference = context.getSharedPreferences(FILE_NAME,
				Context.MODE_WORLD_WRITEABLE);
		_editor = _sharedPreference.edit();
	}

	private String getStringResource(int r) {
		return _context.getResources().getString(r);
	}

	public static PreferencesHandler get(Context context) {
		_instance = new PreferencesHandler(context);
		return _instance;
	}

	public void clear() {
		_editor.clear();
		_editor.commit();
	}
	
	private String PREF_NOTICE_CODE = "PREF_NOTICE_CODE";
	public int getPREF_NOTICE_CODE(){
		return _sharedPreference.getInt(PREF_NOTICE_CODE, 0);
	}
	
	public void setPREF_NOTICE_CODE(int code){
		_editor.putInt(PREF_NOTICE_CODE, code);
		_editor.commit();
	}

	private String PREF_SHOW_NOTICE = "PREF_SHOW_NOTICE";
	public boolean getPREF_SHOW_NOTICE(){
		return _sharedPreference.getBoolean(PREF_SHOW_NOTICE, true);
	}
	
	public void setPREF_SHOW_NOTICE(boolean b){
		_editor.putBoolean(PREF_SHOW_NOTICE, b);
		_editor.commit();
	}
	
	private String PREF_SPECIAL_VERSION = "PREF_SPECIAL_VERSION";
	public boolean getPREF_SPACIAL_VERSION(){
		return _sharedPreference.getBoolean(PREF_SPECIAL_VERSION, false);
	}
	
	public void setPREF_SPECIAL_VERSION(boolean b){
		_editor.putBoolean(PREF_SPECIAL_VERSION, b);
		_editor.commit();
	}
	
	
	
	
	// for display rating dialog
	private String PREF_OPENCOUNT = "PREF_OPENCOUNT";

	public int getPREF_OPENCOUNT() {
		return _sharedPreference.getInt(PREF_OPENCOUNT, 0);
	}

	public void setPREF_OPENCOUNT(int i) {
		_editor.putInt(PREF_OPENCOUNT, i);
		_editor.commit();
	}

	private String PREF_LISENCEDUSER = "lisenced";

	public boolean getPREF_LISENCEDUSER() {
		return _sharedPreference.getBoolean(PREF_LISENCEDUSER, true);
	}

	public void setPREF_LISENCEDUSER(boolean b) {
		_editor.putBoolean(PREF_LISENCEDUSER, b);
		_editor.commit();
	}

	String PREF_SHOWMAPINTRO = "map_intro";

	public boolean getPREF_SHOWMAPINTRO() {
		return _sharedPreference.getBoolean(PREF_SHOWMAPINTRO, true);
	}

	public void setPREF_SHOWMAPINTRO(boolean b) {
		_editor.putBoolean(PREF_SHOWMAPINTRO, b);
		_editor.commit();
	}

	public float getPREF_LATEST_LAT() {
		return _sharedPreference.getFloat("latest_lat", 37.76666666666f);
	}

	public void setPREF_LATEST_LAT(float lat) {
		_editor.putFloat("latest_lat", lat);
		_editor.commit();
	}

	public float getPREF_LATEST_LNG() {
		return _sharedPreference.getFloat("latest_lng", -122.41666666667f);
	}

	public void setPREF_LATEST_LNG(float lng) {
		_editor.putFloat("latest_lng", lng);
		_editor.commit();
	}

	public String getPREF_ACCOUNT() {

		return _sharedPreference.getString(
				getStringResource(R.string.PREF_ACCOUNT),
				getStringResource(R.string.PREF_ACCOUNT_DEFAULT));

	}

	public void setPREF_ACCOUNT(String s) {

		_editor.putString(getStringResource(R.string.PREF_ACCOUNT), s);
		_editor.commit();
	}

	//

	public boolean getPREF_SYNC() {
		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_SYNC), true);
	}

	public void setPREF_SYNC(boolean s) {
		_editor.putBoolean(getStringResource(R.string.PREF_SYNC), s);
		_editor.commit();
	}

	// Auto run panel

	public String getPREF_AUTO_RUN_PANEL() {
		return _sharedPreference.getString(
				getStringResource(R.string.PREF_RUN_PANEL_WHEN_START), "none");
	}

	public void setPREF_AUTO_RUN_PANEL(String fingerprint) {
		_editor.putString(
				getStringResource(R.string.PREF_RUN_PANEL_WHEN_START),
				fingerprint);
		_editor.commit();
	}

	// Speed units

	public static final int VALUE_OF_PREF_SPEED_UNIT_KMH = 1;
	public static final int VALUE_OF_PREF_SPEED_UNIT_MPH = 2;

	public int getPREF_SPEED_UNIT() {
		// km/h and mph

		String unitStr = _sharedPreference.getString(
				getStringResource(R.string.PREF_SPEED_UNIT),
				String.valueOf(VALUE_OF_PREF_SPEED_UNIT_KMH));
		return Integer.parseInt(unitStr);
	}

	public void setPREF_SPEED_UNIT(int i) {
		_editor.putString(getStringResource(R.string.PREF_SPEED_UNIT),
				String.valueOf(i));
		_editor.commit();
	}

	// Alert Speed
	// default in kmh
	public float getPREF_ALERT_SPEED() {
		return _sharedPreference.getFloat(
				getStringResource(R.string.PREF_ALERT_SPEED), 110f);
	}

	public void setPREF_ALERT_SPEED(float f) {
		_editor.putFloat(getStringResource(R.string.PREF_ALERT_SPEED), f);
		_editor.commit();
	}

	// Screen Orientation
	public boolean getPREF_LOCK_SCREEN_ORIENTATION() {
		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_LOCK_SCREEN_ORIENTATION), true);
	}

	public void setPREF_LOCK_SCREEN_ORIENTATION(boolean b) {
		_editor.putBoolean(
				getStringResource(R.string.PREF_LOCK_SCREEN_ORIENTATION), b);
		_editor.commit();
	}

	// Navigation mode

	public static String VALUE_OF_PREF_NAV_MODE_NOR = "NOR";
	public static String VALUE_OF_PREF_NAV_MODE_HUD = "HUD";

	public boolean getPREF_HUD_MODE() {
		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_HUD_MODE), false);
	}

	public void setPREF_HUD_MODE(boolean s) {
		_editor.putBoolean(getStringResource(R.string.PREF_HUD_MODE), s);
		_editor.commit();
	}

	// Brightness Mode
	public boolean getPREF_BRIGHTNESS_SETTING() {

		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_BRIGHTNESS_SETTING), true);
	}

	public void setPREF_BRIGHTNESS_SETTING(boolean s) {
		_editor.putBoolean(getStringResource(R.string.PREF_BRIGHTNESS_SETTING),
				s);
		_editor.commit();
	}

	// Energy Mode

	public boolean getPREF_ENERGY_MODE() {

		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_ENERGY_MODE), false);
	}

	public void setPREF_ENERGY_MODE(boolean s) {
		_editor.putBoolean(getStringResource(R.string.PREF_ENERGY_MODE), s);
		_editor.commit();
	}

	// Show Grid

	public boolean getPREF_SHOW_GRID() {

		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_SHOW_GRID), false);

	}

	public void setPREF_SHOW_GRID(boolean s) {
		_editor.putBoolean(getStringResource(R.string.PREF_SHOW_GRID), s);
		_editor.commit();
	}

	// 24 time format
	public boolean getPREF_24HOUR() {

		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_TIME_FORMAT), false);

	}

	public void setPREF_24HOUR(boolean s) {
		_editor.putBoolean(getStringResource(R.string.PREF_TIME_FORMAT), s);
		_editor.commit();
	}

	// COLOR

	public static String VALUE_OF_PREF_COLOR_CYAN = "CYAN";
	public static String VALUE_OF_PREF_COLOR_LIGHTGRAY = "LIGHT GRAY";
	public static String VALUE_OF_PREF_COLOR_WHITE = "WHITE";
	public static String VALUE_OF_PREF_COLOR_YELLOW = "YELLOW";
	public static String VALUE_OF_PREF_COLOR_GREEN = "GREEN";
	public static String VALUE_OF_PREF_COLOR_BLUE = "BLUE";
	public static String VALUE_OF_PREF_COLOR_ORANGERED = "ORANGE RED";
	public static String VALUE_OF_PREF_COLOR_SKYBLUE = "SKY BLUE";

	public int getPREF_COLOR() {

		String strColor = _sharedPreference.getString(
				getStringResource(R.string.PREF_COLOR),
				getStringResource(R.string.PREF_COLOR_DEFAULT));
		int rgbColor = Color.CYAN;

		if (strColor.equals(VALUE_OF_PREF_COLOR_CYAN)) {
			rgbColor = Color.CYAN;
		} else if (strColor.equals(VALUE_OF_PREF_COLOR_LIGHTGRAY)) {
			rgbColor = Color.LTGRAY;
		} else if (strColor.equals(VALUE_OF_PREF_COLOR_WHITE)) {
			rgbColor = Color.WHITE;
		} else if (strColor.equals(VALUE_OF_PREF_COLOR_YELLOW)) {
			rgbColor = Color.YELLOW;
		} else if (strColor.equals(VALUE_OF_PREF_COLOR_GREEN)) {
			rgbColor = Color.GREEN;
		} else if (strColor.equals(VALUE_OF_PREF_COLOR_BLUE)) {
			rgbColor = Color.BLUE;
		} else if (strColor.equals(VALUE_OF_PREF_COLOR_ORANGERED)) {
			rgbColor = 0xffff4500;
		} else if (strColor.equals(VALUE_OF_PREF_COLOR_SKYBLUE)) {
			rgbColor = 0xff87ceeb;
		} else {
			rgbColor = Color.CYAN;
		}

		return rgbColor;
	}

	public void setPREF_COLOR(String s) {
		_editor.putString(getStringResource(R.string.PREF_COLOR), s);
		_editor.commit();
	}

	// Lock Road
	public boolean getPREF_LOCK_ROAD() {
		return _sharedPreference
				.getBoolean(
						getStringResource(R.string.PREF_LOCK_ROAD),
						Boolean.parseBoolean(getStringResource(R.string.PREF_LOCK_ROAD_DEFAULT)));
	}

	public void setPREF_LOCK_ROAD(boolean b) {
		_editor.putBoolean(getStringResource(R.string.PREF_LOCK_ROAD), b);
		_editor.commit();

	}

	// Alarm SEC
	public int getPREF_TURNALARM_SEC() {
		return Integer.parseInt(_sharedPreference.getString(
				getStringResource(R.string.PREF_TURN_NOTIFY_SEC),
				getStringResource(R.string.PREF_TURN_NOTIFY_SEC_DEFAULT)));
	}

	public void setPREF_TURNALARM_SEC(int sec) {
		_editor.putString(getStringResource(R.string.PREF_TURN_NOTIFY_SEC),
				String.valueOf(sec));
		_editor.commit();
	}

	// Trans type

	public static String VALUE_OF_PREF_TRANS_TYPE_DRIVING = "driving";
	public static String VALUE_OF_PREF_TRANS_TYPE_WALKING = "walking";
	public static String VALUE_OF_PREF_TRANS_TYPE_BICYCLEING = "bicycling";
	// map quest api
	public static String VALUE_OF_PREF_ROUTE_TYPE_FASTEST = "fastest";
	public static String VALUE_OF_PREF_ROUTE_TYPE_PEDESTRIAN = "pedestrian";
	public static String VALUE_OF_PREF_ROUTE_TYPE_BICYCLE = "bicycle";

	public String getPREF_TRANS_TYPE() {
		return _sharedPreference.getString(
				getStringResource(R.string.PREF_TRANS_TYPE),
				VALUE_OF_PREF_ROUTE_TYPE_FASTEST);
	}

	public void setPREF_TRANS_TYPE(String s) {
		_editor.putString(getStringResource(R.string.PREF_TRANS_TYPE), s);
		_editor.commit();
	}


	// Avoid tolls

	public boolean getPREF_AVOID_TOLLS() {

		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_AVOID_TOLLS), false);
	}

	public void setPREF_AVOID_TOLLS(boolean s) {
		_editor.putBoolean(getStringResource(R.string.PREF_AVOID_TOLLS), s);
		_editor.commit();
	}

	// Avoid highway

	public boolean getPREF_AVOID_HIGHWAY() {
		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_AVOID_HIGHWAY), false);
	}

	public void setPREF_AVOID_HIGHWAY(boolean s) {
		_editor.putBoolean(getStringResource(R.string.PREF_AVOID_HIGHWAY), s);
		_editor.commit();
	}

	// speech voice
	public boolean getPREF_NAVI_VOICE() {
		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_NAVI_VOICE), true);
	}

	public void setPREF_NAVI_VOICE(boolean s) {
		_editor.putBoolean(getStringResource(R.string.PREF_NAVI_VOICE), s);
		_editor.commit();
	}

	// Notify sound
	public boolean getPREF_NOTIFY_SOUND() {
		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_NOTIFY_SOUND), true);
	}

	public void setPREF_NOTIFY_SOUND(boolean s) {
		_editor.putBoolean(getStringResource(R.string.PREF_NOTIFY_SOUND), s);
		_editor.commit();
	}

	// Data Initial
	public boolean getPREF_DATAINITIAL() {
		return _sharedPreference.getBoolean(
				getStringResource(R.string.PREF_DATAINITIAL), false);
	}

	public void setPREF_DATAINITIAL(boolean s) {
		_editor.putBoolean(getStringResource(R.string.PREF_DATAINITIAL), s);
		_editor.commit();
	}

	public long getPREF_PROMOTE() {
		// TODO Auto-generated method stub
		return _sharedPreference.getLong("PREF_PROMOTE", 0);
	}
	
	public void setPREF_PROMOTE(long time){
		_editor.putLong("PREF_PROMOTE", time).apply();
	}
}
