package idv.xunqun.navier;

import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.osm.OsmNode;
import idv.xunqun.navier.osm.OsmWay;
import idv.xunqun.navier.parts.Elem_Route;
import idv.xunqun.navier.parts.Parts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Path;
import android.graphics.RectF;
import android.location.Location;
import android.util.Log;
import android.util.Xml;

public class StreetMapRequester implements Runnable {
	
	// OSM sample: http://www.openstreetmap.org/api/0.6/map?bbox=120.21082,22.96754,120.22904,22.98514
	public static final String OSM_REQUEST_URL = "http://www.openstreetmap.org/api/0.6/map?";
	public final double BUFFER_WIDTH=0.005; //arround 1.1 km
	public final float SAFE_RANGE = 0.001f;
	
	//values
	private ArrayList<OsmWay> _wayList;
	private Location _curLocation;
	private Latlng _iniPos;
	private RectF _bufferedRect;
	private RectF _safeRect;
	private int _screenWidth;
	private double _scaleRate;
	private boolean _stopRetriving;
	private boolean _isDoingBufferMap = false;
	
	private MyDBOpenHelper helper;
	private SQLiteDatabase db;
	private Context _context;
	private Elem_Route _parent;
	private Thread _thread;
	

	public String _parameter;
	
	public StreetMapRequester(Context context,Parts parent, Location curLocation,  Latlng iniPos, int screenWidth, double scaleRate){
		
		_curLocation = curLocation;	
		_iniPos = iniPos;
		_screenWidth = screenWidth;
		_scaleRate = scaleRate;
		_context = context;
		_parent = (Elem_Route) parent;
		
		_stopRetriving = false;
		
		_thread = new Thread(this);
		_thread.start();
		
	}
	
	public void threadStop(Boolean stop){
		_stopRetriving = stop;
	}
	
	public Thread.State getThreadState(){
		return _thread.getState();
	}
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		
		initDB();
		while(!_stopRetriving && !_isDoingBufferMap){
			
			
			if(_curLocation!=null){
				if(outOfSafeRact()){
			
					try {
						
						_isDoingBufferMap =true;
						Log.d("osm", "start buffer map");
						long time = System.currentTimeMillis();
						doBufferMap();
						Log.d("osm", "end buffer map with time: " + (System.currentTimeMillis()-time)/1000);
						_parent.involkOsmWayList(_wayList);
						_isDoingBufferMap =false;
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally{
						
					}
				}
			}
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		db.close();
		
	}
	
	/*
	 * update current location
	 */
	public void setCurLocation(Location loc){
		_curLocation = loc;
	}
	
	private void initDB(){
		try{
			helper = new MyDBOpenHelper(_context, null);
			db = helper.getReadableDatabase();
		
		}catch(SQLException e){
			e.printStackTrace();			
		}
		
	}
	
	private boolean outOfSafeRact(){
		
		if(_bufferedRect == null) return true;
		
		if(_curLocation.getLatitude() > _safeRect.top || _curLocation.getLatitude() < _safeRect.bottom || _curLocation.getLongitude() > _safeRect.right || _curLocation.getLongitude()< _safeRect.left){
			return true;
		}
		return false;
	}
	
	private void doBufferMap() throws XmlPullParserException{
		_bufferedRect = new RectF((float)(_curLocation.getLongitude() - BUFFER_WIDTH), (float)(_curLocation.getLatitude() + BUFFER_WIDTH) , 
				(float)(_curLocation.getLongitude() + BUFFER_WIDTH), (float)(_curLocation.getLatitude() - BUFFER_WIDTH ));  //RectF(float left, float top, float right, float bottom)
		
		_safeRect = new RectF( _bufferedRect.left+SAFE_RANGE, _bufferedRect.top-SAFE_RANGE, _bufferedRect.right-SAFE_RANGE, _bufferedRect.bottom+SAFE_RANGE);
		
		
		_wayList = new ArrayList<OsmWay>();
		
		db.delete(MyDBOpenHelper.OSMNODES_TABLE_NAME, "", new String[]{});
		
		sendRequest(_bufferedRect.left, _bufferedRect.bottom, _bufferedRect.right, _bufferedRect.top);		
		
	}


	
	public void sendRequest(double left, double bottom, double right, double top) throws XmlPullParserException{
		
		
		XmlPullParser parser = Xml.newPullParser();
		
		_parameter = "bbox=" + left + "," + bottom + "," + right + "," + top;
		
		BufferedReader in=null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(OSM_REQUEST_URL+_parameter);
			
			
			Log.d("osm",OSM_REQUEST_URL+_parameter);
			
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			
			parser.setInput(in);
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.nextTag();
			
			/*
			StringBuffer sb = new StringBuffer("");
			String line="";
			String NL =System.getProperty("line.separator");
			while((line=in.readLine())!= null){
				sb.append(line+NL);				
			}
			in.close();
			
			String result=sb.toString();		
			*/
						
			parseData(parser);
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine",e.toString());
			e.printStackTrace();
			
		}finally{
			
			if(in!=null){
				try{
					in.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}
	
	private void parseData(XmlPullParser parser) throws XmlPullParserException, IOException{
		
		parser.require(XmlPullParser.START_TAG, null, "osm");
		
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        String name = parser.getName();
	        
	        // Starts by looking for the entry tag
	        if (name.equals("way")) {
	        	_wayList.add(readWay(parser));
	        } else if(name.equals("node")){
	            storeNode2Db(readNode(parser));
	        } else{
	        	skip(parser);
	        }
		}
	}
	
	
	private OsmNode readNode(XmlPullParser parser) throws XmlPullParserException, IOException{
		
		
		parser.require(XmlPullParser.START_TAG, null, "node");
		OsmNode node = new OsmNode();
		node._id = Integer.parseInt(parser.getAttributeValue(null, "id"));  //reference column in db
		node._lat = Double.parseDouble(parser.getAttributeValue(null, "lat"));
		node._lng = Double.parseDouble(parser.getAttributeValue(null,"lon"));
		node._visible = Boolean.parseBoolean(parser.getAttributeValue(null,"visible"));
		
		//skip(parser);
		//Log.d("osm", "node id: " + node._id);
		skip(parser);
		return node;
	}
	
	private OsmWay readWay(XmlPullParser parser) throws XmlPullParserException, IOException{
		
		parser.require(XmlPullParser.START_TAG, null, "way");
		OsmWay way = new OsmWay();
		Path path = new Path();
		int nodeCount = 0;
		
		way._id = parser.getAttributeValue(null, "id");
		//Log.d("osm", "way id: " + way._id);
		
	    while (parser.next() != XmlPullParser.END_TAG) {
	    	if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	    	
	        String name = parser.getName();
	        
	        if(name.equals("nd")){
	        	
	        	OsmNode oNode = readNd(parser);
	        	
	        	if(nodeCount == 0 ){
	        		path.moveTo( (float)oNode.getLngOnMap(_iniPos, _scaleRate, _screenWidth),(float)oNode.getLatOnMap(_iniPos, _scaleRate, _screenWidth));
	        	}else{
	        		path.lineTo( (float)oNode.getLngOnMap(_iniPos, _scaleRate, _screenWidth),(float)oNode.getLatOnMap(_iniPos, _scaleRate, _screenWidth));
	        	}
	        	nodeCount++;
	        	//skip(parser);
	        	
	        }else if(name.equals("tag")){
	        	
	        	if(parser.getAttributeValue(null, "k").equals("name")){
	        		way._name = parser.getAttributeValue(null, "v");
	        	}else if(parser.getAttributeValue(null, "k").equals("highway")){
	        		way._type = parser.getAttributeValue(null, "v");
	        	}
	        	skip(parser);
	        	
	        }else{
	        	skip(parser);
	        }
	    }
	    
	    
	    way._wayPath = path;
	    //Log.d("osm", way._type==null?"null":way._type);
	    parser.require(XmlPullParser.END_TAG, null, "way");
	    return way;
	}
	
	private  OsmNode readNd(XmlPullParser parser) throws XmlPullParserException, IOException{
		parser.require(XmlPullParser.START_TAG, null, "nd");
		String id = parser.getAttributeValue(null, "ref");
		//Log.d("osm", "nd ref: " + id);
		skip(parser);
		
		
    	return getNodeInfo(id);
		
	}
	
	private OsmNode getNodeInfo(String id){
		OsmNode nd = new OsmNode();
		String[] columns = {MyDBOpenHelper.OSMNODES_TABLE_COL_REFERENCE, MyDBOpenHelper.OSMNODES_TABLE_COL_LATITUDE, MyDBOpenHelper.OSMNODES_TABLE_COL_LONGITUDE, MyDBOpenHelper.OSMNODES_TABLE_COL_VISIBLE};
		String selection = MyDBOpenHelper.OSMNODES_TABLE_COL_REFERENCE + "=?";
		String[] selectionArgs = {id};
		Cursor c = db.query(MyDBOpenHelper.OSMNODES_TABLE_NAME, columns, selection, selectionArgs, "", "", "");
		if(c.moveToFirst()){
			nd._id = c.getInt(c.getColumnIndex(MyDBOpenHelper.OSMNODES_TABLE_COL_REFERENCE));
			nd._lat = c.getDouble(c.getColumnIndex(MyDBOpenHelper.OSMNODES_TABLE_COL_LATITUDE));
			nd._lng = c.getDouble(c.getColumnIndex(MyDBOpenHelper.OSMNODES_TABLE_COL_LONGITUDE));
			nd._visible = c.getInt(c.getColumnIndex(MyDBOpenHelper.OSMNODES_TABLE_COL_VISIBLE)) == 1?true:false;
		}
		
		return nd;
	}
	
	private void storeNode2Db(OsmNode node){
		
		ContentValues values = new ContentValues();
		values.put(MyDBOpenHelper.OSMNODES_TABLE_COL_LATITUDE, node._lat);
		values.put(MyDBOpenHelper.OSMNODES_TABLE_COL_LONGITUDE, node._lng);
		values.put(MyDBOpenHelper.OSMNODES_TABLE_COL_VISIBLE, node._visible?1:0);
		values.put(MyDBOpenHelper.OSMNODES_TABLE_COL_REFERENCE, node._id);
		
		db.insert(MyDBOpenHelper.OSMNODES_TABLE_NAME, null, values);
		
	}
	
	private void skip(XmlPullParser parser) throws XmlPullParserException, IOException{
	    if (parser.getEventType() != XmlPullParser.START_TAG) {
	        throw new IllegalStateException();
	    }
	    int depth = 1;
	    while (depth != 0) {
	        switch (parser.next()) {
	        case XmlPullParser.END_TAG:
	            depth--;
	            break;
	        case XmlPullParser.START_TAG:
	            depth++;
	            break;
	        }
	    }
	}

}
