package idv.xunqun.navier;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class LocationService extends Service implements LocationListener {

	public static LocationManager locationManager;
	public static boolean isBackToApp = true;
	public static boolean isListening = false;
	public static boolean isNeedNotify = false;

	private NotificationManager _NotificationManager;
	Notification _notification;
	CharSequence contentTitle = "Navier HUD";
	CharSequence contentText = "If you don't back to the Navier HUD, it will automatically stop tracking after 3 minutes.";
	PendingIntent contentIntent;

	private CountDownTimer timer = new CountDownTimer(3000, 3000) {

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			if (!isBackToApp) {
				removeLocationUpdate();
				try {
					stopSelf();
				} catch (Exception e) {

				}
			}

		}

		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub

		}

	};
	private boolean gps_enabled = false;
	private boolean network_enabled = false;
	private Timer timer1;

	public static final String EXTRA_ONLOCATIONCHANGE = "EXTRA_ONLOCATIONCHANGE";
	public static final String EXTRA_ONPROVIDERDISABLEED = "EXTRA_ONPROVIDERDISABLEED";
	public static final String EXTRA_ONPROVIDERENABLED = "EXTRA_ONPROVIDERENABLED";
	public static final String EXTRA_ONSTATUSCHANGED = "EXTRA_ONSTATUSCHANGED";

	public static final String SERVICE_ACTION_STARTLISTEN = "startListening";
	public static final String SERVICE_ACTION_STOPLISTEN = "stopListening";
	public static final String SERVICE_ACTION_STOPLISTEN_WITHNOTIFY = "stopListening_with_notify";

	private void requestLocationUpdates() {
		try {

			// exceptions will be thrown if provider is not permitted.
			try {
				gps_enabled = locationManager
						.isProviderEnabled(LocationManager.GPS_PROVIDER);
			} catch (Exception ex) {
			}
			try {
				network_enabled = locationManager
						.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			} catch (Exception ex) {
			}

			if (isListening == false) {

				// don't start listeners if no provider is enabled
				if (!gps_enabled && !network_enabled)
					return;

				if (gps_enabled)
					locationManager.requestLocationUpdates(
							LocationManager.GPS_PROVIDER, 0, 0, this);
				if (network_enabled)
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER, 0, 0, this);
				log("start update");
				isListening = true;
			}
			timer1 = new Timer();
			timer1.schedule(new GetLastLocation(), 2000);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeLocationUpdate() {
		if (isListening) {
			locationManager.removeUpdates(this);

			log("Remove location update");
			isListening = false;
		}

	}

	public static void notifyBackToApp() {
		isBackToApp = true;
	}

	/*
	 * public void showStatusBarNotify(){
	 * 
	 * 
	 * int icon = R.drawable.ic_notify; CharSequence tickerText =
	 * "Navier HUD is running in backgroud"; long when =
	 * System.currentTimeMillis();
	 * 
	 * _notification = new Notification(icon, tickerText, when);
	 * 
	 * 
	 * //Panding Intent Context context = getApplicationContext(); Intent
	 * notificationIntent = new Intent(this, NavierPanel.class); contentIntent =
	 * PendingIntent.getActivity(this, 0, notificationIntent,
	 * Notification.FLAG_NO_CLEAR);
	 * 
	 * _notification.setLatestEventInfo(context, contentTitle, contentText,
	 * contentIntent); _NotificationManager.notify(1, _notification); }
	 */
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		log("Location service create");
		locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		// _NotificationManager = (NotificationManager)
		// getSystemService(Context.NOTIFICATION_SERVICE);

	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub

		if (intent == null
				|| intent.getAction().equals(SERVICE_ACTION_STARTLISTEN)) {
			isBackToApp = true;

			requestLocationUpdates();

		} else {

			if (intent.getAction().equals(SERVICE_ACTION_STOPLISTEN)) {

				isBackToApp = false;
				isNeedNotify = false;
				timer.cancel();
				timer.start();

			}
			/*
			 * if(intent.getAction().equals(SERVICE_ACTION_STOPLISTEN_WITHNOTIFY)
			 * ){
			 * 
			 * //showStatusBarNotify(); isBackToApp = false; isNeedNotify =
			 * true; timer.cancel(); timer.start();
			 * 
			 * }
			 */
		}

		return START_STICKY;
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		timer1.cancel();
		Intent it = new Intent(EXTRA_ONLOCATIONCHANGE);
		it.putExtra("location", location);
		LocalBroadcastManager.getInstance(LocationService.this).sendBroadcast(
				it);

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		Intent it = new Intent(EXTRA_ONPROVIDERDISABLEED);
		it.putExtra("provider", provider);
		LocalBroadcastManager.getInstance(LocationService.this).sendBroadcast(
				it);
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		Intent it = new Intent(EXTRA_ONPROVIDERENABLED);
		it.putExtra("provider", provider);
		LocalBroadcastManager.getInstance(LocationService.this).sendBroadcast(
				it);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		Intent it = new Intent(EXTRA_ONSTATUSCHANGED);
		it.putExtra("provider", provider);
		it.putExtra("status", status);
		it.putExtra("extras", extras);
		LocalBroadcastManager.getInstance(LocationService.this).sendBroadcast(
				it);
	}

	private void log(String msg) {
		Log.d("mine", msg);
	}

	class GetLastLocation extends TimerTask {
		@Override
		public void run() {
			Location net_loc = null, gps_loc = null;
			if (gps_enabled)
				gps_loc = locationManager
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if (network_enabled)
				net_loc = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

			// if there are both values use the latest one
			if (gps_loc != null && net_loc != null) {
				if (gps_loc.getTime() > net_loc.getTime()) {

					Intent it = new Intent(EXTRA_ONLOCATIONCHANGE);
					it.putExtra("location", gps_loc);
					LocalBroadcastManager.getInstance(LocationService.this)
							.sendBroadcast(it);
				} else {
					Intent it = new Intent(EXTRA_ONLOCATIONCHANGE);
					it.putExtra("location", net_loc);
					LocalBroadcastManager.getInstance(LocationService.this)
							.sendBroadcast(it);
				}
				return;
			}

			if (gps_loc != null) {
				Intent it = new Intent(EXTRA_ONLOCATIONCHANGE);
				it.putExtra("location", gps_loc);
				LocalBroadcastManager.getInstance(LocationService.this)
						.sendBroadcast(it);
				return;
			}
			if (net_loc != null) {
				Intent it = new Intent(EXTRA_ONLOCATIONCHANGE);
				it.putExtra("location", net_loc);
				LocalBroadcastManager.getInstance(LocationService.this)
						.sendBroadcast(it);
				return;
			}

		}
	}
}
