package idv.xunqun.navier;

import idv.xunqun.navier.content.DirectionStep;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.content.Place;
import idv.xunqun.navier.content.RoutePlan;
import idv.xunqun.navier.http.DirectionRequester;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Location;
import android.util.Log;

public class NavigationManager {

	private static final int SIX_SECOND=1000*6;
	private static final int ONE_SECOND=1000*1;
	public int PERDESTANCE_LIMMIT=60;  //meters
	public double ANGLE_LIMMIT=100;
	public int DESTANCE_SHOWALARM=150; //meters
	public int TIME_SHOWALARM = 30; //seconds
	public int ARRIVE_LIMMIT=60;
	public int CLOSE_RANGE=40;
	
	private Context _context;
	private PreferencesHandler _settings;
	private final int MAX_COUNT_TO_REPLAN=3;
	private int mCount2Replan=0;
	
	public boolean hasRouteInfo=false;
	public RoutePlan mRoutePlan;
	public Place mDestination;
	public static boolean isPlanning = false;
	public double mTotalDistance=0;
	
	public boolean[] mAlarmShowed_100;
	public boolean mAlarmShowed_10=false;
	public boolean mInToleratePhase = false;  // tolerate for leave route
	
	
	//Global values
	
	public String _nextTurnInstruction = null;
	public double _nextTurnAngle = -1;
	public Latlng _perpen_point;
	double _curRoadAngle = 0;
	
	public NavigationManager(Place destination, Context context){
		_context = context;
		mDestination = destination;
		_settings = new PreferencesHandler(_context);
		TIME_SHOWALARM = _settings.getPREF_TURNALARM_SEC();
	}
	
	
	/*
	 * if it need to access totalDistance frequently, use global value mTotalDistance
	 */
	public double totalDistance(){
		
		ArrayList<DirectionStep> arr_steps = mRoutePlan.arr_steps;
		double total=0;
		
		if(mRoutePlan!=null&&mRoutePlan.arr_steps.size()>0){
			for(int i=0;i<arr_steps.size();i++){
				DirectionStep step = arr_steps.get(i);
				for(int j=0;j<step.polyline_list.size()-1;j++){
					Latlng p1 = step.polyline_list.get(j);
					Latlng p2 = step.polyline_list.get(j+1);
					total+=this.distance(p1, p2);
				}
			}
		}
		return total;
	}
	
	
	public boolean directionRequest(Latlng curr){
		
		int avoid = 0;
		if(_settings.getPREF_AVOID_TOLLS())avoid+=DirectionRequester.AVOID_TOLLS;
		if(_settings.getPREF_AVOID_HIGHWAY())avoid+=DirectionRequester.AVOID_HIGHWAY;
		
		DirectionRequester requester = new DirectionRequester(curr,new Latlng(mDestination.latitude,mDestination.longitude),_settings.getPREF_TRANS_TYPE(),avoid, _settings.getPREF_SPEED_UNIT() == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH?"metric":"imperial");
		try {
			String result = requester.sentHttpRequest();
			return initRoutePlan(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("json", e.toString());			
			return false;
		}
		
		
	}
	
	public boolean initRoutePlan(String r) throws JSONException{
		if(r!=null){
			//check route feasible
			
			//retrieving seccussful
			JSONObject json = DirectionRequester.string2Object(r);
			//handle over query limit
			String status = json.getString("status");
			if(!status.equalsIgnoreCase("OVER_QUERY_LIMIT")){
				mRoutePlan = new RoutePlan();
				mRoutePlan.setJson(json);
				initAlarmSetting();
				
				hasRouteInfo=true;
				isPlanning= false;
				mTotalDistance =totalDistance();
				return true;
			}else{
				Log.d("mine", "OVER_QUERY_LIMIT");
				return false;
			}
		}else{
			//internet access fail
			
			return false;
		}
		
	}
	
	
	/*
	 * if angle has to be accessed frequently, dont call this method,
	 * use the global value _nextTurnAngle
	 */
	public double getNextTurnAngle(){
		try{
			int curStep = this.mRoutePlan._current_step;
			if(curStep+1<mRoutePlan.arr_steps.size()){
				DirectionStep arr_curStep = mRoutePlan.arr_steps.get(curStep);
				DirectionStep arr_nextStep = mRoutePlan.arr_steps.get(curStep+1);
				Latlng conner = arr_curStep.polyline_list.get(arr_curStep.polyline_list.size()-1);
				Latlng from =  arr_curStep.polyline_list.get(arr_curStep.polyline_list.size()-2);
				Latlng to = arr_nextStep.polyline_list.get(1);
				double xa = from.getLng() - conner.getLng();
				double ya = from.getLat() - conner.getLat();
				double xb = to.getLng() - conner.getLng();
				double yb = to.getLat()-conner.getLat();
				Log.d("mine", "turnAngle: from("+from.getLat()+","+from.getLng()+") conner("+conner.getLat()+","+conner.getLng()+") to("+to.getLat()+","+to.getLng()+")");
				_nextTurnAngle = this.trunAngle(xa, ya, xb, yb);
				return _nextTurnAngle;
			}else{
				return -1;
			}
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
		
	}
	
	public boolean isArrived(Latlng curr){
		
		return distance(curr,new Latlng(mDestination.latitude,mDestination.longitude))<=ARRIVE_LIMMIT?true:false;
		
	}
	
	public double getAlarmDistance(Latlng curr, float speed){
		
		float meter = speed*TIME_SHOWALARM;
		if(meter<80) meter = 80;
		

		return  meter;
	}
	
	private void initAlarmSetting(){
		mAlarmShowed_100 = new boolean[mRoutePlan.arr_steps.size()];
		for(boolean b:mAlarmShowed_100){
			b = false;
		}
	}
	
	public boolean isNeedRoutePlan(Latlng curr){
		if(!hasRouteInfo){
			mCount2Replan=0;
			return true;
		}
		
		if(isLeaveTheStep(curr)){
			
			int currstep = checkStep(curr);
			Log.d("mine", "currstep:"+currstep);
			if(currstep == -1){
				
				mCount2Replan+=1;
				if(mCount2Replan>this.MAX_COUNT_TO_REPLAN){
					this.mInToleratePhase = false;
					mCount2Replan=0;
					return true;
				}else{
					this.mInToleratePhase = true;
					return false;
				}
			}else{
				this.mRoutePlan._current_step=currstep;
				mCount2Replan=0;
				this.mInToleratePhase = false;
				return false;
			}
			
		}else{
			//return false;
			mCount2Replan=0;
			this.mInToleratePhase = false;
			return false;
		}
	}
	
	
	
	
	public int checkStep(Latlng curr){
		Log.d("mine", "-----checkStep() ----- ");
		double shortest_dest=PERDESTANCE_LIMMIT;
		int shortest_step=-1;
		int curr_rpoint=0;
		boolean fined = false;
		
		for(int i = 0;i<mRoutePlan.arr_steps.size();i++){
			DirectionStep tmpStep = mRoutePlan.arr_steps.get(i);
			List<Latlng> ployLine_list = tmpStep.polyline_list;
			
			for(int j=0;j<ployLine_list.size()-1;j++){
				Latlng p1 = ployLine_list.get(j);
				Latlng p2 = ployLine_list.get(j+1);
				
				Latlng perpen_point = getPerpenPoint(p1,p2,curr);
				double distance_p1 = distance(curr,p1);
				double perpenDistance = distance(curr,perpen_point);
				
				if((checkAngle(p1,p2,curr) && perpenDistance<shortest_dest) || distance_p1< PERDESTANCE_LIMMIT){
					//in the route
					shortest_dest = perpenDistance;
					shortest_step=i;
					curr_rpoint = j;
					_perpen_point = perpen_point;
					fined = true;
				}
			}
			if(fined==true){
				break;
			}
		}
		
		
		this.mRoutePlan._current_rpoint=curr_rpoint;
		
		
		return shortest_step;
	}
	
	public double getRoadAngle(){
		
		Latlng p1 = mRoutePlan.arr_steps.get(mRoutePlan._current_step).polyline_list.get(mRoutePlan._current_rpoint);
		Latlng p2 = mRoutePlan.arr_steps.get(mRoutePlan._current_step).polyline_list.get(mRoutePlan._current_rpoint+1);
		
		double clear_x = p2.getLng() - p1.getLng();
		double clear_y = p2.getLat() - p1.getLat();
		
		return roadAngle(clear_x,clear_y);
	}
	
	public double roadAngle(double x, double y){
		
		double tanA = y/x;
		if(quadrant(x, y)==1){
			return 90 - Math.abs(Math.toDegrees(Math.atan(tanA)));
		}else if(quadrant(x, y)==2){
			return 270 + Math.abs(Math.toDegrees(Math.atan(tanA)));
		}else if(quadrant(x, y)==3){
			return 270 - Math.abs(Math.toDegrees(Math.atan(tanA)));
		}else{
			return 90 + Math.abs(Math.toDegrees(Math.atan(tanA)));
		}
			
		
		
		
		
	}
	
	public boolean isLeaveTheStep(Latlng curr){
		Log.d("mine", "-----isLeaveTheStep() ----- ");
		DirectionStep curr_step = this.mRoutePlan.arr_steps.get(this.mRoutePlan._current_step);
		List<Latlng> ployLine_list = curr_step.polyline_list;
		
		//check is still curr step
		boolean inThisStep = false;
		int curr_rpoint=0;
		
		for(int i=0;i<ployLine_list.size()-1;i++){
			Latlng p1 = ployLine_list.get(i);
			Latlng p2 = ployLine_list.get(i+1);
			
			
			//取得垂點
			Latlng perpen_point = getPerpenPoint(p1,p2,curr);
			
			double perpenDistance = distance(curr,perpen_point);
			//Log.d("mine", "perpen distance = " + perpenDistance);
			if((checkAngle(p1,p2,curr) && perpenDistance<this.PERDESTANCE_LIMMIT) || distance(curr,p1)<PERDESTANCE_LIMMIT){
				//in the route
				inThisStep = true;
				this.mRoutePlan._current_rpoint=i;		
				_perpen_point = perpen_point;
				return false;
			}
	
		}
		return true;
	}
	
	public double distanceOfLeftRpoint(Latlng curr){
		int curr_rpoint = mRoutePlan._current_rpoint;
		DirectionStep curr_step = this.mRoutePlan.arr_steps.get(this.mRoutePlan._current_step);
		List<Latlng> list = curr_step.polyline_list;
		
		if(curr_rpoint == list.size()-1){
			return distance(curr,(Latlng)list.get(curr_rpoint+1));
		}else{
			Latlng next = (Latlng)list.get(curr_rpoint+1);
			double sum=distance(curr,next);
			for(int i=curr_rpoint+1;i<list.size()-1;i++){
				sum+= distance((Latlng)list.get(i),(Latlng)list.get(i+1));
			}
			//Log.d("mine", "distace of left rpoint " + curr_rpoint + " : "+sum );
			return sum;
		}
		
		
	}
	
	public boolean checkAngle(Latlng p1, Latlng p2, Latlng curr){
		int quadrant; //����
		double angle1, angle2;
		
		double x1 = p1.getLng();
		double y1 = p1.getLat();
		
		double x2 = p2.getLng();
		double y2 = p2.getLat();
		
		double xcurr = curr.getLng();
		double ycurr = curr.getLat();
		
		// angle 1
		double clear_x2= x2-x1;
		double clear_y2= y2-y1;
		
		double clear_x1= x1-x2;
		double clear_y1 =y1-y2;
		
		double clear_xcurr=xcurr-x1;
		double clear_ycurr=ycurr-y1;
		
		angle1 = deltaAngle(clear_x2,clear_y2,clear_xcurr,clear_ycurr);
		
		//angle2 = deltaAngle(clear_xcurr,clear_ycurr,clear_x2,clear_y2);
		
		clear_xcurr=xcurr-x2;
		clear_ycurr=ycurr-y2;
		
		angle2 = deltaAngle(clear_x1,clear_y1,clear_xcurr,clear_ycurr);
		
		if(angle1>this.ANGLE_LIMMIT || angle2>this.ANGLE_LIMMIT){	
			
			return false;
		}else{
			// between two rpoint
			
			return true;
		}
	}
	
	public double deltaAngle(double xa, double ya, double xb, double yb){
		
		double tanA = ya/xa;
		double tanB = yb/xb;		
		
		if(quadrant(xa,ya)==1 && quadrant(xb,yb)==1){
			return Math.abs(Math.toDegrees(Math.atan(tanA))-Math.toDegrees(Math.atan(tanB)));
		}else if(quadrant(xa,ya)==1 && quadrant(xb,yb)==2){
			return 180-Math.abs(Math.toDegrees(Math.atan(tanA)))-Math.abs(Math.toDegrees(Math.atan(tanB)));
		}else if(quadrant(xa,ya)==1 && quadrant(xb,yb)==3){
			double angle = 180+Math.abs(Math.toDegrees(Math.atan(tanA)))-Math.abs(Math.toDegrees(Math.atan(tanB)));
			return angle>180?360-angle:angle;
		}else if(quadrant(xa,ya)==1 && quadrant(xb,yb)==4){
			return Math.abs(Math.toDegrees(Math.atan(tanA)))+Math.abs(Math.toDegrees(Math.atan(tanB)));
		}else if(quadrant(xa,ya)==2 && quadrant(xb,yb)==1){
			return 180-Math.abs(Math.toDegrees(Math.atan(tanA)))-Math.abs(Math.toDegrees(Math.atan(tanB)));
		}else if(quadrant(xa,ya)==2 && quadrant(xb,yb)==2){
			return Math.abs(Math.toDegrees(Math.atan(tanA))-Math.toDegrees(Math.atan(tanB)));
		}else if(quadrant(xa,ya)==2 && quadrant(xb,yb)==3){
			return Math.abs(Math.toDegrees(Math.atan(tanA)))+Math.abs(Math.toDegrees(Math.atan(tanB)));
		}else if(quadrant(xa,ya)==2 && quadrant(xb,yb)==4){
			double angle = 180-Math.abs(Math.toDegrees(Math.atan(tanA)))+Math.abs(Math.toDegrees(Math.atan(tanB)));
			return angle>180?360-angle:angle;
		}else if(quadrant(xa,ya)==3 && quadrant(xb,yb)==1){
			double angle = 180-Math.abs(Math.toDegrees(Math.atan(tanA)))+Math.abs(Math.toDegrees(Math.atan(tanB)));
			return angle>180?360-angle:angle;
		}else if(quadrant(xa,ya)==3 && quadrant(xb,yb)==2){
			return Math.abs(Math.toDegrees(Math.atan(tanA)))+Math.abs(Math.toDegrees(Math.atan(tanB)));
		}else if(quadrant(xa,ya)==3 && quadrant(xb,yb)==3){
			return Math.abs(Math.toDegrees(Math.atan(tanA))-Math.toDegrees(Math.atan(tanB)));
		}else if(quadrant(xa,ya)==3 && quadrant(xb,yb)==4){
			return 180-Math.abs(Math.toDegrees(Math.atan(tanA)))-Math.abs(Math.toDegrees(Math.atan(tanB)));
		}else if(quadrant(xa,ya)==4 && quadrant(xb,yb)==1){
			return Math.abs(Math.toDegrees(Math.atan(tanA)))+Math.abs(Math.toDegrees(Math.atan(tanB)));
		}else if(quadrant(xa,ya)==4 && quadrant(xb,yb)==2){
			double angle = 180+Math.abs(Math.toDegrees(Math.atan(tanA)))-Math.abs(Math.toDegrees(Math.atan(tanB)));
			return angle>180?360-angle:angle;
		}else if(quadrant(xa,ya)==4 && quadrant(xb,yb)==3){
			return 180-Math.abs(Math.toDegrees(Math.atan(tanA)))-Math.abs(Math.toDegrees(Math.atan(tanB)));
		}else if(quadrant(xa,ya)==4 && quadrant(xb,yb)==4){
			return Math.abs(Math.toDegrees(Math.atan(tanA))-Math.toDegrees(Math.atan(tanB)));
		}
		
		
		
		return 0;
	}
	
	public double trunAngle(double xfrom, double yfrom, double xto, double yto){
		double angleFrom;
		double angleTo;

		
		double tanfrom = yfrom/xfrom;
		double tanto = yto/xto;		
		angleFrom = Math.abs(Math.toDegrees(Math.atan(tanfrom)));
		angleTo = Math.abs(Math.toDegrees(Math.atan(tanto)));
		
		if(quadrant(xfrom,yfrom)==1 && quadrant(xto,yto)==1){
			return angleFrom>angleTo?180+(angleFrom-angleTo):180-(angleTo-angleFrom);
			
		}else if(quadrant(xfrom,yfrom)==1 && quadrant(xto,yto)==2){
			return angleFrom+angleTo;
			
		}else if(quadrant(xfrom,yfrom)==1 && quadrant(xto,yto)==3){
			return angleFrom>angleTo?angleFrom-angleTo:360-(angleTo-angleFrom);
			
		}else if(quadrant(xfrom,yfrom)==1 && quadrant(xto,yto)==4){
			return 180+angleFrom+angleTo;
			
		}else if(quadrant(xfrom,yfrom)==2 && quadrant(xto,yto)==1){
			return 360-(angleFrom+angleTo);
			
		}else if(quadrant(xfrom,yfrom)==2 && quadrant(xto,yto)==2){			
			return angleFrom>angleTo?180-(angleFrom-angleTo):180+(angleTo-angleFrom);
			
		}else if(quadrant(xfrom,yfrom)==2 && quadrant(xto,yto)==3){
			return 180-(angleFrom+angleTo);
			
		}else if(quadrant(xfrom,yfrom)==2 && quadrant(xto,yto)==4){
			//return angleFrom>angleTo?180-(angleFrom-angleTo):(angleTo-angleFrom);
			return angleFrom>angleTo?360-(angleFrom-angleTo):angleTo-angleFrom;
			
		}else if(quadrant(xfrom,yfrom)==3 && quadrant(xto,yto)==1){
			return angleFrom>angleTo?angleFrom-angleTo:360-(angleTo-angleFrom);
			
		}else if(quadrant(xfrom,yfrom)==3 && quadrant(xto,yto)==2){
			return 180+angleFrom+angleTo;
			
		}else if(quadrant(xfrom,yfrom)==3 && quadrant(xto,yto)==3){
			return angleFrom>angleTo?180+(angleFrom-angleTo):180-(angleTo-angleFrom);
			
		}else if(quadrant(xfrom,yfrom)==3 && quadrant(xto,yto)==4){
			return angleFrom+angleTo;
			
		}else if(quadrant(xfrom,yfrom)==4 && quadrant(xto,yto)==1){
			return 180-(angleFrom+angleTo);
			
		}else if(quadrant(xfrom,yfrom)==4 && quadrant(xto,yto)==2){
			return angleFrom>angleTo?360-(angleFrom-angleTo): angleTo-angleFrom;
			
		}else if(quadrant(xfrom,yfrom)==4 && quadrant(xto,yto)==3){
			return 360-(angleTo+angleFrom);
			
		}else if(quadrant(xfrom,yfrom)==4 && quadrant(xto,yto)==4){
			return angleFrom>angleTo?180-(angleFrom-angleTo):180+(angleTo-angleFrom);
		}
		
		
		
		return 0;
	}
	
	public int quadrant(double x, double y){
		if(x>0 && y>=0){
			return 1;
		}else if(x<=0 && y>0){
			return 2;
		}else if(x<0 && y<=0){
			return 3;
		}else if(x>=0 && y<0){
			return 4;
		}else{
			return 0;
		}
	}
	
	public double distance(Latlng a,Latlng b){
		
		float[] results=new float[2];
		Location.distanceBetween(a.getLat(), a.getLng(), b.getLat(), b.getLng(), results);
		
		return results[0];
	}
	
	public Latlng getPerpenPoint(Latlng p1,Latlng p2, Latlng curr){
		double x1 = p1.getLng();		
		double y1 = p1.getLat();

		double x2 = p2.getLng();
		double y2 = p2.getLat();
		
		double a = curr.getLng();
		double b = curr.getLat();
		
		double xresult = (a*Math.pow((x1-x2),2)+x1*Math.pow((y1-y2),2)-(x1-x2)*(y1-y2)*(y1-b))/(Math.pow((x1-x2),2)+Math.pow((y1-y2),2));
		double yresult = (y1*Math.pow((x1-x2),2)+(b*Math.pow((y1-y2), 2))+((x1-x2)*(y1-y2)*(a-x1)))/(Math.pow((x1-x2),2)+Math.pow((y1-y2),2));
		
		
		
		return new Latlng((yresult),(xresult));
	}
	
	public static boolean isBetterLocation(Location location, Location currentBestLocation) {
		
	    if (currentBestLocation == null) {
	        // A new location is always better than no location
	        return true;
	    }

	    // Check whether the new location fix is newer or older
	    long timeDelta = location.getTime() - currentBestLocation.getTime();
	    boolean isSignificantlyNewer = timeDelta > SIX_SECOND;
	    boolean isSignificantlyOlder = timeDelta < 0;
	    boolean isNewer = timeDelta > 0;

	    // If it's been more than two minutes since the current location, use the new location
	    // because the user has likely moved
	    if (isSignificantlyNewer) {
	        return true;
	    // If the new location is more than two minutes older, it must be worse
	    } else if (isSignificantlyOlder) {
	        return false;
	    }

	    // Check whether the new location fix is more or less accurate
	    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
	    boolean isLessAccurate = accuracyDelta > 0;
	    boolean isMoreAccurate = accuracyDelta < 0;
	    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

	    // Check if the old and new location are from the same provider
	    boolean isFromSameProvider = isSameProvider(location.getProvider(),
	            currentBestLocation.getProvider());

	    // Determine location quality using a combination of timeliness and accuracy
	    if (isMoreAccurate) {
	        return true;
	    } else if (isNewer && !isLessAccurate) {
	        return true;
	    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
	        return true;
	    }
	    return false;
	}
	
	private static boolean isSameProvider(String provider1, String provider2) {
	    if (provider1 == null) {
	      return provider2 == null;
	    }
	    return provider1.equals(provider2);
	}
	
	public String getTrunIntruction(){
		int step = mRoutePlan._current_step;
		//if(step>=0 && step<mRoutePlan.arr_steps.size()){
		try{
			step+=1;
			String s = mRoutePlan.arr_steps.get(step).html_instructions;
			if(step+1<mRoutePlan.arr_steps.size()){
				_nextTurnInstruction =mRoutePlan.arr_steps.get(step+1).html_instructions;
			}else{
				_nextTurnInstruction = null;
			}
			return s;
		}catch(Exception e){
			
		}
		_nextTurnInstruction = null;
		return null;
		//}
		//return "";
	}

	public String getInitTrunIntruction(){
		String s = mRoutePlan.arr_steps.get(0).html_instructions;
		return s;
		
		
	}
}
