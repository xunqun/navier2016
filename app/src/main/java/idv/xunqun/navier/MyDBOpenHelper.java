package idv.xunqun.navier;

import idv.xunqun.navier.v2.content.Panel;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDBOpenHelper extends SQLiteOpenHelper {

	// myplaces Table
	
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "navier.db";
    public static final String TABLE_NAME = "myplaces";
    public static final String COL_ID="_ID";
    public static final String COL_PLACE_NAME = "place_name";
    public static final String COL_ADDRESS = "address";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_LATITUDE="latitude";
    public static final String COL_LONGITUDE = "longitude";
    public static final String COL_FAVERITE="isfavorite";
    public static final String COL_TIMESTAMP = "timestamp";
    public static final String COL_FINGERPRINT = "fingerprint";  //user id as fingerprint may causes one record in two device have different id (fingerprint is the initial timestamp+lat+lng)
    
    
	//sync place Table
	
    public static final String SYNC_TABLE_NAME = "syncstate";
    public static final String SYNC_COL_ID="_ID";
    public static final String SYNC_COL_PLACEID = "place_id";
    public static final String SYNC_COL_SYNCSTATE = "sync_state";
    public static final String SYNC_COL_FINGERPRINT = "fingerprint";
    
    //layout table
    
    public static final String LAYOUT_TABLE_NAME = "layout";
    public static final String LAYOUT_COL_ID = "_ID";
    public static final String LAYOUT_COL_LAYOUT = "layout";
    public static final String LAYOUT_COL_TIMESTAMP = "timestamp"; 
    public static final String LAYOUT_COL_FINGERPRINT = "fingerprint";
    
    //sync layout table
    
    public static final String SYNC_LAYOUT_TABLE_NAME = "synclayout";
    public static final String SYNC_LAYOUT_COL_ID = "_ID";
    public static final String SYNC_LAYOUT_LAYOUTID = "layout_id";
    public static final String SYNC_LAYOUT_SYNCSTATE = "sync_state";
    public static final String SYNC_LAYOUT_FINGERPRINT = "fingerprint";
    
    //sync values
    
    public static final String STATE_NEW = "new";
    public static final String STATE_MODIFY = "modified";
    public static final String STATE_KILLED = "killed";
    
    //sync osm nodes
    public static final String OSMNODES_TABLE_NAME = "osmnode";
    public static final String OSMNODES_TABLE_COL_ID = "_ID";
    public static final String OSMNODES_TABLE_COL_LATITUDE = "latitude";
    public static final String OSMNODES_TABLE_COL_LONGITUDE = "longitude";
    public static final String OSMNODES_TABLE_COL_VISIBLE = "visible";
    public static final String OSMNODES_TABLE_COL_REFERENCE = "reference";
    
    
    
	private static SQLiteDatabase db;
	private static MyDBOpenHelper helper;
    
    private static final String DROP_LAYOUT_TABLE = 
    	    "DROP TABLE IF EXISTS " + LAYOUT_TABLE_NAME;
    

    
    private static final String TABLE_CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                COL_ID + " INTEGER PRIMARY KEY autoincrement," +
                COL_PLACE_NAME + " TEXT NOT NULL, " +
                COL_ADDRESS + " TEXT, "+
                COL_DESCRIPTION + " TEXT, " +
                COL_LATITUDE + " REAL, " +
                COL_LONGITUDE + " REAL, " +
                COL_FAVERITE + " NUMERIC, " +
                COL_TIMESTAMP + " NUMERIC, "+
                COL_FINGERPRINT + " TEXT NOT NULL"+
                ");";
    
    private static final String SYNC_TABLE_CREATE =
            "CREATE TABLE " + SYNC_TABLE_NAME + " (" +
            SYNC_COL_ID + " INTEGER PRIMARY KEY autoincrement," +
            SYNC_COL_PLACEID + " INTEGER NOT NULL," +
            SYNC_COL_FINGERPRINT + " TEXT NOT NULL," +
            SYNC_COL_SYNCSTATE + " TEXT NOT NULL " +
            ");";
    
    private static final String LAYOUT_TABLE_CREATE = 
    		"CREATE TABLE " + LAYOUT_TABLE_NAME + " (" +
    		LAYOUT_COL_ID + " INTEGER PRIMARY KEY autoincrement," +
    		LAYOUT_COL_LAYOUT + " TEXT NOT NULL," +
    		LAYOUT_COL_FINGERPRINT + " TEXT NOT NULL," +
    		LAYOUT_COL_TIMESTAMP + " NUMERIC" +
            ");";
    
    private static final String SYNC_LAYOUT_TABLE_CREATE = 
    		"CREATE TABLE " + SYNC_LAYOUT_TABLE_NAME + " (" +
    		SYNC_LAYOUT_COL_ID + " INTEGER PRIMARY KEY autoincrement," +
    		SYNC_LAYOUT_LAYOUTID + " INTEGER NOT NULL," +
            SYNC_LAYOUT_SYNCSTATE + " TEXT NOT NULL, " +
            SYNC_LAYOUT_FINGERPRINT + "  TEXT NOT NULL" +
            ");";
    
    private static final String OSMNODES_TABLE_CREATE = 
    		"CREATE TABLE " + OSMNODES_TABLE_NAME + " (" +
    				OSMNODES_TABLE_COL_ID + " INTEGER PRIMARY KEY autoincrement," +
    				OSMNODES_TABLE_COL_LATITUDE + " REAL, " +
    				OSMNODES_TABLE_COL_LONGITUDE + " REAL, " +
    				OSMNODES_TABLE_COL_REFERENCE + " NUMERIC, " +
    				OSMNODES_TABLE_COL_VISIBLE + " NUMERIC "+
    		");";
    
    
    		

	public MyDBOpenHelper(Context context, CursorFactory factory) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(TABLE_CREATE);
		db.execSQL(SYNC_TABLE_CREATE);
		db.execSQL(LAYOUT_TABLE_CREATE);
		db.execSQL(SYNC_LAYOUT_TABLE_CREATE);
		db.execSQL(OSMNODES_TABLE_CREATE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}

	public static void clearDB(Context c) throws NullPointerException{
		if(!db.isOpen()){
			initDB(c);
		}
		
		db.delete(MyDBOpenHelper.TABLE_NAME, "", new String[]{});
		db.close();
	}
	
	private static void initDB(Context c){
		try{
			helper = new MyDBOpenHelper(c, null);
			db = helper.getWritableDatabase();
		}catch(SQLException e){
			e.printStackTrace();			
		}
		
	}
	
	public static SQLiteDatabase getDb(Context c){
		if(db == null || !db.isOpen()){
			initDB(c);
		}
		
		return db;
	}
	
	public static void closeDb(){
		if(db != null && db.isOpen()){
			db.close();
			db = null;
		}else if(db != null){
			db = null;
		}
	}
}
