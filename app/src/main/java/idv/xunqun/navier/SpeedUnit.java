package idv.xunqun.navier;

public class SpeedUnit {

	
	public static float kmh2mph(float kmh){
		
		return (float) (kmh*0.621);
	}
	
	public static float mph2kmh(float mph){
		
		return (float)(mph*1.609);
	}
	
	public static float km2mile(double km){
		return  (float)(km*0.62137119);
	}
}
