package idv.xunqun.navier.utils;

import idv.xunqun.navier.LocationService;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.content.Layout;
import idv.xunqun.navier.parts.Elem_Compass;
import idv.xunqun.navier.parts.Elem_Route;
import idv.xunqun.navier.parts.Elem_Speed;
import idv.xunqun.navier.parts.Elem_SpeedDigital;
import idv.xunqun.navier.parts.Parts;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.location.Location;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;


public class Utility {

	private Context _context;

	public Utility(Context context) {
		_context = context;
	}

	public String getStringFromRaw(int id) {

		InputStream is = _context.getResources().openRawResource(id);
		ByteArrayOutputStream os = new ByteArrayOutputStream();

		int tmp;
		try {

			tmp = is.read();
			while (tmp != -1) {
				os.write(tmp);
				tmp = is.read();
			}
			is.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return os.toString();

	}

	public static String getLayoutName(String jsonString) throws JSONException {

		JSONObject json = new JSONObject(jsonString);
		return (String) ((JSONObject) json.get(Layout.JSON_ROOT)).get(Layout.JSON_NAME);
	}

	public static int getLayoutType(String jsonString) throws JSONException {

		JSONObject json = new JSONObject(jsonString);
		return (int) (((JSONObject) json.get(Layout.JSON_ROOT)).optInt(Layout.JSON_TYPE));
	}

	public String typeToString(int t) {
		switch (t) {
			case Layout.TYPE_DASHBOARD:
				return "Normal Panel";

			case Layout.TYPE_NAVIGATION:
				return "Navigation Panel";
		}
		return "Normal Panel";
	}

	public String getPartName(int id) {
		switch (id) {

			case Parts.ELEM_COMPASS:
				return Elem_Compass.ELEM_NAME;

			case Parts.ELEM_ROUTE:
				return Elem_Route.ELEM_NAME;

			case Parts.ELEM_SPEED:
				return Elem_Speed.ELEM_NAME;

			case Parts.ELEM_SPEED_DIGITAL:
				return Elem_SpeedDigital.ELEM_NAME;

		}

		return null;
	}

	public int[] getPartDimension(int id) {
		switch (id) {

			case Parts.ELEM_COMPASS:
				return new int[] { Elem_Compass.ELEM_WIDTH, Elem_Compass.ELEM_HEIGHT };

			case Parts.ELEM_ROUTE:
				return new int[] { Elem_Route.ELEM_WIDTH, Elem_Route.ELEM_HEIGHT };

			case Parts.ELEM_SPEED:
				return new int[] { Elem_Speed.ELEM_WIDTH, Elem_Speed.ELEM_HEIGHT };

			case Parts.ELEM_SPEED_DIGITAL:
				return new int[] { Elem_SpeedDigital.ELEM_WIDTH, Elem_SpeedDigital.ELEM_HEIGHT };

		}

		return null;
	}

	public int getPartThumbnail(int id) {
		switch (id) {

			case Parts.ELEM_COMPASS:
				return Elem_Compass.ELEM_THUMBNAIL;

			case Parts.ELEM_ROUTE:
				return Elem_Route.ELEM_THUMBNAIL;

			case Parts.ELEM_SPEED:
				return Elem_Speed.ELEM_THUMBNAIL;

			case Parts.ELEM_SPEED_DIGITAL:
				return Elem_SpeedDigital.ELEM_THUMBNAIL;

		}

		return 0;
	}

	public static float caculateSpeed(Location currLocation, Location preLocation) {

		float deltaTime = (currLocation.getTime() - preLocation.getTime()) / 1000; // milliSec/1000
		if (deltaTime > 0) {
			float[] results = new float[3];
			Location.distanceBetween(currLocation.getLatitude(), currLocation.getLongitude(), preLocation.getLatitude(), preLocation.getLongitude(), results);

			return Math.abs(results[0] / deltaTime);
		} else {
			return 0;
		}
	}

	public static float caculateAcceration(float speed_a, long time_a, float speed_b, long time_b) {

		float deltaTime = (time_a - time_b) / 1000; // second
		float deltaDistance = (speed_a - speed_b) * 1000; // meter

		return deltaDistance / deltaTime;
	}

	public static void checkLanscapeOrientation(Activity context) {
		Display display = ((WindowManager) context.getSystemService(context.WINDOW_SERVICE)).getDefaultDisplay();
		int orientation = display.getRotation();

		try {
			if (orientation == Surface.ROTATION_270)
			{
				context.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
			} else {
				context.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			}
		} catch (Exception e) {

		}
	}

	public static float meters2kmh(float meterPerSec) {
		return meterPerSec * 3600 / 1000;
	}

	public static void requestLocationServiceUpdate(Context context) {
		LocationService.notifyBackToApp();

		Intent it = new Intent(context, LocationService.class);
		it.setAction(LocationService.SERVICE_ACTION_STARTLISTEN);
		context.startService(it);
	}

	public static void requestLocationServiceStop(Context context) {
		Intent it = new Intent(context, LocationService.class);
		it.setAction(LocationService.SERVICE_ACTION_STOPLISTEN);
		context.startService(it);
	}

	public static void requestLocationServiceStopWithNotify(Context context) {
		Intent it = new Intent(context, LocationService.class);
		it.setAction(LocationService.SERVICE_ACTION_STOPLISTEN_WITHNOTIFY);
		context.startService(it);
	}

	public static Typeface getDefaultFont(Context context) {
		Typeface font;
		try {
			font = Typeface.createFromAsset(context.getAssets(),
					"fonts/mplus-1m-light.ttf");
		} catch (Exception e) {
			font = Typeface.DEFAULT;
			e.printStackTrace();
		}
		return font;
	}

	public static Typeface getDefaultFontBold(Context context) {
		Typeface font;
		try {
			font = Typeface.createFromAsset(context.getAssets(),
					"fonts/mplus.ttf");
		} catch (Exception e) {
			font = Typeface.DEFAULT;
			e.printStackTrace();
		}
		return font;
	}

	public static String getStringFromRaw(Context context, int id) {

		InputStream is = context.getResources().openRawResource(id);
		ByteArrayOutputStream os = new ByteArrayOutputStream();

		int tmp;
		try {

			tmp = is.read();
			while (tmp != -1) {
				os.write(tmp);
				tmp = is.read();
			}
			is.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return os.toString();

	}

	/**
	 * Decoding polyline in Google direction API
	 *
	 * @param encoded
	 * @return
	 */
	public static ArrayList<Latlng> decodePoly(String encoded) {

		ArrayList<Latlng> poly = new ArrayList<Latlng>();
		int index = 0, len = encoded.length();
		int lat = 0, lng = 0;

		while (index < len) {
			int b, shift = 0, result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;

			shift = 0;
			result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;

			double dbl_lat = (double) (lat / 1E5);
			double dbl_lng = (double) (lng / 1E5);
			Latlng p = new Latlng(dbl_lat, dbl_lng);
			poly.add(p);
		}

		return poly;
	}
}
