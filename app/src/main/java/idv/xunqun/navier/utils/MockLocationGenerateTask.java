package idv.xunqun.navier.utils;

import idv.xunqun.navier.R;
import idv.xunqun.navier.utils.KmlParser.Placemark;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.util.Log;

public class MockLocationGenerateTask extends
		AsyncTask<Void, Float, Void> {

	public static boolean _running = false;
	public final int INTERVAL_SEC = 1500;
	private MockLocationProvider mock;
	private Context _context;
	
	public MockLocationGenerateTask(Context context){
		_context = context;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		_running = true;
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
	}

	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
	    mock = new MockLocationProvider(
		        LocationManager.GPS_PROVIDER, _context);
	    
		//Set test location
	    
	    
	    InputStream is = _context.getResources().openRawResource(R.raw.testgps1);
	    KmlParser parser = new KmlParser();
	    
	    try {
			ArrayList<Placemark> list = parser.parse(is);
			for(Placemark point : list){
				Log.d("debug", "mock gps:" + point.lat + " , " + point.lng);
				synchronized(mock){
					mock.pushLocation(point.lat, point.lng);
					mock.wait(INTERVAL_SEC);
				}
				
			}
			mock.shutdown();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	    
		return null;
	}

}
