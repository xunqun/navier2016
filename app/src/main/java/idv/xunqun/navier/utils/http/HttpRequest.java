package idv.xunqun.navier.utils.http;

import android.net.Uri;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class HttpRequest {

	public static String sendHttpGet(Uri uri, HttpClient client) {
		BufferedReader in = null;
		String result = "";
		try {
			
			HttpGet request = new HttpGet(uri.toString());

			HttpResponse response = client.execute(request);
			
			if (response.getStatusLine().getStatusCode() == 200) {
				result = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
				Log.d("HTTP", result);
			}
			

			return result;
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;

	}

	public static String sentHttpPost(String url, ArrayList paramList, HttpClient client) throws Exception {
		String strParam;
		String result = null;

		
		try {

			
			HttpPost request = new HttpPost(url);

			request.setEntity(new UrlEncodedFormEntity(paramList));

			HttpResponse response = client.execute(request);
			Log.d("HTTP", response.getStatusLine().getStatusCode() + "");

			if (response.getStatusLine().getStatusCode() == 200) {
				result = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);

				Log.d("HTTP", result);
			}

			// clearSyncTable();

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("mine", e.toString());
			e.printStackTrace();

		} finally {

		}
		return result;
	}
	
	public static void downloadFile(String fileUrl, File destDir, String fileName) throws Exception {
	   
	        URL url = new URL(fileUrl);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setRequestMethod("GET");
	        connection.setDoOutput(true);
	        connection.connect();

	        if (destDir.isDirectory() && !destDir.exists()) {
	            destDir.mkdirs();
	        }

	        FileOutputStream output = new FileOutputStream(new File(destDir.toString() + "/" + fileName));

	        InputStream input = connection.getInputStream();

	        byte[] buffer = new byte[1024];
	        int byteCount = 0;

	        while ((byteCount = input.read(buffer)) != -1) {
	             output.write(buffer, 0, byteCount);
	        }

	        output.close();
	        input.close();

	}
}
