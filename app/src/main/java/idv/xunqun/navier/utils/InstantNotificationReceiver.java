package idv.xunqun.navier.utils;

import idv.xunqun.navier.v2.RunTimeActivity;
import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.annotation.TargetApi;
import android.app.Notification;
import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class InstantNotificationReceiver extends NotificationListenerService {

	private boolean isInit = false;
	private InstantNotificationReceiverListener mListener;

	public interface InstantNotificationReceiverListener {
		public void onNotificationGot(Notification noti);
	}

	public void setInstantNotificationReceiverListener(InstantNotificationReceiverListener l) {
		mListener = l;
	}

	@Override
	public void onNotificationPosted(StatusBarNotification sbn) {
		Notification notification = sbn.getNotification();
		if(mListener!=null){
			mListener.onNotificationGot(notification);
		}
		Toast.makeText(getBaseContext(),
				notification.toString(), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onNotificationRemoved(StatusBarNotification sbn) {
		// TODO Auto-generated method stub

	}

}
