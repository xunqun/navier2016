package idv.xunqun.navier.utils;

import idv.xunqun.navier.R.string;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

public class KmlParser {
	private static final String ns = "";
	ArrayList<Placemark> entries = new ArrayList<Placemark>();
	   
    public ArrayList<Placemark> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            readFeed(parser);
            return entries;
        } finally {
            in.close();
        }
    }
    
    private void readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
    	

       
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("Document")) {
            	readFeed(parser);
            }else if(name.equals("Folder")){
            	readFeed(parser);
            }else if(name.equals("Placemark")){
            	readFeed(parser);
            }else if(name.equals("Point")){
            	entries.add(readPlacemark(parser));
            }else {
                skip(parser);
            }
        }  
        
    }
    
    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
    	 if (parser.getEventType() != XmlPullParser.START_TAG) {
    	        throw new IllegalStateException();
    	    }
    	    int depth = 1;
    	    while (depth != 0) {
    	        switch (parser.next()) {
    	        case XmlPullParser.END_TAG:
    	            depth--;
    	            break;
    	        case XmlPullParser.START_TAG:
    	            depth++;
    	            break;
    	        }
    	    }
	}

	private Placemark readPlacemark(XmlPullParser parser) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		parser.require(XmlPullParser.START_TAG, ns, "Point");
	    String lat = null;
	    String lng = null;
	    
	    while (parser.next() != XmlPullParser.END_TAG) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        String name = parser.getName();
	        if (name.equals("coordinates")) {
	            String temp = readCoor(parser);
	            String[] coor = temp.split(",");
	            lat = coor[1];
	            lng = coor[0];
	        } else{
	            skip(parser);
	        }
	    }
	    return new Placemark(lat, lng);
		
	}

	private String readCoor(XmlPullParser parser) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		parser.require(XmlPullParser.START_TAG, ns, "coordinates");
	    String title = readText(parser);
	    parser.require(XmlPullParser.END_TAG, ns, "coordinates");
	    return title;
	}

	private String readText(XmlPullParser parser) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stubString result = "";
		String result = "";
	    if (parser.next() == XmlPullParser.TEXT) {
	        result = parser.getText();
	        parser.nextTag();
	    }
	    return result;
		
	}

	public static class Placemark {
        public final double lat;
        public final double lng;
       
        private Placemark(String lat, String lng) {
            this.lat = Double.valueOf(lat);
            this.lng = Double.valueOf(lng);
        }
    }
}
