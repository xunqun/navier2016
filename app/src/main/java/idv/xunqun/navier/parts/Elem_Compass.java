package idv.xunqun.navier.parts;

import idv.xunqun.navier.R;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.hardware.GeomagneticField;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.View;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Elem_Compass extends Parts{
	
	//Default Value
	public static final int ELEM_HEIGHT = 4;
	public static final int ELEM_WIDTH = 4;    	
	public static final int ELEM_PART = Parts.ELEM_COMPASS;
	public static String ELEM_NAME = "SimpleCompass";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_simple_compass;

	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	private float[] mValues = new float[3];
	//Path
	
    private Paint   mPaint = new Paint();
    private Paint indPaint = new Paint();
    private Path    mPath = new Path();
    private Path	indPath = new Path();
    
    private Path textPath_n = new Path();
    private Path textPath_e = new Path();
    private Path textPath_w = new Path();
    private Path textPath_s = new Path();
    
    private int _TEXTSIZE = 40;
    private Location _location = null;
    
    public Elem_Compass(GridBoard parent, int[] pin){
    	super(parent, pin);
    	
    	initProperty();
    	initPath();
    	//setLayerType(View.LAYER_TYPE_HARDWARE, null);

    	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	    	setPivotX(_centerX);
	    	setPivotY(_centerY);
	    	setRotationX(60);
	    	
    	}

    }

    

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
        canvas.translate(_centerX, _centerY);
        if (mValues != null) {        
        	
            canvas.rotate(-mValues[0]-90);
            mPaint.setStyle(Style.FILL);
            canvas.drawTextOnPath("N", textPath_n, _TEXTSIZE/6, 0, mPaint);
            canvas.drawTextOnPath("E" ,textPath_e,_TEXTSIZE/6,0,mPaint );
            canvas.drawTextOnPath("S" ,textPath_s,_TEXTSIZE/6,0,mPaint );
            canvas.drawTextOnPath("W" ,textPath_w,_TEXTSIZE/6,0,mPaint );
            /*
            canvas.drawText("N",0, -mRadius, mPaint);
            canvas.drawText("E",mRadius+10, 10, mPaint);
            canvas.drawText("S",0, mRadius+20, mPaint);
            */
        }
        mPaint.setStyle(Style.STROKE);
        canvas.drawPath(mPath, mPaint);
        canvas.drawPath(indPath,indPaint);
        invalidate();
	}
	
	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub
		_location = location;
	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub
		/*
        mValues = event.values.clone();
        
		if(_location != null){
			GeomagneticField geoField = new GeomagneticField((float)_location.getLatitude(), (float)_location.getLongitude(), (float)_location.getAltitude(), System.currentTimeMillis());
			mValues[0] = mValues[0] + geoField.getDeclination();
			//Log("GeomagneticField Declination: " + geoField.getDeclination());
		}else{
			mValues[0] = mValues[0];
		}
		*/

		
		
	}
	
	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub
		
		
		if(_location != null){
			GeomagneticField geoField = new GeomagneticField((float)_location.getLatitude(), (float)_location.getLongitude(), (float)_location.getAltitude(), System.currentTimeMillis());
			mValues[0] = orientation[0] + geoField.getDeclination();
			//mValues[1] = (float) Math.toDegrees(orientation[1]);
		}else{
		
			mValues[0] = orientation[0];
		}
		//mValues[1] = (float) Math.toDegrees(orientation[1]);
		
		//mValues[2] = orientation[2];
	}
	

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub
		ELEM_PIN = pin;
	}


	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this._iniLeft + (this._width/2);
    	_centerY = this._iniTop + (this._height/2);
    	_TEXTSIZE = _parent._unitPixel;
	}
	
	private void initPath(){
		mPath.addCircle(0, 0, (float)(_width/4), Path.Direction.CW);
		
		
		indPath.addCircle(0, 0, (float)(_width/2), Path.Direction.CW);

		
		//set paint
        mPaint.setAntiAlias(true);
        mPaint.setColor(_parent.GLOBAL_COLOR);
        mPaint.setStrokeWidth(5);
        mPaint.setTextSize(_TEXTSIZE);
        mPaint.setTypeface(_parent._defaultFont);
        mPaint.setTextAlign(Align.CENTER);
        
        
        indPaint.setAntiAlias(true);
    
        indPaint.setColor(_parent.GLOBAL_COLOR);
        indPaint.setStrokeWidth(2);
        indPaint.setStyle(Style.STROKE);
        indPaint.setAlpha(100);
        //indPaint.setAlpha(60);
        
        
        
        mPaint.setStyle(Style.STROKE);
        mPaint.setTextSize(_TEXTSIZE);
        mPaint.setTextAlign(Align.CENTER);
        
        
        int space = (int) (((_width/2)*0.25 - _TEXTSIZE)/2);
        
        textPath_n.moveTo(-_TEXTSIZE, (float) -((_width/2)*0.75)-space);
        textPath_n.lineTo(_TEXTSIZE, (float) -((_width/2)*0.75)-space);
        
        textPath_e.moveTo((float) ((_width/2)*0.75)+space, -_TEXTSIZE);
        textPath_e.lineTo((float) ((_width/2)*0.75)+space, _TEXTSIZE/2);
		
        textPath_w.moveTo((float) (-(_width/2)*0.75)-space, _TEXTSIZE);
        textPath_w.lineTo((float) (-(_width/2)*0.75)-space, -_TEXTSIZE/2);
        
        
        textPath_s.moveTo(-_TEXTSIZE, (float) ((_width/2)*0.75)+space+_TEXTSIZE/2);
        textPath_s.lineTo(_TEXTSIZE, (float) ((_width/2)*0.75)+space+_TEXTSIZE/2);
	}



	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		mPaint.setColor(color);
		indPaint.setColor(color);
		

	}



	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub
		
	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		//Initial values
		int width = uniPixel * ELEM_WIDTH;
		int textsize = uniPixel/2;
		Path path = new Path();
		Paint paint = new Paint();
		
		
		//path
		path.addCircle(0, 0, (float)(width/4), Path.Direction.CW);
		path.addCircle(0, 0, (float)(width/2), Path.Direction.CW);
		
		//paint
		
		paint.setAntiAlias(true);
		paint.setColor(Color.CYAN);
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(5);
		
		canvas.save();
		canvas.translate(iniLeft+pin[0]*uniPixel + width/2, iniTop+pin[1]*uniPixel + width/2);
		canvas.drawPath(path, paint);
		canvas.restore();
		
	}



	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		ELEM_NAME = name;
	}



	@Override
	public void onIsGPSFix(GpsStatus status,boolean isFix) {
		// TODO Auto-generated method stub
		
	}





}
