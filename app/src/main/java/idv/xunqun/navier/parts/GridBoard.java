package idv.xunqun.navier.parts;


import idv.xunqun.navier.BaseNavierPanelContext;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.RRelativeLayout;
import idv.xunqun.navier.content.Layout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

public abstract class GridBoard extends View {
	
	public final static int WIDTH_UNIT_COUNT = 16;
	public final static int HEIGHT_UNIT_COUNT = 8;
	public static int GLOBAL_COLOR = Color.CYAN;
	public static int SECONDARY_COLOR = Color.RED;
	public int GLOBAL_SPEEDUNIT = PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH;
	
	//save instances
	public final static String SAVEDSTATE_GRIDBOARD_GLOBALCOLOR = "SAVEDSTATE_GRIDBOARD_GLOBALCOLOR";
	public final static String SAVEDSTATE_GRIDBOARD_SECONDARYCOLOR = "SAVEDSTATE_GRIDBOARD_SECONDARYCOLOR";
	public final static String SAVEDSTATE_GRIDBOARD_SPEEDUNIT = "SAVEDSTATE_GRIDBOARD_SPEEDUNIT";
	public final static String SAVEDSTATE_GRIDBOARD_SHOWGRID = "SAVEDSTATE_GRIDBOARD_SHOWGRID";
	
	
	public final int ENERGY_SAVING_FPS = 20;
	public final int ENERGY_NORMOAL_FPS = 30;
	
	public Context _context;
	protected BaseNavierPanelContext _baseNavierPanel;
	public int _screenWidth;
	public int _screenHeight;	
	public int _unitPixel=0;
	public int _screenWidthMargin;
	public int _screenHeightMargin;
	public Typeface _defaultFont;
	
	
	public PreferencesHandler _settings;
	public boolean _showGrid= true;
	private boolean _checkgpuforce = false;
	

	
	//paint and drawable
	public Paint _gridPaint;
	public Path _gridPath;	
	
	
	//layout	
	public Layout _layout;
	private RelativeLayout rl;
	private GridBoardHandler _handler;
	
	
	public interface GridBoardHandler{
		public void onLayoutInflate(View v);

	}

	/**
	 * 
	 * @param context  Should be activity
	 * @param basepanel May a activity or fragment that implements the BaseNavierPanel
	 * @param attrs
	 * @param jlayout
	 * @param handler
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public GridBoard(Context context, BaseNavierPanelContext basepanel , JSONObject jlayout,GridBoardHandler handler) {
		super(context);
		// TODO Auto-generated constructor stub
		_context = context;
		_handler = handler;
		_baseNavierPanel = basepanel;
		
		initSettings();
		initBoard();
		initPaints();
		initParts(jlayout);
		
		/*
		Thread td ;
		if(_settings.getPREF_ENERGY_MODE()){
			//lower frame rate
			//new invalidateTimer(ENERGY_SAVING_FPS,ENERGY_SAVING_FPS).start();
			new Thread(new invalidateRunnable(ENERGY_SAVING_FPS)).start();
		}else{
			
			//new invalidateTimer(ENERGY_NORMOAL_FPS,ENERGY_NORMOAL_FPS).start();
			new Thread(new invalidateRunnable(ENERGY_NORMOAL_FPS)).start();
		}
		*/
		

	}

	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		try{

	        if(!_checkgpuforce && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
	            if(canvas.isHardwareAccelerated()){
	            	AlertDialog.Builder builder = new AlertDialog.Builder(_context);
	            	builder.setMessage(R.string.navierpanel_gpuenable_alert)
	            			.setTitle(R.string.navierpanel_gpuenable_alert_title)
	            			.setPositiveButton(R.string.naviernaviconfigure_ok, null);
	            	
	            	builder.create().show();
	            	
	            }
	            
	        	
	        }
		}catch(Exception e){
			
		}
		
		_checkgpuforce = true;
		drawBoard(canvas);
		//super.onDraw(canvas);
		
		
	}



	//abstract method
	public abstract void onLocationChangeHandler(Location location);
	public abstract void onLocationStatusChangeHandle(String provider, int status, Bundle extras);
	public abstract void onLocationProviderDisableHandle(String provider);
	public abstract void onIsGPSFix(GpsStatus status, Boolean isfix);
	public abstract void onSensorChangeHandler(SensorEvent event);
	public abstract void onSensorChangeHandler(float[] orientation);
	public abstract void onGlobalColorChange(int color);
	public abstract void onSpeedUnitChangeHandler(int sunit);
	public abstract void onPause();
	public abstract void onResume();

	public void saveInstance(Bundle bundle){
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onInstanceSave(bundle);
		}
		
		bundle.putInt(SAVEDSTATE_GRIDBOARD_GLOBALCOLOR, GLOBAL_COLOR);
		bundle.putInt(SAVEDSTATE_GRIDBOARD_SECONDARYCOLOR, SECONDARY_COLOR);
		bundle.putInt(SAVEDSTATE_GRIDBOARD_SPEEDUNIT, GLOBAL_SPEEDUNIT);
		bundle.putBoolean(SAVEDSTATE_GRIDBOARD_SHOWGRID, _showGrid);
		
		
	}
	
	public void restoreInstance(Bundle bundle){
		if(bundle != null){
			for(int i=0;i<_layout._parts_list.size();i++){
				Parts p = _layout._parts_list.get(i);
				p.onInstanceRestore(bundle);
			}
		}
		
		if(bundle.containsKey(SAVEDSTATE_GRIDBOARD_GLOBALCOLOR))
			GLOBAL_COLOR = bundle.getInt(SAVEDSTATE_GRIDBOARD_GLOBALCOLOR);
		
		if(bundle.containsKey(SAVEDSTATE_GRIDBOARD_SECONDARYCOLOR))
			SECONDARY_COLOR = bundle.getInt(SAVEDSTATE_GRIDBOARD_SECONDARYCOLOR);
		
		if(bundle.containsKey(SAVEDSTATE_GRIDBOARD_SPEEDUNIT))
			GLOBAL_SPEEDUNIT = bundle.getInt(SAVEDSTATE_GRIDBOARD_SPEEDUNIT);
		
		if(bundle.containsKey(SAVEDSTATE_GRIDBOARD_SHOWGRID))
			_showGrid = bundle.getBoolean(SAVEDSTATE_GRIDBOARD_SHOWGRID);
		
	}
	
	
	private void initSettings(){
		
		_settings = new PreferencesHandler(_context);
		_showGrid = _settings.getPREF_SHOW_GRID();
		setDefaultColor();
		GLOBAL_SPEEDUNIT = _settings.getPREF_SPEED_UNIT();
		
		try {
			_defaultFont = Typeface.createFromAsset(_context.getAssets(),
					"fonts/LiquidCrystal-Normal.otf");
		} catch (Exception e) {
			_defaultFont = Typeface.DEFAULT;
			e.printStackTrace();
		}
	}
	
	private void setDefaultColor(){
		GLOBAL_COLOR = _settings.getPREF_COLOR();
		if(GLOBAL_COLOR == 0xffff4500){
			SECONDARY_COLOR = Color.WHITE;
		}else{
			SECONDARY_COLOR = Color.RED;
		}
	}
	
	private void initBoard(){
		
		Display display = ((WindowManager) _context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(); 
		_screenWidth = display.getWidth();
		_screenHeight = display.getHeight();
		_unitPixel=(int)((_screenWidth/WIDTH_UNIT_COUNT>_screenHeight/HEIGHT_UNIT_COUNT)?_screenHeight/HEIGHT_UNIT_COUNT:_screenWidth/WIDTH_UNIT_COUNT);
		
		_screenWidthMargin = (_screenWidth - (_unitPixel*WIDTH_UNIT_COUNT))/2;
		_screenHeightMargin = (_screenHeight - (_unitPixel*HEIGHT_UNIT_COUNT))/2;
		

	
	}
	
	private void initPaints(){
		_gridPaint = new Paint();
		_gridPaint.setColor(GLOBAL_COLOR);        
		_gridPaint.setStrokeWidth(1);
		_gridPaint.setStyle(Paint.Style.STROKE);
		_gridPaint.setAlpha(0x50);
		
		_gridPath = new Path();
		_gridPath.moveTo(_screenWidthMargin, _screenHeightMargin);
		float tmpX=_screenWidthMargin , tmpY=_screenHeightMargin;
		
		for(int i=0; i<=WIDTH_UNIT_COUNT ; i++){
			_gridPath.lineTo(tmpX, tmpY+HEIGHT_UNIT_COUNT*_unitPixel);
			tmpX += _unitPixel;
			tmpY = _screenHeightMargin;
			_gridPath.moveTo(tmpX, tmpY);
		}
		
		tmpX = _screenWidthMargin;
		tmpY = _screenHeightMargin;		
		_gridPath.moveTo(tmpX, tmpY);
		
		for(int i=0 ; i<=HEIGHT_UNIT_COUNT ; i++){
			_gridPath.lineTo(tmpX+WIDTH_UNIT_COUNT*_unitPixel, tmpY);
			tmpY +=  _unitPixel;
			tmpX = _screenWidthMargin;
			_gridPath.moveTo(tmpX, tmpY);
		}
		
		
	}
	
	public void initParts(JSONObject layout){
		
		JSONObject root;
		try {
			root = layout.getJSONObject(Layout.JSON_ROOT);
			//initParts(layout, root.getInt(Layout.JSON_MODE));
			int mode = _settings.getPREF_HUD_MODE()==false?Layout.MODE_NORMAL:Layout.MODE_HUD;
			initParts(layout, mode);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
	public void initParts(JSONObject layout, int layoutMode){
		_layout = new Layout();
		
		if(rl != null){
			rl.removeAllViews();
		}
		
		try {
			
			JSONObject root= layout.getJSONObject(Layout.JSON_ROOT);
			JSONArray parts = root.getJSONArray(Layout.JSON_PARTS);
			
			_layout._mode = layoutMode;
			_layout._name = root.getString(Layout.JSON_NAME);			
			_layout._type = root.getInt(Layout.JSON_TYPE);			
			_layout._parts_list = Parts.createElemList(parts, this);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		/* 
		 * check and deal with the mode = HUD
		 */
		
		if(_layout._mode == Layout.MODE_NORMAL){
			rl = new RelativeLayout(_context);
		}else{
			rl = new RRelativeLayout(_context);
		}
		
		
		for(int i=0;i<_layout._parts_list.size();i++){
			rl.addView(_layout._parts_list.get(i));
		}
		
		rl.setBackgroundColor(Color.BLACK);
		rl.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT));
		rl.addView(GridBoard.this);
		
		_handler.onLayoutInflate(rl);
		//((Activity)_context).setContentView(rl);
		

		
	}
	
	public void modeSwitch( int layoutMode){
		
		if(rl != null){
			rl.removeAllViews();
		}
		
		if(layoutMode == Layout.MODE_NORMAL){
			rl = new RelativeLayout(_context);
			_layout._mode = Layout.MODE_NORMAL;
		}else{
			rl = new RRelativeLayout(_context);
			_layout._mode = Layout.MODE_HUD;
		}
		
		
		for(int i=0;i<_layout._parts_list.size();i++){
			rl.addView(_layout._parts_list.get(i));
		}
		
		rl.setBackgroundColor(Color.BLACK);
		rl.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT));
		rl.addView(GridBoard.this);
		((Activity)_context).setContentView(rl);
	}
	
	public void drawBoard(Canvas canvas){
		
		canvas.save();
		if(_showGrid == true){
			
			canvas.drawPath(_gridPath, _gridPaint);			
		}
		canvas.restore();
		
	}
	

	
	

	
	public class invalidateRunnable implements Runnable{

		long mInterval = 0;
		public invalidateRunnable(long interval){
			mInterval = interval;
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(!_baseNavierPanel.getIsEndAnimation()){
				if(!_baseNavierPanel.getIsStopAnimation()){
					GridBoard.this.post(new Runnable() {
					    public void run() {
					    	invalidate();
					    } 
					});
				}
				
				try {
					Thread.sleep(mInterval);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public class invalidateTimer extends CountDownTimer{
		
		long millisInFuture;
		long countDownInterval;

		public invalidateTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
			this.millisInFuture = millisInFuture;
			this.countDownInterval = countDownInterval;
			
			
		}

		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			invalidate();
			new invalidateTimer(millisInFuture,countDownInterval).start();
			
		}
		
	}
}
