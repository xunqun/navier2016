package idv.xunqun.navier.parts;



import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.SpeedUnit;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.content.RoutePlan;
import idv.xunqun.navier.v2.content.DirectionRoute;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Bundle;

public class Elem_RouteProgress extends NaviParts {
	
	//Must have
	public static final int ELEM_HEIGHT = 8;
	public static final int ELEM_WIDTH = 2;    	
	public static final int ELEM_PART = Parts.ELEM_ROUTEPROGRESS;
	public static String ELEM_NAME = "Moving Progress";
	public static final boolean ELEM_ISNAVPART = true;
	public static final int ELEM_THUMBNAIL = R.drawable.part_routeprogress;
	
	
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	
	//Path
	private Path _unitTextPath;
	private Path _p25TextPath;
	private Path _p50TextPath;
	private Path _p75TextPath;
	private Path _rulerPath;
	private Path _rulerPath1;
	private Path _rulerPath2;
	private Path _rulerPath3;
	private Path _rulerPath4;
	private Path _positionPath;
	private Path _clipPath;
	
	private Paint _textPaint;
	private Paint _rulerPaint;
	
	//Values
	private NavigationGridBoard _parent;
	private int _currStep = -1;
	private double _distanceInLeftSteps=0;
	
	public Elem_RouteProgress(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		_parent = (NavigationGridBoard) parent;
		initProperty();
		initPath();
	}
	
	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this._iniLeft + (this._width/2);
    	_centerY = this._iniTop + (this._height/2);    	
	}
	

	
	private void initPath(){
		
		_rulerPath = new Path();
		_rulerPath.addRect(_iniLeft+_parent._unitPixel/2, _iniTop, _iniLeft+_parent._unitPixel, _iniTop+_height, Direction.CCW);
		
		_rulerPath1 = new Path();
		_rulerPath1.addRect(_iniLeft+_parent._unitPixel/2, _iniTop, _iniLeft+_parent._unitPixel, _iniTop+_height/4, Direction.CCW);
		
		_rulerPath2 = new Path();
		_rulerPath2.addRect(_iniLeft+_parent._unitPixel/2, _iniTop+_parent._unitPixel*2, _iniLeft+_parent._unitPixel, _iniTop+_parent._unitPixel*4, Direction.CCW);
		
		_rulerPath3 = new Path();
		_rulerPath3.addRect(_iniLeft+_parent._unitPixel/2, _iniTop+_parent._unitPixel*4, _iniLeft+_parent._unitPixel, _iniTop+_parent._unitPixel*6, Direction.CCW);
		
		_rulerPath4 = new Path();
		_rulerPath4.addRect(_iniLeft+_parent._unitPixel/2, _iniTop+_parent._unitPixel*6, _iniLeft+_parent._unitPixel, _iniTop+_parent._unitPixel*8, Direction.CCW);
		
		_clipPath = new Path();
		_clipPath.addRect(_iniLeft, _iniTop, _iniLeft+_width, _iniTop + _height,Direction.CCW);
		
		_rulerPaint = new Paint();
		_rulerPaint.setColor(_parent.GLOBAL_COLOR);
		
		
		_unitTextPath = new Path();
		_unitTextPath.moveTo(_iniLeft+_parent._unitPixel, _iniTop+_parent._unitPixel/3);
		_unitTextPath.lineTo(_iniLeft+_parent._unitPixel*2, _iniTop+_parent._unitPixel/3);
		
		_p25TextPath = new Path();
		_p25TextPath.moveTo(_iniLeft+_parent._unitPixel, _iniTop+_parent._unitPixel*2);
		_p25TextPath.lineTo(_iniLeft+_parent._unitPixel*2, _iniTop+_parent._unitPixel*2);
		
		_p50TextPath = new Path();
		_p50TextPath.moveTo(_iniLeft+_parent._unitPixel, _iniTop+_parent._unitPixel*4);
		_p50TextPath.lineTo(_iniLeft+_parent._unitPixel*2, _iniTop+_parent._unitPixel*4);

		_p75TextPath = new Path();
		_p75TextPath.moveTo(_iniLeft+_parent._unitPixel, _iniTop+_parent._unitPixel*6);
		_p75TextPath.lineTo(_iniLeft+_parent._unitPixel*2, _iniTop+_parent._unitPixel*6);
		
		_textPaint = new Paint();
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setTextSize(_parent._unitPixel/3);
		_textPaint.setTypeface(_parent._defaultFont);
		_textPaint.setTextAlign(Align.CENTER);

		_positionPath = new Path();
		_positionPath.addRect(0,0,_parent._unitPixel,_parent._unitPixel/5,Direction.CCW);
		
		
	}
	
	private double getCurrRouteProgress(){
		double rate=0;
		try{
		
			if(_currStep!=_parent._naviManager.mRoutePlan._current_step){
				_distanceInLeftSteps=0;
				
				for(int i=_parent._naviManager.mRoutePlan._current_step+1;i<_parent._naviManager.mRoutePlan.arr_steps.size();i++){
					_distanceInLeftSteps += _parent._naviManager.mRoutePlan.arr_steps.get(i).distance_value;
				}
				
			}
			
			_distanceInLeftSteps += _parent._leftDistance;
			rate = (_parent._naviManager.mRoutePlan._totalDistance-_distanceInLeftSteps)/_parent._naviManager.mRoutePlan._totalDistance;
			if(rate>=1) rate = 1;
			if(rate<=0) rate = 0;
		}catch(Exception e){
			return 0;
		}
		return rate;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		_rulerPaint.setStyle(Style.FILL);
		_rulerPaint.setAlpha(255);
		_rulerPaint.setColor(_parent.GLOBAL_COLOR);
		canvas.drawPath(_rulerPath1, _rulerPaint);
		
		_rulerPaint.setAlpha(150);
		canvas.drawPath(_rulerPath2, _rulerPaint);
		
		_rulerPaint.setAlpha(100);
		canvas.drawPath(_rulerPath3, _rulerPaint);
		
		_rulerPaint.setAlpha(50);
		canvas.drawPath(_rulerPath4, _rulerPaint);
		
		_rulerPaint.setAlpha(255);
		_rulerPaint.setStyle(Style.STROKE);
		canvas.drawPath(_rulerPath, _rulerPaint);
		
		//canvas.clipPath(_clipPath);
		canvas.clipRect(new Rect(_iniLeft, _iniTop, _iniLeft+(_parent._unitPixel*ELEM_WIDTH), _iniTop + (_parent._unitPixel*ELEM_HEIGHT)));
		if(_parent._naviManager!=null){
			
			double p1 =  ((_parent._naviManager.mTotalDistance*0.25)/1000);
			double p2 = ((_parent._naviManager.mTotalDistance*0.5)/1000);
			double p3 = ((_parent._naviManager.mTotalDistance*0.75)/1000);
			
			if(_parent.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH){
				canvas.drawTextOnPath("KM", _unitTextPath, 0, 0, _textPaint);
				canvas.drawTextOnPath(String.format("%03.1f", p1), _p25TextPath, 0, 0, _textPaint);
				canvas.drawTextOnPath(String.format("%03.1f", p2), _p50TextPath, 0, 0, _textPaint);
				canvas.drawTextOnPath(String.format("%03.1f", p3), _p75TextPath, 0, 0, _textPaint);
			
			}else{
				canvas.drawTextOnPath("MILE", _unitTextPath, 0, 0, _textPaint);
				canvas.drawTextOnPath(String.format("%03.1f", SpeedUnit.km2mile(p1)), _p25TextPath, 0, 0, _textPaint);
				canvas.drawTextOnPath(String.format("%03.1f", SpeedUnit.km2mile(p2)), _p50TextPath, 0, 0, _textPaint);
				canvas.drawTextOnPath(String.format("%03.1f", SpeedUnit.km2mile(p3)), _p75TextPath, 0, 0, _textPaint);
			}
			
			canvas.save();
			_rulerPaint.setAlpha(255);
			_rulerPaint.setStyle(Style.FILL);
			_rulerPaint.setColor(_parent.SECONDARY_COLOR);
			double rate = getCurrRouteProgress();
			
			canvas.translate(_iniLeft, (float) (_iniTop+_height*(1-rate)));
			canvas.drawPath(_positionPath, _rulerPaint);
			canvas.restore();
		}
		invalidate();
	}

	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		_rulerPaint.setColor(color);
		_textPaint.setColor(color);
		_rulerPaint.setColor(sndColor);
		invalidate();
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		//Path
		Path _unitTextPath;
		Path _p25TextPath;
		Path _p50TextPath;
		Path _p75TextPath;
		Path _rulerPath;
		Path _rulerPath1;
		Path _rulerPath2;
		Path _rulerPath3;
		Path _rulerPath4;
		Path _positionPath;
		Path _clipPath;
		
		Paint _textPaint;
		Paint _rulerPaint;
		
		iniLeft += pin[0]*uniPixel;
		iniTop += pin[1]*uniPixel;
		
		_rulerPath = new Path();
		_rulerPath.addRect(iniLeft+uniPixel/2, iniTop, iniLeft+uniPixel, iniTop+uniPixel*ELEM_HEIGHT, Direction.CCW);
		
		_rulerPath1 = new Path();
		_rulerPath1.addRect(iniLeft+uniPixel/2, iniTop, iniLeft+uniPixel, iniTop+(uniPixel*ELEM_HEIGHT)/4, Direction.CCW);
		
		_rulerPath2 = new Path();
		_rulerPath2.addRect(iniLeft+uniPixel/2, iniTop+uniPixel*2, iniLeft+uniPixel, iniTop+uniPixel*4, Direction.CCW);
		
		_rulerPath3 = new Path();
		_rulerPath3.addRect(iniLeft+uniPixel/2, iniTop+uniPixel*4, iniLeft+uniPixel, iniTop+uniPixel*6, Direction.CCW);
		
		_rulerPath4 = new Path();
		_rulerPath4.addRect(iniLeft+uniPixel/2, iniTop+uniPixel*6, iniLeft+uniPixel, iniTop+uniPixel*8, Direction.CCW);
		
		_clipPath = new Path();
		_clipPath.addRect(iniLeft, iniTop, iniLeft+(uniPixel*ELEM_WIDTH), iniTop + (uniPixel*ELEM_HEIGHT),Direction.CCW);
		
		_rulerPaint = new Paint();
		_rulerPaint.setColor(Color.CYAN);
		
		
		_unitTextPath = new Path();
		_unitTextPath.moveTo(iniLeft+uniPixel, iniTop+uniPixel/3);
		_unitTextPath.lineTo(iniLeft+uniPixel*2, iniTop+uniPixel/3);
		
		_p25TextPath = new Path();
		_p25TextPath.moveTo(iniLeft+uniPixel, iniTop+uniPixel*2);
		_p25TextPath.lineTo(iniLeft+uniPixel*2, iniTop+uniPixel*2);
		
		_p50TextPath = new Path();
		_p50TextPath.moveTo(iniLeft+uniPixel, iniTop+uniPixel*4);
		_p50TextPath.lineTo(iniLeft+uniPixel*2, iniTop+uniPixel*4);

		_p75TextPath = new Path();
		_p75TextPath.moveTo(iniLeft+uniPixel, iniTop+uniPixel*6);
		_p75TextPath.lineTo(iniLeft+uniPixel*2, iniTop+uniPixel*6);
		
		_textPaint = new Paint();
		_textPaint.setColor(Color.CYAN);
		_textPaint.setTextSize(uniPixel/3);
		_textPaint.setTextAlign(Align.LEFT);

		_positionPath = new Path();
		_positionPath.addRect(0,0,uniPixel,uniPixel/5,Direction.CCW);
		
		
		
		
		_rulerPaint.setStyle(Style.FILL);
		_rulerPaint.setAlpha(255);
		_rulerPaint.setColor(Color.CYAN);
		canvas.drawPath(_rulerPath1, _rulerPaint);
		
		_rulerPaint.setAlpha(150);
		canvas.drawPath(_rulerPath2, _rulerPaint);
		
		_rulerPaint.setAlpha(100);
		canvas.drawPath(_rulerPath3, _rulerPaint);
		
		_rulerPaint.setAlpha(50);
		canvas.drawPath(_rulerPath4, _rulerPaint);
		
		_rulerPaint.setAlpha(255);
		_rulerPaint.setStyle(Style.STROKE);
		canvas.drawPath(_rulerPath, _rulerPaint);
		
		//canvas.clipPath(_clipPath);
		//if(_parent._naviManager!=null){
			
		double p1 =  ((10000*0.25)/1000);
		double p2 = ((10000*0.5)/1000);
		double p3 = ((10000*0.75)/1000);
		
		
			canvas.drawTextOnPath("KM", _unitTextPath, 0, 0, _textPaint);
			canvas.drawTextOnPath(String.format("%03.1f", p1), _p25TextPath, 0, 0, _textPaint);
			canvas.drawTextOnPath(String.format("%03.1f", p2), _p50TextPath, 0, 0, _textPaint);
			canvas.drawTextOnPath(String.format("%03.1f", p3), _p75TextPath, 0, 0, _textPaint);
		
		
		
		
		_rulerPaint.setAlpha(255);
		_rulerPaint.setStyle(Style.FILL);
		_rulerPaint.setColor(Color.RED);
		double rate = 0.6;
		canvas.save();
		canvas.translate(iniLeft, (float) (iniTop+uniPixel*ELEM_HEIGHT*(1-rate)));
		canvas.drawPath(_positionPath, _rulerPaint);
		canvas.restore();
		//}
	}

	@Override
	public void onRouteReplan(RoutePlan route) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onArrival() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLeftDistanceNotify(double distance, double alarmDistance) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		ELEM_NAME = name;
	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLocationOnRoadChange(Latlng p, double angle) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onIsGPSFix(GpsStatus status,boolean isFix) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDirectionReplan(DirectionRoute directionPlan) {
		// TODO Auto-generated method stub
		
	}
}
