package idv.xunqun.navier.parts;

import idv.xunqun.navier.R;
import idv.xunqun.navier.utils.Utility;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

public class Elem_Acceleration_Chart extends Parts {

	public static final int ELEM_HEIGHT = 2;
	public static final int ELEM_WIDTH = 4;    	
	public static final int ELEM_PART = Parts.ELEM_ACCELERATION_CHART;
	public static String ELEM_NAME = "Acceleration Chart";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_acc;
	
	/*
	 * savedInstancesState naming rule
	 * e.g. SAVEDSTATE_CLASSNAME_VALUENAME
	 */
	
	public static final String SAVEDSTATE_ACCELERATIONCHAR_CURACC = "SAVEDSTATE_ACCELERATIONCHAR_CURACC";
	public static final String SAVEDSTATE_ACCELERATIONCHAR_PRESPEED = "SAVEDSTATE_ACCELERATIONCHAR_PRESPEED";
	public static final String SAVEDSTATE_ACCELERATIONCHAR_IDXHEAD = "SAVEDSTATE_ACCELERATIONCHAR_IDXHEAD";
	public static final String SAVEDSTATE_ACCELERATIONCHAR_DATAARRAY = "SAVEDSTATE_ACCELERATIONCHAR_DATAARRAY";
	public static final String SAVEDSTATE_ACCELERATIONCHAR_PRELOCATION = "SAVEDSTATE_ACCELERATIONCHAR_PRELOCATION";
	
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	
	//Path
	Path _textPath;
	private Path _decorationPath;
	private Path _baseLinePath;
	private Path _movingDataPath;
	
	private Paint _textPaint;
	private Paint _decorationPaint;
	private Paint _baseLinePaint;
	private Paint _movingDataPaint;
	
	//Values
	private final int DATA_COUNT = 50;
	private final int MAX_VALUE = 10;
	private Location _preLocation=null;
	private float _preSpeed = 0;
	
	private int _idxHead;
	private float[] dataArray = new float[DATA_COUNT];
	private float _curAcceration;
	
	public Elem_Acceleration_Chart(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		initProperty();
		initPath();
		new Thread(new MovingDataRunable()).start();
	}
	
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		_movingDataPath.reset();
		genMovingDataPath();
		
		canvas.save();
		canvas.clipRect(_iniLeft, _iniTop, _iniLeft + _width, _iniTop + _height);
		//canvas.drawPath(_decorationPath, _decorationPaint);
		//canvas.drawPath(_baseLinePath, _baseLinePaint);
		canvas.drawPath(_movingDataPath, _movingDataPaint);
		canvas.drawTextOnPath("acc. chart", _textPath, 0, 0, _textPaint);
		canvas.restore();
		
		
		invalidate();
	}



	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	
	}
	
	private void initPath(){
		
		Typeface font = _parent._defaultFont;
	
		_textPath = new Path();
		_textPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel/2);
		_textPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel/2);
	
		
		_textPaint = new Paint();
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setTypeface(font);
		_textPaint.setTextSize(_parent._unitPixel/2);
		_textPaint.setAlpha(100);
		
		_decorationPath = new Path();
		_decorationPath.addRect(_iniLeft, _iniTop, _iniLeft + _width, _iniTop + _height, Direction.CCW);
		
		_decorationPaint = new Paint();
		_decorationPaint.setStyle(Style.STROKE);
		_decorationPaint.setColor(_parent.GLOBAL_COLOR);
		_decorationPaint.setStrokeWidth(_parent._unitPixel/32);
		
		_baseLinePath = new Path();
		_baseLinePath.moveTo(_iniLeft, _iniTop + _height/2);
		_baseLinePath.lineTo(_iniLeft + _width, _iniTop + _height/2);
		
		_baseLinePaint = new Paint();
		_baseLinePaint.setStyle(Style.STROKE);
		_baseLinePaint.setColor(_parent.SECONDARY_COLOR);
		_baseLinePaint.setStrokeWidth(_parent._unitPixel/16);
		_baseLinePaint.setAlpha(100);
		
		_movingDataPath = new Path();
		
		_movingDataPaint = new Paint();
		_movingDataPaint.setStyle(Style.STROKE);
		_movingDataPaint.setColor(_parent.GLOBAL_COLOR);
		_movingDataPaint.setStrokeWidth(_parent._unitPixel/8);
		_movingDataPaint.setAntiAlias(true);
		
	}
	
	private void genMovingDataPath(){
		
		int index = _idxHead-1;
		int space = (int)((_width/(DATA_COUNT-1))+1);
		
		if(index<0){
			index = DATA_COUNT - 1;
		}
		
		_movingDataPath.reset();
		_movingDataPath.moveTo(_iniLeft + _width, _iniTop + _height/2 - (_height * (dataArray[index]/MAX_VALUE)));
		//Log.d("mine", "index: " + index + " value: " + dataArray[index]);
		for(int i = 0; i<DATA_COUNT-1 ; i++){
			
			index -=1;
			if(index<0)
				index = DATA_COUNT - 1;

			
			_movingDataPath.lineTo(_iniLeft + _width - space * (i), _iniTop + _height/2 - _height * dataArray[index]/MAX_VALUE);
		}
		_movingDataPath.lineTo(_iniLeft, _iniTop+_height/2);
		//_movingDataPath.lineTo(_iniLeft + _width, _iniTop+_height/2);
		//_movingDataPath.close();
		
	}
	
	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub
		if(_preLocation!=null){
			float speed = 0;
			
			if(location.getProvider().equals(LocationManager.GPS_PROVIDER) && location.getSpeed()!=0 ){
				speed = location.getSpeed()/1000;
				_curAcceration = Utility.caculateAcceration(speed, location.getTime(), _preLocation.getSpeed()/1000, _preLocation.getTime());
			}
			
			if(location.getProvider().equals(LocationManager.GPS_PROVIDER) && location.getSpeed()==0 && _preLocation!=null){
				
				speed = Utility.caculateSpeed(location, _preLocation)/1000;	
				
				
				_curAcceration = Utility.caculateAcceration(speed, location.getTime(), _preSpeed, _preLocation.getTime());
			}
			_preSpeed = speed;
		}
		//Log.d("mine", "o curSpeed: " + _curSpeed);
		_preLocation = location;
	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		_decorationPaint.setColor(color);
		_baseLinePaint.setColor(color);
		_movingDataPaint.setColor(color);
		_textPaint.setColor(color);
		_textPaint.setAlpha(100);
		
		_baseLinePaint.setColor(sndColor);
		_baseLinePaint.setAlpha(100);
		

	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		savedStates.putFloatArray(SAVEDSTATE_ACCELERATIONCHAR_DATAARRAY, dataArray);
		savedStates.putInt(SAVEDSTATE_ACCELERATIONCHAR_IDXHEAD, _idxHead);
		savedStates.putFloat(SAVEDSTATE_ACCELERATIONCHAR_CURACC, _curAcceration);
		savedStates.putParcelable(SAVEDSTATE_ACCELERATIONCHAR_PRELOCATION, _preLocation);
		savedStates.putFloat(SAVEDSTATE_ACCELERATIONCHAR_PRESPEED, _preSpeed);

	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		if(savedStates.containsKey(SAVEDSTATE_ACCELERATIONCHAR_DATAARRAY))
			dataArray = savedStates.getFloatArray(SAVEDSTATE_ACCELERATIONCHAR_DATAARRAY);
			
		if(savedStates.containsKey(SAVEDSTATE_ACCELERATIONCHAR_IDXHEAD))
			_idxHead = savedStates.getInt(SAVEDSTATE_ACCELERATIONCHAR_IDXHEAD);
		
			
		if(savedStates.containsKey(SAVEDSTATE_ACCELERATIONCHAR_CURACC))
			_curAcceration = savedStates.getFloat(SAVEDSTATE_ACCELERATIONCHAR_CURACC);
		
			
		if(savedStates.containsKey(SAVEDSTATE_ACCELERATIONCHAR_PRELOCATION))
			_preLocation = savedStates.getParcelable(SAVEDSTATE_ACCELERATIONCHAR_PRELOCATION);

		if(savedStates.containsKey(SAVEDSTATE_ACCELERATIONCHAR_PRESPEED))
			_preSpeed = savedStates.getFloat(SAVEDSTATE_ACCELERATIONCHAR_PRESPEED);
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}
	
	private class MovingDataRunable implements Runnable{

		
		private final int INTERVAL = 2000;
		
		
		
		public MovingDataRunable(){
			//initial array
			for(int i = 0; i < DATA_COUNT; i++){
				dataArray[i] = 0;
			}
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(true){
				try {
					Thread.sleep(INTERVAL);
					dataArray[_idxHead] = _curAcceration;
					_idxHead += 1;
					if(_idxHead >= DATA_COUNT)
						_idxHead = 0;
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				

			}
		}
		
	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		//property
		int _width = uniPixel * ELEM_WIDTH ;
		int _height = uniPixel * ELEM_HEIGHT;
		int _iniTop = iniTop + pin[1]*uniPixel;
		int _iniLeft = iniLeft + pin[0]*uniPixel;
		
		int MAX_VALUE = 160;
		int DATA_COUNT = 100;
		int _idxHead = 0;
		
		float[] dataArray = new float[DATA_COUNT];
		
		//Path
		Path _textPath;
		Path _decorationPath;
		Path _baseLinePath;
		Path _movingDataPath;
		
		Paint _textPaint;
		Paint _decorationPaint;
		Paint _baseLinePaint;
		Paint _movingDataPaint;
		
		//Initial path and paint
		Typeface font;
		try{
			font = Typeface.createFromAsset(context.getAssets(), "fonts/SquadaOne-Regular.ttf");
		}catch(Exception e){
			font = Typeface.DEFAULT;
		}
		
		
		_textPath = new Path();
		_textPath.moveTo( _iniLeft,_iniTop+uniPixel/2);
		_textPath.lineTo( _iniLeft+ELEM_WIDTH*uniPixel, _iniTop+uniPixel/2);
		
		_textPaint = new Paint();
		_textPaint.setColor(Color.CYAN);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setTypeface(font);
		_textPaint.setTextSize(uniPixel/2);
		_textPaint.setAlpha(100);
		
		_decorationPath = new Path();
		_decorationPath.addRect(_iniLeft, _iniTop, _iniLeft + _width, _iniTop + _height, Direction.CCW);
		
		_decorationPaint = new Paint();
		_decorationPaint.setStyle(Style.STROKE);
		_decorationPaint.setColor(Color.CYAN);
		_decorationPaint.setStrokeWidth(uniPixel/32);
		
		_baseLinePath = new Path();
		_baseLinePath.moveTo(_iniLeft, _iniTop + _height/2);
		_baseLinePath.lineTo(_iniLeft + _width, _iniTop + _height/2);
		
		_baseLinePaint = new Paint();
		_baseLinePaint.setStyle(Style.STROKE);
		_baseLinePaint.setColor(Color.RED);
		_baseLinePaint.setAlpha(100);
		_baseLinePaint.setStrokeWidth(uniPixel/16);
		
		_movingDataPath = new Path();
		
		_movingDataPaint = new Paint();
		_movingDataPaint.setStyle(Style.STROKE);
		_movingDataPaint.setColor(Color.CYAN);
		_movingDataPaint.setStrokeWidth(uniPixel/8);
		
		//generate data
		int data = 0;
		for(int i=0 ; i<DATA_COUNT ; i++){
			if(i<20){
				dataArray[i] = data;
				data += 1;
			}
			
			if(i>=20 && i<50){
				dataArray[i] = data;
				data -= 1.2;
			}
			
			if(i>=50 && i<80){
				dataArray[i] = data;
				data += 1.3;
			}
			
			if(i>=80){
				dataArray[i] = data;
				data -= 2;
			}
		}
		
		// gen data path
		int index = _idxHead-1;
		int space = (int)(_width/DATA_COUNT);
		
		if(index<0){
			index = DATA_COUNT - 1;
		}
		
		_movingDataPath.reset();
		_movingDataPath.moveTo(_iniLeft + _width, _iniTop + _height/2 - (_height * (dataArray[index]/MAX_VALUE)));
		//Log.d("mine", "index: " + index + " value: " + dataArray[index]);
		for(int i = 0; i<DATA_COUNT-1 ; i++){
			
			index -=1;
			if(index<0)
				index = DATA_COUNT - 1;

			
			_movingDataPath.lineTo(_iniLeft + _width - space * (i+1), _iniTop + _height/2 - _height * dataArray[index]/MAX_VALUE);
		}
		_movingDataPath.lineTo(_iniLeft, _iniTop+_height/2);
		//_movingDataPath.lineTo(_iniLeft + _width, _iniTop+_height/2);
		//_movingDataPath.close();
		
		//draw
		canvas.save();
		canvas.clipRect(_iniLeft, _iniTop, _iniLeft + _width, _iniTop + _height);
		canvas.drawPath(_decorationPath, _decorationPaint);
		canvas.drawPath(_baseLinePath, _baseLinePaint);
		canvas.drawPath(_movingDataPath, _movingDataPaint);
		canvas.drawTextOnPath("acc. chart", _textPath, 0, 0, _textPaint);
		canvas.restore();
	}



	@Override
	public void onIsGPSFix(GpsStatus status, boolean isFix) {
		// TODO Auto-generated method stub
		
	}

}
