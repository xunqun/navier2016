package idv.xunqun.navier.parts;

import idv.xunqun.navier.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

public class Elem_GPSState extends Parts {
	
	public static final int ELEM_HEIGHT = 2;
	public static final int ELEM_WIDTH = 4;    	
	public static final int ELEM_PART = Parts.ELEM_GPSSTATE;
	public static String ELEM_NAME = "GPS state";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_gpsstate;
	

	//Part properties (must have)
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	
	private int _wordspace;
	
	
	//paths & paints
	private Path _textPath;
	private Path _digiPath;
	
	private Paint _textPaint;
	private Paint _digiPaint;
	
	//values
	private String _provider = "";
	private float _accuracy = -1;
	private String _showText = "Not Fix";
	private String _state = "Not Fix";

	public Elem_GPSState(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		initProperty();
		initPath();
		
	}

	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this._iniLeft + (this._width/2);
    	_centerY = this._iniTop + (this._height/2);
    	
    	_showText = "No GPS";;
    	
	}
	

	
	private void initPath(){
		
		Typeface font = _parent._defaultFont;

		
		
		
		_textPath = new Path();
		_textPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel/2);
		_textPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel/2);
		
		_textPaint = new Paint();
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setTypeface(font);
		_textPaint.setAlpha(100);
		_textPaint.setTextSize(_parent._unitPixel/2);
		
		
		_digiPath = new Path();
		_digiPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel*ELEM_HEIGHT);
		_digiPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel*ELEM_HEIGHT);
		
		_digiPaint = new Paint();
		_digiPaint.setColor(_parent.GLOBAL_COLOR);
		_digiPaint.setLinearText(true);
		_digiPaint.setTextAlign(Align.LEFT);
		_digiPaint.setTypeface(font);
		_digiPaint.setTextSize((float) (_parent._unitPixel*1.5));
		
		
	}
	
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		
		String text = "gps accur (M)";
		if(_provider =="" || !_provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER)){
			_showText = _state;
		}else{
			_showText = String.format("%.0f", _accuracy);
		}
		
		canvas.save();
		canvas.drawTextOnPath(text, _textPath, 0, 0, _textPaint);
		canvas.drawTextOnPath( _showText, _digiPath, 0, 0, _digiPaint);
		canvas.restore();
	}

	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub
		
		_provider = location.getProvider();
		_accuracy = location.getAccuracy();
		invalidate();

	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub
		_state = provider;
		invalidate();
	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		_textPaint.setColor(color);
		_textPaint.setAlpha(100);
		_digiPaint.setColor(color);
		invalidate();
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}

	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		//paths & paints
		Path textPath;
		Path digiPath;
		
		Paint textPaint;
		Paint digiPaint;
		
		Typeface font;
		try{
			font = Typeface.createFromAsset(context.getAssets(), "fonts/SquadaOne-Regular.ttf");
		}catch(Exception e){
			font = Typeface.DEFAULT;
		}
		
		textPath = new Path();
		textPath.moveTo( iniLeft+pin[0]*uniPixel,iniTop+uniPixel+pin[1]*uniPixel);
		textPath.lineTo( iniLeft+ELEM_WIDTH*uniPixel+pin[0]*uniPixel, iniTop+uniPixel+pin[1]*uniPixel);
		
		textPaint = new Paint();
		textPaint.setColor(Color.CYAN);
		textPaint.setLinearText(true);
		textPaint.setTextAlign(Align.LEFT);
		textPaint.setTypeface(font);
		textPaint.setTextSize(uniPixel/2);
		textPaint.setAlpha(100);
		
		
		digiPath = new Path();
		digiPath.moveTo( iniLeft+pin[0]*uniPixel,iniTop+uniPixel*ELEM_HEIGHT+pin[1]*uniPixel);
		digiPath.lineTo( iniLeft+ELEM_WIDTH*uniPixel+pin[0]*uniPixel, iniTop+uniPixel*ELEM_HEIGHT+pin[1]*uniPixel);
		
		digiPaint = new Paint();
		digiPaint.setColor(Color.CYAN);
		digiPaint.setLinearText(true);
		digiPaint.setTextAlign(Align.LEFT);
		digiPaint.setTypeface(font);
		digiPaint.setTextSize((float) (uniPixel));
		
		//
		
		String text = "gps accuracy(meters) ";
		
		canvas.save();
		canvas.drawTextOnPath(text, textPath, 0, 0, textPaint);
		canvas.drawTextOnPath( "12.5 m", digiPath, 0, 0, digiPaint);
		
		canvas.restore();
		
		
		
	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		ELEM_NAME = name;
	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onIsGPSFix(GpsStatus status,boolean isFix) {
		// TODO Auto-generated method stub
		
	}
}
