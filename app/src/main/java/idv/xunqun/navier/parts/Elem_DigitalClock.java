package idv.xunqun.navier.parts;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;

import java.util.Calendar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.Log;

public class Elem_DigitalClock extends Parts {

	
	//Default Value
	public static final int ELEM_HEIGHT = 2;
	public static final int ELEM_WIDTH = 4;    	
	public static final int ELEM_PART = Parts.ELEM_DIGITALCLOCK;
	public static String ELEM_NAME = "Digital Clock";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_digitalclock;
	
	
	GridBoard _parent;
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	private int _TEXTSIZE;
	private PreferencesHandler _pref;
	
	//paint & path
	private Path _ampmPath;
	private TextPaint _textPaint;
	private Path _hourPath;
	private Path _dashPath;
	private Path _minPath;
	private Paint _dashPaint;
	private String _h;
	private String _m;
	private long _timeCounter;

	private String _ampm;
	private long _lastTime = 0;
	private TextPaint _titlePaint;
	
	public Elem_DigitalClock(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		_parent = parent;
		_pref = new PreferencesHandler(parent._context);
		initProperty();
		initPath();
	}
	

	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this._iniLeft + (this._width/2);
    	_centerY = this._iniTop + (this._height/2);
    	_TEXTSIZE = (int) (_parent._unitPixel*1.5);
    	
	}
	
	
	private void initPath(){
		
		Typeface font = _parent._defaultFont;
		

		_titlePaint = new TextPaint();
		_titlePaint.setColor(_parent.GLOBAL_COLOR);
		_titlePaint.setTypeface(font);
		_titlePaint.setTextSize(_parent._unitPixel/2);
		_titlePaint.setAlpha(100);
		_titlePaint.setTextAlign(Align.CENTER);
		
		_textPaint = new TextPaint();
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setTypeface(font);
		
		_textPaint.setTextAlign(Align.CENTER);

		_dashPaint = new Paint();
		_dashPaint.setColor(_parent.GLOBAL_COLOR);
		_dashPaint.setTypeface(font);
		_dashPaint.setTextSize(_TEXTSIZE);
		_dashPaint.setTextAlign(Align.CENTER);

		_textPaint.setTextSize(_TEXTSIZE / 4);
		_ampmPath = new Path();
		_ampmPath.moveTo(0, (float) ( _parent._unitPixel));
		_ampmPath.lineTo(_titlePaint.measureText("AM"),
				(float) ( _parent._unitPixel));

		_textPaint.setTextSize(_TEXTSIZE);
		float dash_start = (ELEM_WIDTH) * _parent._unitPixel / 2
				- (_textPaint.measureText(":") / 2);
		float dash_end = (ELEM_WIDTH) * _parent._unitPixel / 2
				+ (_textPaint.measureText(":") / 2);

		_hourPath = new Path();
		_hourPath.moveTo(0, ELEM_HEIGHT * _parent._unitPixel);
		_hourPath.lineTo(dash_start, ELEM_HEIGHT * _parent._unitPixel);

		_dashPath = new Path();
		_dashPath.moveTo(dash_start, ELEM_HEIGHT * _parent._unitPixel);
		_dashPath.lineTo(dash_end, ELEM_HEIGHT * _parent._unitPixel);

		_minPath = new Path();
		_minPath.moveTo(dash_end, ELEM_HEIGHT * _parent._unitPixel);
		_minPath.lineTo((ELEM_WIDTH) * _parent._unitPixel, ELEM_HEIGHT
				* _parent._unitPixel);
		
	}
	

	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		long elapsed = 0;
		Calendar c = Calendar.getInstance();
		_h = String.format("%02.0f", (float) c.get(Calendar.HOUR_OF_DAY));
		_m = String.format("%02.0f", (float) c.get(Calendar.MINUTE));
		
		
		long now = System.currentTimeMillis();
		elapsed = now-_lastTime;
		_timeCounter += elapsed;
		
		if (!_pref.getPREF_24HOUR()) {
			int intH = Integer.valueOf(_h);
			_ampm = "AM";
			if (intH == 0) {
				_h = "12";
			}
			if (intH > 12) {
				intH = intH - 12;
				_h = String.format("%2d", intH);
				_ampm = "PM";
			}

		} else {
			_ampm = "";
		}

		
		if (_timeCounter > 1000) {
			if (_dashPaint.getAlpha() != 0) {
				_dashPaint.setAlpha(0);
			} else {
				_dashPaint.setAlpha(255);
			}
			_timeCounter = 0;
			
		}
		
		_lastTime  = now;
		
		
		
		// draw
		canvas.save();
		canvas.translate(_iniLeft, _iniTop);
		_textPaint.setTextSize(_TEXTSIZE);
		
		canvas.drawTextOnPath(_h, _hourPath, 0, 0, _textPaint);
		canvas.drawTextOnPath(_m, _minPath, 0, 0, _textPaint);
		canvas.drawTextOnPath(":", _dashPath, 0, 0, _dashPaint);
		_textPaint.setTextSize(_TEXTSIZE / 4);
		canvas.drawTextOnPath(_ampm, _ampmPath, 0, 0, _titlePaint);
		canvas.restore();
		
		invalidate();
	}


	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		_textPaint.setColor(color);
		_titlePaint.setColor(color);
		_titlePaint.setAlpha(100);
		_dashPaint.setColor(color);
		
		
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}

	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		//paint & path
		Path textPath;
		Paint textPaint;
		
		int TEXTSIZE = uniPixel*1;
		
		textPath = new Path();
		textPath.moveTo( iniLeft+pin[0]*uniPixel,iniTop+ELEM_HEIGHT*uniPixel+pin[1]*uniPixel);
		textPath.lineTo( iniLeft+(ELEM_WIDTH)*uniPixel+pin[0]*uniPixel, iniTop+ELEM_HEIGHT*uniPixel+pin[1]*uniPixel);
		
		textPaint = new Paint();
		textPaint.setColor(Color.CYAN);
		textPaint.setLinearText(true);
		textPaint.setTextAlign(Align.CENTER);
		
		Typeface font;
		try{
			font = Typeface.createFromAsset(context.getAssets(), "fonts/SquadaOne-Regular.ttf");
		}catch(Exception e){
			font = Typeface.DEFAULT;
		}
		
		textPaint.setTypeface(font);
		textPaint.setTextSize(TEXTSIZE);
		
		
		canvas.save();
		canvas.drawTextOnPath("14:56", textPath, 0, 0, textPaint);
		canvas.restore();
	}


	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		ELEM_NAME = name;
	}


	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onIsGPSFix(GpsStatus status,boolean isFix) {
		// TODO Auto-generated method stub
		
	}
}
