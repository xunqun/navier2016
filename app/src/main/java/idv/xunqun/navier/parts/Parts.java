package idv.xunqun.navier.parts;

import idv.xunqun.navier.R;
import idv.xunqun.navier.content.Layout;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Bundle;
import android.view.View;

public abstract class Parts extends View{
	

	//DEFAULT Values

	
	
	public static final int ELEM_COMPASS = 1;
	public static final int ELEM_SIMPLECOMPASS_TINY = 2;
	public static final int ELEM_COMAPSS_WHEEL = 3;
	
	
	public static final int ELEM_SPEED = 10;	
	public static final int ELEM_SPEED_DIGITAL = 11;	
	public static final int ELEM_SPEED_DIGITAL_LARGE=12;
	public static final int ELEM_AVERAGESPEED = 13;
	public static final int ELEM_SPEEDCHART = 14;
	public static final int ELEM_SPEEDCHART_LARGE = 15;
	public static final int ELEM_ACCELERATION_CHART = 16;
	public static final int ELEM_MAX_SPEED = 17;
	public static final int ELEM_ACCELERAT_DIGITAL = 18;
	
	public static final int ELEM_DIGITALCLOCK = 20;
	public static final int ELEM_TOTALTIME = 22;
	
	
	public static final int ELEM_DISTANCE = 30;
	public static final int ELEM_DISTANCELEFT = 31;
	
	
	public static final int ELEM_GPSSTATE = 40;
	public static final int ELEM_GPSCHART = 41;
	public static final int ELEM_GPSCHART_LARGE = 42;
	
	public static final int ELEM_BATTERY = 50;
	public static final int ELEM_BATTERY_DIGITAL = 51;
	
	public static final int ELEM_ROUTE = 200;
	public static final int ELEM_NEXTTURN = 201;
	public static final int ELEM_ROUTEPROGRESS = 202;
			
	public static final int PIN_WIDTH = 0;
	public static final int PIN_HEIGHT =1; 
	
	//Part properties (must have)
	
	public int ELEM_HEIGHT;
	public int ELEM_WIDTH;    	
	public int ELEM_PART;
	public int[] ELEM_PIN;
	
	//members
	public GridBoard  _parent;
	

	
	//abstract methods
	public abstract void onLocationChange(Location location);
	public abstract void onLocationStatusChange(String provider, int status, Bundle extras);
	public abstract void onLocationProviderDisable(String provider);
	public abstract void onIsGPSFix(GpsStatus status, boolean isFix);
	public abstract void onSensorChange(SensorEvent event);
	public abstract void onSensorChange(float[] orientation);
	public abstract void onGlobalColorChange(int mainColor, int sndColor);
	public abstract void onSpeedUnitChange(int speedUnit);
	public abstract void onPause();
	public abstract void onResume();
	public abstract void onInstanceSave(Bundle savedStates);
	public abstract void onInstanceRestore(Bundle savedStates);
	public abstract void setName(String name);
	
	//must add pin location properties
	public abstract void setELEM_PIN(int[] pin);

	
	public Parts(GridBoard parent, int[] pin) {
		super(parent._context);
		ELEM_PIN = pin;
		_parent = parent;
		
		// TODO Auto-generated constructor stub
	}
	
	
	
	/**
	 * Use for api9 and lower, whcih using View instead of SurfaceView.
	 * @param layoutArrangement
	 * @param p
	 * @return
	 * @throws JSONException
	 */
	public static ArrayList<Parts> createElemList(JSONArray layoutArrangement,GridBoard p) throws JSONException{
		
		ArrayList<Parts> list = new ArrayList<Parts>();
		
		for(int i=0; i<layoutArrangement.length();i++){
			
			JSONObject obj = (JSONObject) layoutArrangement.get(i);
			Parts profile = null;
			int[] pin = {obj.getJSONObject(Layout.JSON_PARTS_PIN).getInt("x"),obj.getJSONObject(Layout.JSON_PARTS_PIN).getInt("y")};
			
			switch(obj.getInt(Layout.JSON_PARTS_PART)){
			
				case ELEM_COMPASS:
					profile = new Elem_Compass(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_compass));
					//((Elem_Compass)profile).setELEM_PIN(pin);
					break;
				
				case ELEM_SIMPLECOMPASS_TINY:
					profile = new Elem_SimpleCompass_Tiny(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_simplecompass_tiny));
					break;
					
				case ELEM_COMAPSS_WHEEL:
					profile = new Elem_CompassWheel(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_compasswheel));
					break;

				case ELEM_ROUTE:
					profile = new Elem_Route(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_route));
					//((Elem_Route)profile).setELEM_PIN(pin);
					break;
				
				case ELEM_SPEED:
					profile = new Elem_Speed(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_speed));
					//((Elem_Speed)profile).setELEM_PIN(pin);
					break;
					
				case ELEM_SPEED_DIGITAL:
					profile = new Elem_SpeedDigital(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_speeddigital));
					break;
				case ELEM_SPEED_DIGITAL_LARGE:
					profile = new Elem_SpeedDigital_Large(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_speeddigital_large));
					break;
					
				case ELEM_SPEEDCHART:
					profile = new Elem_SpeedChart(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_speedchart));
					break;
					
				case ELEM_SPEEDCHART_LARGE:
					profile = new Elem_SpeedChart_Large(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_speedchart_large));
					break;	
				
				case ELEM_ACCELERATION_CHART:
					profile = new Elem_Acceleration_Chart(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_accchart));
					break;	
					
				case ELEM_MAX_SPEED:
					profile = new Elem_MaxSpeed(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_maxspeed));
					break;
					
				case ELEM_ACCELERAT_DIGITAL:
					profile = new Elem_AccDigital(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_accdigital));
					break;
					
				case ELEM_DIGITALCLOCK:
					profile = new Elem_DigitalClock(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_digitalclock));
					break;
					
				case ELEM_AVERAGESPEED:
					profile = new Elem_AverageSpeed(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_averagespeed));
					break;
					
				case ELEM_DISTANCE:
					profile = new Elem_TotalDistance(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_distance));
					break;
				
				case ELEM_DISTANCELEFT:
					profile = new Elem_DistanceLeft(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_distanceleft));
					break;
					
				case ELEM_TOTALTIME:
					profile = new Elem_TotalTime(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_totaltime));
					break;
					
				case ELEM_GPSSTATE:
					profile = new Elem_GPSState(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_gpsstate));
					break;
				
				case ELEM_GPSCHART:
					profile = new Elem_GPSChart(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_gpschart));
					break;
					
				case ELEM_GPSCHART_LARGE:
					profile = new Elem_GpsChart_Large(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_gpschart_large));
					break;
					
				case ELEM_BATTERY:
					profile = new Elem_Battery(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_battery));
					break;
					
				case ELEM_NEXTTURN:
					profile = new Elem_NextTurn(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_nextturn));
					break;

				case ELEM_ROUTEPROGRESS:
					profile = new Elem_RouteProgress(p, pin);
					profile.setName(profile.getResources().getString(R.string.part_routeprogress));
					break;
				
			}
			
			
			
			list.add(profile);
			
			
		}
			
		return list;
	}
	
	
	/**
	 * Use for api14 and upper, whcih using SurfaceView instead of View.
	 * @param layoutArrangement
	 * @param p
	 * @return
	 * @throws JSONException
	 */
	/*
	public static ArrayList<Parts> createElemList(JSONArray layoutArrangement, BaseGridBoard p) throws JSONException{
		
		ArrayList<Parts> list = new ArrayList<Parts>();
		
		for(int i=0; i<layoutArrangement.length();i++){
			
			JSONObject obj = (JSONObject) layoutArrangement.get(i);
			Parts profile = null;
			int[] pin = {obj.getJSONObject(Layout.JSON_PARTS_PIN).getInt("x"),obj.getJSONObject(Layout.JSON_PARTS_PIN).getInt("y")};
			
			switch(obj.getInt(Layout.JSON_PARTS_PART)){
			
			
				case ELEM_COMPASS:
					profile = new Elem_Compass(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_compass));
					//((Elem_Compass)profile).setELEM_PIN(pin);
					break;
				
				case ELEM_SIMPLECOMPASS_TINY:
					profile = new Elem_SimpleCompass_Tiny(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_simplecompass_tiny));
					break;
					
				case ELEM_COMAPSS_WHEEL:
					profile = new Elem_CompassWheel(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_compasswheel));
					break;

				case ELEM_ROUTE:
					profile = new Elem_Route(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_route));
					//((Elem_Route)profile).setELEM_PIN(pin);
					break;
				
				case ELEM_SPEED:
					profile = new Elem_Speed(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_speed));
					//((Elem_Speed)profile).setELEM_PIN(pin);
					break;
					
				case ELEM_SPEED_DIGITAL:
					profile = new Elem_SpeedDigital(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_speeddigital));
					break;
				case ELEM_SPEED_DIGITAL_LARGE:
					profile = new Elem_SpeedDigital_Large(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_speeddigital_large));
					break;
					
				case ELEM_SPEEDCHART:
					profile = new Elem_SpeedChart(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_speedchart));
					break;
					
				case ELEM_SPEEDCHART_LARGE:
					profile = new Elem_SpeedChart_Large(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_speedchart_large));
					break;	
				
				case ELEM_ACCELERATION_CHART:
					profile = new Elem_Acceleration_Chart(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_accchart));
					break;	
					
				case ELEM_MAX_SPEED:
					profile = new Elem_MaxSpeed(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_maxspeed));
					break;
					
				case ELEM_ACCELERAT_DIGITAL:
					profile = new Elem_AccDigital(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_accdigital));
					break;
					
				case ELEM_DIGITALCLOCK:
					profile = new Elem_DigitalClock(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_digitalclock));
					break;
					
				case ELEM_AVERAGESPEED:
					profile = new Elem_AverageSpeed(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_averagespeed));
					break;
					
				case ELEM_DISTANCE:
					profile = new Elem_TotalDistance(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_distance));
					break;
				
				case ELEM_DISTANCELEFT:
					profile = new Elem_DistanceLeft(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_distanceleft));
					break;
					
				case ELEM_TOTALTIME:
					profile = new Elem_TotalTime(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_totaltime));
					break;
					
				case ELEM_GPSSTATE:
					profile = new Elem_GPSState(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_gpsstate));
					break;
				
				case ELEM_GPSCHART:
					profile = new Elem_GPSChart(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_gpschart));
					break;
					
				case ELEM_GPSCHART_LARGE:
					profile = new Elem_GpsChart_Large(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_gpschart_large));
					break;
					
				case ELEM_BATTERY:
					profile = new Elem_Battery(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_battery));
					break;
					
				case ELEM_NEXTTURN:
					profile = new Elem_NextTurn(p,pin);
					profile.setName(profile.getResources().getString(R.string.part_nextturn));
					break;

				case ELEM_ROUTEPROGRESS:
					profile = new Elem_RouteProgress(p, pin);
					profile.setName(profile.getResources().getString(R.string.part_routeprogress));
					break;
					
				
			}
			
			
			
			list.add(profile);
			
			
		}
			
		return list;
	}
*/

}
