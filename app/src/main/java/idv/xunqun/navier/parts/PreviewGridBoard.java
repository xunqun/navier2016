package idv.xunqun.navier.parts;

import idv.xunqun.navier.content.Layout;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;

public class PreviewGridBoard extends View {


	public Context _context;
	public Layout _layout;
	private RelativeLayout rl;
	private String _name;
	private ArrayList<Partnail> _parts = new ArrayList<Partnail>();;
	
	public PreviewGridBoard(Context context, JSONObject json) {
		super(context);
		// TODO Auto-generated constructor stub
		_context = context;
		
		try {
			initLayout(json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	
	public void initLayout(JSONObject json) throws JSONException{
		
		
		JSONObject root = json.getJSONObject(Layout.JSON_ROOT);
		_name = root.getString(Layout.JSON_NAME);
		
		JSONArray list = json.getJSONArray(Layout.JSON_PARTS);
		
		for(int i=0; i<list.length();i++){
			JSONObject part = (JSONObject) list.get(i);
			Partnail nail = new Partnail();
			nail.PART = part.getString(Layout.JSON_PARTS_PART);
			nail.WIDTH = part.getJSONObject(Layout.JSON_PARTS_PIN).getInt("x");
			nail.HEIGHT = part.getJSONObject(Layout.JSON_PARTS_PIN).getInt("y");
			
			_parts.add(nail);
			
		}
		
		
	}

	public class Partnail{
		public String PART;
		public int WIDTH;
		public int HEIGHT;
		
	}


}
