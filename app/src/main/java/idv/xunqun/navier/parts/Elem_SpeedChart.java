package idv.xunqun.navier.parts;

import idv.xunqun.navier.R;
import idv.xunqun.navier.utils.Utility;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

public class Elem_SpeedChart extends Parts {

	
	public static final int ELEM_HEIGHT = 2;
	public static final int ELEM_WIDTH = 4;    	
	public static final int ELEM_PART = Parts.ELEM_SPEEDCHART;
	public static String ELEM_NAME = "Speed Chart";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_speedchart;
	
	/*
	 * savedInstancesState naming rule
	 * e.g. SAVEDSTATE_CLASSNAME_VALUENAME
	 */
	
	public static final String SAVEDSTATE_SPEEDCHART_CURSPEED = "SAVEDSTATE_SPEEDCHART_CURSPEED";
	public static final String SAVEDSTATE_SPEEDCHART_IDXHEAD = "SAVEDSTATE_SPEEDCHART_IDXHEAD";
	public static final String SAVEDSTATE_SPEEDCHART_DATAARRAY = "SAVEDSTATE_SPEEDCHART_DATAARRAY";
	public static final String SAVEDSTATE_SPEEDCHART_PRELOCATION = "SAVEDSTATE_SPEEDCHART_PRELOCATION";
	
	
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;

	//path and paint
	Path _textPath;
	Path _movingDataPath;
	Path _decorationPath;
	Path _maxValuePath;
	Path _alertPath;
	
	Paint _textPaint;
	Paint _movingDataPaint;
	Paint _linePaint;
	Paint _alartPaint;
	
	//values
	private final int DATA_COUNT = 50;
	private final int MAX_VALUE = 160;
	
	private Location _preLocation=null;
	private float _curSpeed = 0;
	private float _maxSpeed = 0;
	private int _idxHead = 0;
	private float[] dataArray = new float[DATA_COUNT];
	
	public Elem_SpeedChart(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		initProperty();
		initPath();
		new Thread(new MovingDataRunable()).start();
	}
	
	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	
	}
	

	
	private void initPath(){
		
		Typeface font = _parent._defaultFont;
		
		
		_textPath = new Path();
		_textPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel);
		_textPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel);

		_movingDataPath = new Path();
		

		
		_alertPath = new Path();
		_alertPath.moveTo(_iniLeft, _iniTop+_height-(_parent._settings.getPREF_ALERT_SPEED()/MAX_VALUE)*_height);
		_alertPath.lineTo(_iniLeft+_width, _iniTop+_height-(_parent._settings.getPREF_ALERT_SPEED()/MAX_VALUE)*_height);
		
		_maxValuePath = new Path();
		
		_textPaint = new Paint();
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setAlpha(100);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setTypeface(font);
		_textPaint.setTextSize(_parent._unitPixel/2);
		
		_movingDataPaint = new Paint();
		_movingDataPaint.setStyle(Style.FILL);
		_movingDataPaint.setColor(_parent.GLOBAL_COLOR);
		_movingDataPaint.setAlpha(200);
		_movingDataPaint.setAntiAlias(true);

		_linePaint = new Paint();
		_linePaint.setStyle(Style.STROKE);
		_linePaint.setStrokeWidth(_parent._unitPixel/32);
		_linePaint.setColor(_parent.GLOBAL_COLOR);
		
		_alartPaint = new Paint();
		_alartPaint.setStyle(Style.STROKE);
		_alartPaint.setStrokeWidth(_parent._unitPixel/32);
		_alartPaint.setColor(_parent.SECONDARY_COLOR);
		
	}
	
	
	private void genMovingDataPath(){
		
		int index = _idxHead-1;
		int space = (int)((_width/(DATA_COUNT-1))+1);
		
		if(index<0){
			index = DATA_COUNT - 1;
		}
		_maxSpeed =  dataArray[index];
		_movingDataPath.reset();
		_movingDataPath.moveTo(_iniLeft + _width, _iniTop + _height - (_height * (dataArray[index]/MAX_VALUE)));
		//Log.d("mine", "index: " + index + " value: " + dataArray[index]);
		for(int i = 0; i<DATA_COUNT-1 ; i++){
			
			index -=1;
			if(index<0)
				index = DATA_COUNT - 1;
			
			if(dataArray[index]>_maxSpeed)
				_maxSpeed = dataArray[index];
			
			_movingDataPath.lineTo(_iniLeft + _width - space * (i+1), _iniTop + _height - _height * dataArray[index]/MAX_VALUE);
		}
		_movingDataPath.lineTo(_iniLeft, _iniTop+_height);
		_movingDataPath.lineTo(_iniLeft + _width, _iniTop+_height);
		_movingDataPath.close();
		
	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		canvas.save();
		genMovingDataPath();
		canvas.clipRect(_iniLeft, _iniTop, _iniLeft+_width, _iniTop+_height);
		
		if(_curSpeed > _parent._settings.getPREF_ALERT_SPEED()){
			_movingDataPaint.setColor(_parent.SECONDARY_COLOR);
		}else{
			_movingDataPaint.setColor(_parent.GLOBAL_COLOR);
		}
		
		canvas.drawPath(_movingDataPath, _movingDataPaint);
		//canvas.drawPath(_decorationPath, _linePaint);
		
		_maxValuePath.reset();
		_maxValuePath.moveTo(_iniLeft, _iniTop+_height-(_curSpeed/MAX_VALUE*_height));
		_maxValuePath.lineTo(_iniLeft + _width, _iniTop+_height-(_curSpeed/MAX_VALUE*_height));
		canvas.drawPath(_maxValuePath, _linePaint);
		//canvas.drawPath(_alertPath, _alartPaint);
		canvas.drawTextOnPath("speed chart", _textPath, 0, 0, _textPaint);
		canvas.restore();
		invalidate();
	}

	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub
		if(location.getProvider().equals(LocationManager.GPS_PROVIDER) && location.getSpeed()!=0){
			_curSpeed = location.getSpeed()/1000*3600;
		}
		
		if(location.getProvider().equals(LocationManager.GPS_PROVIDER) && location.getSpeed()==0 && _preLocation!=null){
			_curSpeed = Utility.caculateSpeed(location, _preLocation)/1000*3600;			
		}
		
		//Log.d("mine", "o curSpeed: " + _curSpeed);
		_preLocation = location;
	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		_movingDataPaint.setColor(color);
		_linePaint.setColor(color);
		_textPaint.setColor(color);
		_alartPaint.setColor(sndColor);
		_textPaint.setAlpha(100);
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		savedStates.putFloat(SAVEDSTATE_SPEEDCHART_CURSPEED, _curSpeed);
		savedStates.putInt(SAVEDSTATE_SPEEDCHART_IDXHEAD, _idxHead);
		savedStates.putFloatArray(SAVEDSTATE_SPEEDCHART_DATAARRAY, dataArray);
		if(_preLocation!=null)
			savedStates.putParcelable(SAVEDSTATE_SPEEDCHART_PRELOCATION, _preLocation);

	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		if(savedStates.containsKey(SAVEDSTATE_SPEEDCHART_CURSPEED))
			_curSpeed = savedStates.getFloat(SAVEDSTATE_SPEEDCHART_CURSPEED);
		
		if(savedStates.containsKey(SAVEDSTATE_SPEEDCHART_IDXHEAD))
			_idxHead = savedStates.getInt(SAVEDSTATE_SPEEDCHART_IDXHEAD);
		
		if(savedStates.containsKey(SAVEDSTATE_SPEEDCHART_PRELOCATION))
			_preLocation = savedStates.getParcelable(SAVEDSTATE_SPEEDCHART_PRELOCATION);
		
		if(savedStates.containsKey(SAVEDSTATE_SPEEDCHART_DATAARRAY))
			dataArray = savedStates.getFloatArray(SAVEDSTATE_SPEEDCHART_DATAARRAY);
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}
	
	private class MovingDataRunable implements Runnable{

		
		private final int INTERVAL = 2000;
		
		
		
		public MovingDataRunable(){
			//initial array
			for(int i = 0; i < DATA_COUNT; i++){
				dataArray[i] = 0;
			}
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(true){
				try {
					Thread.sleep(INTERVAL);
					dataArray[_idxHead] = _curSpeed;
					_idxHead += 1;
					if(_idxHead >= DATA_COUNT)
						_idxHead = 0;
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		iniLeft += pin[0]*uniPixel;
		iniTop += pin[1]*uniPixel;
		int _width = uniPixel * ELEM_WIDTH;
		int _height = uniPixel * ELEM_HEIGHT;
		int DATA_COUNT = 100;
		int MAX_VALUE = 160;
		float[] dataArray = new float[DATA_COUNT];
		
		
		
		//path and paint
		Typeface font;
		try{
			font = Typeface.createFromAsset(context.getAssets(), "fonts/SquadaOne-Regular.ttf");
		}catch(Exception e){
			font = Typeface.DEFAULT;
		}
		
		Path _movingDataPath;
		
		Path _decorationPath;
		Path _maxValuePath;
		Path _alertPath;
		Path _textPath;
		
		Paint _movingDataPaint;
		Paint _linePaint;
		Paint _alartPaint;
		Paint _textPaint;
		
		_movingDataPath = new Path();
		
		_decorationPath = new Path();
		_decorationPath.addRect(iniLeft, iniTop, iniLeft+_width, iniTop+_height , Direction.CCW);
		

		
		_alertPath = new Path();
		_alertPath.moveTo(iniLeft, iniTop+_height-(100/MAX_VALUE)*_height);
		_alertPath.lineTo(iniLeft+_width, iniTop+_height-(100/MAX_VALUE)*_height);
		
		_maxValuePath = new Path();
		
		_movingDataPaint = new Paint();
		_movingDataPaint.setStyle(Style.FILL);
		_movingDataPaint.setColor(Color.CYAN);
		_movingDataPaint.setAlpha(200);
		_movingDataPaint.setAntiAlias(true);
		
		_linePaint = new Paint();
		_linePaint.setStyle(Style.STROKE);
		_linePaint.setStrokeWidth(uniPixel/32);
		_linePaint.setColor(Color.CYAN);
		
		_alartPaint = new Paint();
		_alartPaint.setStyle(Style.STROKE);
		_alartPaint.setStrokeWidth(uniPixel/32);
		_alartPaint.setColor(Color.RED);
		
		
		_textPath = new Path();
		_textPath.moveTo( iniLeft,iniTop+uniPixel/2);
		_textPath.lineTo( iniLeft+ELEM_WIDTH*uniPixel, iniTop+uniPixel/2);
		
		_textPaint = new Paint();
		_textPaint.setColor(Color.CYAN);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setTypeface(font);
		_textPaint.setTextSize(uniPixel/2);
		_textPaint.setAlpha(100);
		
		//generate data
		int data = 60;
		for(int i=0 ; i<DATA_COUNT ; i++){
			if(i<20){
				dataArray[i] = data;
				
			}
			
			if(i>=20 && i<50){
				dataArray[i] = data;
				data -= 0.7;
			}
			
			if(i>=50 && i<80){
				dataArray[i] = data;
				data += 1.3;
			}
			
			if(i>=80){
				dataArray[i] = data;
				data -= 2;
			}
		}
		
		
		//generate moving data path
		int _idxHead = 0;
		int index = _idxHead-1;
		float space = (_width/(DATA_COUNT-1))+1;
		
		if(index<0){
			index = DATA_COUNT - 1;
		}
		
		_movingDataPath.moveTo(iniLeft + _width, iniTop + _height - _height * dataArray[index]/MAX_VALUE);
		
		for(int i = 0; i<DATA_COUNT ; i++){
			
			index -=1;
			if(index<0)
				index = DATA_COUNT - 1;
			
			_movingDataPath.lineTo(iniLeft + _width - space * (i+1), iniTop + _height - _height * dataArray[index]/MAX_VALUE);
		}
		
		_movingDataPath.lineTo(iniLeft, iniTop+_height);
		_movingDataPath.lineTo(iniLeft + _width, iniTop+_height);
		_movingDataPath.close();
		

		
		canvas.save();
		canvas.clipRect(iniLeft, iniTop, iniLeft+_width, iniTop+_height);
		canvas.drawPath(_movingDataPath, _movingDataPaint);
		canvas.drawPath(_decorationPath, _linePaint);
		canvas.drawPath(_alertPath, _alartPaint);
		
		_maxValuePath.reset();
		_maxValuePath.moveTo(iniLeft, iniTop+_height-(80/MAX_VALUE)*_height);
		_maxValuePath.lineTo(iniLeft + _width, iniTop+_height-(80/MAX_VALUE)*_height);
		
		canvas.drawPath(_maxValuePath, _linePaint);
		canvas.drawTextOnPath("speed chart", _textPath, 0, 0, _textPaint);
		canvas.restore();
	}

	@Override
	public void onIsGPSFix(GpsStatus status,boolean isFix) {
		// TODO Auto-generated method stub
		
	}

}
