package idv.xunqun.navier.parts;

import idv.xunqun.navier.BaseNavierPanelContext;
import idv.xunqun.navier.NavierPanel;
import idv.xunqun.navier.NavigationManager;
import idv.xunqun.navier.R;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.content.Place;
import idv.xunqun.navier.content.RoutePlan;
import idv.xunqun.navier.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;


public class  NavigationGridBoard extends GridBoard {

	//	
	public static final int ELEM_HEIGHT = 6;
	public static final int ELEM_WIDTH = 14;    	
	public static final int[] ELEM_PIN = {1,1};
	
	//Dialog
	public static final int DIALOG_INIT = 0;
	public static final int DIALOG_TURN = 1;
	public static final int DIALOG_ARRIVAL = 2;
	public static final int DIALOG_REPLAN = 3;
	
	
	//Global properties
	
	
	//layout property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	private int _iniTextX;
	private int _iniTextY;
	private int _textWidth;
	
	
	public Location _currentBestLocation;
	
	private Context _context;
	
	
	public Place _destination;
	public Place _current;
	public String _raw_route = null; //retrun from direction API
	public boolean _isArrived = false;
	
	public boolean _firstInstructionShowed = false;
	public boolean _showFirstInstruction = false;
	public boolean _showTurnInstruction = false;
	public boolean _showArrival = false;
	public boolean _showDialog = false;
	public int _dialogType = DIALOG_INIT;
	public float _turnAngle=0;
	String _instruction;
	
	public NavigationManager _naviManager;
	
	
	//Path and Paint
	private Path _formPath;
	
	private Path _signPath;
	private Path _fromPath;
	
	private Paint _formPaint;
	private TextPaint _textPaint;
	private Paint _signPaint;
	private Paint _fromPaint;
	
	//Global values
	protected double _leftDistance;
	private Location _preLocation=null;
	private double _textSize;
	private boolean _isSpeedAlarmDuration = false;
	
	private long _speedAlarmDurationSec = 5000;
	private boolean _overspeed;
	private MediaPlayer mediaPlayer;
	private MediaPlayer speedAlarmPlayer;
	
	/**
	 * 
	 * @param context  Should be activity
	 * @param basepanel May a activity or fragment that implements the BaseNavierPanel
	 * @param jlayout
	 * @param handler
	 */
	
	public NavigationGridBoard(Context context, BaseNavierPanelContext basepanel , JSONObject layout, GridBoardHandler handler ) {
		super(context, basepanel,layout, handler);
		// TODO Auto-generated constructor stub
		_context = context;
		
		
		initProperties();
		initPath();
		
		if(_raw_route == null){		
			new HttpRequestTask().execute(new Latlng(_current.latitude,_current.longitude));
		}else{
			_naviManager = new NavigationManager(_destination,_context);
			
			try {
				if(_naviManager.initRoutePlan(_raw_route)){
				
					if(_naviManager.mRoutePlan!=null){
						try{
							
							//mRoute.setRoute(_naviManager.mRoutePlan);
							onRoutePlanningComplete(_naviManager.mRoutePlan);
							//Thread.sleep(1000);
							
						}catch(Exception e){
							new HttpRequestTask().execute(new Latlng(_current.latitude,_current.longitude));
						}
					}else{
						
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				new HttpRequestTask().execute(new Latlng(_current.latitude,_current.longitude));
				e.printStackTrace();
			}
		}
		

	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		/*
		if(_showTurnInstruction){
		
			drawTurnInstruction(canvas);
			
		}
		
		if(_showFirstInstruction){
			drawFirstInstruction(canvas);
		}
		
		if(_showArrival){
			drawArrival(canvas);
		}
		*/
		if(_showDialog){
			
			
			switch(_dialogType){
			case DIALOG_INIT:
				
				drawFirstInstruction(canvas);			
				break;
			case DIALOG_TURN:
				
				drawTurnInstruction(canvas);			
				break;
			case DIALOG_ARRIVAL:
				
				drawArrival(canvas);
				break;
			case DIALOG_REPLAN:
				
				drawReplan(canvas);	
				break;
			}
		}
	}


	

	@Override
	public void onSensorChangeHandler(SensorEvent event) {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onSensorChange(event);
		}
	}

	@Override
	public void onSensorChangeHandler(float[] orientation) {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onSensorChange(orientation);
		}
		
	}


	@Override
	public void onLocationChangeHandler(Location location) {
		// TODO Auto-generated method stub
		
    	
		if(!_naviManager.isPlanning){
			try {
				for(int i=0;i<_layout._parts_list.size();i++){
					Parts p = _layout._parts_list.get(i);
					p.onLocationChange(location);
				}
				
				if(_naviManager.isPlanning == false){
					runTimeDecision(location);
				}
				
				//check and play speed alarm clip
				playSpeedAlarm(location);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    		
    	

	}

	@Override
	public void onLocationStatusChangeHandle(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onLocationStatusChange(provider, status, extras);
		}
	}

	@Override
	public void onLocationProviderDisableHandle(String provider) {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onLocationProviderDisable(provider);
		}
	}
	
	@Override
	public void onGlobalColorChange(int color) {
		// TODO Auto-generated method stub
		_signPaint.setColor(color);
		
		_fromPaint.setColor(color);
		_formPaint.setAlpha(200);
		
		_textPaint.setColor(color);
		
		_gridPaint.setColor(color);
		_gridPaint.setAlpha(0x50);
		
		if(GLOBAL_COLOR == 0xffff4500){
			SECONDARY_COLOR = Color.WHITE;
		}else{
			SECONDARY_COLOR = Color.RED;
		}
		
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onGlobalColorChange(color, SECONDARY_COLOR);
		}
	}
	

	@Override
	public void onSpeedUnitChangeHandler(int sunit) {
		// TODO Auto-generated method stub
		this.GLOBAL_SPEEDUNIT = sunit;
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onSpeedUnitChange(sunit);
		}
	}

	private void Log(String msg){
		Log.d("parts", msg);
	}
	
	private void playSpeedAlarm(Location location){
		
		_overspeed = Utility.meters2kmh(location.getSpeed()) > _settings.getPREF_ALERT_SPEED();
		
		if(!_isSpeedAlarmDuration && _overspeed ){
		
			try{
				if(_settings.getPREF_NOTIFY_SOUND()){
					_isSpeedAlarmDuration = true;
					speedAlarmPlayer.start();
					new CountDownTimer(_speedAlarmDurationSec, _speedAlarmDurationSec){

						@Override
						public void onFinish() {
							// TODO Auto-generated method stub
							_isSpeedAlarmDuration = false;
						}

						@Override
						public void onTick(long millisUntilFinished) {
							// TODO Auto-generated method stub
							
						}
						
					}.start();
				}
			}catch(Exception e){
				
			}
		}
		

	}

	private void playSound(){
		try{
			
			if(_settings.getPREF_NOTIFY_SOUND()){
				mediaPlayer.start();
			}
		}catch(Exception e){
			
		}
	}
	
	private void initPath(){
		
		int leftCenterX = (int) (_iniLeft + 2.5*_unitPixel);
		int leftCenterY = _iniTop + _height/2;
		int signWidth = (int) (_unitPixel*0.7);
		
		_textSize = this._unitPixel*0.8;
		
		_formPath = new Path();
		_formPath.addRoundRect(new RectF(_iniLeft,_iniTop,_iniLeft+_width,_iniTop+_height), 10, 10, Direction.CCW);
		
		
		_signPath = new Path();
		_signPath.moveTo(leftCenterX-signWidth/2, leftCenterY);
		_signPath.lineTo(leftCenterX-signWidth/2, leftCenterY-signWidth*2);
		_signPath.lineTo(leftCenterX-signWidth, leftCenterY-signWidth*2);
		_signPath.lineTo(leftCenterX, leftCenterY-3*signWidth);
		_signPath.lineTo(leftCenterX+signWidth, leftCenterY-signWidth*2);
		_signPath.lineTo(leftCenterX+signWidth/2, leftCenterY-signWidth*2);
		_signPath.lineTo(leftCenterX+signWidth/2, leftCenterY);
		_signPath.close();
		
		_fromPath = new Path();
		_fromPath.moveTo(leftCenterX-signWidth/2, leftCenterY);
		_fromPath.lineTo(leftCenterX-signWidth/2, leftCenterY+signWidth*3);
		_fromPath.lineTo(leftCenterX+signWidth/2, leftCenterY+signWidth*3);
		_fromPath.lineTo(leftCenterX+signWidth/2, leftCenterY);
		_fromPath.close();
		_fromPath.addCircle(leftCenterX, leftCenterY, signWidth/2, Direction.CCW);
		
		//paint
		
		_formPaint = new Paint();
		
		
		_formPaint.setStrokeWidth(signWidth/4);
		
		
		
		_signPaint = new Paint();
		_signPaint.setColor(GLOBAL_COLOR);
		_signPaint.setStyle(Style.FILL);
		_signPaint.setStrokeCap(Cap.ROUND);
		_signPaint.setAntiAlias(true);
		
		_fromPaint = new Paint();
		_fromPaint.setColor(GLOBAL_COLOR);
		_fromPaint.setStyle(Style.FILL);
		_fromPaint.setStrokeCap(Cap.ROUND);
		
		
		
		//text paint
		_textPaint = new TextPaint();
		_textPaint.setColor(GLOBAL_COLOR);
		_textPaint.setTextSize((float) (_textSize));
		_textPaint.setAntiAlias(true);
		
		
		
	}
	

	
	private void drawMessageFrame(Canvas canvas){
		
		_formPaint.setStyle(Style.FILL);
		_formPaint.setColor(Color.BLACK);
		_formPaint.setAlpha(200);
		_formPaint.setAntiAlias(true);
		canvas.drawPath(_formPath, _formPaint);
		_formPaint.setStyle(Style.STROKE);
		_formPaint.setColor(GLOBAL_COLOR);
		
		canvas.drawPath(_formPath, _formPaint);
		
		
	}
	
	private void drawTurnInstruction(Canvas canvas){
		

		drawMessageFrame(canvas);
		canvas.drawPath(_fromPath, _fromPaint);
		
		canvas.save();
		
		_signPaint.setStyle(Style.FILL);
		canvas.rotate(_turnAngle,(int) (_iniLeft + 2.5*_unitPixel),_iniTop + _height/2);
		canvas.drawPath(_signPath, _signPaint);
		_signPaint.setStyle(Style.STROKE);
		canvas.drawPath(_signPath, _signPaint);
		
		
		
		
		
		canvas.restore();
		
		canvas.save();
		canvas.clipRect(new RectF(_iniLeft, _iniTop, _iniLeft+_width, _iniTop+_height));
		try{
			drawMessage(_instruction,canvas);
		}catch(Exception e){
			
		}
		canvas.restore();
		
		
	}
	
	private String textlayout(String text){
		
		text.replaceAll("<div", "<b");
		text.replaceAll("</div", "</b");
		
		
		
		return Html.fromHtml(text).toString();
		
	}
	
	private void drawFirstInstruction(Canvas canvas){
		
		canvas.save();
		drawMessageFrame(canvas);
		//drawMessage(_instruction,canvas);
		canvas.clipRect(new RectF(_iniLeft, _iniTop, _iniLeft+_width, _iniTop+_height));
		canvas.translate(_iniLeft+_unitPixel, _iniTop+_unitPixel);		
		new StaticLayout(textlayout(_instruction), _textPaint, _width - 2*_unitPixel, Alignment.ALIGN_CENTER, 1.2f, 1f ,true).draw(canvas);
		
		canvas.restore();
	}
	
	private void drawReplan(Canvas canvas){
		
		canvas.save();
		drawMessageFrame(canvas);
		canvas.clipRect(new RectF(_iniLeft, _iniTop, _iniLeft+_width, _iniTop+_height));
		canvas.translate(_iniLeft+_unitPixel, _iniTop+_unitPixel);			
		new StaticLayout(textlayout(_instruction), _textPaint, _width - 2*_unitPixel, Alignment.ALIGN_CENTER, 1.2f, 1f ,true).draw(canvas);
		canvas.restore();
	}
	
	private void drawMessage(String s,Canvas canvas)throws Exception{
		canvas.save();
		canvas.clipRect(new RectF(_iniTextX, _iniTextY, _iniTextX+_textWidth, _iniTextY+_textWidth-_unitPixel));
		canvas.translate(_iniTextX, _iniTextY);
		
		new StaticLayout(textlayout(s), _textPaint, _textWidth, Alignment.ALIGN_CENTER, 1.2f, 1f ,true).draw(canvas);
		canvas.restore();
	}
	
	
	private void runTimeDecision(Location location){
		
		Latlng curr = new Latlng(location.getLatitude(),location.getLongitude());
		
		
		if(!_firstInstructionShowed){
			// show first instruction
			
			//showFirstInstruction();
			showDialog(DIALOG_INIT);
		}
		
		
		
		if(_isArrived){
			
		}else{
			
			if(_naviManager.isNeedRoutePlan(curr)){
				Log.d("mine", "Need route plan");
				//if(mNavManager.directionRequest(curr)){	
				showDialog(DIALOG_REPLAN);
				new HttpRequestTask().execute(curr);	
					
				
			}else{
				Log.d("mine", "No need route plan");
				if(_naviManager.isArrived(curr)){
					//NotifyArrival
					_isArrived = true;
					notifyArrival();
					showDialog(DIALOG_ARRIVAL);
				}else{
					
					if(_naviManager.mInToleratePhase == false){
						
						_leftDistance = _naviManager.distanceOfLeftRpoint(curr);	
						//double totalDistance = _naviManager.mRoutePlan.arr_steps.get(_naviManager.mRoutePlan._current_step).distance_value;
						
						
						//double alarmDistance = _naviManager.getAlarmDistance(curr, location.getSpeed());
						double alarmDistance;
						if( location.getSpeed()!=0){
							alarmDistance = _naviManager.getAlarmDistance(curr, location.getSpeed());
						}else{
							if(_preLocation!=null){
								alarmDistance = _naviManager.getAlarmDistance(curr, Utility.caculateSpeed(location, _preLocation));
							}else{
								alarmDistance = _naviManager.getAlarmDistance(curr, 0);
							}
						}
						_preLocation = location;
						//Show leftDistance here
						notifyLeftDistance(_leftDistance, alarmDistance);
						
						if(_leftDistance < alarmDistance){
							// do next step notification
							if(!_naviManager.mAlarmShowed_100[_naviManager.mRoutePlan._current_step]){
								_naviManager.mAlarmShowed_100[_naviManager.mRoutePlan._current_step]=true;
								//showTurnInstruction( );
								showDialog(DIALOG_TURN);
							}							
						}//End turn alarm						
					}//End tolerate phase	
				}//End checking arrival or not
			}
			broadcastLockPosition();
		}//When arrival do no decision
	}
	
	private void broadcastLockPosition(){
		for(int i=0;i<_layout._parts_list.size();i++){
			
			Parts p = _layout._parts_list.get(i);
			
			// only called objects that are instance of NaviPart
			
			if(p instanceof NaviParts && _naviManager._perpen_point != null){
				((NaviParts) p).onLocationOnRoadChange(_naviManager._perpen_point, _naviManager.getRoadAngle());
			}
		}
			if(_naviManager._perpen_point != null){
				Log.d("mine", "perpen: (" + _naviManager._perpen_point.getLat() + " , " + _naviManager._perpen_point.getLng() + ")" + " Angle: " + _naviManager.getRoadAngle());
			}
			
	}
	
	private void notifyLeftDistance(double distance , double alarmDistance){
		for(int i=0;i<_layout._parts_list.size();i++){
			
			Parts p = _layout._parts_list.get(i);
			
			// only called objects that are instance of NaviPart
			
			if(p instanceof NaviParts){
				((NaviParts) p).onLeftDistanceNotify(distance, alarmDistance);
			}
		}
	}
	

	
	private void drawArrival(Canvas canvas){
		
		canvas.save();
		drawMessageFrame(canvas);		
		_instruction = this.getResources().getString(R.string.navierpanel_arrival);
		//drawMessage(_instruction,canvas);
		canvas.clipRect(new RectF(_iniLeft, _iniTop, _iniLeft+_width, _iniTop+_height));
		canvas.translate(_iniLeft+_unitPixel, _iniTop+_unitPixel);		
		new StaticLayout(textlayout(_instruction), _textPaint, _width - 2*_unitPixel, Alignment.ALIGN_CENTER, 1.2f, 1f ,true).draw(canvas);
		
		canvas.restore();
	}
	

	
	private void showDialog(int type){
		
		int finish = 10000;
		int tick=10000;
		
		
		
		_dialogType = type;
		
		switch(type){
		case DIALOG_INIT:
			playSound();
			_instruction = _naviManager.getInitTrunIntruction();
			_firstInstructionShowed = true;			
			_baseNavierPanel.doTTS(Html.fromHtml(_instruction).toString());
			
			
			break;
		case DIALOG_TURN:
			playSound();
			_instruction = _naviManager.getTrunIntruction();
			_turnAngle = (float) _naviManager.getNextTurnAngle();
			_baseNavierPanel.doTTS(Html.fromHtml(_instruction).toString());
			
			break;
		case DIALOG_ARRIVAL:
			playSound();
			_instruction = _context.getString(R.string.navierpanel_arrival);			
			_baseNavierPanel.doTTS(Html.fromHtml(_instruction).toString());
			
			break;
		case DIALOG_REPLAN:
			
			finish = 5000;
			_instruction = _context.getString(R.string.navierpanel_replan);			
			_baseNavierPanel.doTTS(Html.fromHtml(_instruction).toString());
			
			break;
		}
		
		_showDialog = true;
		new CountDownTimer( finish, tick){

			int count = 0;
			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				_showDialog = false;
			}
			
		}.start();
	}
	

	

	private void initProperties() {
		// TODO Auto-generated method stub
		_destination = _baseNavierPanel.getDestPlace();
		_current = _baseNavierPanel.getCurrPlace();
		
		if(_baseNavierPanel.getRowRoute()!=null){
			_raw_route = _baseNavierPanel.getRowRoute();
		}
		
		_width = _unitPixel * ELEM_WIDTH;
		_height = _unitPixel * ELEM_HEIGHT;
		_iniTop = _screenHeightMargin + ELEM_PIN[Parts.PIN_HEIGHT]*_unitPixel;
		_iniLeft = _screenWidthMargin + ELEM_PIN[Parts.PIN_WIDTH]*_unitPixel;
    	_centerX = _iniLeft + (this._width/2);
    	_centerY = _iniTop + (this._height/2);
    	
    	_iniTextX = _iniLeft + 5*_unitPixel;
    	_iniTextY = _iniTop + 20;
		_textWidth = 9*_unitPixel;
		
		mediaPlayer = MediaPlayer.create(_context, R.raw.bip1);
		speedAlarmPlayer = MediaPlayer.create(_context, R.raw.cowbell);
         
	}
	
	

	
	private void onRoutePlanningComplete(RoutePlan r){
		
		for(int i=0;i<_layout._parts_list.size();i++){
			
			Parts p = _layout._parts_list.get(i);
			
			// only called objects that are instance of NaviPart
			
			if(p instanceof NaviParts){
				((NaviParts) p).onRouteReplan(r);
			}
		}
	}
	
	private void notifyArrival(){
		for(int i=0;i<_layout._parts_list.size();i++){
			
			Parts p = _layout._parts_list.get(i);
			
			// only called objects that are instance of NaviPart
			
			if(p instanceof NaviParts){
				((NaviParts) p).onArrival();
			}
		}
	}
	


	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onPause();
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onResume();
		}
	}

	//asyncTask
	private class HttpRequestTask extends AsyncTask{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			
			_naviManager.isPlanning = true;
			
		}

		@Override
		protected void onPostExecute(Object result) {
			// TODO Auto-generated method stub
			_naviManager.isPlanning=false;

		}

		@Override
		protected Object doInBackground(Object... curr) {
			// TODO Auto-generated method stub
			
			
			
			_naviManager = new NavigationManager(_destination,_context);
			_naviManager.directionRequest((Latlng)curr[0]);
			Log.d("mine", "task complete");
			if(_naviManager.mRoutePlan!=null){
				try{
					
					//mRoute.setRoute(_naviManager.mRoutePlan);
					onRoutePlanningComplete(_naviManager.mRoutePlan);
					Thread.sleep(1000);
								
				}catch(Exception e){
					
				}
			}
			
			return null;
		}
		
	}

	@Override
	public void onIsGPSFix(GpsStatus status,Boolean isfix) {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onIsGPSFix(status, isfix);
		}
	}

}
