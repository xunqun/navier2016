package idv.xunqun.navier.parts;


import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.SpeedUnit;
import idv.xunqun.navier.utils.Utility;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Path;
import android.graphics.RectF;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;

public class Elem_Speed extends Parts {

	//Default Value
	public static final int ELEM_HEIGHT = 8;
	public static final int ELEM_WIDTH = 8;    	
	public static final int ELEM_PART = Parts.ELEM_SPEED;
	public static String ELEM_NAME = "Speedometer";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_speedometer;
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	
	//path & paint
    private Paint mPaint = new Paint();
    private Paint mInnerPaint = new Paint();
    private Paint mEndPaint = new Paint();
    private Paint mTextPaint = new Paint();
    private Paint mDotPaint = new Paint();
    
    
    private Path mInnerPath = new Path();
    private Path mEndPath = new Path();
    private Path mAlertPath = new Path();
    private Path mDotPath = new Path();
    
    // properties
    public static int MAX_PROGRESS_45=45;
    public static int MAX_PROGRESS_135=135;
    public static int MAX_PROGRESS_225=225;
    public static int MAX_PROGRESS_315=315;
    private int PROGRESS_WIDTH=40;
	private int MAX_SPEED = 160;
	private int ALERT_SPEED = 100 ;
	public static final long TIMER_TOTAL = 1000;
	public static final long TIMER_TICK = 100;
    
	//Global value
	int _radius;
	float _speed=0;
	private CountDownTimer _timer;	
	private float _animationStep;
	private int _animationCount;
	private float _oldSpeed;
	private PreferencesHandler _setting;
	private Location _preLocation=null;
	private Path mIndicator;
    
	public Elem_Speed(GridBoard p, int[] pin) {
		super(p,pin);
		_setting = new PreferencesHandler(p._context);
		initProperty();
		initPath();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		Path mPath = new Path();
		mPath.addArc(new RectF(0-_radius+PROGRESS_WIDTH , 0-_radius+PROGRESS_WIDTH , _radius-PROGRESS_WIDTH , _radius-PROGRESS_WIDTH), 270+225, (float) (_speed*270/MAX_SPEED));
        
        canvas.translate(_centerX, _centerY);
        mDotPaint.setColor(_parent.GLOBAL_COLOR);
        canvas.drawPath(mDotPath, mDotPaint);
        mDotPaint.setColor(_parent.SECONDARY_COLOR);
        canvas.drawPath(mAlertPath, mDotPaint);
        
        if(_speed >= _setting.getPREF_ALERT_SPEED()){
        	mPaint.setColor(_parent.SECONDARY_COLOR);
        }else{
        	mPaint.setColor(_parent.GLOBAL_COLOR);
        }
        canvas.drawPath(mPath, mPaint);
        canvas.drawPath(mInnerPath,mInnerPaint);
        canvas.drawPath(mEndPath,mEndPaint);
        
	}

	

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub
        float unit = 10;
        if(_parent.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH){
        	unit = 10;
        }else{
        	unit = SpeedUnit.mph2kmh(10);
        }
        
        mDotPath.reset();
        for(int i=0;i<MAX_SPEED;i+=unit){
        	mDotPath.addArc(new RectF(0-_radius+PROGRESS_WIDTH+PROGRESS_WIDTH/6,0-_radius+PROGRESS_WIDTH+PROGRESS_WIDTH/6,_radius-PROGRESS_WIDTH-PROGRESS_WIDTH/6,_radius-PROGRESS_WIDTH-PROGRESS_WIDTH/6), 270+225+(i*270/MAX_SPEED),1  );
        }
        
        
        
	}

	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub
		
		if(location.getProvider().equals(LocationManager.GPS_PROVIDER) && location.getSpeed()!=0){
			_oldSpeed = _speed;
			
			 animSpeed(location.getSpeed());
			 
		}
		
		if(location.getProvider().equals(LocationManager.GPS_PROVIDER) && location.getSpeed()==0 && _preLocation!=null){
			_oldSpeed = _speed;
			float speed = Utility.caculateSpeed(location, _preLocation);
			
			animSpeed(speed);
		}
		_preLocation = location;
		invalidate();
		
	}

	private void animSpeed(float speed){
		_animationStep = ((speed/1000*3600)-_oldSpeed)/(TIMER_TOTAL/TIMER_TICK);
		_animationCount = 1;
		 
		 _timer = new CountDownTimer(TIMER_TOTAL, TIMER_TICK) {

		     public void onTick(long millisUntilFinished) {
		    	 stepUP();
		         
		     }

		     public void onFinish() {
		    	 //stepUP();
		     }
		     
		     public void stepUP(){
		    	 _speed += _animationStep;
		         _animationCount+=1;
		         if(_speed>MAX_SPEED)_speed = MAX_SPEED;
		     }
		  };
		 _timer.start();
	}
	
	
	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub
		ELEM_PIN = pin;
	}

	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this._iniLeft + (this._width/2);
    	_centerY = this._iniTop + (this._height/2);
    	PROGRESS_WIDTH = (int) (_parent._unitPixel/1.5);
    	
	}
	
	private void initPath(){

		_radius = _width/2;
		
		//progress paint
        mPaint.setAntiAlias(true);
        mPaint.setColor(_parent.GLOBAL_COLOR);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(this.PROGRESS_WIDTH);
        mPaint.setStrokeCap(Cap.BUTT);
        
		//inner circle paint
        mInnerPaint.setAntiAlias(true);
        mInnerPaint.setColor(_parent.GLOBAL_COLOR);
        mInnerPaint.setStyle(Paint.Style.STROKE);
        mInnerPaint.setStrokeWidth(_parent._unitPixel/32);
        
        //mInnerPath.addCircle(0, 0, (float) (_radius-PROGRESS_WIDTH), Direction.CCW);
        mInnerPath.addArc(new RectF(0-_radius+PROGRESS_WIDTH,0-_radius+PROGRESS_WIDTH,_radius-PROGRESS_WIDTH,_radius-PROGRESS_WIDTH), 270+225, 270);
        //endPaint
        
        mEndPaint.setColor(_parent.GLOBAL_COLOR);
        mEndPaint.setStyle(Paint.Style.STROKE);
        mEndPaint.setStrokeWidth(this.PROGRESS_WIDTH);
		
        //text paint
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setStyle(Paint.Style.STROKE);
        
        mTextPaint.setTextSize(_radius/4);
        mTextPaint.setTypeface(_parent._defaultFont);
        mTextPaint.setAlpha(100);
        mTextPaint.setTextAlign(Align.CENTER);
        
        
        
        
        mEndPath.addArc(new RectF(0-_radius+PROGRESS_WIDTH,0-_radius+PROGRESS_WIDTH,_radius-PROGRESS_WIDTH,_radius-PROGRESS_WIDTH), 270+225,5);
        mEndPath.addArc(new RectF(0-_radius+PROGRESS_WIDTH,0-_radius+PROGRESS_WIDTH,_radius-PROGRESS_WIDTH,_radius-PROGRESS_WIDTH), 270+130,5);
        mAlertPath.addArc(new RectF(0-_radius+PROGRESS_WIDTH+PROGRESS_WIDTH/6,0-_radius+PROGRESS_WIDTH+PROGRESS_WIDTH/6,_radius-PROGRESS_WIDTH-PROGRESS_WIDTH/6,_radius-PROGRESS_WIDTH-PROGRESS_WIDTH/6), 270+225+(_setting.getPREF_ALERT_SPEED()),5);
        
        
        float unit = 10;
        if(_parent.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH){
        	unit = 10;
        }else{
        	unit = SpeedUnit.mph2kmh(10);
        }
        
        for(int i=0;i<MAX_SPEED;i+=unit){
        	mDotPath.addArc(new RectF(0-_radius+PROGRESS_WIDTH+PROGRESS_WIDTH/6,0-_radius+PROGRESS_WIDTH+PROGRESS_WIDTH/6,_radius-PROGRESS_WIDTH-PROGRESS_WIDTH/6,_radius-PROGRESS_WIDTH-PROGRESS_WIDTH/6), 270+225+(i*270/MAX_SPEED),1  );
        }
        mDotPaint.setColor(_parent.GLOBAL_COLOR);
        mDotPaint.setStyle(Paint.Style.STROKE);
        mDotPaint.setStrokeWidth(this.PROGRESS_WIDTH/3);
        
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		 mDotPaint.setColor(color);
		 mPaint.setColor(color);
		 mInnerPaint.setColor(color);
		 mEndPaint.setColor(color);
		 
		 
	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		int width = uniPixel * ELEM_WIDTH;		
		int radius = width/2;
		int progressWidth = 40;
		int maxSpd = 160;
		
		Path innerPath = new Path();
		Path endPath = new Path();
		Path dotPath = new Path();
		
		innerPath.addArc(new RectF(0-radius+progressWidth,0-radius+progressWidth,radius-progressWidth,radius-progressWidth), 270+225, 270);
		
		endPath.addArc(new RectF(0-radius+progressWidth,0-radius+progressWidth,radius-progressWidth,radius-progressWidth), 270+225,5);
        endPath.addArc(new RectF(0-radius+progressWidth,0-radius+progressWidth,radius-progressWidth,radius-progressWidth), 270+130,5);
        
        for(int i=0;i<maxSpd;i+=10){
        	dotPath.addArc(new RectF(0-radius+progressWidth+progressWidth/6,0-radius+progressWidth+progressWidth/6,radius-progressWidth-progressWidth/6,radius-progressWidth-progressWidth/6), 270+225+(i*270/maxSpd),1  );
        }
        
        Paint paint = new Paint();
        
        paint.setColor(Color.CYAN);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        paint.setAntiAlias(true);
        
        canvas.save();
        canvas.translate(iniLeft+width/2+pin[0]*uniPixel, iniTop+width/2+pin[1]*uniPixel);
        canvas.drawPath(innerPath, paint);
        canvas.drawPath(endPath, paint);
        canvas.drawPath(dotPath, paint);
        canvas.restore();
	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		ELEM_NAME = name;
	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onIsGPSFix(GpsStatus status,boolean isFix) {
		// TODO Auto-generated method stub
		
	}
}
