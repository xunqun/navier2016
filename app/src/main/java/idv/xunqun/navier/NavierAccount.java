package idv.xunqun.navier;

import idv.xunqun.navier.http.MylayoutCheckRequester;
import idv.xunqun.navier.utils.Utility;
import idv.xunqun.navier.utils.http.NetworkState;
import idv.xunqun.navier.v2.HomeActivity;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class NavierAccount extends ListActivity {

	//dialog
	private static final int DIALOG_SYNC_PROGRESS = 1;
	private static final int DIALOG_FREE_VERSION = 0;
	
	private AccountManager _accountManager;
	private SimpleAdapter _listAdapter;
	private String[] _strAccounts;
	private ListView _lv;
	private PreferencesHandler _settings;
	private ArrayList<HashMap<String,Object>> _list = new ArrayList<HashMap<String,Object>>();
	private Account[] accounts;
	private SQLiteDatabase db = null;
	private MyDBOpenHelper helper;
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		//Check version and provide ad free layout in version Premium
		if(Version.isLite(this)){
			setContentView(R.layout.account);
		}else{
			setContentView(R.layout.account_4premium);
		}
		
		initProperties();
		
		getAccountList();

		setListAdapter(_listAdapter);
		
	}
	

	
	





	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//Utility.checkLanscapeOrientation(this);
		Utility.requestLocationServiceUpdate(this);
	}









	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Utility.requestLocationServiceStop(this);
	}









	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		Dialog dialog = null;
		switch(id){
		
		case DIALOG_SYNC_PROGRESS:
			dialog = ProgressDialog.show(NavierAccount.this, "", "Data syncing...", false);
			break;		
		case DIALOG_FREE_VERSION:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getString(R.string.navieraccount_freeversion))
			       .setCancelable(false)
			       .setPositiveButton(getString(R.string.navieraccount_upgradebtn), new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			               //link to google play 
			        	   Intent it = new Intent(Intent.ACTION_VIEW); 
			        	   it.setData(Uri.parse("market://details?id=idv.xunqun.navier.premium"));
			        	   startActivity(it);
			           }
			       })
			       .setNegativeButton(getString(R.string.navieraccount_cancelbtn), new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			                dialog.cancel();
			           }
			       });
			dialog = builder.create();
		}
		return dialog;
	}



	private void initProperties(){
		_accountManager = AccountManager.get(this);
		_settings = new PreferencesHandler(this);
		_listAdapter = new SimpleAdapter(this,_list,R.layout.menu_list_item,new String[]{"name","type"},new int[]{R.id.name,R.id.type});
		
		
		_lv = getListView();
		_lv.setTextFilterEnabled(true);	
		_lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
		        int position, long id) {
		      // When clicked, show a toast with the TextView text
				
				//check edition
				if(Version.isLite(NavierAccount.this)){
					
					showDialog(DIALOG_FREE_VERSION);
					
				}else{
					
					if(_strAccounts[position].equalsIgnoreCase(_settings.getPREF_ACCOUNT())){				
						
//						if(_settings.getPREF_DATAINITIAL()==true){
//																
//							//doSync();
//						}else{
//							//doAccountChange();
//						}
					}else{
						if(NetworkState.checkNetworkConnected(NavierAccount.this)){
							
							_settings.setPREF_DATAINITIAL(false);
							_settings.setPREF_ACCOUNT(_strAccounts[position]);	
							Intent it = new Intent(NavierAccount.this, HomeActivity.class);
							it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(it);
							
							getAccountList();	
							_listAdapter.notifyDataSetChanged();
							
						}else{
							
							Toast.makeText(NavierAccount.this, getString(R.string.naviermap_connection), Toast.LENGTH_SHORT).show();
						}
						//doAccountChange();
						
					}
					
					
				}
				//Toast.makeText(getApplicationContext(), "Syncing data with account: " + _strAccounts[position],Toast.LENGTH_SHORT).show();
		    }
		});
	}
	
	
	private void doAccountChange(){
		
		showDialog(DIALOG_SYNC_PROGRESS);
		//

		
		//my layout
		
		CheckAccountLayoutTask task = new CheckAccountLayoutTask(this);
		task.execute();
		
		
		//my places
		if(db==null || !db.isOpen()){
			initDB();
		}
		
		db.delete(MyDBOpenHelper.TABLE_NAME, "", new String[]{});
		db.delete(MyDBOpenHelper.SYNC_TABLE_NAME, "", new String[]{});
		
		doSync();
		db.close();
		_settings.setPREF_DATAINITIAL(true);
	}
	
	private void initDB(){
		
		
		try{
			helper = new MyDBOpenHelper(this, null);
			if(db!=null&&db.isOpen())db.close();
			db = helper.getWritableDatabase();
		}catch(SQLException e){
			e.printStackTrace();			
		}
			
		
	}
	
	private void getAccountList(){
		
		if(_list.size()>0)_list.clear();
		
		accounts =  _accountManager.getAccountsByType("com.google");
		_strAccounts = new String[accounts.length];
		
		for(int i=0 ; i< accounts.length ; i++){
			_strAccounts[i]=accounts[i].name;
			
			HashMap map = new HashMap();
			map.put("name", _strAccounts[i]);
			if(!_settings.getPREF_ACCOUNT().equalsIgnoreCase(_strAccounts[i])){
				map.put("type", R.drawable.null36);
			}else{
				map.put("type", R.drawable.sync);
			}
			
			_list.add(map);
		}
		
	}

	private void doSync(){
			
		Intent intent = new Intent(this, SyncManager.class);
		intent.putExtra(SyncManager.EXTRA_ACCOUNT, _settings.getPREF_ACCOUNT());			
		startService(intent);
	}
	
	private class CheckAccountLayoutTask extends AsyncTask{

		Context context;
		public CheckAccountLayoutTask(Context c){
			context = c;
		}
		
		@Override
		protected Object doInBackground(Object... params) {
			// TODO Auto-generated method stub
			String reply;
			MylayoutCheckRequester requester = new MylayoutCheckRequester(((NavierAccount)context)._settings.getPREF_ACCOUNT(),NavierAccount.this);
			
			try{
				reply = requester.sentHttpRequest();
				JSONObject jsonRoot = new JSONObject(reply);
				if(jsonRoot.has("mylayouts") && jsonRoot.getJSONArray("mylayouts").length()>0){
					
					//kill current db
					killCurrentDB();
					
					JSONArray mylayouts = jsonRoot.getJSONArray("mylayouts");
					for(int i=0; i<mylayouts.length(); i++){
						
						
						JSONObject layout = (JSONObject) mylayouts.get(i);
						int id = Integer.parseInt(layout.getString("id"));
						String fingerprint = layout.getString(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT);
						HashMap map = new HashMap();
						
						map.put(MyDBOpenHelper.LAYOUT_COL_ID, layout.getString("id"));
						map.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, layout.getString(MyDBOpenHelper.LAYOUT_COL_LAYOUT) );
						map.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, layout.getString(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));
						map.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT, fingerprint);
						
						if(checkMylayoutDataAvailable(fingerprint)){
							//update data
							
							updateMylayout2DB(map);
							
							
							
						}else{
						
			
				
							addMylayout2DB(map);
						}
						
					}
				}else{
					
					killCurrentDB();
					//initial data to db
					
					//navigation classic
					Utility util  =  new Utility(NavierAccount.this);
					String jsonstring = util.getStringFromRaw(R.raw.navigation_classic);
					try {
						JSONObject json = new JSONObject(jsonstring);
						json.getJSONObject("layout").put("name", getString(R.string.panels_navigationclassic));
						jsonstring = json.toString();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					long currTimeMillis = System.currentTimeMillis();
					
					ContentValues values = new ContentValues();
					values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, jsonstring);
					values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, currTimeMillis);
					values.put(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT, currTimeMillis);
					long id = db.insert(MyDBOpenHelper.LAYOUT_TABLE_NAME, null, values);
					
					values = new ContentValues();			
					values.put(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE, MyDBOpenHelper.STATE_NEW);
					values.put(MyDBOpenHelper.SYNC_LAYOUT_LAYOUTID, id);
					values.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT, currTimeMillis);
					
					db.insert(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME,null,values);
					
					
					//digital dashboard
					currTimeMillis = System.currentTimeMillis();
					jsonstring = util.getStringFromRaw(R.raw.digital_dashboard);
					try {
						JSONObject json = new JSONObject(jsonstring);
						json.getJSONObject("layout").put("name", getString(R.string.panels_digitaldashboard));
						jsonstring = json.toString();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					values = new ContentValues();
					values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, jsonstring);
					values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, currTimeMillis);
					values.put(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT,currTimeMillis);
					id = db.insert(MyDBOpenHelper.LAYOUT_TABLE_NAME, null, values);
					
					values = new ContentValues();			
					values.put(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE, MyDBOpenHelper.STATE_NEW);
					values.put(MyDBOpenHelper.SYNC_LAYOUT_LAYOUTID, id);
					values.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT, currTimeMillis);
					db.insert(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME,null,values);
					
					_settings.setPREF_DATAINITIAL(true);
					
				}
				db.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			
			dismissDialog(DIALOG_SYNC_PROGRESS);
			return null;
		}
		
		private void killCurrentDB(){
			if(db==null || !db.isOpen()){
				
				initDB();
			}
			
			db.delete(MyDBOpenHelper.LAYOUT_TABLE_NAME, "", new String[]{});
			db.delete(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME, "", new String[]{});
			
		}
		
		private boolean checkMylayoutDataAvailable(String fingerprint){
			if(db==null || !db.isOpen()){
				
				initDB();
			}
			
			String[] columns = {MyDBOpenHelper.LAYOUT_COL_ID};
			String selection = MyDBOpenHelper.LAYOUT_COL_LAYOUT + "=?"; 
			String[] selectionArgs = {fingerprint};
			String groupBy = "";
			String having = "";
			String orderBy = MyDBOpenHelper.COL_TIMESTAMP+" DESC";
			
			Cursor c = db.query(MyDBOpenHelper.LAYOUT_TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);
			
			if(c.getCount()>0){
				c.close();
				return true;
			}else{
				c.close();
				return false;
			}
			
		}
		
		private void updateMylayout2DB(HashMap map){
			if(db==null || !db.isOpen()){
				initDB();
			}
			
			ContentValues values = new ContentValues();
			
			
			values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, (String) map.get(MyDBOpenHelper.LAYOUT_COL_LAYOUT));
			values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, (String) map.get(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));

			
			String[] args = {(String) map.get(MyDBOpenHelper.LAYOUT_COL_LAYOUT)};
			
			db.update(MyDBOpenHelper.LAYOUT_TABLE_NAME, values, MyDBOpenHelper.LAYOUT_COL_LAYOUT +"=?", args);
			db.close();
		}
		
		private void addMylayout2DB(HashMap map){
			if(db==null || !db.isOpen()){
				initDB();
			}
			
			ContentValues values = new ContentValues();
			//values.put(MyPlacesDBOpenHelper.COL_ID, (String) map.get(MyPlacesDBOpenHelper.COL_ID));
			values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, (String) map.get(MyDBOpenHelper.LAYOUT_COL_LAYOUT));
			values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, (String) map.get(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));
			values.put(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT, (String) map.get(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT));
			
			
			db.insert(MyDBOpenHelper.LAYOUT_TABLE_NAME, "", values);
			db.close();
		}
		
	}
}
