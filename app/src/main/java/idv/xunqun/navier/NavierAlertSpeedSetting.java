package idv.xunqun.navier;


import idv.xunqun.navier.utils.Utility;
import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.TextView.OnEditorActionListener;

public class NavierAlertSpeedSetting extends Activity {

	private EditText et_kmh;
	private EditText et_mph;
	private PreferencesHandler _settings;
	private boolean _isMphChange = false;
	private boolean _isKmhChange = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		

		this.setContentView(R.layout.alertspeedsetting);
		initProperties();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//Utility.checkLanscapeOrientation(this);
		Utility.requestLocationServiceUpdate(this);
	}
	
	

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Utility.requestLocationServiceStop(this);
	}

	private void initProperties(){
		
		_settings = new PreferencesHandler(this);
		
		et_kmh = (EditText) this.findViewById(R.id.kmh);
		et_mph = (EditText) this.findViewById(R.id.mph);
		
		et_kmh.setText(String.valueOf((int)_settings.getPREF_ALERT_SPEED()),BufferType.EDITABLE);
		et_mph.setText(String.valueOf((int)SpeedUnit.kmh2mph(_settings.getPREF_ALERT_SPEED())),BufferType.EDITABLE);
		
		et_kmh.setOnEditorActionListener(listener);
		et_mph.setOnEditorActionListener(listener);
		/*
		et_mph.setOnKeyListener(new OnKeyListener(){

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				
				try{
					_isMphChange = true;
					int n = Integer.parseInt(et_mph.getText().toString());
					handleEtMph(n);
					
				}catch(Exception e){
					
				}
				return false;
			}
			
		});
		
		et_mph.setOnFocusChangeListener(new OnFocusChangeListener(){

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(!hasFocus && _isMphChange == true){
					try{
						
						int n = Integer.parseInt(et_mph.getText().toString());
						handleEtMph(n);
						
					}catch(Exception e){
						
					}finally{
						_isMphChange = false;
					}
				}
			}
			
		});
		
		et_kmh.setOnKeyListener(new OnKeyListener(){

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				
				try{
					_isKmhChange = true;
					int n = Integer.parseInt(et_kmh.getText().toString());
					handleEtKmh(n);
				}catch(Exception e){
					
				}
				
				return false;
			}
			
		});
		
		et_kmh.setOnFocusChangeListener(new OnFocusChangeListener(){

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(!hasFocus && _isKmhChange == true){
					try{
						
						int n = Integer.parseInt(et_kmh.getText().toString());
						handleEtKmh(n);
						
					}catch(Exception e){
						
					}finally{
						_isKmhChange = false;
					}
				}
			}
			
		});
		
*/

		

	}
	
	private void handleEtMph(int n){
		et_kmh.setText(String.valueOf((int)SpeedUnit.mph2kmh(n)));
		_settings.setPREF_ALERT_SPEED((int)SpeedUnit.mph2kmh(n));
	}
	
	private void handleEtKmh(int n){
		et_mph.setText(String.valueOf((int)SpeedUnit.kmh2mph(n)));
		_settings.setPREF_ALERT_SPEED((int)n);
	}
	
	private OnEditorActionListener listener = new OnEditorActionListener() {
		
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			// TODO Auto-generated method stub
			Log.d("mine", "IME action = " + actionId);
			if(actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NONE){
				if(v.equals(et_kmh)){
					String speed = et_kmh.getText().toString();
					if(TextUtils.isEmpty(speed)){
						speed = "100";
						et_kmh.setText(speed);
					}
					
					handleEtKmh(Integer.parseInt(speed));
				}
				
				if(v.equals(et_mph)){
					String speed = et_mph.getText().toString();
					if(TextUtils.isEmpty(speed)){
						speed = "65";
						et_mph.setText(speed);
					}
					
					handleEtMph((int)Double.parseDouble(speed));
				}
			}
			
			return true;
		}
	};
}
