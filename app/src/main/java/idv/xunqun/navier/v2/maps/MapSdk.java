package idv.xunqun.navier.v2.maps;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import idv.xunqun.navier.v2.MyApplication;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

public interface MapSdk {

    boolean isInitialized();

    void initBeforeSetContentView(Activity activity);

    void onViewCreate(Activity activity, View view);

    void onResume();

    void onPause();

    void destory();

    void onSaveInstanceState(Bundle outState);

    void onLowMemory();

    void moveToLocation(LatLng latLng);

    void animateToCurrentLocation(LatLng latLng);

    void animateToLocation(LatLng latLng);

    void addMarker(LatLng latLng, String label, String snippets);

    void cleanMarkers();

    void addPolyline(ArrayList<LatLng> pointList, int color);

    void cleanPolyline();

    void setMyLocation(LatLng latLng);

    LatLng getScreenLocation(int x, int y);

    void setZoom(int level);
}
