package idv.xunqun.navier.v2;

import idv.xunqun.navier.NavierAbout;
import idv.xunqun.navier.R;
import idv.xunqun.navier.Version;
import idv.xunqun.navier.R.id;
import idv.xunqun.navier.utils.Utility;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class DownloadActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_download);
		initViews();

	}

	@Override
	protected void onResume() {

		super.onResume();
		Utility.requestLocationServiceUpdate(this);
	}

	@Override
	protected void onPause() {

		super.onPause();
		Utility.requestLocationServiceStop(this);
	}

	private void initViews() {

		Button unlock = (Button) findViewById(id.unlock);

		unlock.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent it = new Intent(Intent.ACTION_VIEW);

				it.setData(Uri.parse("market://details?id=idv.xunqun.navier.premium"));

				startActivity(it);

			}
		});

		findViewById(id.app_corner).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri
						.parse("market://details?id=com.whilerain.holomap&referrer=utm_source%3DNavierHUD%26utm_medium%3DMoreApps%26utm_campaign%3DCrossPromote"));
				startActivity(intent);

			}
		});
		findViewById(id.app_2battery).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri
						.parse("market://details?id=com.a0soft.gphone.uninstaller&referrer=utm_source%3DWhilerain%26utm_medium%3DMoreApps%26utm_campaign%3DCrossPromote"));
				startActivity(intent);

			}
		});
		findViewById(id.app_appmgr).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri
						.parse("market://details?id=com.a0soft.gphone.app2sd&referrer=utm_source%3DNavierHUD%26utm_medium%3DMoreApps%26utm_campaign%3DCrossPromote"));
				startActivity(intent);

			}
		});
		findViewById(id.app_one_tap).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri
						.parse("market://details?id=com.a0soft.gphone.acc.free&referrer=utm_source%3DNavierHUD%26utm_medium%3DMoreApps%26utm_campaign%3DCrossPromote"));
				startActivity(intent);

			}
		});
	}
}
