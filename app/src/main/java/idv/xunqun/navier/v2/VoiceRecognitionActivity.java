package idv.xunqun.navier.v2;

import java.util.ArrayList;


import idv.xunqun.navier.R;
import idv.xunqun.navier.utils.Utility;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class VoiceRecognitionActivity extends ListActivity {

	public static final int RESULT_CODE = 123;

	public static final String EXTRA_LATITUDE = "latitude";
	public static final String EXTRA_LONGITUDE = "longitude";

	private static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;

	private double _latitude;
	private double _longitude;
	private ListView _listView;
	private ArrayList<String> _matches;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_voice_recognition);

		initViews();

		Intent it = this.getIntent();
		_latitude = it.getDoubleExtra(EXTRA_LATITUDE, 0);
		_longitude = it.getDoubleExtra(EXTRA_LONGITUDE, 0);

		startVoiceRecognition();

	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Utility.checkLanscapeOrientation(this);
		Utility.requestLocationServiceUpdate(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Utility.requestLocationServiceStop(this);
	}

	private void initViews() {
		// TODO Auto-generated method stub
		setTitle(getString(R.string.naviervrecognition_vc));
		_listView = getListView();
	}

	private void startVoiceRecognition() {
		Intent it = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		it.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass()
				.getPackage().getName());
		it.putExtra(RecognizerIntent.EXTRA_PROMPT,
				getString(R.string.naviervrecognition_vc));
		it.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		it.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
		startActivityForResult(it, VOICE_RECOGNITION_REQUEST_CODE);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == VOICE_RECOGNITION_REQUEST_CODE
				&& resultCode == RESULT_OK) {
			// Fill the list view with the strings the recognizer thought it
			// could have heard
			_matches = data
					.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
			_listView.setAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1, _matches));
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_voice_recognition, menu);
		return true;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);

		String selected = _matches.get(position);

		Intent resultIntent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putString("result", selected);
		resultIntent.putExtras(bundle);
		setResult(RESULT_OK, resultIntent);
		finish();

	}

}
