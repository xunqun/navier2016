package idv.xunqun.navier.v2.content;

import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.http.PushPanelTask;
import idv.xunqun.navier.utils.Utility;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class PanelStore {

	public static final int FILTER_ALL = 0;
	public static final int FILTER_NORMAL = 1;
	public static final int FILTER_NAVIGATION = 2;
	
	private static ArrayList<Panel> mPanelStore;

	public static ArrayList<Panel> getPanelStore(Context context) {

		if (mPanelStore == null || mPanelStore.size() == 0) {

			try {
				return init(context);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return mPanelStore;
	}

	private static ArrayList<Panel> init(Context context) throws JSONException {

		PreferencesHandler pref = new PreferencesHandler(context);
		mPanelStore = new ArrayList<Panel>();

		/*
		 * Do sync and check db If there have no panel add the default panels
		 */
		if (!pref.getPREF_DATAINITIAL()
				&& pref.getPREF_ACCOUNT().equalsIgnoreCase("NONE")) {
			addDefaultPanelsToDb(context);
			pref.setPREF_DATAINITIAL(true);
		} 
		return mPanelStore;
	}

	private static void addDefaultPanelsToDb(Context context) throws JSONException {
		
		PreferencesHandler pref = new PreferencesHandler(context);
		
		boolean stored = false;
		String rawString;
		JSONObject json;
		Panel panel;
		SQLiteDatabase db = MyDBOpenHelper.getDb(context);

		Utility util = new Utility(context);
		String jsonstring = util.getStringFromRaw(R.raw.navigation_classic);

		json = new JSONObject(jsonstring);
		json.getJSONObject("layout").put("name", context.getString(R.string.panels_navigationclassic));
		jsonstring = json.toString();

		panel = new Panel(json);
		String fp = String.valueOf(System.currentTimeMillis());
		long id = panel.storeToDb(db, fp);
		
		
		addDbSyncPanel(context, id,  MyDBOpenHelper.STATE_NEW, fp );

		// digital dashboard

		jsonstring = util.getStringFromRaw(R.raw.digital_dashboard);

		json = new JSONObject(jsonstring);
		json.getJSONObject("layout").put("name", context.getString(R.string.panels_digitaldashboard));
		jsonstring = json.toString();

		panel = new Panel(json);
		fp = String.valueOf(System.currentTimeMillis());
		id = panel.storeToDb(db, fp);
		
		
		
		
		if (!pref.getPREF_ACCOUNT().equals(context.getString(R.string.PREF_ACCOUNT_DEFAULT)) && pref.getPREF_SYNC()) {
			addDbSyncPanel(context, id,  MyDBOpenHelper.STATE_NEW,fp );
			new PushPanelTask(context, null, pref.getPREF_ACCOUNT()).execute();
		}
		Toast.makeText(context, R.string.panels_add_default, Toast.LENGTH_LONG).show();
	}

	public static long addDbPanel(Context c, String json, String fingerPrint){

		ContentValues values = new ContentValues();
		values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, json.toString() );
		values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, fingerPrint);
		values.put(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT, fingerPrint);
		
		return  MyDBOpenHelper.getDb(c).insert(MyDBOpenHelper.LAYOUT_TABLE_NAME, null, values);
	}
	
	public static void updateDbPanel(Context c, String json, String fingerPrint){
		ContentValues values = new ContentValues();
		values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, json.toString() );
		values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, System.currentTimeMillis());
		
		String whereClause = MyDBOpenHelper.LAYOUT_COL_FINGERPRINT + "=?";
		String[] whereArgs = new String[]{String.valueOf(fingerPrint)};
		
		MyDBOpenHelper.getDb(c).update(MyDBOpenHelper.LAYOUT_TABLE_NAME, values, whereClause, whereArgs);
	}
	
	public static void addDbSyncPanel(Context c, long id, String state, String fingerprint) {
		// TODO Auto-generated method stub
		ContentValues values = new ContentValues();
		values.put(MyDBOpenHelper.SYNC_LAYOUT_LAYOUTID, id);
		values.put(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE, state);
		values.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT, fingerprint);
		
		MyDBOpenHelper.getDb(c).insert(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME, null, values);
	}

	public static void notifyLayoutSetChange(Context context) {

		if (mPanelStore == null || mPanelStore.size() == 0) {

			try {
				init(context);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		Utility util = new Utility(context);
		Panel obj;
		HashMap map;

		mPanelStore = null;
		mPanelStore = new ArrayList<Panel>();

		SQLiteDatabase db = MyDBOpenHelper.getDb(context);
		try {

			// get from db
			String[] columns = { MyDBOpenHelper.LAYOUT_COL_ID,
					MyDBOpenHelper.LAYOUT_COL_LAYOUT,
					MyDBOpenHelper.LAYOUT_COL_TIMESTAMP,
					MyDBOpenHelper.LAYOUT_COL_FINGERPRINT
			};
			String selection = ""; // if press faverite button
			String[] selectionArgs = {};
			String groupBy = "";
			String having = "";
			String orderBy = MyDBOpenHelper.LAYOUT_COL_FINGERPRINT + " ASC";
			;

			Cursor c = db.query(MyDBOpenHelper.LAYOUT_TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);
			// Cursor c =
			// context.getContentResolver().query(LayoutTableMetadata.CONTENT_URI,
			// columns, null, null, null);
			// Cursor c = this.managedQuery(LayoutTableMetadata.CONTENT_URI,
			// columns, selection, selectionArgs, orderBy);
			while (c.moveToNext()) {
				JSONObject json = new JSONObject(c.getString(c.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_LAYOUT)));
				obj = new Panel(json);
				obj.LAYOUT_ID = c.getInt(c.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_ID));
				obj.mTimeStamp = c.getLong(c.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP));
				obj.LAYOUT_FINGERPRINT = c.getString(c.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT));
				mPanelStore.add(obj);
			}
			
			if(c.getCount()==0){
				addDefaultPanelsToDb(context);
			}

			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}
	
	public static void deletePanel(Context context ,Panel p) {

		SQLiteDatabase db = MyDBOpenHelper.getDb(context);

		String whereClause = MyDBOpenHelper.LAYOUT_COL_ID + "=?";
		String[] whereArgs = new String[] { String.valueOf(p.LAYOUT_ID) };

		db.delete(MyDBOpenHelper.LAYOUT_TABLE_NAME, whereClause, whereArgs);

		// insert layout sync
		ContentValues values = new ContentValues();
		values.put(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE, MyDBOpenHelper.STATE_KILLED);
		values.put(MyDBOpenHelper.SYNC_LAYOUT_LAYOUTID, p.LAYOUT_ID);
		values.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT, p.LAYOUT_FINGERPRINT);

		db.insert(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME, null, values);

		PanelStore.notifyLayoutSetChange(context);		

	}
	
}
