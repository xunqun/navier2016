package idv.xunqun.navier.v2;


import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.ads.AdView;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;

import java.util.Calendar;
import java.util.Locale;

import idv.xunqun.navier.LocationService;
import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.NavierAbout;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.Version;
import idv.xunqun.navier.utils.Utility;
import idv.xunqun.navier.v2.PanelChooserFragment.OnNavierMenuHandler;
import idv.xunqun.navier.v2.PlacesChooserFragment.PlacesChooserCallback;
import idv.xunqun.navier.v2.content.NavierLisenceChecker;
import idv.xunqun.navier.v2.dialog.ScoreStartDialog;

public class HomeActivity extends AppCompatActivity implements
		OnNavierMenuHandler, PlacesChooserCallback, ActionBar.TabListener {

	/**
	 * A handler object, used for deferring UI operations.
	 */
	private static final int DIALOG_FREE_VERSION = 0;
	private boolean _ShowingBack = false;

	private SectionsPagerAdapter mSectionsPagerAdapter;

	private ViewPager mViewPager;

	private Handler mLicenseHandler;

	private WakeLock _wl;

	private PreferencesHandler mPref;

	private AdView mAdView;

	private MenuItem vRefreshBtn;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mPref = new PreferencesHandler(this);

		setContentView(R.layout.activity_home);
		initViews(savedInstanceState);
		screenBrightSetting();

		// start LocationService
		Intent it = new Intent(this, LocationService.class);
		it.setAction(LocationService.SERVICE_ACTION_STARTLISTEN);
		this.startService(it);

		try {
			screenLockSetting();
		} catch (Exception e1) {

			e1.printStackTrace();
		}
		// do refresh panel

		checkLisence();

		// for show rete me dialog
		int count = PreferencesHandler.get(this).getPREF_OPENCOUNT();
		if (count >= 0)
			PreferencesHandler.get(this).setPREF_OPENCOUNT(count + 1);

		
	}

	@Override
	public void onStart() {
		super.onStart();

		
		Calendar calendar = Calendar.getInstance();
		long timestamp = PreferencesHandler.get(this).getPREF_PROMOTE();

		/*
		try {
			int vcode = this.getPackageManager().getPackageInfo(
					"idv.xunqun.navier", PackageManager.GET_ACTIVITIES).versionCode;
			if (mPref.getPREF_NOTICE_CODE() != vcode) {
				NoticeDialog dialog = new NoticeDialog(this);
				dialog.show(getSupportFragmentManager(), "");
				mPref.setPREF_NOTICE_CODE(vcode);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
	}

	@Override
	public void onStop() {
		super.onStop();

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 0) {
			if (mSectionsPagerAdapter.fragments[0] != null)
				((HomeFragment) mSectionsPagerAdapter.fragments[0])
						.onRefreschClicked();
			if (mSectionsPagerAdapter.fragments[1] != null)
				((HomeFragment) mSectionsPagerAdapter.fragments[1])
						.onRefreschClicked();
		}
	}

	private void checkLisence() {

		if (Version.isLite(this)) {
			return;
		}

		NavierLisenceChecker checker = new NavierLisenceChecker(this,
				new LicenseCheckerCallback() {

					@Override
					public void allow(int reason) {

						Log.d("mine", "Lisence allow");
						mPref.setPREF_LISENCEDUSER(true);
						setProgressBarIndeterminateVisibility(false);
					}

					@Override
					public void dontAllow(int reason) {

						Log.d("mine", "Lisence dont allow");
						if (isFinishing()) {
							// Don't update UI if Activity is finishing.
							return;
						}

						if (reason == Policy.NOT_LICENSED) {
							Log.d("mine", "Lisence NOT_LICENSED");
							mPref.setPREF_LISENCEDUSER(false);
						} else if (reason == Policy.RETRY) {
							Log.d("mine", "Lisence RETRY");
							mPref.setPREF_LISENCEDUSER(true);
						} else {
							mPref.setPREF_LISENCEDUSER(true);
						}
						setProgressBarIndeterminateVisibility(false);
					}

					@Override
					public void applicationError(int errorCode) {

						Log.d("mine", "Lisence error");
						mPref.setPREF_LISENCEDUSER(true);
						setProgressBarIndeterminateVisibility(false);
					}

				});

		setProgressBarIndeterminateVisibility(true);
		checker.doCheck();
	}

	@Override
	public void onDestroy() {
		if (mAdView != null) {
			mAdView.destroy();
		}
		
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.fragment_panelchooser, menu);
		vRefreshBtn = menu.findItem(R.id.menu_refresh);
		if (Version.isLite(this)) {
			vRefreshBtn.setVisible(false);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent it;

		if (item.getItemId() == R.id.menu_refresh) {
			vRefreshBtn.setVisible(false);
			new CountDownTimer(60000, 60000) {

				@Override
				public void onTick(long millisUntilFinished) {

				}

				@Override
				public void onFinish() {
					if (vRefreshBtn != null) {
						vRefreshBtn.setEnabled(true);
						vRefreshBtn.setVisible(true);
					}
				}
			}.start();
			try {
				((HomeFragment) mSectionsPagerAdapter.fragments[0])
						.onRefreschClicked();
				((HomeFragment) mSectionsPagerAdapter.fragments[1])
						.onRefreschClicked();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		else if (item.getItemId() == R.id.menu_account) {
			it = new Intent(this, AccountActivity.class);
			startActivityForResult(it, 0);
		}

		else if (item.getItemId() == R.id.menu_settings) {
			it = new Intent(this, SettingsActivity.class);
			startActivity(it);
		}

		else if (item.getItemId() == R.id.menu_about) {
			it = new Intent(this, NavierAbout.class);
			startActivity(it);
		}

		// else if (item.getItemId() == R.id.special) {
		// it = new Intent(this, ProductRegisterActivity.class);
		// startActivity(it);
		// }

		else if (item.getItemId() == R.id.menu_moreapps) {
			Intent myIntent = new Intent(this, DownloadActivity.class);
			startActivity(myIntent);

		}
		return true;
	}

	@Override
	protected void onPause() {
		super.onPause();

		// screen lock
		_wl.release();
		MyDBOpenHelper.closeDb();
		Utility.requestLocationServiceStop(this);
	}

	@Override
	protected void onResume() {

		super.onResume();

		// screen lock
		_wl.acquire();
		Utility.requestLocationServiceUpdate(this);

		showScoreMeDialog();
	}

	private void showScoreMeDialog() {
		if (PreferencesHandler.get(this).getPREF_OPENCOUNT() > 5) {
			ScoreStartDialog dialog = new ScoreStartDialog();
			dialog.show(getSupportFragmentManager(), "");
			PreferencesHandler.get(this).setPREF_OPENCOUNT(0);
		}

	}

	private void screenLockSetting() throws Exception {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			if (mPref.getPREF_LOCK_SCREEN_ORIENTATION()) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			}

		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {

	}

	private void screenBrightSetting() {

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		_wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "My Tag");
	}

	private void initViews(Bundle savedInstanceState) {

		// Set up the action bar.
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}

		// Set up the action bar.

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.filled_box));

	}

	@Override
	public void onAddClick() {

		// flipCard();
	}



	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public Fragment[] fragments = new Fragment[2];

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = null;

			if (position == 0) {
				fragment = new PanelChooserFragment();
				fragments[0] = fragment;
			} else if (position == 1) {
				fragment = new PlacesChooserFragment();
				fragments[1] = fragment;
			}
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.naviermenu_title).toUpperCase(l);
			case 1:
				return getString(R.string.naviermenu_title_place)
						.toUpperCase(l);

			}
			return null;
		}
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

	}

}
