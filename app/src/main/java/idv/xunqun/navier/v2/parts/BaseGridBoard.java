package idv.xunqun.navier.v2.parts;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.RRelativeLayout;
import idv.xunqun.navier.content.Layout;
import idv.xunqun.navier.parts.Parts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.RelativeLayout;
/*
public class BaseGridBoard extends SurfaceView implements
		SurfaceHolder.Callback {

	public final static int WIDTH_UNIT_COUNT = 16;
	public final static int HEIGHT_UNIT_COUNT = 8;
	public static int GLOBAL_COLOR = Color.CYAN;
	public static int SECONDARY_COLOR = Color.RED;
	public int GLOBAL_SPEEDUNIT = PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH;

	public Context _context;
	

	// Preference Handler
	protected PreferencesHandler _pref;

	// Layout properties
	public int _screenWidth;
	public int _screenHeight;
	public int _unitPixel = 0;
	public int _screenWidthMargin;
	public int _screenHeightMargin;

	// Layout data
	protected Layout _layout;
	private RelativeLayout rl;
	private boolean _retry;



	public BaseGridBoard(Context context) throws JSONException {
		super(context);
		// TODO Auto-generated constructor stub
		_context = context;
		_pref = new PreferencesHandler(context);
		
		
		

		
	}
	
	public View initParts(JSONObject layout, int layoutMode) {
		_layout = new Layout();

		if (rl != null) {
			rl.removeAllViews();
		}

		try {

			JSONObject root = layout.getJSONObject(Layout.JSON_ROOT);
			JSONArray parts = root.getJSONArray(Layout.JSON_PARTS);

			_layout._mode = layoutMode;
			_layout._name = root.getString(Layout.JSON_NAME);
			_layout._type = root.getInt(Layout.JSON_TYPE);
			_layout._parts_list = Parts.createElemList(parts,BaseGridBoard.this);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		if (_layout._mode == Layout.MODE_NORMAL) {
			rl = new RelativeLayout(_context);
		} else {
			rl = new RRelativeLayout(_context);
		}



		rl.setBackgroundColor(Color.BLACK);
		rl.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		rl.addView(BaseGridBoard.this);
		return rl;

	}
	


	public void onLocationChangeHandler(Location location) {
		// TODO Auto-generated method stub
		for ( Parts p : _layout._parts_list) {
			p.onLocationChange(location);
		}
	}

	public void onLocationStatusChangeHandle(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub
		for ( Parts p : _layout._parts_list) {
			p.onLocationStatusChange(provider, status, extras);
		}
	}

	
	public void onLocationProviderDisableHandle(String provider) {
		// TODO Auto-generated method stub
		for ( Parts p : _layout._parts_list) {
			p.onLocationProviderDisable(provider);
		}
	}

	
	public void onIsGPSFix(GpsStatus status, Boolean isfix) {
		// TODO Auto-generated method stub
		for ( Parts p : _layout._parts_list) {
			p.onIsGPSFix(status, _retry);
		}
	}


	public void onSensorChangeHandler(SensorEvent event) {
		// TODO Auto-generated method stub
		for ( Parts p : _layout._parts_list) {
			p.onSensorChange(event);
		}
	}

	
	public void onSensorChangeHandler(float[] orientation) {
		// TODO Auto-generated method stub
		for ( Parts p : _layout._parts_list) {
			p.onSensorChange(orientation);
		}
	}

	
	public void onGlobalColorChange(int color, int color2) {
		// TODO Auto-generated method stub
		for ( Parts p : _layout._parts_list) {
			p.onGlobalColorChange(color, color);
		}
	}

	
	public void onSpeedUnitChangeHandler(int sunit) {
		// TODO Auto-generated method stub
		for ( Parts p : _layout._parts_list) {
			p.onSpeedUnitChange(sunit);
		}
	}

	
	public void onPause() {
		// TODO Auto-generated method stub
		for ( Parts p : _layout._parts_list) {
			p.onPause();
		}
	}

	
	public void onResume() {
		// TODO Auto-generated method stub
		for ( Parts p : _layout._parts_list) {
			p.onResume();
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	

	



}*/
