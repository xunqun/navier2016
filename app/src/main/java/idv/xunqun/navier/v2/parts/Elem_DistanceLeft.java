package idv.xunqun.navier.v2.parts;



import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.v2.parts.GridBoard;
import idv.xunqun.navier.v2.content.DirectionRoute;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Bundle;

public class Elem_DistanceLeft extends Parts {

	//Default Value
	public static final int ELEM_HEIGHT = 2;
	public static final int ELEM_WIDTH = 4;    	
	public static final int ELEM_PART = Parts.ELEM_DISTANCELEFT;
	public static String ELEM_NAME = "Distance Left";
	public static final boolean ELEM_ISNAVPART = true;
	public static final int ELEM_THUMBNAIL = R.drawable.part_distanceleft;
	
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	private int _TEXTSIZE;
	
	//paths & paints
	private Path _textPath;
	private Path _digiPath;
	
	private Paint _textPaint;
	private Paint _digiPaint;
	
	//Values
	//private NavigationGridBoard _parent;
	

	private double _distanceInLeftSteps=0;
	
	public Elem_DistanceLeft(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		//_parent = (NavigationGridBoard) parent;
		initProperty();
		initPath();
		
	}

	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this._iniLeft + (this._width/2);
    	_centerY = this._iniTop + (this._height/2);    	
	}
	
	private void initPath(){
		
		Typeface font;
		
		font = _parent._defaultFont;

		_textPath = new Path();
		_textPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel);
		_textPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel);
		
		_textPaint = new Paint();
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setTypeface(font);
		_textPaint.setAlpha(100);
		_textPaint.setTextSize(_parent._unitPixel/2);
		
		
		_digiPath = new Path();
		_digiPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel*ELEM_HEIGHT);
		_digiPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel*ELEM_HEIGHT);
		
		_digiPaint = new Paint();
		_digiPaint.setColor(_parent.GLOBAL_COLOR);
		_digiPaint.setLinearText(true);
		_digiPaint.setTextAlign(Align.LEFT);
		_digiPaint.setTypeface(font);
		_digiPaint.setTextSize((float) (_parent._unitPixel));
	}
	
	private double getLeftDistance(){
		double rate=0;
		try{
		
			
			_distanceInLeftSteps=0;
			DirectionRoute plan = ((NavigationGridBoard)_parent)._naviManager._directionPlan;
			int currentLeg = plan._current_leg;
			int currentStep = plan._current_step;
			
			
			//distance of current leg
			for(int i=currentStep+1;i<plan._legList.get(currentLeg)._stepList.size();i++){
				_distanceInLeftSteps += plan._legList.get(currentLeg)._stepList.get(i).distance_value;
			}
				
			//distance of left legs
			for(int i=currentLeg+1;i<plan._legList.size();i++){
				_distanceInLeftSteps += plan._legList.get(i)._totalDistance;
			}
			
			
			
			_distanceInLeftSteps += ((NavigationGridBoard)_parent)._leftDistance;
			rate = (((NavigationGridBoard)_parent)._naviManager._directionPlan._totalDistance-_distanceInLeftSteps)/((NavigationGridBoard)_parent)._naviManager._directionPlan._totalDistance;
			
			if(_distanceInLeftSteps<=0) rate = 0;
		}catch(Exception e){
			return 0;
		}
		return _distanceInLeftSteps;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		String text = "distance left ";
		String distance;
		double result = getLeftDistance()/1000;
		
		if(result<1){
			
			if(_parent.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH){
				text = text + "(meter)";
				distance = String.format("%.0f",result*1000);
			}else{
				text = text + "(feet)";
				distance = String.format("%.0f",result*1000*3.2808399);
			}
		}else{
			if(_parent.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH){
				text = text + "(Km)";
				distance = String.format("%.1f",result);
			}else{
				text = text + "(mile)";
				distance = String.format("%.1f",result*0.621371192);
			}
		}
		
		
		
		
		canvas.drawTextOnPath(text, _textPath, 0, 0, _textPaint);
		canvas.drawTextOnPath(distance, _digiPath, 0, 0, _digiPaint);
		invalidate();
	}

	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onIsGPSFix(GpsStatus status, boolean isFix) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int mainColor, int sndColor) {
		// TODO Auto-generated method stub
		_textPaint.setColor(mainColor);
		_digiPaint.setColor(mainColor);
		_textPaint.setAlpha(100);
		invalidate();
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_digiPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setAlpha(100);
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}

	public static void drawShadow(Canvas canvas, int uniPixel, int _iniLeft, int _iniTop , int[] pin, Context context){
		
		_iniLeft += uniPixel * pin[0];
		_iniTop += uniPixel * pin[1];

		//paths & paints
		Path _textPath;
		Path _digiPath;
		
		Paint _textPaint;
	    Paint _digiPaint;
    	
		Typeface font;
		try{
			font = Typeface.createFromAsset(context.getAssets(), "fonts/SquadaOne-Regular.ttf");
		}catch(Exception e){
			font = Typeface.DEFAULT;
		}

		_textPath = new Path();
		_textPath.moveTo( _iniLeft,_iniTop+uniPixel/2);
		_textPath.lineTo( _iniLeft+ELEM_WIDTH*uniPixel, _iniTop+uniPixel/2);
		
		_textPaint = new Paint();
		_textPaint.setAntiAlias(true);
		_textPaint.setColor(Color.CYAN);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setTypeface(font);
		_textPaint.setTextSize(uniPixel/2);
		
		
		_digiPath = new Path();
		_digiPath.moveTo( _iniLeft,_iniTop+uniPixel*ELEM_HEIGHT);
		_digiPath.lineTo( _iniLeft+ELEM_WIDTH*uniPixel, _iniTop+uniPixel*ELEM_HEIGHT);
		
		_digiPaint = new Paint();
		_digiPaint.setAntiAlias(true);
		_digiPaint.setColor(Color.CYAN);
		_digiPaint.setLinearText(true);
		_digiPaint.setTextAlign(Align.LEFT);
		_digiPaint.setTypeface(font);
		_digiPaint.setTextSize((float) (uniPixel*1.5));
		
		
		
		String text = "distance left ";
		String distance;
		double result = 6.6;

		text = text + "(Km)";
		distance = String.format("%.1f",result);
			
		canvas.drawTextOnPath(text, _textPath, 0, 0, _textPaint);
		canvas.drawTextOnPath(distance, _digiPath, 0, 0, _digiPaint);
	}
}
