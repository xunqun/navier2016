package idv.xunqun.navier.v2.dialog;


import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.http.SyncPlaceTask;
import idv.xunqun.navier.http.SyncPlaceTask.SyncPlaceHandler;
import idv.xunqun.navier.v2.content.Panel;

import java.util.HashMap;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PlaceNameSettingDialog extends DialogFragment {

	TextView nameTextView;
	private HashMap mPlace;
	private PreferencesHandler mPref;
	private OnPlaceNameSettingListener mListener;

	public interface OnPlaceNameSettingListener {
		void onNameComplete();
	}

	public void setPlace(HashMap place) {
		mPlace = place;
	}

	public void setOnPanelNameSettingListener(OnPlaceNameSettingListener listener) {
		mListener = listener;
	}

	@Override
	public void onActivityCreated(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onActivityCreated(arg0);
		mPref = new PreferencesHandler(getActivity());
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		View view = inflater.inflate(R.layout.dialog_panelnamesetting, null);
		nameTextView = (TextView) view.findViewById(R.id.name);
		nameTextView.setText((String) mPlace.get(MyDBOpenHelper.COL_PLACE_NAME));

		Button ok = (Button) view.findViewById(R.id.ok_name);
		Button cancel = (Button) view.findViewById(R.id.cancel_name);
		ok.setOnClickListener(onClickListener);
		cancel.setOnClickListener(onClickListener);

		builder.setView(view);
		return builder.create();
	}

	private void doRename(HashMap<String, Object> p) {

		SQLiteDatabase db = MyDBOpenHelper.getDb(getActivity());

		ContentValues values = new ContentValues();
		values.put(MyDBOpenHelper.COL_PLACE_NAME, (String) p.get(MyDBOpenHelper.COL_PLACE_NAME));
		// String sql = "UPDATE "+ MyPlacesDBOpenHelper.TABLE_NAME + " SET " +
		// MyPlacesDBOpenHelper.COL_PLACE_NAME + "='" + s + "' WHERE " +
		// MyPlacesDBOpenHelper.COL_ID + " = "+ this.mSelectedId;

		db.update(MyDBOpenHelper.TABLE_NAME, values, MyDBOpenHelper.COL_FINGERPRINT + "=?", new String[] { (String) p.get(MyDBOpenHelper.COL_FINGERPRINT) });
		// db.execSQL(sql);

		// add to sync state table
		putSyncState((String) p.get(MyDBOpenHelper.COL_ID), MyDBOpenHelper.STATE_MODIFY, (String) p.get(MyDBOpenHelper.COL_FINGERPRINT));

		getDialog().dismiss();

	}

	private void putSyncState(String id, String state, String fingerprint) {
		// add to sync state table

		SQLiteDatabase db = MyDBOpenHelper.getDb(getActivity());
		ContentValues values = new ContentValues();
		values.put(MyDBOpenHelper.SYNC_COL_PLACEID, id);
		values.put(MyDBOpenHelper.SYNC_COL_SYNCSTATE, state);
		values.put(MyDBOpenHelper.SYNC_COL_FINGERPRINT, fingerprint);

		db.insert(MyDBOpenHelper.SYNC_TABLE_NAME, null, values);

	}

	private void doSync() {
		// TODO Auto-generated method stub

		if (!mPref.getPREF_ACCOUNT().equals(getString(R.string.PREF_ACCOUNT_DEFAULT))) {
			new SyncPlaceTask(getActivity(), new SyncPlaceHandler() {

				@Override
				public void onSyncPlaceComplete(boolean ok) {
					// TODO Auto-generated method stub

				}
			}, mPref.getPREF_ACCOUNT()).execute();
		}
	}

	OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.ok_name) {
				String name = nameTextView.getText().toString();
				if (TextUtils.isEmpty(name)) {
					nameTextView.setError("Required field");
				} else {
					mPlace.put(MyDBOpenHelper.COL_PLACE_NAME, name);
					doRename(mPlace);
					mListener.onNameComplete();
					getDialog().dismiss();

				}
			}

			else if (v.getId() == R.id.cancel_name) {
				getDialog().dismiss();
			}
		}
	};

}
