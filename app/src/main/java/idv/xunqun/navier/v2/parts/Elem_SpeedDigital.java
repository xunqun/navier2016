package idv.xunqun.navier.v2.parts;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.SpeedUnit;
import idv.xunqun.navier.utils.Utility;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;



public class Elem_SpeedDigital extends Parts {

	
	//Default Value
	public static final int ELEM_HEIGHT = 2;
	public static final int ELEM_WIDTH = 4;    	
	public static final int ELEM_PART = Parts.ELEM_SPEED_DIGITAL;
	public static String ELEM_NAME = "Digital Speed";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_digital_speed;
	
	public static final String UNIT_KMH = "Km/h";
	public static final String UNIT_MPH = "Mph";
	public static final long TIMER_TOTAL = 1000;
	public static final long TIMER_TICK = 100;
	
	//Paint and Path
	Paint _digiPaint;
	Paint _textPaint;

	Path _digiPath;
	Path _textPath;
	
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	
	private float _speed = 0;
	private float _wordspace;
	private float _duration;
	private Location _preLocation=null;
	CountDownTimer _timer;
	
	private float _animationStep;
	private int _animationCount;
	private float _oldSpeed;
	private PreferencesHandler _settings;
	
	
	private GridBoard _parent;
	
	
	public Elem_SpeedDigital(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		this.ELEM_PIN = pin;
		_parent = parent;
		
		initProperty();
		initPath();
		
	}

	
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		String text = "cur. speed ";
		float spd;
		if(_parent.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH){
			text = text + "("+UNIT_KMH+")";
			spd = _speed;
		}else{
			text = text + "("+UNIT_MPH+")";
			spd = SpeedUnit.kmh2mph(_speed);
		}
		
		
		if(_speed < _settings.getPREF_ALERT_SPEED()){
			_digiPaint.setColor(_parent.GLOBAL_COLOR);
			
		}else{
			_digiPaint.setColor(_parent.SECONDARY_COLOR);
		}
		
		canvas.save();
		canvas.drawTextOnPath(text, _textPath, 0, 0, _textPaint);
		canvas.drawTextOnPath(String.format("%.0f", Math.abs(spd)), _digiPath, 0, 0, _digiPaint);
		canvas.restore();
		invalidate();
	}


	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub
		
		if(location.getProvider().equals(LocationManager.GPS_PROVIDER) && location.getSpeed()!=0){
			 _oldSpeed = _speed;
			 _speed = location.getSpeed()/1000*3600;
			 //animSpeed(location.getSpeed());
			 
		}
		
		if(location.getProvider().equals(LocationManager.GPS_PROVIDER) && location.getSpeed()==0 && _preLocation!=null){
			_oldSpeed = _speed;
			_speed = Utility.caculateSpeed(location, _preLocation)/1000*3600;
			//float speed = Utility.caculateSpeed(location, _preLocation);
			//animSpeed(speed);
		}
		_preLocation = location;
	}

	private void animSpeed(float speed){
		_animationStep = ((speed/1000*3600)-_oldSpeed)/(TIMER_TOTAL/TIMER_TICK);
		_animationCount = 1;
		 
		 _timer = new CountDownTimer(TIMER_TOTAL, TIMER_TICK) {

		     public void onTick(long millisUntilFinished) {
		    	 stepUP();
		         
		     }

		     public void onFinish() {
		    	 //stepUP();
		     }
		     
		     public void stepUP(){
		    	 _speed += _animationStep;
		         _animationCount+=1;
		         
		     }
		  };
		 _timer.start();
	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}

	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this._iniLeft + (this._width/2);
    	_centerY = this._iniTop + (this._height/2);
    	_wordspace = _parent._unitPixel/6;
    	
    	_settings = new PreferencesHandler(_parent._context);
    	
    	
	}
	
	public void initPath(){
		
		Typeface font = _parent._defaultFont;
		
		
		
		
		
		_textPath = new Path();
		_textPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel);
		_textPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel);
		
		_textPaint = new Paint();
		_textPaint.setAntiAlias(true);
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setAlpha(100);
		_textPaint.setTypeface(font);
		_textPaint.setTextSize(_parent._unitPixel/2);
		
		
		_digiPath = new Path();
		_digiPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel*ELEM_HEIGHT);
		_digiPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel*ELEM_HEIGHT);
		
		_digiPaint = new Paint();
		_digiPaint.setAntiAlias(true);
		_digiPaint.setColor(_parent.GLOBAL_COLOR);
		_digiPaint.setLinearText(true);
		_digiPaint.setTextAlign(Align.LEFT);
		_digiPaint.setTypeface(font);
		_digiPaint.setTextSize((float) (_parent._unitPixel));
	}



	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		_textPaint.setColor(color);
		_digiPaint.setColor(color);
		_textPaint.setAlpha(100);
	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		//paths & paints
		Path textPath;
		Path digiPath;
		
		Paint textPaint;
		Paint digiPaint;
		
		Typeface font;
		try{
			font = Typeface.createFromAsset(context.getAssets(), "fonts/SquadaOne-Regular.ttf");
		}catch(Exception e){
			font = Typeface.DEFAULT;
		}
		
		textPath = new Path();
		textPath.moveTo( iniLeft+pin[0]*uniPixel,iniTop+uniPixel/2+pin[1]*uniPixel);
		textPath.lineTo( iniLeft+ELEM_WIDTH*uniPixel+pin[0]*uniPixel, iniTop+uniPixel/2+pin[1]*uniPixel);
		
		textPaint = new Paint();
		textPaint.setAntiAlias(true);
		textPaint.setColor(Color.CYAN);
		textPaint.setLinearText(true);
		textPaint.setTextAlign(Align.LEFT);
		textPaint.setTypeface(font);
		textPaint.setTextSize(uniPixel/2);
		
		
		digiPath = new Path();
		digiPath.moveTo( iniLeft+pin[0]*uniPixel,iniTop+uniPixel*ELEM_HEIGHT+pin[1]*uniPixel);
		digiPath.lineTo( iniLeft+ELEM_WIDTH*uniPixel+pin[0]*uniPixel, iniTop+uniPixel*ELEM_HEIGHT+pin[1]*uniPixel);
		
		digiPaint = new Paint();
		digiPaint.setAntiAlias(true);
		digiPaint.setColor(Color.CYAN);
		digiPaint.setLinearText(true);
		digiPaint.setTextAlign(Align.LEFT);
		digiPaint.setTypeface(font);
		digiPaint.setTextSize((float) (uniPixel*1.5));
		
		//
		canvas.save();
		canvas.drawTextOnPath("cur. speed "+"("+UNIT_MPH+")", textPath, 0, 0, textPaint);
		canvas.drawTextOnPath(String.format("%03.1f", Math.abs(34.5)), digiPath, 0, 0, digiPaint);
		
		canvas.restore();
	}



	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_digiPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setAlpha(100);
	}
	
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		ELEM_NAME = name;
	}



	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onIsGPSFix(GpsStatus status,boolean isFix) {
		// TODO Auto-generated method stub
		
	}
}
