package idv.xunqun.navier.v2;

import idv.xunqun.navier.R;
import idv.xunqun.navier.v2.content.Place;
import idv.xunqun.navier.v2.dialog.NaviConfigDialog;
import idv.xunqun.navier.v2.dialog.NaviConfigDialog.OnNavierConfigListener;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PlanListFragment extends ListFragment {

	private final int REQUEST_CODE_PLACECONFIGURE = 1;

	PlanListFragmentHandler _context;
	public PlacesAdapter _placesAdaper;
	private ListView _listView;

	public interface PlanListFragmentHandler {
		void onPlanSequenceChange();

		void onNavigationConfigChange();

		void onPlaceRemove();
		
		void onGo();
	}

	private DropListener _dropListener = new DropListener() {
		public void onDrop(int from, int to) {
			ListAdapter adapter = getListAdapter();
			if (adapter instanceof PlacesAdapter) {
				((PlacesAdapter) adapter).onDrop(from, to);
				getListView().invalidateViews();
			}
		}
	};

	private RemoveListener _removeListener = new RemoveListener() {
		public void onRemove(int which) {
			ListAdapter adapter = getListAdapter();
			if (adapter instanceof PlacesAdapter) {
				((PlacesAdapter) adapter).removePlace(which);
				getListView().invalidateViews();
			}
		}
	};

	private DragListener _dragListener = new DragListener() {

		int backgroundColor = 0xe0bf6f78;
		int defaultBackgroundColor;

		public void onDrag(int x, int y, ListView listView) {
			// TODO Auto-generated method stub
			int hoverPosition = listView.pointToPosition(x, y);
			_placesAdaper.setHoverItemBg(hoverPosition);

		}

		public void onStartDrag(View itemView) {
			itemView.setVisibility(View.INVISIBLE);
			defaultBackgroundColor = itemView.getDrawingCacheBackgroundColor();
			itemView.setBackgroundColor(backgroundColor);

		}

		public void onStopDrag(View itemView) {
			itemView.setVisibility(View.VISIBLE);
			itemView.setBackgroundColor(defaultBackgroundColor);

		}

	};
	private ImageView _btn_setting;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		_context = (PlanListFragmentHandler) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		_placesAdaper = new PlacesAdapter();
		setListAdapter(_placesAdaper);

		_listView = getListView();

		((DragNDropListView) _listView).setDropListener(_dropListener);
		((DragNDropListView) _listView).setRemoveListener(_removeListener);
		((DragNDropListView) _listView).setDragListener(_dragListener);

		initViews();
	}

	private void initViews() {
		// TODO Auto-generated method stub
		/*
		 * _btn_setting = (ImageView)getActivity().findViewById(R.id.setting);
		 * _btn_setting.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub Intent it = new Intent(getActivity(),
		 * PlaceConfigureActivity.class); Bundle bundle = new Bundle();
		 * bundle.putSerializable(PlaceConfigureActivity.EXTRA_PLACE, getTag())
		 * startActivityForResult(it, REQUEST_CODE_PLACECONFIGURE); } });
		 */

		ImageView cleanBtn = (ImageView) getActivity().findViewById(R.id.clean);
		cleanBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setIcon(R.drawable.ic_alert)
						.setMessage(R.string.navieroperation_deleteall)
						.setPositiveButton(getString(R.string.naviernaviconfigure_ok), new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								cleanList();
							}

						})
						.setNegativeButton(getString(R.string.naviernaviconfigure_cancel), new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								
							}
						});

				builder.create().show();
			}
		});

		ImageView configBtn = (ImageView) getActivity().findViewById(R.id.config);
		configBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				NaviConfigDialog dialog = new NaviConfigDialog();
				dialog.setOnNavierConfigListener(new OnNavierConfigListener() {

					@Override
					public void onConfig() {
						// TODO Auto-generated method stub
						_context.onNavigationConfigChange();
					}
				});
				dialog.show(getFragmentManager(), "");
			}
		});
		
		ImageView goBtn = (ImageView) getActivity().findViewById(R.id.go);
		goBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				_context.onGo();
			}
		});

	}

	private void cleanList() {
		// TODO Auto-generated method stub
		_placesAdaper.removeAllPlace();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case REQUEST_CODE_PLACECONFIGURE:

			if (resultCode == Activity.RESULT_OK) {
				Bundle bundle = data.getExtras();
				int position = bundle
						.getInt(PlaceConfigureActivity.EXTRA_POSITION);
				int actionsum = bundle
						.getInt(PlaceConfigureActivity.EXTRA_ACTIONSUM);

				if (actionsum == PlaceConfigureActivity.ACTION_DELETE) {
					Place place = _placesAdaper._placeList.get(position - 1);
					place.marker.remove();
					_placesAdaper.removePlace(position - 1);
					_placesAdaper.notifyDataSetChanged();
					_placesAdaper.notifyDataSetInvalidated();

				}

				else if (actionsum == PlaceConfigureActivity.ACTION_RENAME) {
					String name = bundle
							.getString(PlaceConfigureActivity.EXTRA_PLACENAME);
					Place place = _placesAdaper._placeList.get(position - 1);
					place.name = name;
					place.marker.setTitle(name);

					_placesAdaper.notifyDataSetChanged();
					_placesAdaper.notifyDataSetInvalidated();
				}
			}

			break;

		default:
			break;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragment_planlist, null);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	class PlacesAdapter extends BaseAdapter {

		public ArrayList<Place> _placeList;
		private int _hoverPos = -1;

		public PlacesAdapter() {
			_placeList = new ArrayList<Place>();
		}

		public boolean addPlace(Place p) {
			if (_placeList.size() > 0) {
				_placeList.add(_placeList.size() - 1, p);
			} else {
				_placeList.add(p);
			}
			this.notifyDataSetChanged();
			this.notifyDataSetInvalidated();
			return true;
		}

		public void removePlace(int position) {

			if (position < 0 || position > _placeList.size())
				return;
			_placeList.remove(position);
			_context.onPlaceRemove();
			notifyDataSetInvalidated();

		}
		
		public void removeAllPlace(){
			_placeList.clear();
			_context.onPlaceRemove();
			notifyDataSetInvalidated();
		}

		public void onDrop(int from, int to) {
			from--;
			to--;
			if (to >= 0 && from >= 0) {
				Place temp = _placeList.get(from);
				_placeList.remove(from);
				_placeList.add(to, temp);
				_context.onPlanSequenceChange();
				_hoverPos = -1;
				notifyDataSetInvalidated();
			}
		}

		public void setHoverItemBg(int pos) {
			if (_hoverPos != pos) {
				_hoverPos = pos;
				notifyDataSetInvalidated();
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return _placeList.size() + 1;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			if (position == 0) {
				return null;
			} else {
				return _placeList.get(position - 1);
			}
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ImageView icon;
			TextView title, subTitle, distance;
			RelativeLayout wraper;

			View view;
			if (convertView != null) {
				view = convertView;
			} else {
				LayoutInflater inflater = (LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				view = inflater.inflate(R.layout.planlist_item, null);
			}

			title = (TextView) view.findViewById(R.id.title);
			subTitle = (TextView) view.findViewById(R.id.subtitle);
			icon = (ImageView) view.findViewById(R.id.icon);
			wraper = (RelativeLayout) view.findViewById(R.id.wraper);
			distance = (TextView) view.findViewById(R.id.distance);

			if (position == 0) {
				title.setText(getString(R.string.naviermap_mylocation));
				subTitle.setVisibility(View.GONE);
				icon.setImageResource(R.drawable.ic_poi_marker_s);
				distance.setVisibility(View.GONE);

			} else {
				title.setText(_placeList.get(position - 1).name);
				subTitle.setText(_placeList.get(position - 1).description);
				distance.setText(_placeList.get(position - 1).legDistance);

				Log.d("debug", _hoverPos + "");
				if (position == _hoverPos) {
					wraper.setBackgroundColor(0x60dbebbf);
				} else {
					wraper.setBackgroundColor(0x00000000);
				}

				if (position == _placeList.size()) {
					icon.setImageResource(R.drawable.ic_poi_marker_e);
				} else {
					icon.setImageResource(R.drawable.ic_poi_marker_m);
				}
			}

			return view;
		}

	}

	class placeSettingOnClickListener implements OnClickListener {

		private int _position;
		private Place _place;

		public placeSettingOnClickListener(int position, Place place) {
			_position = position;
			_place = place;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent it = new Intent(getActivity(), PlaceConfigureActivity.class);
			Bundle bundle = new Bundle();
			bundle.putInt(PlaceConfigureActivity.EXTRA_POSITION, _position);
			bundle.putSerializable(PlaceConfigureActivity.EXTRA_PLACENAME,
					_place.name);
			it.putExtras(bundle);
			startActivityForResult(it, REQUEST_CODE_PLACECONFIGURE);
		}

	}

}
