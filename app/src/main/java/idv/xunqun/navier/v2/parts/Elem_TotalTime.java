package idv.xunqun.navier.v2.parts;

import idv.xunqun.navier.R;
import idv.xunqun.navier.v2.parts.GridBoard;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Bundle;

public class Elem_TotalTime extends Parts {

	public static final int ELEM_HEIGHT = 2;
	public static final int ELEM_WIDTH = 4;    	
	public static final int ELEM_PART = Parts.ELEM_TOTALTIME;
	public static String ELEM_NAME = "Time Counter";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_totaltime;
	

	//Part properties (must have)
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	
	private int _wordspace;
	
	/*
	 * savedInstancesState naming rule
	 * e.g. SAVEDSTATE_CLASSNAME_VALUENAME
	 */
	public static final String SAVEDSTATE_TOTALTIME_STARTTIME = "STATEDSTATE_TOTALTIME_STARTTIME";
	
	
	
	//paths & paints
	private Path _textPath;
	private Path _digiPath;
	
	private Paint _textPaint;
	private Paint _digiPaint;
	
	//values
	private long _startTime=0;
	
	public Elem_TotalTime(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		initProperty();
		initPath();
		
	}

	
	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this._iniLeft + (this._width/2);
    	_centerY = this._iniTop + (this._height/2);
    	
    	_startTime = System.currentTimeMillis();
	}
	

	
	private void initPath(){
		
		Typeface font = _parent._defaultFont;
		
		
		
		_textPath = new Path();
		_textPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel);
		_textPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel);
		
		_textPaint = new Paint();
		_textPaint.setAntiAlias(true);
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setAlpha(100);
		_textPaint.setTypeface(font);
		_textPaint.setTextSize(_parent._unitPixel/2);
		
		
		_digiPath = new Path();
		_digiPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel*ELEM_HEIGHT);
		_digiPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel*ELEM_HEIGHT);
		
		_digiPaint = new Paint();
		_digiPaint.setAntiAlias(true);
		_digiPaint.setColor(_parent.GLOBAL_COLOR);
		_digiPaint.setLinearText(true);
		_digiPaint.setTextAlign(Align.LEFT);
		_digiPaint.setTypeface(font);
		_digiPaint.setTextSize((float) (_parent._unitPixel*1));
		
		
	}
	
	private String getTime(long millisec){
		
		
		String showText = "";
		long sec = millisec/1000;
		
		if(sec < 60){  // < 1 min
			showText = sec + "\"";
			
		}else if(sec<3600){ // < 1 hour
			// show m and s
			int m = (int) (sec/60);
			int s = (int) (sec - m*60);
			showText = m+"\' "+s+"\" ";
		}else{
			//show h and m and s
			int h = (int)(sec/3600);
			int m = (int) ((sec-h*3600)/60);
			
			showText = h+"h "+m+"'";
		}
		

		
		return showText;
	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		String text = "total time ";
		long currTime = System.currentTimeMillis();
		long deltaTime = currTime - _startTime;
		
		String time = getTime(deltaTime);
		_textPaint.setAlpha(100);
		canvas.drawTextOnPath(text, _textPath, 0, 0, _textPaint);
		canvas.drawTextOnPath(time, _digiPath, 0, 0, _digiPaint);
		
		invalidate();
	}


	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		
		_textPaint.setColor(color);
		_digiPaint.setColor(color);
		_textPaint.setAlpha(100);
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}


	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		//paths & paints
		Path textPath;
		Path digiPath;
		
		Paint textPaint;
		Paint digiPaint;
		
		Typeface font;
		try{
			font = Typeface.createFromAsset(context.getAssets(), "fonts/SquadaOne-Regular.ttf");
		}catch(Exception e){
			font = Typeface.DEFAULT;
		}
		
		textPath = new Path();
		textPath.moveTo( iniLeft+pin[0]*uniPixel,iniTop+uniPixel/2+pin[1]*uniPixel);
		textPath.lineTo( iniLeft+ELEM_WIDTH*uniPixel+pin[0]*uniPixel, iniTop+uniPixel/2+pin[1]*uniPixel);
		
		textPaint = new Paint();
		textPaint.setAntiAlias(true);
		textPaint.setColor(Color.CYAN);
		textPaint.setLinearText(true);
		textPaint.setTextAlign(Align.LEFT);
		textPaint.setTypeface(font);
		textPaint.setTextSize(uniPixel/2);
		
		
		digiPath = new Path();
		digiPath.moveTo( iniLeft+pin[0]*uniPixel,iniTop+uniPixel*ELEM_HEIGHT+pin[1]*uniPixel);
		digiPath.lineTo( iniLeft+ELEM_WIDTH*uniPixel+pin[0]*uniPixel, iniTop+uniPixel*ELEM_HEIGHT+pin[1]*uniPixel);
		
		digiPaint = new Paint();
		digiPaint.setAntiAlias(true);
		digiPaint.setColor(Color.CYAN);
		digiPaint.setLinearText(true);
		digiPaint.setTextAlign(Align.LEFT);
		digiPaint.setTypeface(font);
		digiPaint.setTextSize((float) (uniPixel*1.5));
		
		//
		
		String text = "total time ";
		
		canvas.save();
		canvas.drawTextOnPath(text, textPath, 0, 0, textPaint);
		canvas.drawTextOnPath("1h22m3s", digiPath, 0, 0, digiPaint);
		
		canvas.restore();
		
		
		
	}


	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		savedStates.putLong(SAVEDSTATE_TOTALTIME_STARTTIME, _startTime);
	}


	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		if(savedStates.containsKey(SAVEDSTATE_TOTALTIME_STARTTIME)){
			_startTime = savedStates.getLong(SAVEDSTATE_TOTALTIME_STARTTIME);
		}
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_digiPaint.setColor(_parent.GLOBAL_COLOR);
	}
	
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		ELEM_NAME = name;
	}


	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onIsGPSFix(GpsStatus status, boolean isFix) {
		// TODO Auto-generated method stub
		
	}
}
