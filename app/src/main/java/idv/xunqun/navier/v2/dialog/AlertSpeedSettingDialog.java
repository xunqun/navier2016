package idv.xunqun.navier.v2.dialog;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.SpeedUnit;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class AlertSpeedSettingDialog extends DialogFragment {

	private EditText kmhEditText;
	private EditText mphEditText;
	private PreferencesHandler _settings;


	public AlertSpeedSettingDialog(){
		_settings = new PreferencesHandler(getActivity());
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		View view = inflater.inflate(R.layout.alertspeedsetting, null);
		 kmhEditText = (EditText) view.findViewById(R.id.kmh);
		 mphEditText = (EditText) view.findViewById(R.id.mph);
		kmhEditText.setOnEditorActionListener(null);
		builder.setView(view);

		return builder.create();
	}

	
	private OnEditorActionListener listener = new OnEditorActionListener() {
		
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			// TODO Auto-generated method stub
			if(actionId == EditorInfo.IME_NULL){
				if(v.equals(kmhEditText)){
					String speed = kmhEditText.getText().toString();
					if(TextUtils.isEmpty(speed)){
						speed = "100";
						kmhEditText.setText(speed);
					}
					
					handleEtKmh(Integer.parseInt(speed));
				}
				
				if(v.equals(mphEditText)){
					String speed = mphEditText.getText().toString();
					if(TextUtils.isEmpty(speed)){
						speed = "65";
						mphEditText.setText(speed);
					}
					
					handleEtMph(Integer.parseInt(speed));
				}
			}
			
			return true;
		}
	};
	
	private void handleEtMph(int n){
		kmhEditText.setText(String.valueOf((int)SpeedUnit.mph2kmh(n)));
		_settings.setPREF_ALERT_SPEED((int)SpeedUnit.mph2kmh(n));
	}
	
	private void handleEtKmh(int n){
		mphEditText.setText(String.valueOf((int)SpeedUnit.kmh2mph(n)));
		_settings.setPREF_ALERT_SPEED((int)n);
	}
}
