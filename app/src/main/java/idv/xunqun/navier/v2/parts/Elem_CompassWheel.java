package idv.xunqun.navier.v2.parts;

import idv.xunqun.navier.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.hardware.GeomagneticField;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Bundle;

public class Elem_CompassWheel extends Parts {

	//Default Value
	public static final int ELEM_HEIGHT = 1;
	public static final int ELEM_WIDTH = 8;    	
	public static final int ELEM_PART = Parts.ELEM_COMAPSS_WHEEL;
	public static String ELEM_NAME = "Compass Wheel";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_compasswheel;
	
	public static final String UNIT_KMH = "Km/h";
	public static final String UNIT_MPH = "Mph";
	
	//property
	public int width;
	public int _height;
	public int iniTop;
	public int iniLeft;
	private int _centerX;
	private int _centerY;
	private float[] mValues = {0,0};
	
	private int textSize;
	
	//paths & paints
	private Path _textPath;
	private Path _circlePath;
	private Path _framePath;
	private Path _clipPath;
	
	private Paint _textPaint;
	private Paint _circlePaint;
	private Paint _framePaint;
	
	//values
	private GridBoard _parent;
	private int uniPixel;
	private Location _location = null;
	
	public Elem_CompassWheel(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		
		_parent = parent;
		uniPixel = _parent._unitPixel;
		
		initProperty();
		initPath();
		
	}

	private void initProperty(){
		
		width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this.iniLeft + (this.width/2);
    	_centerY = this.iniTop + (this._height/2);
    	
    	textSize = (int) (uniPixel)/2;
	}
	
	private void initPath(){
		
		Typeface font = _parent._defaultFont;
		
		
		_framePath = new Path();
		_framePath.addRect(iniLeft, iniTop, iniLeft+ELEM_WIDTH*uniPixel, iniTop+ELEM_HEIGHT*uniPixel, Direction.CCW);
		_framePath.moveTo(iniLeft + width/2 , iniTop);
		_framePath.lineTo(iniLeft + width/2 , iniTop + _height);
		
		_framePaint = new Paint();
		_framePaint.setAntiAlias(true);
		_framePaint.setColor(_parent.GLOBAL_COLOR);
		_framePaint.setStyle(Style.STROKE);
		_framePaint.setStrokeWidth(uniPixel/32);
		
		_circlePath = new Path();
		
		_circlePaint = new Paint();
		_circlePaint.setAntiAlias(true);
		_circlePaint.setColor(_parent.GLOBAL_COLOR);
		
		_textPath = new Path();
		_textPaint = new Paint();
		_textPaint.setAntiAlias(true);
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setTextAlign(Align.CENTER);
		_textPaint.setTextSize(textSize);
		_textPaint.setTypeface(font);
		
		
		_clipPath = new Path();
		_clipPath.addRect(iniLeft, iniTop, iniLeft+ELEM_WIDTH*uniPixel, iniTop+ELEM_HEIGHT*uniPixel, Direction.CCW);
		
	}
	
	private void drawData(Canvas canvas){
		canvas.save();
		canvas.clipPath(_clipPath);
		
		float center = mValues[0];
		float center_n45 = center - 45;
		float center_p45 = center + 45;
		
		if(center_n45 < 0) center_n45 += 360;
		if(center_p45 >360) center_p45 -= 360;
		
		int degree = (int)Math.ceil(center_n45);
		int unit = width/90;
		
		int initPos = (int)(iniLeft + 4.5*unit + (degree - center_n45)*unit);
		String text;
		
		for(int i = 0 ; i <= 90 ; i++ ){
			
			if(degree>=360) degree = degree -360;
			
			
			//draw text
			switch(degree){
			case 0: 
				drawText(canvas, "N", initPos + i * unit - textSize, iniTop + textSize);
				break;
				
			case 45:
				drawText(canvas, "NE", initPos + i * unit - textSize, iniTop + textSize);
				break;
				
			case 90:
				drawText(canvas, "E", initPos + i * unit - textSize, iniTop + textSize);
				break;
			
			case 135:
				drawText(canvas, "SE", initPos + i * unit - textSize, iniTop + textSize);
				break;
				
			case 180:
				drawText(canvas, "S", initPos + i * unit - textSize, iniTop + textSize);
				break;
				
			case 225:
				drawText(canvas, "SW", initPos + i * unit - textSize, iniTop + textSize);
				break;
			
			case 270:
				drawText(canvas, "W", initPos + i * unit - textSize, iniTop + textSize);
				break;
				
			case 315:
				drawText(canvas, "NW", initPos + i * unit - textSize, iniTop + textSize);
				break;
			
			case 360:
				drawText(canvas, "N", initPos + i * unit - textSize, iniTop + textSize);
				break;
				
			default:
				//draw circle
				
				if(degree%9==0){
					_circlePath.addCircle(initPos + i * unit, iniTop + _height/2, textSize/16, Direction.CCW);
					canvas.drawPath(_circlePath, _circlePaint);
					
				}
			}
			
			degree += 1;
			
			
		}
		
		
		canvas.restore();
	}
	
	private void drawText(Canvas canvas, String s, int x , int y){
		_textPath = new Path();
		_circlePath = new Path();
		_textPath.moveTo(x, y);
		_textPath.lineTo(x + textSize * 2, y);
		
		canvas.drawTextOnPath(s, _textPath, 0, textSize/6, _textPaint);
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		
		//canvas.drawPath(_framePath, _framePaint);
		drawData(canvas);
		invalidate();
	}

	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub
		_location = location;
	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub
//		mValues = event.values.clone();
//        
//		if(_location != null){
//			GeomagneticField geoField = new GeomagneticField((float)_location.getLatitude(), (float)_location.getLongitude(), (float)_location.getAltitude(), System.currentTimeMillis());
//			mValues[0] = mValues[0] + geoField.getDeclination();
//			//Log("GeomagneticField Declination: " + geoField.getDeclination());
//		}else{
//			mValues[0] = mValues[0];
//		}
		

	}
	
	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub
		if(_location != null){
			GeomagneticField geoField = new GeomagneticField((float)_location.getLatitude(), (float)_location.getLongitude(), (float)_location.getAltitude(), System.currentTimeMillis());
			mValues[0] = orientation[0] + geoField.getDeclination();
			//mValues[1] = (float) Math.toDegrees(orientation[1]);
		}else{
		
			mValues[0] = orientation[0];
		}
	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		_textPaint.setColor(color);
		_circlePaint.setColor(color);
		_framePaint.setColor(color);
		

	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		iniLeft += pin[0] * uniPixel;
		iniTop += pin[1] * uniPixel;
		
		//paths & paints
		Path _textPath;
		Path _circlePath;
		Path _framePath;
		Path _clipPath;
		
		Paint _textPaint;
		Paint _circlePaint;
		Paint _framePaint;
		
		int width = uniPixel * ELEM_WIDTH;
		int height = uniPixel* ELEM_HEIGHT;
		int textSize = (int) (uniPixel/1.5);
		
		Typeface font;
		try{
			font = Typeface.createFromAsset(context.getAssets(), "fonts/SquadaOne-Regular.ttf");
		}catch(Exception e){
			font = Typeface.DEFAULT;
		}
		
		_framePath = new Path();
		_framePath.addRect(iniLeft, iniTop, iniLeft+ELEM_WIDTH*uniPixel, iniTop+ELEM_HEIGHT*uniPixel, Direction.CCW);
		_framePath.moveTo(iniLeft + width/2 , iniTop);
		_framePath.lineTo(iniLeft + width/2 , iniTop + height);
		
		_framePaint = new Paint();
		_framePaint.setAntiAlias(true);
		_framePaint.setColor(Color.CYAN);
		_framePaint.setStyle(Style.STROKE);
		_framePaint.setStrokeWidth(uniPixel/32);
		
		_circlePath = new Path();
		
		_circlePaint = new Paint();
		_circlePaint.setAntiAlias(true);
		_circlePaint.setColor(Color.CYAN);
		
		_textPath = new Path();
		_textPaint = new Paint();
		_textPaint.setAntiAlias(true);
		_textPaint.setColor(Color.CYAN);
		_textPaint.setTextAlign(Align.CENTER);
		_textPaint.setTextSize(textSize);
		_textPaint.setTypeface(font);
		
		
		_clipPath = new Path();
		_clipPath.addRect(iniLeft, iniTop, iniLeft+ELEM_WIDTH*uniPixel, iniTop+ELEM_HEIGHT*uniPixel, Direction.CCW);
		
		canvas.save();
		canvas.drawPath(_framePath, _framePaint);
		
		canvas.clipRect(new Rect(iniLeft, iniTop, iniLeft+ELEM_WIDTH*uniPixel, iniTop+ELEM_HEIGHT*uniPixel));
		
		float center = 100;
		float center_n45 = center - 45;
		float center_p45 = center + 45;
		
		if(center_n45 < 0) center_n45 += 360;
		if(center_p45 >360) center_p45 -= 360;
		
		int degree = (int)Math.ceil(center_n45);
		int unit = width/90;
		
		int initPos = (int)(iniLeft + 4.5*unit + (degree - center_n45)*unit);
		String text;
		
		for(int i = 0 ; i <= 90 ; i++ ){
			
			if(degree>=360) degree = degree -360;
			
			
			//draw text
			switch(degree){

				
		
				
			case 90:
				
				_textPath = new Path();
				_circlePath = new Path();
				_textPath.moveTo(initPos + i * unit - textSize, iniTop + textSize);
				_textPath.lineTo(initPos + i * unit - textSize + textSize * 2, iniTop + textSize);
				
				canvas.drawTextOnPath("E", _textPath, 0, textSize/6, _textPaint);
				break;
			
			case 135:
				
				_textPath = new Path();
				_circlePath = new Path();
				_textPath.moveTo(initPos + i * unit - textSize, iniTop + textSize);
				_textPath.lineTo(initPos + i * unit - textSize + textSize * 2, iniTop + textSize);
				
				canvas.drawTextOnPath("SE", _textPath, 0, textSize/6, _textPaint);
				break;
				
				
			default:
				//draw circle
				
				if(degree%9==0){
					_circlePath.addCircle(initPos + i * unit, iniTop + height/2, textSize/16, Direction.CCW);
					canvas.drawPath(_circlePath, _circlePaint);
					
				}
			}
			
			degree += 1;
			
			
		}
		
		
		canvas.restore();
		
	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_circlePaint.setColor(_parent.GLOBAL_COLOR);
		_framePaint.setColor(_parent.GLOBAL_COLOR);
	}
	
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		ELEM_NAME = name;
	}

	@Override
	public void onIsGPSFix(GpsStatus status, boolean isFix) {
		// TODO Auto-generated method stub
		
	}



}
