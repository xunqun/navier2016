package idv.xunqun.navier.v2.parts;


import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.SpeedUnit;
import idv.xunqun.navier.utils.Utility;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;


public class Elem_SpeedDigital_Large extends Parts {

	
	//Default Value
	public static final int ELEM_HEIGHT = 6;
	public static final int ELEM_WIDTH = 8;    	
	public static final int ELEM_PART = Parts.ELEM_SPEED_DIGITAL_LARGE;
	public static String ELEM_NAME = "Digital Speed";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_digital_speed_large;
	
	public static final String UNIT_KMH = "Km/h";
	public static final String UNIT_MPH = "Mph";
	public static final long TIMER_TOTAL = 1000;
	public static final long TIMER_TICK = 200;
	
	//Paint and Path
	Paint _digPaint;
	
	
	Path _digPath;
	
	Path _unitPath;
	
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	
	private float _speed=0;
	private float _wordspace;
	private float _duration;
	private Location _preLocation=null;
	
	CountDownTimer _timer;
	
	private float _animationStep;
	private int _animationCount;
	private float _oldSpeed;
	
	
	private GridBoard _parent;
	private PreferencesHandler _settings;
	
	
	public Elem_SpeedDigital_Large(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		this.ELEM_PIN = pin;
		_parent = parent;
		
		initProperty();
		initPath();
		
	}

	
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		String showText;
		String unitText;
		
		if(_parent.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH){
			showText = String.format("%.0f", Math.abs(_speed));
			unitText = UNIT_KMH;
		}else{
			showText = String.format("%.0f", Math.abs(SpeedUnit.kmh2mph(_speed)));
			unitText = UNIT_MPH;
		}
		
		
		
		

		if(_speed < _settings.getPREF_ALERT_SPEED()){
			_digPaint.setColor(_parent.GLOBAL_COLOR);
			
		}else{
			_digPaint.setColor(_parent.SECONDARY_COLOR);
		}
		
		canvas.drawTextOnPath(showText, _digPath, 0 , 0,  _digPaint);
		invalidate();
		
		
	}


	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub
		
		//if(_timer!=null)_timer.cancel();
		
		
		if(location.getProvider().equals(LocationManager.GPS_PROVIDER) && location.getSpeed()!=0){
			 _oldSpeed = _speed;
			 _speed = location.getSpeed()/1000*3600;
			 //animSpeed(location.getSpeed());
			 
		}
		
		if(location.getProvider().equals(LocationManager.GPS_PROVIDER) && location.getSpeed()==0 && _preLocation!=null){
			_oldSpeed = _speed;
			_speed = Utility.caculateSpeed(location, _preLocation)/1000*3600;
			//float speed = Utility.caculateSpeed(location, _preLocation);
			//animSpeed(speed);
		}
		_preLocation = location;
		
	}
	
	private void animSpeed(float speed){
		speed = speed/1000*3600;
		_animationStep = (speed-_oldSpeed)/(TIMER_TOTAL/TIMER_TICK);
		_animationCount = 1;
		 
		 _timer = new CountDownTimer(TIMER_TOTAL, TIMER_TICK) {

		     public void onTick(long millisUntilFinished) {
		    	 stepUP();
		         
		     }

		     public void onFinish() {
		    	 //stepUP();
		     }
		     
		     public void stepUP(){
		    	 _speed += _animationStep;
		         _animationCount+=1;
		         
		     }
		  };
		 _timer.start();
	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}

	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this._iniLeft + (this._width/2);
    	_centerY = this._iniTop + (this._height/2);
    	_wordspace = _parent._unitPixel/6;
    	
    	
    	_settings = new PreferencesHandler(_parent._context);
    	
	}
	
	public void initPath(){
		
		// add word line
		
		_digPaint = new Paint();
		_digPaint.setAntiAlias(true);
		_digPaint.setColor(_parent.GLOBAL_COLOR);
		_digPaint.setLinearText(true);
		_digPaint.setTextAlign(Align.CENTER);
		_digPaint.setTextSize(_parent._unitPixel*ELEM_HEIGHT*0.8f);
		
		Typeface font=_parent._defaultFont;
		_digPaint.setTypeface(font);
		
		
		
		//_digPaint.setTextSkewX(-0.25f);
		
		_digPath = new Path();
		_digPath.moveTo( _iniLeft,_iniTop+ELEM_HEIGHT*_parent._unitPixel);
		_digPath.lineTo( _iniLeft+(ELEM_WIDTH)*_parent._unitPixel, _iniTop+ELEM_HEIGHT*_parent._unitPixel);
		
		
	}



	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		_digPaint.setColor(color);
	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		Typeface font;
		try{
			font = Typeface.createFromAsset(context.getAssets(), "fonts/SquadaOne-Regular.ttf");
		}catch(Exception e){
			font = Typeface.DEFAULT;
		}
		
		Path digPath = new Path();	
		
		digPath.moveTo( iniLeft+pin[0]*uniPixel,iniTop+ELEM_HEIGHT*uniPixel+pin[1]*uniPixel);
		digPath.lineTo( iniLeft+(ELEM_WIDTH)*uniPixel+pin[0]*uniPixel, iniTop+ELEM_HEIGHT*uniPixel+pin[1]*uniPixel);
		
		Paint digPaint = new Paint();
		digPaint.setAntiAlias(true);
		digPaint.setColor(Color.CYAN);
		digPaint.setLinearText(true);
		digPaint.setTypeface(font);
		digPaint.setTextAlign(Align.CENTER);
		
		
		digPaint.setTypeface(font);
		
		//digPaint.setTextScaleX((ELEM_WIDTH)*uniPixel);

		
		canvas.save();
		
		
		digPaint.setTextAlign(Align.CENTER);
		digPaint.setTextSize(uniPixel*ELEM_HEIGHT*0.8f);
		
		canvas.drawTextOnPath("065", digPath, 0 , -uniPixel/6,  digPaint);

		canvas.restore();
	}



	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		_digPaint.setColor(_parent.GLOBAL_COLOR);
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		ELEM_NAME = name;
	}



	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onIsGPSFix(GpsStatus status,boolean isFix) {
		// TODO Auto-generated method stub
		
	}
}
