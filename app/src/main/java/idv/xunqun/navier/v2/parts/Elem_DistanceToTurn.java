package idv.xunqun.navier.v2.parts;

import idv.xunqun.navier.NavierPanelDesigner;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.content.RoutePlan;
import idv.xunqun.navier.v2.content.DirectionRoute;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.location.GpsStatus;
import android.os.Bundle;
import android.text.TextPaint;

public class Elem_DistanceToTurn extends NaviParts {

	// Default Value
	public static final int ELEM_HEIGHT = 2;
	public static final int ELEM_WIDTH = 4;
	public static final int ELEM_PART = Parts.ELEM_DISTANCE_TO_TURN;
	public static final boolean ELEM_ISNAVPART = true;
	public static final int ELEM_THUMBNAIL = R.drawable.part_distance_to_turn;

	private double mDistance;
	private DirectionRoute mDirectionPlan;
	private boolean isArrival;

	private NavigationGridBoard mParent;

	// property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;

	// path & paint
	private Path mTextPath, mDigiPath;
	private Paint mTextPaint, mDigiPaint;

	public Elem_DistanceToTurn(GridBoard parent, int[] pin) {
		super(parent, pin);
		mParent = (NavigationGridBoard) parent;
		this.ELEM_PIN = pin;
		initProperty();
		initPath();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		String text = "dist. to turn ";
		String distance;

		int curLeg = mDirectionPlan._current_leg;
		int curStep = mDirectionPlan._current_step + 1;

		if (!isArrival) {
			double result = mDistance / 1000;
			if (result < 1) {

				if (_parent.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH) {

					distance = String.format("%.0f", result * 1000) + " m";
				} else {

					distance = String.format("%.0f", result * 1000 * 3.2808399)
							+ " ft";
				}
			} else {
				if (_parent.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH) {

					distance = String.format("%.1f", result) + " km";
				} else {

					distance = String.format("%.1f", result * 0.621371192)
							+ " mile";
				}
			}

		} else {
			distance = "";
		}

		canvas.save();
		canvas.drawTextOnPath(text, mTextPath, 0, 0, mTextPaint);
		canvas.drawTextOnPath(distance, mDigiPath, 0, 0, mDigiPaint);
		canvas.restore();
		invalidate();
	}

	private void initProperty() {

		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]
				* _parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]
				* _parent._unitPixel;
		_centerX = this._iniLeft + (this._width / 2);
		_centerY = this._iniTop + (this._height / 2);

	}

	public void initPath() {

		Typeface font = _parent._defaultFont;
		mTextPath = new Path();
		mTextPath.moveTo(_iniLeft, _iniTop + _parent._unitPixel);
		mTextPath.lineTo(_iniLeft + ELEM_WIDTH * _parent._unitPixel, _iniTop
				+ _parent._unitPixel);

		mTextPaint = new Paint();
		mTextPaint.setAntiAlias(true);
		mTextPaint.setColor(_parent.GLOBAL_COLOR);
		mTextPaint.setLinearText(true);
		mTextPaint.setTextAlign(Align.LEFT);
		mTextPaint.setAlpha(100);
		mTextPaint.setTypeface(font);
		mTextPaint.setTextSize(_parent._unitPixel / 2);

		mDigiPath = new Path();
		mDigiPath.moveTo(_iniLeft, _iniTop + _parent._unitPixel * ELEM_HEIGHT);
		mDigiPath.lineTo(_iniLeft + ELEM_WIDTH * _parent._unitPixel, _iniTop
				+ _parent._unitPixel * ELEM_HEIGHT);

		mDigiPaint = new Paint();
		mDigiPaint.setAntiAlias(true);
		mDigiPaint.setColor(_parent.GLOBAL_COLOR);
		mDigiPaint.setLinearText(true);
		mDigiPaint.setTextAlign(Align.LEFT);
		mDigiPaint.setTypeface(font);
		mDigiPaint.setTextSize((float) (_parent._unitPixel));
	}

	@Override
	public void onRouteReplan(RoutePlan route) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDirectionReplan(DirectionRoute directionPlan) {
		mDirectionPlan = directionPlan;

	}

	@Override
	public void onArrival() {
		isArrival = true;

	}

	@Override
	public void onLeftDistanceNotify(double distance, double alarmDistance) {
		mDistance = distance;

	}

	@Override
	public void onLocationOnRoadChange(Latlng p, double d) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onIsGPSFix(GpsStatus status, boolean isFix) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int mainColor, int sndColor) {
		
		mTextPaint.setColor(mainColor); 
		mTextPaint.setAlpha(100);
		mDigiPaint.setColor(mainColor);
		invalidate();
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub

	}

	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft,
			int iniTop, int[] pin, NavierPanelDesigner context) {
		//paths & paints
		Path textPath;
		Path digiPath;

		Paint textPaint;
		Paint digiPaint;

		Typeface font;
		try {
			font = Typeface.createFromAsset(context.getAssets(),
					"fonts/SquadaOne-Regular.ttf");
		} catch (Exception e) {
			font = Typeface.DEFAULT;
		}

		textPath = new Path();
		textPath.moveTo(iniLeft + pin[0] * uniPixel, iniTop + uniPixel / 2
				+ pin[1] * uniPixel);
		textPath.lineTo(iniLeft + ELEM_WIDTH * uniPixel + pin[0] * uniPixel,
				iniTop + uniPixel / 2 + pin[1] * uniPixel);

		textPaint = new Paint();
		textPaint.setAntiAlias(true);
		textPaint.setColor(Color.CYAN);
		textPaint.setLinearText(true);
		textPaint.setTextAlign(Align.LEFT);
		textPaint.setTypeface(font);
		textPaint.setTextSize(uniPixel / 2);

		digiPath = new Path();
		digiPath.moveTo(iniLeft + pin[0] * uniPixel, iniTop + uniPixel
				* ELEM_HEIGHT + pin[1] * uniPixel);
		digiPath.lineTo(iniLeft + ELEM_WIDTH * uniPixel + pin[0] * uniPixel,
				iniTop + uniPixel * ELEM_HEIGHT + pin[1] * uniPixel);

		digiPaint = new Paint();
		digiPaint.setAntiAlias(true);
		digiPaint.setColor(Color.CYAN);
		digiPaint.setLinearText(true);
		digiPaint.setTextAlign(Align.LEFT);
		digiPaint.setTypeface(font);
		digiPaint.setTextSize((float) (uniPixel * 1.5));

		//
		canvas.save();
		canvas.drawTextOnPath("dist. to turn ", textPath, 0, 0, textPaint);
		canvas.drawTextOnPath("123 m", digiPath, 0, 0, digiPaint);

		canvas.restore();

	}

}
