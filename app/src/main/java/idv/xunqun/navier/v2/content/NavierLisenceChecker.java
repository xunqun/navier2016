package idv.xunqun.navier.v2.content;

import idv.xunqun.navier.R;
import idv.xunqun.navier.Version;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;


public class NavierLisenceChecker {
	private LicenseChecker mChecker;
	private FragmentActivity mContext;
	private Handler mLicenseHandler;
	private LicenseCheckerCallback mCallback;
	
	// Generate 20 random bytes, and put them here.
    private static final byte[] SALT = new byte[] {
     -46, 65, 30, -128, -103, -57, 74, -64, 51, 88, -95,
     -45, 77, -117, -36, -113, -11, 32, -64, 89
     };
	
	public NavierLisenceChecker(FragmentActivity context, LicenseCheckerCallback callback){
		
		mContext = context;
		mCallback = callback;
		
		if(!Version.isLite(context)){
			
			//Check licensing --------------------
			mLicenseHandler = new Handler();
			// Construct the LicenseCheckerCallback. The library calls this when done.
	        
	
	        // Try to use more data here. ANDROID_ID is a single point of attack.
	       
	        String deviceId = Settings.Secure.getString(mContext.getContentResolver(),Settings.Secure.ANDROID_ID);
	        
	        // Construct the LicenseChecker with a Policy.
	        mChecker = new LicenseChecker(  context, new ServerManagedPolicy(context,
	                new AESObfuscator(SALT, context.getPackageName(), deviceId)),
	            context.getString(R.string.publish_key)  // Your public licensing key.
	            );
	        
	        
	        //------------------------------------
	        
		}
	}
	

	
	public void doCheck() {
        
        mContext.setProgressBarIndeterminateVisibility(true);
        
        mChecker.checkAccess(mCallback);
    }
	

	private void displayResult(final String result) {
        mLicenseHandler.post(new Runnable() {
            public void run() {
            	/*
                mStatusText.setText(result);
                setProgressBarIndeterminateVisibility(false);
                mCheckLicenseButton.setEnabled(true);
                */
            }
        });
    }
}
