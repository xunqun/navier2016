package idv.xunqun.navier.v2.parts;

import idv.xunqun.navier.R;

import java.util.Iterator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

public class Elem_GpsChart_Large extends Parts {

	public static final int ELEM_HEIGHT = 4;
	public static final int ELEM_WIDTH = 8;    	
	public static final int ELEM_PART = Parts.ELEM_GPSCHART_LARGE;
	public static String ELEM_NAME = "GPS CHART LARGE";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_gpschart;
	
	/*
	 * savedInstancesState naming rule
	 * e.g. SAVEDSTATE_CLASSNAME_VALUENAME
	 */
	
	
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	
	//path & paint
	Path _decorationPath;
	Path _textPath;
	
	Paint _decorationPaint;
	Paint _movingDataPaint;
	Paint _textPaint;
	
	
	//values
	private final int SAT_MAXCOUNT = 10;
	private Iterable<GpsSatellite> _satellites;
	private long _latestTime=0;
	private int _numOfSat = 0;
	private String _stateDesc="";
	public boolean _isFix = false;
	
	
	public Elem_GpsChart_Large(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		initProperty();
		initPath();
	}

	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	
	}
	

	
	private void initPath(){
		
		Typeface font = _parent._defaultFont;
		
		
		_decorationPath = new Path();
		_decorationPath.addRect(_iniLeft, (int)(_iniTop + _height*0.6), _iniLeft + _width, _iniTop + _height, Direction.CCW);
		
		
		_decorationPaint = new Paint();
		_decorationPaint.setAntiAlias(true);
		_decorationPaint.setStyle(Style.STROKE);
		_decorationPaint.setStrokeWidth(_parent._unitPixel/32);
		_decorationPaint.setColor(_parent.GLOBAL_COLOR);
		
		
		_movingDataPaint = new Paint();
		_movingDataPaint.setAntiAlias(true);
		_movingDataPaint.setStyle(Style.FILL);
		_movingDataPaint.setColor(_parent.GLOBAL_COLOR);
		
		_textPath = new Path();
		_textPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel);
		_textPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel);
		
		
		_textPaint = new Paint();
		_textPaint.setAntiAlias(true);
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setTypeface(font);
		_textPaint.setTextSize(_parent._unitPixel/2);
		_textPaint.setAlpha(100);
		
	}
	
	private void drawMovingDataPath(Canvas canvas){
		Path dataPath = new Path();
		Path strokePath = new Path();
		
		long deltaTime = (System.currentTimeMillis()-_latestTime);
		if(_satellites!=null && deltaTime<5000){
			
			int space = (_width/SAT_MAXCOUNT);
			_numOfSat = 0;
			int count = 0;
			int prePrn = -1;
			
			
			Iterator<GpsSatellite> it = _satellites.iterator();
			while(it.hasNext() ){
				
				dataPath.reset();
				strokePath.reset();
				GpsSatellite sat = it.next();
				int prn = sat.getPrn();
				float snr = sat.getSnr();
				if(prn != prePrn){
					//Log.d("mine", "sat prn: " + prn + " snr: "+ snr + " deltaTime: "+ deltaTime);

					
					int dataHeight = (int) (_height*snr/100);
					int apha = (int) (155 + snr);
					dataPath.addRect(_iniLeft+ space*count, _iniTop + _height - dataHeight, _iniLeft + space*(count+1), _iniTop + _height, Direction.CCW);
					strokePath.addRect(_iniLeft+ space*count, _iniTop + _height - dataHeight, _iniLeft + space*(count+1), _iniTop + _height, Direction.CCW);
					
					_movingDataPaint.setAlpha(255);
					_movingDataPaint.setStyle(Style.STROKE);
					_movingDataPaint.setColor(_parent.GLOBAL_COLOR);

					canvas.drawPath(strokePath, _movingDataPaint);
					
					
					
					if(sat.usedInFix()){
						_numOfSat+=1;
						_movingDataPaint.setAlpha(255);
						
					}else{
						_movingDataPaint.setAlpha(150);
					}
					
					_movingDataPaint.setStyle(Style.FILL);
					canvas.drawPath(dataPath, _movingDataPaint);
					//count++;
				}
				prePrn = prn;
				count++;
				
			}
			if(_isFix){
				_stateDesc = "("+_numOfSat+"/"+count+")  Fixed";
			}else{
				_stateDesc = "("+_numOfSat+"/"+count+")";
			}
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		canvas.clipRect(_iniLeft, _iniTop, _iniLeft + _width, _iniTop + _height);
		canvas.drawPath(_decorationPath, _decorationPaint);
		drawMovingDataPath(canvas);
		canvas.drawTextOnPath("gps chart " + _stateDesc, _textPath, 0, 0, _textPaint);
		invalidate();
	}
	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub
		if(location.getProvider().equals(LocationManager.GPS_PROVIDER)){
			_isFix = true;
		}else{
			_isFix = false;
		}
		
	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onIsGPSFix(GpsStatus status, boolean isFix) {
		// TODO Auto-generated method stub
		_satellites = status.getSatellites();
		_latestTime = System.currentTimeMillis();
		//_isFix = isFix;
		//invalidate();
	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		_decorationPaint.setColor(color);
		_movingDataPaint.setColor(color);
		_textPaint.setColor(color);
		_textPaint.setAlpha(100);
		invalidate();
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		_decorationPaint.setColor(_parent.GLOBAL_COLOR);
		_movingDataPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setAlpha(100);
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		//property
		int _width = uniPixel * ELEM_WIDTH;
		int _height = uniPixel * ELEM_HEIGHT;
		int _iniTop = iniTop + pin[1]*uniPixel;
		int _iniLeft = iniLeft + pin[0]*uniPixel;
		
		//path & paint
		Path _decorationPath;
		Path _textPath;
		
		Paint _decorationPaint;
		Paint _movingDataPaint;
		Paint _textPaint;
		
		// initPath
		
		Typeface font;
		try{
			font = Typeface.createFromAsset(context.getAssets(), "fonts/SquadaOne-Regular.ttf");
		}catch(Exception e){
			font = Typeface.DEFAULT;
		}
		
		_decorationPath = new Path();
		_decorationPath.addRect(_iniLeft, (int)(_iniTop + _height*0.6), _iniLeft + _width, _iniTop + _height, Direction.CCW);
		
		
		_decorationPaint = new Paint();
		_decorationPaint.setAntiAlias(true);
		
		_decorationPaint.setStyle(Style.STROKE);
		_decorationPaint.setStrokeWidth(uniPixel/32);
		_decorationPaint.setColor(Color.CYAN);
		
		
		_movingDataPaint = new Paint();
		_movingDataPaint.setAntiAlias(true);
		_movingDataPaint.setStyle(Style.FILL);
		_movingDataPaint.setColor(Color.CYAN);
		
		_textPath = new Path();
		_textPath.moveTo( _iniLeft,_iniTop+uniPixel/2);
		_textPath.lineTo( _iniLeft+ELEM_WIDTH*uniPixel, _iniTop+uniPixel/2);
		
		_textPaint = new Paint();
		_textPaint.setAntiAlias(true);
		_textPaint.setColor(Color.CYAN);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setTypeface(font);
		_textPaint.setTextSize(uniPixel/2);
		_textPaint.setAlpha(100);
		
		canvas.save();
		canvas.clipRect(_iniLeft, _iniTop, _iniLeft + _width, _iniTop + _height);
		canvas.drawPath(_decorationPath, _decorationPaint);
		
		//gen data path
		
		Path dataPath = new Path();
		Path strokePath = new Path();
		
		
		
			
		int space = (_width/10);
		
		int count = 0;
		int prePrn = -1;
		
		
		
		dataPath.reset();
		strokePath.reset();

		//1
		int snr = 32;
		int dataHeight = (int) (_height*snr/100);
		int apha = (int) (155 + snr);
		
		dataPath.addRect(_iniLeft+ space*count, _iniTop + _height - dataHeight, _iniLeft + space*(count+1), _iniTop + _height, Direction.CCW);
		strokePath.addRect(_iniLeft+ space*count, _iniTop + _height - dataHeight, _iniLeft + space*(count+1), _iniTop + _height, Direction.CCW);
		
		_movingDataPaint.setAlpha(255);
		_movingDataPaint.setStyle(Style.STROKE);
		_movingDataPaint.setColor(Color.CYAN);

		canvas.drawPath(strokePath, _movingDataPaint);
		_movingDataPaint.setAlpha(150);
		_movingDataPaint.setStyle(Style.FILL);
		canvas.drawPath(dataPath, _movingDataPaint);
			//count++;
		

		//2
		snr = 23;
		dataHeight = (int) (_height*snr/100);
		apha = (int) (155 + snr);
		count  +=1;
		
		dataPath.addRect(_iniLeft+ space*count, _iniTop + _height - dataHeight, _iniLeft + space*(count+1), _iniTop + _height, Direction.CCW);
		strokePath.addRect(_iniLeft+ space*count, _iniTop + _height - dataHeight, _iniLeft + space*(count+1), _iniTop + _height, Direction.CCW);
		
		_movingDataPaint.setAlpha(255);
		_movingDataPaint.setStyle(Style.STROKE);
		_movingDataPaint.setColor(Color.CYAN);

		canvas.drawPath(strokePath, _movingDataPaint);
		_movingDataPaint.setAlpha(150);
		_movingDataPaint.setStyle(Style.FILL);
		canvas.drawPath(dataPath, _movingDataPaint);
		
		//3
		snr = 12;
		dataHeight = (int) (_height*snr/100);
		apha = (int) (155 + snr);
		count  +=1;
		
		dataPath.addRect(_iniLeft+ space*count, _iniTop + _height - dataHeight, _iniLeft + space*(count+1), _iniTop + _height, Direction.CCW);
		strokePath.addRect(_iniLeft+ space*count, _iniTop + _height - dataHeight, _iniLeft + space*(count+1), _iniTop + _height, Direction.CCW);
		
		_movingDataPaint.setAlpha(255);
		_movingDataPaint.setStyle(Style.STROKE);
		_movingDataPaint.setColor(Color.CYAN);

		canvas.drawPath(strokePath, _movingDataPaint);
		_movingDataPaint.setAlpha(150);
		_movingDataPaint.setStyle(Style.FILL);
		canvas.drawPath(dataPath, _movingDataPaint);
		
		//4
		snr = 38;
		dataHeight = (int) (_height*snr/100);
		apha = (int) (155 + snr);
		count  +=1;
		
		dataPath.addRect(_iniLeft+ space*count, _iniTop + _height - dataHeight, _iniLeft + space*(count+1), _iniTop + _height, Direction.CCW);
		strokePath.addRect(_iniLeft+ space*count, _iniTop + _height - dataHeight, _iniLeft + space*(count+1), _iniTop + _height, Direction.CCW);
		
		_movingDataPaint.setAlpha(255);
		_movingDataPaint.setStyle(Style.STROKE);
		_movingDataPaint.setColor(Color.CYAN);

		canvas.drawPath(strokePath, _movingDataPaint);
		_movingDataPaint.setAlpha(150);
		_movingDataPaint.setStyle(Style.FILL);
		canvas.drawPath(dataPath, _movingDataPaint);
		
		
		String _stateDesc = "(3/4)  Fixed";

		
		canvas.drawTextOnPath("gps chart " + _stateDesc, _textPath, 0, 0, _textPaint);
		canvas.restore();
	}

}
