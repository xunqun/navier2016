package idv.xunqun.navier.v2.parts;



import idv.xunqun.navier.R;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.content.RoutePlan;
import idv.xunqun.navier.v2.parts.GridBoard;
import idv.xunqun.navier.v2.content.DirectionRoute;
import idv.xunqun.navier.v2.content.DirectionStep;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Bundle;
import android.text.Html;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;

public class Elem_NextTurn extends NaviParts {


	public static final int ELEM_HEIGHT = 2;
	public static final int ELEM_WIDTH = 8;    	
	public static final int ELEM_PART = Parts.ELEM_NEXTTURN;
	public static String ELEM_NAME = "Next Turn";
	public static final boolean ELEM_ISNAVPART = true;
	public static final int ELEM_THUMBNAIL = R.drawable.part_nextturn;
	
	public boolean _showNextTurn = false;
	public static final int DISTANCE = 700;
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	private int _textSize;
	private int _signcenterX;
	private int _signcenterY;
	
	
	//paths & paints
	private Path _textPath;
	private Path _signPath;
	private Path _signToPath;
	
	private TextPaint _textPaint;
	private Paint _signPaint;
	
	//values
	public NavigationGridBoard _parent;
	public int[] _pin;
	private boolean _arrived = false;
	
	
	public Elem_NextTurn(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		_parent = (NavigationGridBoard) parent;
		
		_pin = pin;
		
		initProperty();
		initPath();
		
	}

	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this._iniLeft + (this._width/2);
    	_centerY = this._iniTop + (this._height/2);
    	_textSize = _parent._unitPixel/2;
    	_signcenterX = _iniLeft + _parent._unitPixel;
    	_signcenterY = _iniTop +  _parent._unitPixel;
    	
	}
	

	
	private void initPath(){
		
		// text
		_textPath = new Path();
		_textPath.moveTo(_iniLeft+2*_parent._unitPixel, _iniTop+_textSize);
		_textPath.lineTo(_iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_textSize);
		_textPath.moveTo(_iniLeft+2*_parent._unitPixel, _iniTop+_textSize*2);
		_textPath.lineTo(_iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_textSize*2);
		
		_textPaint = new TextPaint();
		_textPaint.setAntiAlias(true);
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setTextSize(_textSize);
		
		
		
		// sign  draw during runtime
		int width =  _parent._unitPixel/2;
		_signPath = new Path(); 
		_signPath.addRect(_signcenterX-width/2, _signcenterY, _signcenterX+width/2, _signcenterY+_parent._unitPixel, Direction.CCW);
		_signPath.addCircle(_signcenterX, _signcenterY, width/2, Direction.CCW);
		
		_signToPath = new Path();
		
		_signToPath.moveTo(_signcenterX-width/2, _signcenterY);
		_signToPath.lineTo(_signcenterX-width/2, _signcenterY+width);
		_signToPath.lineTo(_signcenterX-width, _signcenterY+width);
		_signToPath.lineTo(_signcenterX, _signcenterY+width*2);
		_signToPath.lineTo(_signcenterX+width, _signcenterY+width);
		_signToPath.lineTo(_signcenterX+width/2, _signcenterY+width);
		_signToPath.lineTo(_signcenterX+width/2, _signcenterY);
		_signToPath.close();
		
		_signPaint = new Paint();
		_signPaint.setAntiAlias(true);
		_signPaint.setColor(_parent.GLOBAL_COLOR);
		_signPaint.setStyle(Style.FILL);
		_signPaint.setAntiAlias(true);
		
	}
	
	private void drawSign(Canvas canvas){

		canvas.drawPath(_signPath, _signPaint);
		float angle = (float) _parent._naviManager.getNextTurnAngle();
		if(angle != -1 ){
			canvas.save();
			canvas.rotate(angle, _signcenterX, _signcenterY);
			canvas.drawPath(_signToPath, _signPaint);		
					
			canvas.restore();
		}
		
	}
	
	private String textlayout(String text){
		
		text.replaceAll("<div", "<b");
		text.replaceAll("</div", "</b");
		
		
		
		return Html.fromHtml(text).toString();
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		if(_parent._naviManager!=null && _showNextTurn){
			canvas.clipRect(new RectF(_iniLeft, _iniTop, _iniLeft+_width, _iniTop+_height));
			canvas.save();
			canvas.translate(_iniLeft, _iniTop);
			String instr = _parent._naviManager._nextTurnInstruction;
			
			if(instr!=null){
				//new StaticLayout(Html.fromHtml(instr), _textPaint, _width, Alignment.ALIGN_CENTER, 1.2f, 1f ,true).draw(canvas);
				new StaticLayout(textlayout(instr), _textPaint, _width, Alignment.ALIGN_CENTER, 1.2f, 1f ,true).draw(canvas);
			}
			canvas.restore();
		}
	}


	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		int textSize = uniPixel/2;
		iniLeft += pin[0] * uniPixel;
		iniTop += pin[1] * uniPixel;
		
		//paths & paints
		Path textPath;

		TextPaint textPaint;
		
		
		// text
		textPath = new Path();
		textPath.moveTo(iniLeft+2*uniPixel, iniTop+textSize);
		textPath.lineTo(iniLeft+ELEM_WIDTH*uniPixel, iniTop+textSize);
		textPath.moveTo(iniLeft+2*uniPixel, iniTop+textSize*2);
		textPath.lineTo(iniLeft+ELEM_WIDTH*uniPixel, iniTop+textSize*2);
		
		textPaint = new TextPaint();
		textPaint.setAntiAlias(true);
		textPaint.setColor(Color.CYAN);
		textPaint.setTextSize(textSize);
		
		canvas.save();
		canvas.clipRect(new RectF(iniLeft, iniTop, iniLeft+uniPixel*ELEM_WIDTH, iniTop+uniPixel*ELEM_HEIGHT));
		
		canvas.translate(iniLeft, iniTop);
		String instr = "Turn right onto N Rengstorff Ave";
		
		if(instr!=null){
			new StaticLayout(Html.fromHtml(instr), textPaint, uniPixel*ELEM_WIDTH, Alignment.ALIGN_CENTER, 1.2f, 1f ,true).draw(canvas);
		}
		canvas.restore();
		
	}

	@Override
	public void onRouteReplan(RoutePlan route) {
		// TODO Auto-generated method stub
		_arrived = false;
	}

	@Override
	public void onArrival() {
		// TODO Auto-generated method stub
		_arrived = true;
	}

	@Override
	public void onLeftDistanceNotify(double distance, double alarmDistance) {
		// TODO Auto-generated method stub
		int curLeg = _parent._naviManager._directionPlan._current_leg;
		int curStep = _parent._naviManager._directionPlan._current_step;
		DirectionStep step = _parent._naviManager._directionPlan._legList.get(curLeg)._stepList.get(curStep);
		if(distance < DISTANCE && step.distance_value > DISTANCE && distance > alarmDistance){
			_showNextTurn = true;
		}else{
			_showNextTurn = false;
		}
		
		invalidate();
	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		ELEM_NAME = name;
	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLocationOnRoadChange(Latlng p, double angle) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onIsGPSFix(GpsStatus status,boolean isFix) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDirectionReplan(DirectionRoute directionPlan) {
		// TODO Auto-generated method stub
		
	}

}
