package idv.xunqun.navier.v2.dialog;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;


import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.content.Layout;
import idv.xunqun.navier.content.LayoutItem;
import idv.xunqun.navier.http.PushPanelTask;
import idv.xunqun.navier.http.PushPanelTask.OnPushPanelTaskListener;
import idv.xunqun.navier.http.SyncPlaceTask;
import idv.xunqun.navier.provider.LayoutProviderMetadata.LayoutTableMetadata;
import idv.xunqun.navier.v2.content.Panel;
import idv.xunqun.navier.v2.content.PanelStore;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PanelNameSettingDialog extends DialogFragment {

	TextView nameTextView;
	private Panel mPanel;
	private PreferencesHandler mPref;
	private OnPanelNameSettingListener mListener;

	public interface OnPanelNameSettingListener {
		void onNameComplete();
	}

	public void setPanel(Panel panel) {
		mPanel = panel;
	}

	public void setOnPanelNameSettingListener(OnPanelNameSettingListener listener) {
		mListener = listener;
	}

	@Override
	public void onActivityCreated(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onActivityCreated(arg0);
		mPref = new PreferencesHandler(getActivity());
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		View view = inflater.inflate(R.layout.dialog_panelnamesetting, null);
		nameTextView = (TextView) view.findViewById(R.id.name);
		nameTextView.setText(mPanel.LAYOUT_NAME);

		Button ok = (Button) view.findViewById(R.id.ok_name);
		Button cancel = (Button) view.findViewById(R.id.cancel_name);
		ok.setOnClickListener(onClickListener);
		cancel.setOnClickListener(onClickListener);

		builder.setView(view);
		return builder.create();

	}

	private void doRename(Panel panel, String name) {

		SQLiteDatabase db = MyDBOpenHelper.getDb(getActivity());

		try {
			JSONObject json = new JSONObject(panel.LAYOUT_JSONSTR);
			((JSONObject) json.get(Layout.JSON_ROOT)).put(Layout.JSON_NAME, name);

			panel.LAYOUT_NAME = name;

			ContentValues values = new ContentValues();
			values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, json.toString());

			// String whereClause = MyPlacesDBOpenHelper.LAYOUT_COL_ID + "=?";
			String whereClause = MyDBOpenHelper.LAYOUT_COL_FINGERPRINT + "=?";
			String[] whereArgs = new String[] { String.valueOf(panel.LAYOUT_FINGERPRINT) };

			db.update(MyDBOpenHelper.LAYOUT_TABLE_NAME, values, whereClause, whereArgs);
			// this.getContentResolver().update(Uri.withAppendedPath(LayoutTableMetadata.CONTENT_URI,layout.LAYOUT_FINGERPRINT),
			// values, null, null);

			// insert layout sync
			values = new ContentValues();
			values.put(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE, MyDBOpenHelper.STATE_MODIFY);
			values.put(MyDBOpenHelper.SYNC_LAYOUT_LAYOUTID, panel.LAYOUT_ID);
			values.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT, panel.LAYOUT_FINGERPRINT);

			db.insert(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME, null, values);
			
			PanelStore.notifyLayoutSetChange(getActivity());
			doSync();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		getDialog().dismiss();

	}

	private void doSync() {
		// TODO Auto-generated method stub

		if (!mPref.getPREF_ACCOUNT().equals(getString(R.string.PREF_ACCOUNT_DEFAULT))) {
			new PushPanelTask(getActivity(), null, mPref.getPREF_ACCOUNT()).execute();
		}
	}

	OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.ok_name) {
				String name = nameTextView.getText().toString();
				if (TextUtils.isEmpty(name)) {
					nameTextView.setError("Required field");
				} else {
					doRename(mPanel, name);
					mListener.onNameComplete();
					getDialog().dismiss();

				}
			}

			else if (v.getId() == R.id.cancel_name) {
				getDialog().dismiss();
			}
		}
	};

}
