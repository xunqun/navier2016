package idv.xunqun.navier.v2;

import idv.xunqun.navier.LocationService;
import idv.xunqun.navier.NavigationManager;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.Version;
import idv.xunqun.navier.utils.InstantNotificationReceiver;
import idv.xunqun.navier.utils.InstantNotificationReceiver.InstantNotificationReceiverListener;
import idv.xunqun.navier.utils.MockLocationGenerateTask;
import idv.xunqun.navier.utils.Utility;
import idv.xunqun.navier.v2.RuntimePanelFragment.RtPanelFragmentHandler;
import idv.xunqun.navier.v2.parts.NavigationGridBoard;

import java.util.Locale;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

@TargetApi(VERSION_CODES.HONEYCOMB_MR2)
public class RunTimeActivity extends Activity implements RtPanelFragmentHandler {

	private final int MaxFilterCount = 6;

	private boolean _showingBack = false;
	private Handler _handler = new Handler();

	private RuntimePanelFragment _rtPanelFragment;

	// hardware controler
	SensorManager _sensorManager;
	LocationManager _locationManager;
	PowerManager.WakeLock _wl;
	private TextToSpeech _tts;
	final int _matrix_size = 16;

	// Sensor Listener
	public boolean _isGPSFix = false;
	public long _LastLocationMillis;
	public GPSStateListener _gpsStateListener;

	// HW properties
	public Location _currentBestLocation;
	WindowManager.LayoutParams _layoutParams;
	private static MediaPlayer mediaPlayer;
	// Preferences
	private PreferencesHandler _settings;

	private static final int _AVERAGE_NUM = 10;
	private int _averageCounting = 0;
	private int _averageIndex = 0;
	private int _preAverage = 0;
	private float[] _valuesToAverage = new float[_AVERAGE_NUM];

	// for location dither filter
	private Location mPrelocation;
	private int mFilterCount = 0;
	private InterstitialAd interstitial;
	private boolean isAdShowed = false;

	private final SensorEventListener _sensorListener = new SensorEventListener() {
		final int matrix_size = 16;

		float[] R = new float[matrix_size];
		float[] outR = new float[matrix_size];
		float[] I = new float[matrix_size];
		float[] values = new float[3];
		boolean isReady = false;
		float[] mags = null;
		float[] accels = null;
		float[] orients = new float[3];

		DigitalAverage[] avg = { new DigitalAverage(), new DigitalAverage(), new DigitalAverage() };

		private long lastMagsTime;
		private long lastAccelsTime;
		private long lastOrientsTime;

		public void onSensorChanged(SensorEvent event) {

			Sensor sensor = event.sensor;
			int type = sensor.getType();

			switch (type) {
			case Sensor.TYPE_MAGNETIC_FIELD:
				mags = event.values.clone();

				break;
			case Sensor.TYPE_ACCELEROMETER:

				accels = event.values.clone();
				/*
				 * if(accels[0]>100){ accels = null; }
				 */
				break;
			case Sensor.TYPE_ORIENTATION:
				orients = event.values.clone();
				try {
					_rtPanelFragment._gridBoard.onSensorChangeHandler(event);
				} catch (Exception e) {
					e.printStackTrace();
				}

				break;

			}

			if (mags != null && accels != null) {

				SensorManager.getRotationMatrix(R, null, accels, mags);
				SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_Y,
						SensorManager.AXIS_MINUS_X, outR);
				SensorManager.getOrientation(outR, values);

				// Log.d("mine", "accels: " + accels[0] + " mags: " + mags[0] +
				// " angle: " + v[0]);
				// Log.d("mine", "oriention: (" + Math.toDegrees(values[0]) +
				// " , " + Math.toDegrees(values[1]) + " , " +
				// Math.toDegrees(values[2]) +" )" );

				values[0] = (getAverage((float) Math.toDegrees(values[0])) + 360) % 360 - 90
						+ getOffset();

				values[1] = ((float) Math.toDegrees(values[1]) + 360) % 360;
				values[2] = ((float) Math.toDegrees(values[2]) + 360) % 360;
				try {

					// if (Math.abs(orients[2]) < 60) {
					_rtPanelFragment._gridBoard.onSensorChangeHandler(values);

					Log.i("heading", "heading " + values[0]);
					// } else {
					// orients[0] = orients[0] + getOffset();
					// _rtPanelFragment._gridBoard
					// .onSensorChangeHandler(orients);
					// }
				} catch (Exception e) {
					e.printStackTrace();
				}
				accels = null;
				mags = null;

			}
		}

		private int getOffset() {

			Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
					.getDefaultDisplay();
			int orientation = 0;
			int result = 0;
			orientation = display.getRotation();
			switch (orientation) {
			case Surface.ROTATION_0:
				result = 0;
				break;
			case Surface.ROTATION_90:
				result = 90;
				break;
			case Surface.ROTATION_180:
				result = 180;
				break;
			case Surface.ROTATION_270:
				result = 270;
				break;
			}

			return result;
		}

		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};

	private BroadcastReceiver _MessageReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (!_rtPanelFragment.isAdded())
				return;

			if (intent.getAction().equals(LocationService.EXTRA_ONLOCATIONCHANGE)) {

				try {
					Location location = intent.getParcelableExtra("location");

					// filter the dither location
					if (mPrelocation != null
							&& location.distanceTo(mPrelocation) < location.getAccuracy()
							&& mFilterCount < MaxFilterCount) {
						mFilterCount++;
						return;
					} else {
						mFilterCount = 0;
						mPrelocation = location;
					}

					if (NavigationManager.isBetterLocation(location, _currentBestLocation)) {
						_rtPanelFragment._gridBoard.onLocationChangeHandler(location);
						_currentBestLocation = location;
						_LastLocationMillis = SystemClock.elapsedRealtime();
					} else {
						// just update location
						// mRoute.setCurrentLocation(new
						// Latlng(location.getLatitude(),location.getLongitude()));
						// mRoute.invalidate();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (intent.getAction().equals(LocationService.EXTRA_ONPROVIDERDISABLEED)) {

				String provider = intent.getStringExtra("provider");
				_rtPanelFragment._gridBoard.onLocationProviderDisableHandle(provider);
			}

			if (intent.getAction().equals(LocationService.EXTRA_ONPROVIDERENABLED)) {

			}

			if (intent.getAction().equals(LocationService.EXTRA_ONSTATUSCHANGED)) {
				try {
					String provider = intent.getStringExtra("provider");
					int status = intent.getIntExtra("status", -1);
					Bundle extras = intent.getBundleExtra("extras");

					_rtPanelFragment._gridBoard.onLocationStatusChangeHandle(provider, status,
							extras);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

	};

	private ActionBar _actionBar;
	private MockLocationGenerateTask _mock;
	protected boolean mTtsMissing = false;
	private boolean mActionbarVisible;
	private GestureDetectorCompat _detector;
	private static boolean _isPlayingSound = false;
	InstantNotificationReceiver mNotificationReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_runtime);

		_settings = new PreferencesHandler(this);

		// HW setting
		screenBrightSetting();

		initValue();
		initView(savedInstanceState);
		initAd();

		createTextToSpeech(this, Locale.getDefault());
	}

	private void initAd() {
		if (Version.isLite(this)) {
			// 建立插頁式廣告。
			interstitial = new InterstitialAd(this);
			interstitial.setAdUnitId("ca-app-pub-2264979852880694/6279699220");

			// 建立廣告請求。
			AdRequest adRequest = new AdRequest.Builder().build();

			// 開始載入您的插頁式廣告。
			interstitial.loadAd(adRequest);
		}
	}

	@Override
	public void onBackPressed() {
		// Used to specify is Activity is returned
		if (Version.isLite(this) && !isAdShowed) {
			isAdShowed = true;
			if(!displayInterstitial()){
				super.onBackPressed();
			}

		} else {
			super.onBackPressed();
		}

	}

	public boolean displayInterstitial() {
		if (interstitial.isLoaded()) {
			interstitial.setAdListener(new AdListener() {
				@Override
				public void onAdClosed() {
					finish();
					super.onAdClosed();
					
				}
				
			});
			interstitial.show();
			return true;
		}else{
			return false;
		}
	}

	
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		_detector.onTouchEvent(event);
		return true;
	}


	private float getAverage(float value) {

		_valuesToAverage[_averageIndex] = value;

		if (_averageIndex < _AVERAGE_NUM - 1) {
			_averageIndex += 1;
		} else {
			_averageIndex = 0;
		}
		if (_averageCounting < _AVERAGE_NUM) {
			_averageCounting++;
		}

		boolean cross360 = false;

		if (_averageCounting > 1) {
			for (int i = 0; i < _averageCounting - 1; i++) {

				if (Math.abs(_valuesToAverage[i] - _valuesToAverage[i + 1]) > 180) {
					cross360 = true;

				}
			}

			if (cross360) {

				for (int i = 0; i < _averageCounting; i++) {

					if (_valuesToAverage[i] > 0) {
						_valuesToAverage[i] = _valuesToAverage[i] - 360;
					}

				}

				float sum = 0;
				for (int i = 0; i < _averageCounting; i++) {
					sum = sum + _valuesToAverage[i];
				}

				return sum / _averageCounting;

			} else {
				float sum = 0;
				for (int i = 0; i < _averageCounting; i++) {
					sum = sum + _valuesToAverage[i];
				}

				return sum / _averageCounting;
			}

		} else {
			return value;
		}

	}

	public void initHWSensor() {
		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
		// sensor

		_sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

		// have been deprecated

		Sensor sensor = _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		_sensorManager.registerListener(_sensorListener, sensor, SensorManager.SENSOR_DELAY_GAME);

		sensor = _sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		_sensorManager.registerListener(_sensorListener, sensor, SensorManager.SENSOR_DELAY_GAME);

		sensor = _sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		_sensorManager.registerListener(_sensorListener, sensor, SensorManager.SENSOR_DELAY_GAME);

		// GPS sensor

		_locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		_gpsStateListener = new GPSStateListener();
		_locationManager.addGpsStatusListener(_gpsStateListener);

		// audio
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		mediaPlayer = new MediaPlayer();
		// tts

		// mock location
		// if ((getApplication().getApplicationInfo().flags &
		// ApplicationInfo.FLAG_DEBUGGABLE) != 0) {
		// if(_mock == null && MockLocationGenerateTask._running == false){
		// _mock = new MockLocationGenerateTask(this);
		// _mock.execute();
		// }
		// }

	}

	public void createTextToSpeech(final Context context, final Locale locale) {

		_tts = new TextToSpeech(context, new OnInitListener() {
			@Override
			public void onInit(int status) {

				if (status == TextToSpeech.SUCCESS) {
					try {
						Locale defaultOrPassedIn = locale;
						if (locale == null) {
							defaultOrPassedIn = Locale.getDefault();
						}
						// check if language is available

						switch (_tts.isLanguageAvailable(defaultOrPassedIn)) {
						case TextToSpeech.LANG_AVAILABLE:
						case TextToSpeech.LANG_COUNTRY_AVAILABLE:
						case TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE:

							// _tts.setLanguage(locale);
							// pass the tts back to the main
							// activity for use
							break;
						case TextToSpeech.LANG_MISSING_DATA:

							if (_rtPanelFragment._gridBoard instanceof NavigationGridBoard) {

								mTtsMissing = true;
							}

							break;
						case TextToSpeech.LANG_NOT_SUPPORTED:

							if (_rtPanelFragment._gridBoard instanceof NavigationGridBoard) {

								mTtsMissing = true;
							}
							break;
						}
					} catch (Exception e) {
						mTtsMissing = true;
					}
				}
			}
		});
	}

	public void ttsSpeak(String s) {
		try {
			if (_settings.getPREF_NAVI_VOICE() && !mTtsMissing) {

				_tts.speak(s, TextToSpeech.QUEUE_ADD, null);
			} else if (_settings.getPREF_NAVI_VOICE() && mTtsMissing) {

				String url = "http://translate.google.com/translate_tts?ie=utf-8";
				url += "&tl=" + Locale.getDefault();
				url += "&q=" + s;

				playOlSound(url);

			}
		} catch (Exception e) {

		}
	}

	private void playOlSound(final String url) {

		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {

				if (!_isPlayingSound) {
					play();
				} else {
					while (_isPlayingSound) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					play();

				}

			}

			void play() {
				try {
					_isPlayingSound = true;

					// mediaPlayer = new MediaPlayer();
					mediaPlayer.reset();
					mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

						@Override
						public void onCompletion(MediaPlayer mp) {

							_isPlayingSound = false;
						}
					});
					mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
					mediaPlayer.setDataSource(url);
					mediaPlayer.prepare(); // might take long! (for buffering,
											// etc)
					mediaPlayer.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		thread.start();

	}

	public void onRequestAgps() {
		Bundle bundle = new Bundle();
		_locationManager.sendExtraCommand(LocationManager.GPS_PROVIDER, "delete_aiding_data",
				bundle);
		_locationManager.sendExtraCommand(LocationManager.GPS_PROVIDER, "force_xtra_injection",
				bundle);
		_locationManager.sendExtraCommand(LocationManager.GPS_PROVIDER, "force_time_injection",
				bundle);
		Toast.makeText(this, getString(R.string.navierpanel_agps), Toast.LENGTH_SHORT).show();
	}

	private void screenLockSetting() throws Exception {

		if (Build.VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD) {
			if (_settings.getPREF_LOCK_SCREEN_ORIENTATION()) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			}
		}

	}

	private void screenBrightSetting() {

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		_wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "TAG");

		// _wl.acquire();

		// ..screen will stay on during this section..
		_layoutParams = getWindow().getAttributes();
	}

	public void releaseBright() {
		if (_wl != null) {
			_wl.release();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(_MessageReceiver);

		// if (_settings.getPREF_BRIGHTNESS_SETTING()) {
		// _layoutParams.screenBrightness =
		// WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
		// getWindow().setAttributes(_layoutParams);
		// }

		try {
			if (_tts != null && _tts.isSpeaking()) {

				_tts.stop();
			}
			if (_wl != null)
				_wl.release();

		} catch (Exception e) {

		}

		Utility.requestLocationServiceStop(this);
		_sensorManager.unregisterListener(_sensorListener);
	}

	@Override
	protected void onResume() {
		super.onResume();
		initHWSensor();
		_actionBar.hide();
		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver,
				new IntentFilter(LocationService.EXTRA_ONLOCATIONCHANGE));
		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver,
				new IntentFilter(LocationService.EXTRA_ONPROVIDERDISABLEED));
		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver,
				new IntentFilter(LocationService.EXTRA_ONPROVIDERENABLED));
		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver,
				new IntentFilter(LocationService.EXTRA_ONSTATUSCHANGED));

		if (_wl == null) {
			screenBrightSetting();
		}
		_wl.acquire();

		try {
			screenLockSetting();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (_settings.getPREF_BRIGHTNESS_SETTING()) {

			_layoutParams.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;

			getWindow().setAttributes(_layoutParams);

		}

		Utility.requestLocationServiceUpdate(this);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Log.d("lifecycle", "RuntimePanelFragment.onOptionsItemSelected()");
		int id = item.getItemId();

		if (_rtPanelFragment.isResumed()) {
			if (id == R.id.doswitch) {
				_rtPanelFragment.onModeSwitch();
			}

			else if (id == R.id.speed) {
				_rtPanelFragment.onSpeedUnitChange();
			}

			else if (id == R.id.color) {
				_rtPanelFragment.onColorChange();
			}

			else if (id == R.id.grid) {
				_rtPanelFragment.onTogleGrid();

			}

			// else if(id == R.id.rightleft){
			// _rtPanelFragment.onLeftRightSwitch();
			// }

			else if (item.getItemId() == android.R.id.home) {
				Intent it = new Intent(this, HomeActivity.class);
				startActivity(it);
			}

			/*
			 * if(id == R.id.agps){ onRequestAgps(); }
			 */
		}

		return true;

	}

	@Override
	protected void onDestroy() {

		try {
			// _wl.release(); // screan bright
			_tts.shutdown();
			_tts = null;
		} catch (Exception e) {

		}
		super.onDestroy();
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {

		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.panel_menu, menu);
		return true;
	}

	private void initValue() {
		_detector = new GestureDetectorCompat(this, new MyGestureListener());

		final FrameLayout contentView = (FrameLayout) findViewById(R.id.container);
		// Set up an instance of SystemUiHider to control the system UI for
		// this activity.

	}

	private void initView(Bundle savedInstanceState) {

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		_actionBar = getActionBar();
		_actionBar.setDisplayHomeAsUpEnabled(true);
		_actionBar.setDisplayShowTitleEnabled(false);
		_actionBar.setDisplayUseLogoEnabled(false);
		_actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.underline_in_black));
		_actionBar.setTitle("");

		if (savedInstanceState == null) {
			_rtPanelFragment = new RuntimePanelFragment();
			getFragmentManager().beginTransaction().add(R.id.container, _rtPanelFragment).commit();
		} else {
			// pop pre fragment
			_rtPanelFragment = (RuntimePanelFragment) getFragmentManager().findFragmentById(
					R.id.container);
		}

	}

	private class DigitalAverage {

		final int history_len = 5;
		double[] mLocHistory = new double[history_len];
		int mLocPos = 0;
		float preAvg = 0;

		// ------------------------------------------------------------------------------------------------------------
		float average(double d) {

			float avg = 0;
			double sum2 = 0;
			double sum3 = 0;
			double z = 0;

			mLocHistory[mLocPos] = d;

			mLocPos++;
			if (mLocPos > mLocHistory.length - 1) {
				mLocPos = 0;
			}
			for (double h : mLocHistory) {
				avg += h;
			}
			avg /= mLocHistory.length;

			for (int e = 0; e < mLocHistory.length; e++) {
				sum2 += (mLocHistory[e] - avg) * (mLocHistory[e] - avg);
			}

			sum3 = (sum2 / (mLocHistory.length));

			z = Math.sqrt(sum3);

			return avg;

		}
	}

	private class GPSStateListener implements GpsStatus.Listener {
		public void onGpsStatusChanged(int event) {

			switch (event) {
			case GpsStatus.GPS_EVENT_SATELLITE_STATUS:

				GpsStatus status = _locationManager.getGpsStatus(null);

				if (_currentBestLocation != null)
					_isGPSFix = (SystemClock.elapsedRealtime() - _LastLocationMillis) < 3000;

				try {
					if (_isGPSFix) { // A fix has been acquired.
						// Do something.
						_rtPanelFragment._gridBoard.onIsGPSFix(status, true);
					} else { // The fix has been lost.
						// Do something.
						_rtPanelFragment._gridBoard.onIsGPSFix(status, false);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				break;
			case GpsStatus.GPS_EVENT_FIRST_FIX:
				// Do something.
				_isGPSFix = true;

				break;
			}

		}
	}

	@Override
	public void doTTS(String text) {

		this.ttsSpeak(text);
	}

	/*
	 * Gesture Listener
	 */
	private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

		@Override
		public boolean onSingleTapUp(MotionEvent e) {

			if (_actionBar.isShowing()) {
				_actionBar.hide();
			} else {

				// auto hide the action bar after 5 sec
				_actionBar.show();
				new CountDownTimer(5000, 5000) {

					@Override
					public void onTick(long arg0) {

					}

					@Override
					public void onFinish() {
						_actionBar.hide();

					}
				}.start();
			}
			return true;
		}

	}
}
