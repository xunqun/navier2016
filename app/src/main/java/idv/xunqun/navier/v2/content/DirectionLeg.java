package idv.xunqun.navier.v2.content;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.content.Latlng;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class DirectionLeg {

	public ArrayList<DirectionStep> _stepList = new ArrayList();
	public int _current_step = 0;
	public int _current_rpoint = 0;
	public int _totalDistance = 0; // in meter

	public int _totalDuration = 0; // in seconds
	public String _distanString;
	public String _durationString;
	public boolean _isPassed = false;
	public int[] maneuverIndexes;

	public boolean setJson(JSONObject j) throws JSONException {

		JSONArray steps = new JSONArray();

		steps = j.getJSONArray("steps");
		_totalDistance = j.getJSONObject("distance").getInt("value");
		_distanString = j.getJSONObject("distance").getString("text");
		_totalDuration = j.getJSONObject("duration").getInt("value");
		_durationString = j.getJSONObject("duration").getString("text");
		for (int i = 0; i < steps.length(); i++) {
			JSONObject obj = steps.getJSONObject(i);
			DirectionStep step = new DirectionStep();
			step.setJson(obj);
			_stepList.add(step);

		}

		return true;
	}

	public boolean setMapQuestJson(Context context, JSONObject j, ArrayList<Latlng> all_ploy) throws JSONException {
		JSONArray steps = new JSONArray();
		steps = j.getJSONArray("maneuvers");
		_totalDistance = (int) (j.getDouble("distance") * 1000);
		_distanString = "";
		_totalDuration = j.getInt("time");
		_durationString = j.getString("formattedTime");

		for (int i = 0; i < steps.length(); i++) {
			JSONObject obj = steps.getJSONObject(i);
			DirectionStep step = new DirectionStep();

			step.setMapQuestJson(obj);

			List<Latlng> list;
			if (i == steps.length() - 1) {
				list = all_ploy.subList(maneuverIndexes[i], all_ploy.size());
			} else {
				list = all_ploy.subList(maneuverIndexes[i], maneuverIndexes[i + 1]);
			}
			step.setPolyline(list);
			_stepList.add(step);

		}

		return true;
	}

	public void setManeuverIndexes(JSONArray indexArray) throws JSONException {
		maneuverIndexes = new int[indexArray.length()];
		for (int i = 0; i < indexArray.length(); i++) {
			maneuverIndexes[i] = indexArray.getInt(i);
		}
	}

	public void setHereJson(Context context, JSONObject j, ArrayList<Latlng> vectorList) throws JSONException {
		_totalDistance = j.getInt("length");
		_totalDuration = j.getInt("travelTime");
		if(PreferencesHandler.get(context).getPREF_SPEED_UNIT() == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH){
			_distanString = String.format("%1$.1f", _totalDistance / 1000f) + " km";
		}else{
			_distanString = String.format("%1$.1f", _totalDistance *0.000621371192f) + " mile";
		}
		_durationString = String.format("%1$.1f", _totalDuration / 60f) + " min)";

		JSONArray maneuver = j.getJSONArray("maneuver");
		for (int i = 0; i < maneuver.length(); i++) {
			JSONObject obj = maneuver.getJSONObject(i);
			DirectionStep step = new DirectionStep();
			step.setHereJson(context,obj);
			_stepList.add(step);

		}

		for (int i = 0; i < _stepList.size(); i++) {
			DirectionStep step = _stepList.get(i);
			DirectionStep nextStep;
			if (i + 1 < _stepList.size()) {
				nextStep = _stepList.get(i + 1);
			} else {
				nextStep = null;
			}
			boolean in = false;
			ArrayList<Latlng> vectors = new ArrayList<Latlng>();
			for (int k = 0; k < vectorList.size(); k++) {
				if (step.startlocation_lat == vectorList.get(k).getLat() && step.startlocation_lng == vectorList.get(k).getLng()) {
					in = true;
				}
				if (nextStep != null && nextStep.startlocation_lat == vectorList.get(k).getLat()
						&& nextStep.startlocation_lng == vectorList.get(k).getLng()) {
					in = false;
					vectors.add(new Latlng(vectorList.get(k).getLat(), vectorList.get(k).getLng()));
					break;
				}
				if (in) {
					vectors.add(new Latlng(vectorList.get(k).getLat(), vectorList.get(k).getLng()));
				}
			}
			step.setPolyline(vectors);
		}

	}

}
