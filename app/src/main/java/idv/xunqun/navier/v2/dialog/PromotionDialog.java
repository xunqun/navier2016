package idv.xunqun.navier.v2.dialog;

import idv.xunqun.navier.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ViewFlipper;

public class PromotionDialog extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		View view = inflater.inflate(R.layout.dialog_promotion, null);
		view.findViewById(R.id.go).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				String url = "http://ezrt.io/c630cd1";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
		builder.setView(view);
		Dialog dialog = builder.create();
		
	    
		return dialog;
	}



}
