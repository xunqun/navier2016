package idv.xunqun.navier.v2.dialog;

import idv.xunqun.navier.R;
import idv.xunqun.navier.v2.content.DirectionRoute;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("ValidFragment")
public class IntroListDialog extends DialogFragment {

	private LayoutInflater mInflater;
	private Context mContext;
	private DirectionRoute mRoute;

	public IntroListDialog(Context context, DirectionRoute route) {
		mContext = context;
		mRoute = route;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		View view = mInflater.inflate(R.layout.view_intro_list, null);
		ListView listView = (ListView) view.findViewById(R.id.listView);
		listView.setAdapter(new IntroListAdapter());

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(view);
		return builder.create();
	}

	private class IntroListAdapter extends BaseAdapter {

		private LayoutInflater inflater;

		public IntroListAdapter() {
			inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			try {
				return mRoute._legList.get(mRoute._current_leg)._stepList.size();

			} catch (Exception e) {
				return 0;
			}
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.view_introlist_item, null);
			}

			ImageView image = (ImageView) convertView.findViewById(R.id.image);
			image.setVisibility(View.GONE);
			TextView intro = (TextView) convertView.findViewById(R.id.intro);
			
			

			String introString = mRoute._legList.get(mRoute._current_leg)._stepList.get(position).html_instructions;
			intro.setText(Html.fromHtml(introString).toString());


			return convertView;
		}

	}
}
