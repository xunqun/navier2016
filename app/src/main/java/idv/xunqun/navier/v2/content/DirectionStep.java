package idv.xunqun.navier.v2.content;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.utils.Utility;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification.Action;
import android.content.Context;

public class DirectionStep {
	public String distance_text;
	public int distance_value;
	public String duration_text;
	public int duration_value;
	public double endlocation_lat;
	public double endlocation_lng;
	public String polyline;
	public List<Latlng> polyline_list;
	public String html_instructions;
	public double startlocation_lat;
	public double startlocation_lng;
	public String travel_mode;
	public boolean _isStepAlarmShowed = false;
	public String streets = "";
	private Latlng location;
	public String nextStreets;
	public String action;

	public void setJson(JSONObject json) throws JSONException {
		distance_text = json.getJSONObject("distance").getString("text");
		distance_value = json.getJSONObject("distance").getInt("value");
		duration_text = json.getJSONObject("duration").getString("text");
		duration_value = json.getJSONObject("duration").getInt("value");
		endlocation_lat = json.getJSONObject("end_location").getDouble("lat");
		endlocation_lng = json.getJSONObject("end_location").getDouble("lng");
		html_instructions = json.getString("html_instructions");

		startlocation_lat = json.getJSONObject("start_location").getDouble(
				"lat");
		startlocation_lng = json.getJSONObject("start_location").getDouble(
				"lng");
		travel_mode = json.getString("travel_mode");

		setPolyline(json.getJSONObject("polyline").getString("points"));
	}

	public void setMapQuestJson(JSONObject json) throws JSONException {

		JSONArray array = json.getJSONArray("streets");
		for (int i = 0; i < array.length(); i++) {
			if (i == 0) {
				streets = array.getString(i);
			} else if (i == array.length() - 1) {
				streets += array.getString(i);

			} else {
				streets += array.getString(i) + "/";
			}
		}

		html_instructions = json.getString("narrative");

		setMapQuestJson(json, streets, html_instructions);
		// setPolyline( json.getJSONObject("polyline").getString("points"));
	}

	public void setMapQuestJson(JSONObject json, String streetName,
			String narrative) throws JSONException {
		distance_text = "";
		distance_value = (int) (json.getDouble("distance") * 1000);
		duration_text = json.getString("formattedTime");
		duration_value = json.getInt("time");
		JSONArray array = json.getJSONArray("streets");
		streets = streetName;
		// endlocation_lat =
		// json.getJSONObject("end_location").getDouble("lat");
		// endlocation_lng =
		// json.getJSONObject("end_location").getDouble("lng");
		html_instructions = narrative;

		startlocation_lat = json.getJSONObject("startPoint").getDouble("lat");
		startlocation_lng = json.getJSONObject("startPoint").getDouble("lng");
		travel_mode = json.getString("transportMode");

	}

	public void setPolyline(List<Latlng> list) {

		polyline_list = list;
	}

	public void setPolyline(String s) {
		this.polyline = s;
		this.polyline_list = Utility.decodePoly(s);
	}

	public void setHereJson(Context context, JSONObject json)
			throws JSONException {
		location = new Latlng(json.getJSONObject("position").getDouble(
				"latitude"), json.getJSONObject("position").getDouble(
				"longitude"));
		distance_value = json.getInt("length");
		action = json.getString("action");
		if (PreferencesHandler.get(context).getPREF_SPEED_UNIT() == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH) {
			distance_text = String.format("%1$.1f", distance_value / 1000f)
					+ " km";
		} else {
			distance_text = String.format("%1$.1f", distance_value *0.000621371192f)
					+ " mile";
		}
		duration_value = json.getInt("travelTime");
		duration_text = String.format("%1$.1f", duration_value / 60f) + " min)";

		// endlocation_lat =
		// json.getJSONObject("position").getDouble("latitude");
		// endlocation_lng =
		// json.getJSONObject("position").getDouble("longitude");
		html_instructions = json.getString("instruction");
		// maneuver = json.has("maneuver") ? json.getString("maneuver") : "";
		streets = json.getString("roadName");
		nextStreets = json.getString("nextRoadName");
		action = json.getString("action");
		startlocation_lat = json.getJSONObject("position")
				.getDouble("latitude");
		startlocation_lng = json.getJSONObject("position").getDouble(
				"longitude");

	}

}
