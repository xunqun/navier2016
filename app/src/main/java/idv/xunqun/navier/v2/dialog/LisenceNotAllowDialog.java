package idv.xunqun.navier.v2.dialog;

import idv.xunqun.navier.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ViewFlipper;

public class LisenceNotAllowDialog extends DialogFragment {
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		
		
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		View view = inflater.inflate(R.layout.dialog_license_not_allow, null);
		

		builder.setView(view);
		return builder.create();
	}
}
