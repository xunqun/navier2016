package idv.xunqun.navier.v2.content;

import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.utils.Utility;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

/**
 * Each DirectionPlan contains several DirectionLeg, Each DirectionLeg contains
 * several DirectionStep ex:
 * http://maps.google.com/maps/api/directions/json?origin
 * =22.975548%2C120.220565&
 * destination=22.981952%2C120.228718&waypoints=22.981715
 * %2C120.215714%7C22.981952
 * %2C120.228718&mode=driving&sensor=true&language=zh_TW
 * 
 * @author xunqun
 * 
 */
public class DirectionRoute {

	public String _oriJsonString;
	public String _oriMapQuestJsonString;
	public String _oriHereJsonString;
	public ArrayList<DirectionLeg> _legList = new ArrayList<DirectionLeg>();
	public ArrayList<Latlng> _overview_polyline;
	public ArrayList<Latlng> _wayPointList = new ArrayList<Latlng>(); // The
																		// last
																		// one
																		// in
																		// list
																		// is
																		// the
																		// destination
																		// for
																		// Google
																		// direction
																		// API
																		// [waypoint1,
																		// waypoint2,
																		// ...,
																		// destination]
	
	public String _fromString;
	public Latlng _from;
	public int _current_leg = 0;
	public int _current_step = 0;
	public int _current_rpoint = 0;
	public Latlng _current_perpen_point;
	public int _totalDistance = 0;
	public String mSummary;
	public LatLng bound_ne;
	public LatLng bound_sw;
	private ArrayList<Latlng> _turnPoints;
	public String _toString;
	

	public boolean setJson(String jsonString) throws Exception {

		_oriJsonString = jsonString;

		JSONObject jsonObject = new JSONObject(jsonString);

		JSONArray legs = jsonObject.getJSONArray("legs");
		_overview_polyline = Utility.decodePoly(jsonObject.getJSONObject("overview_polyline").getString("points"));
		mSummary = jsonObject.getString("summary");
		double lat = jsonObject.getJSONObject("bounds").getJSONObject("northeast").getDouble("lat");
		double lng = jsonObject.getJSONObject("bounds").getJSONObject("northeast").getDouble("lng");
		bound_ne = new LatLng(lat, lng);
		lat = jsonObject.getJSONObject("bounds").getJSONObject("southwest").getDouble("lat");
		lng = jsonObject.getJSONObject("bounds").getJSONObject("southwest").getDouble("lng");
		bound_sw = new LatLng(lat, lng);
		for (int i = 0; i < legs.length(); i++) {
			DirectionLeg leg = new DirectionLeg();
			leg.setJson(legs.getJSONObject(i));
			_legList.add(leg);
		}

		return true;
	}

	public boolean setMapQuestJson(Context context, JSONObject route)throws Exception {

		_oriMapQuestJsonString = route.toString();
		JSONArray legs = route.getJSONArray("legs");
		_overview_polyline = Utility.decodePoly(route.getJSONObject("shape").getString("shapePoints"));
		mSummary = "";
		double lat = route.getJSONObject("boundingBox").getJSONObject("ul").getDouble("lat");
		double lng = route.getJSONObject("boundingBox").getJSONObject("ul").getDouble("lng");
		bound_ne = new LatLng(lat, lng);
		lat = route.getJSONObject("boundingBox").getJSONObject("lr").getDouble("lat");
		lng = route.getJSONObject("boundingBox").getJSONObject("lr").getDouble("lat");
		bound_sw = new LatLng(lat, lng);

		for (int i = 0; i < legs.length(); i++) {
			DirectionLeg leg = new DirectionLeg();
			leg.setManeuverIndexes(route.getJSONObject("shape").getJSONArray("maneuverIndexes"));
			leg.setMapQuestJson(context, legs.getJSONObject(i), _overview_polyline);

			_legList.add(leg);
		}

		return true;
	}
	
	public boolean setHereJson(Context context,JSONObject route) {
		try {
			_oriHereJsonString = route.toString();
			JSONObject leg = route.getJSONArray("leg").getJSONObject(0);
			JSONArray shape = route.getJSONArray("shape");
			_overview_polyline = new ArrayList<Latlng>();
			
			_fromString = route.getJSONArray("waypoint").getJSONObject(0).getString("mappedRoadName");
			_toString = route.getJSONArray("waypoint").getJSONObject(1).getString("mappedRoadName");
			
			for (int i = 0; i < shape.length(); i++) {
				String coor = shape.getString(i);
				String[] arr = coor.split(",");
				double lat = Double.valueOf(arr[0]);
				double lng = Double.valueOf(arr[1]);
				_overview_polyline.add(new Latlng(lat, lng));
			}

			_turnPoints = new ArrayList<Latlng>();
			for (int i = 0; i < leg.getJSONArray("maneuver").length(); i++) {
				JSONObject maneuver = leg.getJSONArray("maneuver").getJSONObject(i);
				double lat = maneuver.getJSONObject("position").getDouble("latitude");
				double lng = maneuver.getJSONObject("position").getDouble("longitude");
				_turnPoints.add(new Latlng(lat, lng));
			}

			mSummary = "";
			double lat = route.getJSONObject("boundingBox").getJSONObject("topLeft").getDouble("latitude");
			double lng = route.getJSONObject("boundingBox").getJSONObject("topLeft").getDouble("longitude");
			bound_ne = new LatLng(lat, lng);
			lat = route.getJSONObject("boundingBox").getJSONObject("bottomRight").getDouble("latitude");
			lng = route.getJSONObject("boundingBox").getJSONObject("bottomRight").getDouble("longitude");
			bound_sw = new LatLng(lat, lng);

			DirectionLeg directionLeg = new DirectionLeg();
			directionLeg.setHereJson(context,leg, _overview_polyline);
			_legList.add(directionLeg);

			return true;
		} catch (Exception e) {

		}
		return false;
	}

	/**
	 * Given a waypoint/destination list. The last one in list is the
	 * destination for Google direction API [waypoint1, waypoint2, ...,
	 * destination]
	 * 
	 * @param list
	 * @return the JSON String for inter-activity transition
	 * @throws JSONException
	 */
	public void setWayPoint(ArrayList<Latlng> list) throws JSONException {

		_wayPointList = list;

	}

	public ArrayList<Latlng> setWayPoint(JSONArray array) throws JSONException {

		_wayPointList = new ArrayList<Latlng>();
		for (int i = 0; i < array.length(); i++) {

			JSONObject point = array.getJSONObject(i);
			Latlng latlng = new Latlng(point.getDouble("lat"), point.getDouble("lng"));
			_wayPointList.add(latlng);

		}
		return _wayPointList;

	}

	public static String wayPointToJsonString(ArrayList<Latlng> list) throws JSONException {
		JSONArray array = new JSONArray();
		for (Latlng latlng : list) {
			JSONObject point = new JSONObject();
			point.put("lat", latlng.getLat());
			point.put("lng", latlng.getLng());
			array.put(point);
		}
		return array.toString();
	}
}
