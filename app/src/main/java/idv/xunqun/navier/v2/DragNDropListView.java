package idv.xunqun.navier.v2;



import idv.xunqun.navier.R;
import android.R.integer;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class DragNDropListView extends ListView {

	private DropListener _dropListener;
	private RemoveListener _removeListener;
	private DragListener _dragListener;
	private boolean _dragMode;
	
	int _startPosition;
	int _endPosition;
	int _dragPointOffset;		//Used to adjust drag view location
	private ImageView _dragView;
	

	public DragNDropListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public void setDropListener(DropListener l) {
		_dropListener = l;
	}

	public void setRemoveListener(RemoveListener l) {
		_removeListener = l;
	}
	
	public void setDragListener(DragListener l) {
		_dragListener = l;
	}
	

	
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		final int action = ev.getAction();
		final int x = (int) ev.getX();
		final int y = (int) ev.getY();	
		
		if (action == MotionEvent.ACTION_DOWN ) {  //&& x < this.getWidth()/2
			_dragMode = true;
		}

		if (!_dragMode) 
			return false;

		switch (action) {
			case MotionEvent.ACTION_DOWN:
				_startPosition = pointToPosition(x,y);
				if (_startPosition != INVALID_POSITION && _startPosition > 0) {  //Position 0 is my location
					int mItemPosition = _startPosition - getFirstVisiblePosition();
                    _dragPointOffset = y - getChildAt(mItemPosition).getTop();
                    _dragPointOffset -= ((int)ev.getRawY()) - y;
					startDrag(mItemPosition,y);
					drag(0,y);// replace 0 with x if desired
				}	
				break;
			case MotionEvent.ACTION_MOVE:
				drag(0,y);// replace 0 with x if desired
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
			default:
				_dragMode = false;
				_endPosition = pointToPosition(x,y);
				stopDrag(_startPosition - getFirstVisiblePosition());
				if (_dropListener != null && _startPosition != INVALID_POSITION && _endPosition != INVALID_POSITION) 
	        		 _dropListener.onDrop(_startPosition, _endPosition);
				break;
		}
		return true;
	}
	
	// move the drag view
	private void drag(int x, int y) {
		if (_dragView != null) {
			WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) _dragView.getLayoutParams();
			layoutParams.x = x;
			layoutParams.y = y - _dragPointOffset;
			WindowManager mWindowManager = (WindowManager) getContext()
					.getSystemService(Context.WINDOW_SERVICE);
			mWindowManager.updateViewLayout(_dragView, layoutParams);

			if (_dragListener != null)
				_dragListener.onDrag(x, y, this);// change null to "this" when ready to use
		}
	}

	// enable the drag view for dragging
	private void startDrag(int itemIndex, int y) {
		stopDrag(itemIndex);

		View item = getChildAt(itemIndex);
		if (item == null) return;
		item.setDrawingCacheEnabled(true);
		if (_dragListener != null)
			_dragListener.onStartDrag(item);
		
        // Create a copy of the drawing cache so that it does not get recycled
        // by the framework when the list tries to clean up memory
        Bitmap bitmap = Bitmap.createBitmap(item.getDrawingCache());
        
        WindowManager.LayoutParams mWindowParams = new WindowManager.LayoutParams();
        mWindowParams.gravity = Gravity.TOP;
        mWindowParams.x = 0;
        mWindowParams.y = y - _dragPointOffset;

        mWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mWindowParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        mWindowParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        mWindowParams.format = PixelFormat.TRANSLUCENT;
        mWindowParams.windowAnimations = 0;
        
        Context context = getContext();
        ImageView v = new ImageView(context);
        v.setImageBitmap(bitmap);      

        WindowManager mWindowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        mWindowManager.addView(v, mWindowParams);
        _dragView = v;
	}

	// destroy drag view
	private void stopDrag(int itemIndex) {
		if (_dragView != null) {
			if (_dragListener != null)
				_dragListener.onStopDrag(getChildAt(itemIndex));
            _dragView.setVisibility(GONE);
            WindowManager wm = (WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE);
            wm.removeView(_dragView);
            _dragView.setImageDrawable(null);
            _dragView = null;
        }
	}
}
