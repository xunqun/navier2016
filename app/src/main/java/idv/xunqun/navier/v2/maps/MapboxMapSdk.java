package idv.xunqun.navier.v2.maps;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerView;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import java.util.ArrayList;
import java.util.List;

import idv.xunqun.navier.R;

/**
 * Created by xunqun on 16/8/30.
 */
public class MapboxMapSdk implements MapSdk {
    MapView vMapView;
    MapboxMap mapboxMap;
    MapboxAccountManager mapboxAccountManager;
    MapboxMapSdkListener listener;
    List<MarkerViewOptions> markerHashCode = new ArrayList<>();
    int zoom = 14;

    public interface MapboxMapSdkListener {
        void onLongPress(LatLng latLng);

        void onAnnotationSelected(int index);

        void onInitComplete();
    }

    public void setListener(MapboxMapSdkListener l) {
        this.listener = l;
    }

    @Override
    public boolean isInitialized(){
        if(mapboxMap == null)return false;
        return true;
    }
    @Override
    public void initBeforeSetContentView(Activity activity) {
        mapboxAccountManager = MapboxAccountManager.start(activity, activity.getString(R.string.mapbox_token));

    }

    @Override
    public void onViewCreate(Activity activity, View view) {

        vMapView = (MapView) view;
        vMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap map) {
                mapboxMap = map;
                mapboxMap.setMyLocationEnabled(true);
                mapboxMap.setOnMapClickListener(new MapboxMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(@NonNull com.mapbox.mapboxsdk.geometry.LatLng point) {
                        if (listener != null) {
                            listener.onLongPress(new LatLng(point.getLatitude(), point.getLongitude()));
                        }
                    }

                });
                mapboxMap.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(@NonNull Marker marker) {
                        if (listener != null) {

                            for (int i = 0; i < markerHashCode.size(); i++) {
                                MarkerViewOptions options = markerHashCode.get(i);
                                if (options.getPosition().getLatitude() == marker.getPosition().getLatitude() && options.getPosition().getLongitude() == marker.getPosition().getLongitude()) {
                                    listener.onAnnotationSelected(i);
                                    break;
                                }
                            }
                            return true;
                        }
                        return false;
                    }
                });
            }
        });
    }

    public void onViewCreate(Activity activity, View view, Bundle savedInstanceState) {
        ((MapView) view).onCreate(savedInstanceState);
        onViewCreate(activity, view);
    }

    @Override
    public void onResume() {
        if (vMapView != null && !vMapView.isActivated()) return;
        vMapView.onResume();
    }

    @Override
    public void onPause() {
        if (vMapView != null && !vMapView.isActivated()) return;
        vMapView.onPause();
    }

    @Override
    public void destory() {
        if (vMapView != null && !vMapView.isActivated()) return;
        vMapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (vMapView != null && !vMapView.isActivated()) return;
        vMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        if (vMapView != null && !vMapView.isActivated()) return;
        vMapView.onLowMemory();
    }

    @Override
    public void moveToLocation(LatLng latLng) {

        if (mapboxMap == null) return;
        com.mapbox.mapboxsdk.camera.CameraPosition position = new com.mapbox.mapboxsdk.camera.CameraPosition.Builder()
                .target(new com.mapbox.mapboxsdk.geometry.LatLng(latLng.latitude, latLng.longitude)) // Sets the new camera position
                .zoom(zoom) // Sets the zoom
                .build(); // Creates a CameraPosition from the builder

        mapboxMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position));
    }

    @Override
    public void animateToCurrentLocation(LatLng latLng) {
        animateToLocation(latLng);
    }

    @Override
    public void animateToLocation(LatLng latLng) {
        com.mapbox.mapboxsdk.camera.CameraPosition position = new com.mapbox.mapboxsdk.camera.CameraPosition.Builder()
                .target(new com.mapbox.mapboxsdk.geometry.LatLng(latLng.latitude, latLng.longitude)) // Sets the new camera position
                .zoom(zoom) // Sets the zoom
                .bearing(0) // Rotate the camera
                .build(); // Creates a CameraPosition from the builder

        mapboxMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), 3000);
    }

    @Override
    public void addMarker(LatLng latLng, String label, String snippets) {
        if (mapboxMap == null) return;
        MarkerViewOptions marker = new MarkerViewOptions()
                .title(label)
                .snippet(snippets)
                .position(new com.mapbox.mapboxsdk.geometry.LatLng(latLng.latitude, latLng.longitude));

        markerHashCode.add(marker);

        MarkerView markerView = mapboxMap.addMarker(marker);

    }

    @Override
    public void cleanMarkers() {
        if (mapboxMap == null) return;
        mapboxMap.removeAnnotations();
        markerHashCode.clear();
    }

    @Override
    public void addPolyline(final ArrayList<LatLng> pointList, final int color) {

        new AsyncTask<Void, Void, List<com.mapbox.mapboxsdk.geometry.LatLng>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }


            @Override
            protected List<com.mapbox.mapboxsdk.geometry.LatLng> doInBackground(Void... voids) {
                List<com.mapbox.mapboxsdk.geometry.LatLng> points = new ArrayList<>();
                for (LatLng latLng : pointList) {
                    points.add(new com.mapbox.mapboxsdk.geometry.LatLng(latLng.latitude, latLng.longitude));
                }
                return points;
            }

            @Override
            protected void onPostExecute(List<com.mapbox.mapboxsdk.geometry.LatLng> points) {
                super.onPostExecute(points);
                if (points.size() > 0) {
                    com.mapbox.mapboxsdk.geometry.LatLng[] pointsArray = points.toArray(new com.mapbox.mapboxsdk.geometry.LatLng[points.size()]);

                    // Draw Points on MapView
                    mapboxMap.addPolyline(new PolylineOptions()
                            .add(pointsArray)
                            .color(color)
                            .width(6));
                }
            }
        }.execute();
    }

    @Override
    public void cleanPolyline() {
        mapboxMap.removeAnnotations();
        markerHashCode.clear();
    }

    @Override
    public void setMyLocation(LatLng latLng) {

    }

    @Override
    public LatLng getScreenLocation(int x, int y) {
        return null;
    }

    @Override
    public void setZoom(int level) {
        zoom = level;
    }
}
