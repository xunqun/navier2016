package idv.xunqun.navier.v2.parts;

import idv.xunqun.navier.R;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Bundle;

public class Elem_Battery extends Parts {

	public static final int ELEM_HEIGHT = 1;
	public static final int ELEM_WIDTH = 2;    	
	public static final int ELEM_PART = Parts.ELEM_BATTERY;
	public static String ELEM_NAME = "Battery";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_battery;
	
	
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	
	public int _margin = 10;

	
	//Path & Paint
	private Path _headPath;
	private Path _bodyPath;
	private Path _fillPath;
	
	private Paint _headPaint;
	private Paint _bodyPaint;
	private Paint _fillPaint;
	


	
	public Elem_Battery(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		initProperty();
		initPath();
		
		
		
	}

	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;


	}
	
	private float getBatteryLevel(){
		IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		Intent batteryStatus = _parent._context.registerReceiver(null, ifilter);
		
		int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

		float batteryPct = level / (float)scale;
		//Log.d("mine", "battery: " + batteryPct);
		return batteryPct;
		
	}

	
	private void initPath(){
		
		
		
		_bodyPath = new Path();
		_bodyPath.addRect(_iniLeft + _margin, _iniTop + _margin, _iniLeft+_width*7/8-_margin, _iniTop+_height-_margin, Direction.CCW);
		
		_headPath = new Path();
		_headPath.addRect(_iniLeft+_width*7/8 -_margin, _iniTop+_height*1/3, _iniLeft+_width-_margin, _iniTop+_height*2/3, Direction.CCW);
		
		_fillPath = new Path();
		
		
		_bodyPaint = new Paint();
		_bodyPaint.setAntiAlias(true);
		_bodyPaint.setColor(_parent.GLOBAL_COLOR);
		_bodyPaint.setStyle(Style.STROKE);
		_bodyPaint.setStrokeWidth(_width/32);
		
		_headPaint = new Paint();
		_headPaint.setAntiAlias(true);
		_headPaint.setColor(_parent.GLOBAL_COLOR);
		_headPaint.setStyle(Style.FILL);
		
		_fillPaint = new Paint();
		_fillPaint.setAntiAlias(true);
		_fillPaint.setColor(_parent.GLOBAL_COLOR);
		_headPaint.setStyle(Style.FILL);
		_fillPaint.setAlpha(100);
		
	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		_fillPath.addRect(_iniLeft+_margin+10, _iniTop+_margin+10, _iniLeft+_width*7/8*getBatteryLevel()-_margin-10, _iniTop+_height-_margin-10, Direction.CCW);
		_fillPaint.setAlpha(100);
		canvas.drawPath(_headPath, _headPaint);
		canvas.drawPath(_bodyPath, _bodyPaint);
		canvas.drawPath(_fillPath, _fillPaint);
		invalidate();
	}

	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		_bodyPaint.setColor(color);
		_headPaint.setColor(color);
		_fillPaint.setColor(color);
		_fillPaint.setAlpha(100);
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		_bodyPaint.setColor(_parent.GLOBAL_COLOR);
		_headPaint.setColor(_parent.GLOBAL_COLOR);
		_fillPaint.setColor(_parent.GLOBAL_COLOR);
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		iniLeft = iniLeft+pin[0]*uniPixel;
		iniTop = iniTop+pin[1]*uniPixel;
		int width = uniPixel *ELEM_WIDTH;
		int height = uniPixel *ELEM_HEIGHT;
		
		//Path & Paint
		Path _headPath;
		Path _bodyPath;
		Path _fillPath;
		
		Paint _headPaint;
		Paint _bodyPaint;
		
		
		_bodyPath = new Path();
		_bodyPath.addRect(iniLeft, iniTop, iniLeft+width*7/8, iniTop+height, Direction.CCW);
		
		_headPath = new Path();
		_headPath.addRect(iniLeft+width*7/8, iniTop+(height*1/3), iniLeft+width, iniTop+(height*2/3), Direction.CCW);
		
		_fillPath = new Path();
		
		
		_bodyPaint = new Paint();
		_bodyPaint.setAntiAlias(true);
		_bodyPaint.setColor(Color.CYAN);
		_bodyPaint.setStyle(Style.STROKE);
		_bodyPaint.setStrokeWidth(width/32);
		
		_headPaint = new Paint();
		_headPaint.setAntiAlias(true);
		_headPaint.setColor(Color.CYAN);
		_headPaint.setStyle(Style.FILL);
		
		
		_fillPath.addRect(iniLeft, iniTop, (float) (iniLeft+width*7/8*0.5), iniTop+height, Direction.CCW);
		
		canvas.save();
		canvas.drawPath(_headPath, _headPaint);
		canvas.drawPath(_bodyPath, _bodyPaint);
		canvas.drawPath(_fillPath, _headPaint);
		canvas.restore();
	}

	@Override
	public void onIsGPSFix(GpsStatus status, boolean isFix) {
		// TODO Auto-generated method stub
		
	}

}
