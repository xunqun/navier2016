package idv.xunqun.navier.v2.dialog;

import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.R;
import idv.xunqun.navier.v2.PlacesChooserFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

@SuppressLint("NewApi")
public class PlaceChooserDialog_Support extends DialogFragment {

	private ArrayList<HashMap<String, Object>> mPlaceList;
	private ListAdapter mListAdapter;
	private ListView mListView;
	private OnPlaceChoosedListener mHandler;
	private RadioButton[] mRadioButton = new RadioButton[2];

	public interface OnPlaceChoosedListener {
		void OnClick(String name, double lat, double lng);
	}

	public void setOnPlaceChoosedListener(OnPlaceChoosedListener handler) {
		mHandler = handler;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		View view = inflater.inflate(R.layout.dialog_placechooser, null);
		
		initViews(view);
		
		builder.setView(view);

		return builder.create();
	}

	private void initViews(View view) {
		// TODO Auto-generated method stub
		mListView = (ListView) view.findViewById(R.id.dialog_placelist);
		mRadioButton[0] = (RadioButton) view.findViewById(R.id.radio_history);
		mRadioButton[1] = (RadioButton) view.findViewById(R.id.radio_favorite);

		mRadioButton[0].setOnCheckedChangeListener(onCheckedChangeListener);
		mRadioButton[1].setOnCheckedChangeListener(onCheckedChangeListener);
	}

	@Override
	public void onActivityCreated(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onActivityCreated(arg0);

		dataAccess(PlacesChooserFragment.TYPE_ALL);

		// initBeforeSetContentView list adapter

		mListAdapter = new ListAdapter();
		mListView.setAdapter(mListAdapter);
		mListAdapter.notifyDataSetChanged();
		mListAdapter.notifyDataSetInvalidated();
	}

	private void dataAccess(int type) {

		String[] columns = { MyDBOpenHelper.COL_ID,
				MyDBOpenHelper.COL_PLACE_NAME,
				MyDBOpenHelper.COL_ADDRESS,
				MyDBOpenHelper.COL_DESCRIPTION,
				MyDBOpenHelper.COL_FAVERITE,
				MyDBOpenHelper.COL_LATITUDE,
				MyDBOpenHelper.COL_LONGITUDE,
				MyDBOpenHelper.COL_TIMESTAMP,
				MyDBOpenHelper.COL_FINGERPRINT };

		String selection = type == PlacesChooserFragment.TYPE_ALL ? "" : MyDBOpenHelper.COL_FAVERITE + "=1";

		String[] selectionArgs = {};
		String groupBy = "";
		String having = "";
		String orderBy = MyDBOpenHelper.COL_TIMESTAMP + " DESC";

		Cursor c = MyDBOpenHelper.getDb(getActivity()).query(MyDBOpenHelper.TABLE_NAME, columns,
				selection, selectionArgs, groupBy, having, orderBy);

		int index_id = c.getColumnIndex(MyDBOpenHelper.COL_ID), index_name = c.getColumnIndex(MyDBOpenHelper.COL_PLACE_NAME), index_address = c.getColumnIndex(MyDBOpenHelper.COL_ADDRESS), index_description = c.getColumnIndex(MyDBOpenHelper.COL_DESCRIPTION), index_faverite = c
				.getColumnIndex(MyDBOpenHelper.COL_FAVERITE), index_latitude = c.getColumnIndex(MyDBOpenHelper.COL_LATITUDE), index_longitude = c.getColumnIndex(MyDBOpenHelper.COL_LONGITUDE), index_timestamp = c.getColumnIndex(MyDBOpenHelper.COL_TIMESTAMP), index_fingerprint = c
				.getColumnIndex(MyDBOpenHelper.COL_FINGERPRINT);

		// c.moveToFirst();
		if (mPlaceList != null) {
			mPlaceList.clear();
		} else {
			mPlaceList = new ArrayList<HashMap<String, Object>>();
		}

		while (c.moveToNext()) {

			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(MyDBOpenHelper.COL_ID,
					String.valueOf(c.getInt(index_id)));
			map.put(MyDBOpenHelper.COL_PLACE_NAME,
					c.getString(index_name));
			map.put(MyDBOpenHelper.COL_ADDRESS,
					c.getString(index_address));
			map.put(MyDBOpenHelper.COL_FAVERITE,
					String.valueOf(c.getInt(index_faverite)));
			if (c.getInt(index_faverite) == 0) {
				map.put("star", R.drawable.star_small);
			} else {
				map.put("star", R.drawable.star_small_enable);
			}
			map.put(MyDBOpenHelper.COL_LATITUDE,
					String.valueOf(c.getDouble(index_latitude)));
			map.put(MyDBOpenHelper.COL_LONGITUDE,
					String.valueOf(c.getDouble(index_longitude)));
			map.put(MyDBOpenHelper.COL_DESCRIPTION,
					c.getString(index_description));

			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(c.getLong(index_timestamp));

			map.put("date", PlacesChooserFragment.timeRepresent(calendar));
			map.put(MyDBOpenHelper.COL_TIMESTAMP,
					String.valueOf(c.getLong(index_timestamp)));
			map.put(MyDBOpenHelper.COL_FINGERPRINT,
					c.getString(index_fingerprint));

			mPlaceList.add(map);

		}
		// c.close();

	}

	private class ListAdapter extends BaseAdapter {

		Drawable startedDrawable, nonStartedDrawable;
		private LayoutInflater inflater;

		public ListAdapter() {

			inflater = (LayoutInflater) getActivity().getSystemService
					(Context.LAYOUT_INFLATER_SERVICE);

			startedDrawable = getResources().getDrawable(R.drawable.star_small_enable);
			nonStartedDrawable = getResources().getDrawable(R.drawable.star_small);

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mPlaceList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mPlaceList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.placelist_item_narrow, null);
			}

			ImageButton star = (ImageButton) convertView.findViewById(R.id.imageButton1);
			ImageButton config = (ImageButton) convertView.findViewById(R.id.config);
			TextView name = (TextView) convertView.findViewById(R.id.name);
			TextView date = (TextView) convertView.findViewById(R.id.date);
			TextView addr = (TextView) convertView.findViewById(R.id.addr);

			star.setImageDrawable(((Integer) mPlaceList.get(position).get("star") == R.drawable.star_small ? nonStartedDrawable : startedDrawable));
			name.setText((String) mPlaceList.get(position).get(MyDBOpenHelper.COL_PLACE_NAME));
			date.setText((String) mPlaceList.get(position).get("date"));
			addr.setText((String) mPlaceList.get(position).get(MyDBOpenHelper.COL_ADDRESS));

			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (mHandler != null) {
						mHandler.OnClick((String) mPlaceList.get(position).get(MyDBOpenHelper.COL_PLACE_NAME),
								Double.parseDouble((String) mPlaceList.get(position).get(MyDBOpenHelper.COL_LATITUDE)),
								Double.parseDouble((String) mPlaceList.get(position).get(MyDBOpenHelper.COL_LONGITUDE)));

						getDialog().dismiss();
					}
				}
			});

			return convertView;
		}

	}
	
	private OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			
			if(isChecked && buttonView.equals(mRadioButton[0])){
				dataAccess(PlacesChooserFragment.TYPE_ALL);
				mListAdapter.notifyDataSetChanged();
				mListAdapter.notifyDataSetInvalidated();
				
			}
			
			else if(isChecked && buttonView.equals(mRadioButton[1])){
				dataAccess(PlacesChooserFragment.TYPE_FAV);
				mListAdapter.notifyDataSetChanged();
				mListAdapter.notifyDataSetInvalidated();
			}
			
		}
	};
}
