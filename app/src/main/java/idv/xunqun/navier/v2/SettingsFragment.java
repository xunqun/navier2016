package idv.xunqun.navier.v2;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import android.os.Bundle;
import android.preference.PreferenceFragment;

public class SettingsFragment extends PreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Load the preferences from an XML resource
		try {
			addPreferencesFromResource(R.xml.settings_v2);
		} catch (ClassCastException e) {
			PreferencesHandler.get(getActivity()).clear();
			addPreferencesFromResource(R.xml.settings_v2);
		}

	}

}
