package idv.xunqun.navier.v2.dialog;


import idv.xunqun.navier.NavierAbout;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.Version;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class ScoreStartDialog extends DialogFragment {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(getString(R.string.navierabout_btn1));
		builder.setCancelable(true);
		builder.setNegativeButton(getString(R.string.naviernaviconfigure_cancel), new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				PreferencesHandler.get(getActivity()).setPREF_OPENCOUNT(0);
			}
		});
		builder.setPositiveButton(R.string.score_5, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				PreferencesHandler.get(getActivity()).setPREF_OPENCOUNT(-1);
				Intent it = new Intent(Intent.ACTION_VIEW); 
				if(Version.isLite(getActivity())){
	                it.setData(Uri.parse("market://details?id=idv.xunqun.navier"));
					
				}else{
					it.setData(Uri.parse("market://details?id=idv.xunqun.navier.premium"));
				}
				startActivity(it);
				dismiss();
			}
		});
		builder.setNeutralButton(R.string.feedback, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				PreferencesHandler.get(getActivity()).setPREF_OPENCOUNT(-1);
				Intent emailIntent = new Intent(Intent.ACTION_SEND);
				emailIntent.setType("text/html");
				emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{ "lu.shangchiun@gmail.com"});
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Navier HUD Feedback");
				emailIntent.putExtra(Intent.EXTRA_TEXT, "");
				startActivity(Intent.createChooser(emailIntent, "Give me a feedback..."));
				dismiss();
			}
		});
		builder.setIcon(R.drawable.googleplay);
		return builder.create();
	}
}
