package idv.xunqun.navier.v2;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.http.DirectionRequester;
import idv.xunqun.navier.http.DirectionTask;
import idv.xunqun.navier.http.DirectionTask.DirectionTaskListener;
import idv.xunqun.navier.v2.content.DirectionLeg;
import idv.xunqun.navier.v2.content.DirectionRoute;
import idv.xunqun.navier.v2.content.DirectionStep;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.content.Context;
import android.location.Location;
import android.util.Log;

public class NavigationManager {

	private static final int SIX_SECOND = 1000 * 6;
	private static final int ONE_SECOND = 1000 * 1;
	public int PERDESTANCE_LIMMIT = 60; // meters
	public double ANGLE_LIMMIT = 100;
	public int DESTANCE_SHOWALARM = 150; // meters
	public int TIME_SHOWALARM = 30; // seconds
	public int ARRIVE_LIMMIT = 60;
	public int CLOSE_RANGE = 40;

	private Context _context;
	private PreferencesHandler _settings;
	private final int MAX_COUNT_TO_REPLAN = 5;
	private int _count2Replan = 0;

	public boolean _hasDirectionInfo = false;
	public DirectionRoute _directionPlan;
	public static boolean _isPlanning = false;
	public double _totalDistance = 0;

	public boolean[] _isStepAlarmShowed;

	public boolean _inToleratePhase = false; // tolerate for leave route

	// Global values

	public String _nextTurnInstruction = null;
	public double _nextTurnAngle = -1;
	public Latlng _perpen_point;
	double _curRoadAngle = 0;
	public Latlng _currLatlng;

	/*
	 * public NavigationManager(Place destination, Context context){ _context =
	 * context; mDestination = destination; _settings = new
	 * PreferencesHandler(_context); TIME_SHOWALARM =
	 * _settings.getPREF_TURNALARM_SEC(); }
	 */

	public NavigationManager(DirectionRoute directionPlan, Context context) {

		_context = context;
		_directionPlan = directionPlan;
		_settings = new PreferencesHandler(_context);
		TIME_SHOWALARM = _settings.getPREF_TURNALARM_SEC();
		initProperties();
	}

	/*
	 * if it need to access totalDistance frequently, use global value
	 * mTotalDistance
	 */
	public double totalDistance() {

		double total = 0;

		if (_hasDirectionInfo != false) {
			for (DirectionLeg leg : _directionPlan._legList) {
				total += leg._totalDistance;
			}
		}

		return total;
	}

	public DirectionRoute initDirectonPlan(Context context, String rowJson) throws Exception {

		DirectionRoute plan = new DirectionRoute();
		plan._from = _currLatlng;
		// plan._oriJsonString = rowJson;
		plan._oriHereJsonString = rowJson;
		plan._wayPointList = (ArrayList<Latlng>) _directionPlan._wayPointList.clone();
		plan.setHereJson(context, new JSONObject(rowJson));

		_directionPlan = null;
		_directionPlan = plan;
		initProperties();
		return _directionPlan;
	}

	public boolean initProperties() {

		// initail DirectionPlan

		_hasDirectionInfo = true;
		_isPlanning = false;
		_totalDistance = totalDistance();
		return true;
	}

	/*
	 * if angle has to be accessed frequently, dont call this method, use the
	 * global value _nextTurnAngle
	 */
	public double getNextTurnAngle() {
		try {

			int curLeg = _directionPlan._current_leg;
			int curStep = _directionPlan._current_step;

			if (curStep + 1 < _directionPlan._legList.get(curLeg)._stepList.size()) {
				// When next step is in current leg
				return calTurnAngle(curLeg, curStep, curLeg, curStep + 1);

			} else if (curLeg + 1 < _directionPlan._legList.size()) {
				// When next step is in next leg
				return calTurnAngle(curLeg, curStep, curLeg + 1, 0);

			} else {
				return -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}

	}

	private double calTurnAngle(int curLeg, int curStep, int nextLeg, int nextStep) {
		DirectionStep arr_curStep = _directionPlan._legList.get(curLeg)._stepList.get(curStep);
		DirectionStep arr_nextStep = _directionPlan._legList.get(nextLeg)._stepList.get(nextStep);
		Latlng conner = arr_curStep.polyline_list.get(arr_curStep.polyline_list.size() - 1);
		Latlng from = arr_curStep.polyline_list.get(arr_curStep.polyline_list.size() - 2);
		Latlng to = arr_nextStep.polyline_list.get(1);
		double xa = from.getLng() - conner.getLng();
		double ya = from.getLat() - conner.getLat();
		double xb = to.getLng() - conner.getLng();
		double yb = to.getLat() - conner.getLat();
		Log.d("mine",
				"turnAngle: from(" + from.getLat() + "," + from.getLng() + ") conner(" + conner.getLat() + "," + conner.getLng() + ") to("
						+ to.getLat() + "," + to.getLng() + ")");
		_nextTurnAngle = this.trunAngle(xa, ya, xb, yb);
		return _nextTurnAngle;
	}

	public boolean isArrived(Latlng curr) {

		return distance(curr, _directionPlan._wayPointList.get(_directionPlan._wayPointList.size() - 1)) <= ARRIVE_LIMMIT ? true : false;

	}

	public double getAlarmDistance(Latlng curr, float speed) {

		float meter = speed * TIME_SHOWALARM;
		if (meter < 80)
			meter = 80;

		return meter;
	}

	public boolean isNeedRoutePlan(Latlng curr) {
				
		if (!_hasDirectionInfo) {
			_count2Replan = 0;
			return true;
		}

		if (isLeaveTheStep(curr)) {

			int[] currShortest = checkShortest(curr);
			Log.d("mine", "currstep:" + currShortest);

			if (currShortest.length == 0) {

				_count2Replan += 1;
				if (_count2Replan > this.MAX_COUNT_TO_REPLAN) {
					this._inToleratePhase = false;
					_count2Replan = 0;
					return true;
				} else {
					this._inToleratePhase = true;
					return false;
				}
			} else {

				_count2Replan = 0;
				this._inToleratePhase = false;
				return false;
			}

		} else {
			// return false;
			_count2Replan = 0;
			this._inToleratePhase = false;
			return false;
		}

	}

	/**
	 * Check which leg and step is currently stay
	 * 
	 * @param curr
	 *            Current location
	 * @return return an array with 3 value, value[0] is leg, value[1] is step,
	 *         value[2] is rPoint. If value.length = 0, current location is out
	 *         of the planned route.
	 */
	public int[] checkShortest(Latlng curr) {
		Log.d("mine", "-----checkStep() ----- ");
		double shortest_dest = PERDESTANCE_LIMMIT;
		int shortest_step = -1;
		int shortest_leg = 0;
		int curr_rpoint = 0;
		boolean fined = false;
		Latlng _perpenPoint = null;

		for (int legIndex = _directionPlan._current_leg; legIndex < _directionPlan._legList.size(); legIndex++) {

			int search_from = _directionPlan._current_step == 0 ? 0 : _directionPlan._current_step - 1;
			int search_to = Math.min(search_from + 4, _directionPlan._legList.get(legIndex)._stepList.size());
			for (int i = search_from; i < search_to; i++) {
				DirectionStep tmpStep = _directionPlan._legList.get(legIndex)._stepList.get(i);
				List<Latlng> ployLine_list = tmpStep.polyline_list;

				for (int j = 0; j < ployLine_list.size() - 1; j++) {
					Latlng p1 = ployLine_list.get(j);
					Latlng p2 = ployLine_list.get(j + 1);

					Latlng perpen_point = getPerpenPoint(p1, p2, curr);
					double distance_p1 = distance(curr, p1);
					double perpenDistance = distance(curr, perpen_point);

					if ((checkAngle(p1, p2, curr) && perpenDistance < shortest_dest) || distance_p1 < PERDESTANCE_LIMMIT) {
						// in the route
						shortest_dest = perpenDistance;
						shortest_leg = legIndex;
						shortest_step = i;
						curr_rpoint = j;
						_perpenPoint = perpen_point;
						_perpen_point = perpen_point;
						fined = true;
					}
				}
				if (fined == true) {
					break;
				}
			}
		}

		if (shortest_step != -1) {
			_directionPlan._current_leg = shortest_leg;
			_directionPlan._current_step = shortest_step;
			_directionPlan._current_rpoint = curr_rpoint;
			_directionPlan._current_perpen_point = _perpenPoint;
			return new int[] { shortest_leg, shortest_step, curr_rpoint };
		}

		return new int[] {};
	}

	/**
	 * Angle of each rpoint
	 * 
	 * @return
	 */
	public double getRoadAngle() {

		Latlng p1 = _directionPlan._legList.get(_directionPlan._current_leg)._stepList.get(_directionPlan._current_step).polyline_list
				.get(_directionPlan._current_rpoint);
		Latlng p2 = _directionPlan._legList.get(_directionPlan._current_leg)._stepList.get(_directionPlan._current_step).polyline_list
				.get(_directionPlan._current_rpoint + 1);

		double clear_x = p2.getLng() - p1.getLng();
		double clear_y = p2.getLat() - p1.getLat();

		return roadAngle(clear_x, clear_y);
	}

	public double roadAngle(double x, double y) {

		double tanA = y / x;
		if (quadrant(x, y) == 1) {
			return 90 - Math.abs(Math.toDegrees(Math.atan(tanA)));
		} else if (quadrant(x, y) == 2) {
			return 270 + Math.abs(Math.toDegrees(Math.atan(tanA)));
		} else if (quadrant(x, y) == 3) {
			return 270 - Math.abs(Math.toDegrees(Math.atan(tanA)));
		} else {
			return 90 + Math.abs(Math.toDegrees(Math.atan(tanA)));
		}

	}

	public boolean isLeaveTheStep(Latlng curr) {
		Log.d("mine", "-----isLeaveTheStep() ----- ");
		Log.d("mine", "leg:" + _directionPlan._current_leg + " step:" + _directionPlan._current_step + " rpoint:" + _directionPlan._current_rpoint);
		DirectionStep curr_step = _directionPlan._legList.get(_directionPlan._current_leg)._stepList.get(_directionPlan._current_step);
		List<Latlng> ployLine_list = curr_step.polyline_list;

		// check is still curr step
		boolean inThisStep = false;
		int curr_rpoint = 0;

		for (int i = 0; i < ployLine_list.size() - 1; i++) {
			Latlng p1 = ployLine_list.get(i);
			Latlng p2 = ployLine_list.get(i + 1);

			Latlng perpen_point = getPerpenPoint(p1, p2, curr);

			double perpenDistance = distance(curr, perpen_point);
			// Log.d("mine", "perpen distance = " + perpenDistance);
			if ((checkAngle(p1, p2, curr) && perpenDistance < this.PERDESTANCE_LIMMIT) || distance(curr, p1) < PERDESTANCE_LIMMIT) {
				// in the route
				inThisStep = true;
				_directionPlan._current_rpoint = i;
				_perpen_point = perpen_point;
				return false;
			}

		}

		return true;
	}

	public double distanceOfLeftRpoint(Latlng curr) {
		int curr_rpoint = _directionPlan._current_rpoint;
		DirectionStep curr_step = _directionPlan._legList.get(_directionPlan._current_leg)._stepList.get(_directionPlan._current_step);

		List<Latlng> list = curr_step.polyline_list;

		if (curr_rpoint == list.size() - 1) {
			return distance(curr, (Latlng) list.get(curr_rpoint + 1));
		} else {
			Latlng next = (Latlng) list.get(curr_rpoint + 1);
			double sum = distance(curr, next);
			for (int i = curr_rpoint + 1; i < list.size() - 1; i++) {
				sum += distance((Latlng) list.get(i), (Latlng) list.get(i + 1));
			}
			// Log.d("mine", "distace of left rpoint " + curr_rpoint + " : "+sum
			// );
			return sum;
		}

	}

	public boolean checkAngle(Latlng p1, Latlng p2, Latlng curr) {
		int quadrant;
		double angle1, angle2;

		double x1 = p1.getLng();
		double y1 = p1.getLat();

		double x2 = p2.getLng();
		double y2 = p2.getLat();

		double xcurr = curr.getLng();
		double ycurr = curr.getLat();

		// angle 1
		double clear_x2 = x2 - x1;
		double clear_y2 = y2 - y1;

		double clear_x1 = x1 - x2;
		double clear_y1 = y1 - y2;

		double clear_xcurr = xcurr - x1;
		double clear_ycurr = ycurr - y1;

		angle1 = deltaAngle(clear_x2, clear_y2, clear_xcurr, clear_ycurr);

		// angle2 = deltaAngle(clear_xcurr,clear_ycurr,clear_x2,clear_y2);

		clear_xcurr = xcurr - x2;
		clear_ycurr = ycurr - y2;

		angle2 = deltaAngle(clear_x1, clear_y1, clear_xcurr, clear_ycurr);

		if (angle1 > this.ANGLE_LIMMIT || angle2 > this.ANGLE_LIMMIT) {

			return false;
		} else {
			// between two rpoint

			return true;
		}
	}

	public double deltaAngle(double xa, double ya, double xb, double yb) {

		double tanA = ya / xa;
		double tanB = yb / xb;

		if (quadrant(xa, ya) == 1 && quadrant(xb, yb) == 1) {
			return Math.abs(Math.toDegrees(Math.atan(tanA)) - Math.toDegrees(Math.atan(tanB)));
		} else if (quadrant(xa, ya) == 1 && quadrant(xb, yb) == 2) {
			return 180 - Math.abs(Math.toDegrees(Math.atan(tanA))) - Math.abs(Math.toDegrees(Math.atan(tanB)));
		} else if (quadrant(xa, ya) == 1 && quadrant(xb, yb) == 3) {
			double angle = 180 + Math.abs(Math.toDegrees(Math.atan(tanA))) - Math.abs(Math.toDegrees(Math.atan(tanB)));
			return angle > 180 ? 360 - angle : angle;
		} else if (quadrant(xa, ya) == 1 && quadrant(xb, yb) == 4) {
			return Math.abs(Math.toDegrees(Math.atan(tanA))) + Math.abs(Math.toDegrees(Math.atan(tanB)));
		} else if (quadrant(xa, ya) == 2 && quadrant(xb, yb) == 1) {
			return 180 - Math.abs(Math.toDegrees(Math.atan(tanA))) - Math.abs(Math.toDegrees(Math.atan(tanB)));
		} else if (quadrant(xa, ya) == 2 && quadrant(xb, yb) == 2) {
			return Math.abs(Math.toDegrees(Math.atan(tanA)) - Math.toDegrees(Math.atan(tanB)));
		} else if (quadrant(xa, ya) == 2 && quadrant(xb, yb) == 3) {
			return Math.abs(Math.toDegrees(Math.atan(tanA))) + Math.abs(Math.toDegrees(Math.atan(tanB)));
		} else if (quadrant(xa, ya) == 2 && quadrant(xb, yb) == 4) {
			double angle = 180 - Math.abs(Math.toDegrees(Math.atan(tanA))) + Math.abs(Math.toDegrees(Math.atan(tanB)));
			return angle > 180 ? 360 - angle : angle;
		} else if (quadrant(xa, ya) == 3 && quadrant(xb, yb) == 1) {
			double angle = 180 - Math.abs(Math.toDegrees(Math.atan(tanA))) + Math.abs(Math.toDegrees(Math.atan(tanB)));
			return angle > 180 ? 360 - angle : angle;
		} else if (quadrant(xa, ya) == 3 && quadrant(xb, yb) == 2) {
			return Math.abs(Math.toDegrees(Math.atan(tanA))) + Math.abs(Math.toDegrees(Math.atan(tanB)));
		} else if (quadrant(xa, ya) == 3 && quadrant(xb, yb) == 3) {
			return Math.abs(Math.toDegrees(Math.atan(tanA)) - Math.toDegrees(Math.atan(tanB)));
		} else if (quadrant(xa, ya) == 3 && quadrant(xb, yb) == 4) {
			return 180 - Math.abs(Math.toDegrees(Math.atan(tanA))) - Math.abs(Math.toDegrees(Math.atan(tanB)));
		} else if (quadrant(xa, ya) == 4 && quadrant(xb, yb) == 1) {
			return Math.abs(Math.toDegrees(Math.atan(tanA))) + Math.abs(Math.toDegrees(Math.atan(tanB)));
		} else if (quadrant(xa, ya) == 4 && quadrant(xb, yb) == 2) {
			double angle = 180 + Math.abs(Math.toDegrees(Math.atan(tanA))) - Math.abs(Math.toDegrees(Math.atan(tanB)));
			return angle > 180 ? 360 - angle : angle;
		} else if (quadrant(xa, ya) == 4 && quadrant(xb, yb) == 3) {
			return 180 - Math.abs(Math.toDegrees(Math.atan(tanA))) - Math.abs(Math.toDegrees(Math.atan(tanB)));
		} else if (quadrant(xa, ya) == 4 && quadrant(xb, yb) == 4) {
			return Math.abs(Math.toDegrees(Math.atan(tanA)) - Math.toDegrees(Math.atan(tanB)));
		}

		return 0;
	}

	public double trunAngle(double xfrom, double yfrom, double xto, double yto) {
		double angleFrom;
		double angleTo;

		double tanfrom = yfrom / xfrom;
		double tanto = yto / xto;
		angleFrom = Math.abs(Math.toDegrees(Math.atan(tanfrom)));
		angleTo = Math.abs(Math.toDegrees(Math.atan(tanto)));

		if (quadrant(xfrom, yfrom) == 1 && quadrant(xto, yto) == 1) {
			return angleFrom > angleTo ? 180 + (angleFrom - angleTo) : 180 - (angleTo - angleFrom);

		} else if (quadrant(xfrom, yfrom) == 1 && quadrant(xto, yto) == 2) {
			return angleFrom + angleTo;

		} else if (quadrant(xfrom, yfrom) == 1 && quadrant(xto, yto) == 3) {
			return angleFrom > angleTo ? angleFrom - angleTo : 360 - (angleTo - angleFrom);

		} else if (quadrant(xfrom, yfrom) == 1 && quadrant(xto, yto) == 4) {
			return 180 + angleFrom + angleTo;

		} else if (quadrant(xfrom, yfrom) == 2 && quadrant(xto, yto) == 1) {
			return 360 - (angleFrom + angleTo);

		} else if (quadrant(xfrom, yfrom) == 2 && quadrant(xto, yto) == 2) {
			return angleFrom > angleTo ? 180 - (angleFrom - angleTo) : 180 + (angleTo - angleFrom);

		} else if (quadrant(xfrom, yfrom) == 2 && quadrant(xto, yto) == 3) {
			return 180 - (angleFrom + angleTo);

		} else if (quadrant(xfrom, yfrom) == 2 && quadrant(xto, yto) == 4) {
			// return
			// angleFrom>angleTo?180-(angleFrom-angleTo):(angleTo-angleFrom);
			return angleFrom > angleTo ? 360 - (angleFrom - angleTo) : angleTo - angleFrom;

		} else if (quadrant(xfrom, yfrom) == 3 && quadrant(xto, yto) == 1) {
			return angleFrom > angleTo ? angleFrom - angleTo : 360 - (angleTo - angleFrom);

		} else if (quadrant(xfrom, yfrom) == 3 && quadrant(xto, yto) == 2) {
			return 180 + angleFrom + angleTo;

		} else if (quadrant(xfrom, yfrom) == 3 && quadrant(xto, yto) == 3) {
			return angleFrom > angleTo ? 180 + (angleFrom - angleTo) : 180 - (angleTo - angleFrom);

		} else if (quadrant(xfrom, yfrom) == 3 && quadrant(xto, yto) == 4) {
			return angleFrom + angleTo;

		} else if (quadrant(xfrom, yfrom) == 4 && quadrant(xto, yto) == 1) {
			return 180 - (angleFrom + angleTo);

		} else if (quadrant(xfrom, yfrom) == 4 && quadrant(xto, yto) == 2) {
			return angleFrom > angleTo ? 360 - (angleFrom - angleTo) : angleTo - angleFrom;

		} else if (quadrant(xfrom, yfrom) == 4 && quadrant(xto, yto) == 3) {
			return 360 - (angleTo + angleFrom);

		} else if (quadrant(xfrom, yfrom) == 4 && quadrant(xto, yto) == 4) {
			return angleFrom > angleTo ? 180 - (angleFrom - angleTo) : 180 + (angleTo - angleFrom);
		}

		return 0;
	}

	public int quadrant(double x, double y) {
		if (x > 0 && y >= 0) {
			return 1;
		} else if (x <= 0 && y > 0) {
			return 2;
		} else if (x < 0 && y <= 0) {
			return 3;
		} else if (x >= 0 && y < 0) {
			return 4;
		} else {
			return 0;
		}
	}

	public double distance(Latlng a, Latlng b) {

		float[] results = new float[2];
		Location.distanceBetween(a.getLat(), a.getLng(), b.getLat(), b.getLng(), results);

		return results[0];
	}

	public Latlng getPerpenPoint(Latlng p1, Latlng p2, Latlng curr) {
		double x1 = p1.getLng();
		double y1 = p1.getLat();

		double x2 = p2.getLng();
		double y2 = p2.getLat();

		double a = curr.getLng();
		double b = curr.getLat();

		double xresult = (a * Math.pow((x1 - x2), 2) + x1 * Math.pow((y1 - y2), 2) - (x1 - x2) * (y1 - y2) * (y1 - b))
				/ (Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
		double yresult = (y1 * Math.pow((x1 - x2), 2) + (b * Math.pow((y1 - y2), 2)) + ((x1 - x2) * (y1 - y2) * (a - x1)))
				/ (Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));

		return new Latlng((yresult), (xresult));
	}

	private static boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}

	public String getTrunIntruction() {
		int step = _directionPlan._current_step;
		// if(step>=0 && step<mRoutePlan.arr_steps.size()){
		String s = null;
		try {

			if (step + 1 < _directionPlan._legList.get(_directionPlan._current_leg)._stepList.size()) {
				step += 1;
				s = _directionPlan._legList.get(_directionPlan._current_leg)._stepList.get(step).html_instructions;
				if (step + 1 < _directionPlan._legList.get(_directionPlan._current_leg)._stepList.size()) {

					_nextTurnInstruction = _directionPlan._legList.get(_directionPlan._current_leg)._stepList.get(step + 1).html_instructions;

				} else if (_directionPlan._current_leg + 1 < _directionPlan._legList.size()) {

					_nextTurnInstruction = _directionPlan._legList.get(_directionPlan._current_leg + 1)._stepList.get(0).html_instructions;

				} else {
					_nextTurnInstruction = null;
				}
			} else if (_directionPlan._current_leg + 1 < _directionPlan._legList.size()) {
				step = 0;
				s = _directionPlan._legList.get(_directionPlan._current_leg + 1)._stepList.get(step).html_instructions;
				if (step + 1 < _directionPlan._legList.get(_directionPlan._current_leg + 1)._stepList.size()) {

					_nextTurnInstruction = _directionPlan._legList.get(_directionPlan._current_leg + 1)._stepList.get(step + 1).html_instructions;

				} else if (_directionPlan._current_leg + 2 < _directionPlan._legList.size()) {

					_nextTurnInstruction = _directionPlan._legList.get(_directionPlan._current_leg + 2)._stepList.get(0).html_instructions;

				} else {
					_nextTurnInstruction = null;
				}

			}
			return s;
		} catch (Exception e) {

		}
		_nextTurnInstruction = null;
		return null;
		// }
		// return "";
	}

	public String getInitTrunIntruction() {
		String s = _directionPlan._legList.get(0)._stepList.get(0).html_instructions;
		return s;

	}

}
