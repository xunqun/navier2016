package idv.xunqun.navier.v2.parts;



import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.v2.parts.GridBoard;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

public class Elem_TotalDistance extends Parts {

	public static final int ELEM_HEIGHT = 2;
	public static final int ELEM_WIDTH = 4;    	
	public static final int ELEM_PART = Parts.ELEM_DISTANCE;
	public static String ELEM_NAME = "Distance";
	public static final boolean ELEM_ISNAVPART = false;
	public static final int ELEM_THUMBNAIL = R.drawable.part_totaldistance;
	
	/*
	 * savedInstancesState naming rule
	 * e.g. SAVEDSTATE_CLASSNAME_VALUENAME
	 */
	public static final String SAVEDSTATE_TOTALDISTANCE_DISTANCE = "STATEDSTATE_TOTALDISTANCE_DISTANCE";
	public static final String SAVEDSTATE_TOTALDISTANCE_PRELAT = "STATEDSTATE_TOTALDISTANCE_PRELAT";
	public static final String SAVEDSTATE_TOTALDISTANCE_PRELNG = "STATEDSTATE_TOTALDISTANCE_PRELNG";
	
	
	public static final String UNIT_KMH = "Km/h";
	public static final String UNIT_MPH = "Mph";
	
	//property
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	
	private int _wordspace;
	
	
	//paths & paints
	private Path _textPath;
	private Path _digiPath;
	
	private Paint _textPaint;
	private Paint _digiPaint;
	
	//values
	private Latlng _curLocation = null;
	private double _distance = 0;
	private PreferencesHandler _settings;
	
	public Elem_TotalDistance(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
		initProperty();
		initPath();
	}

	private void initProperty(){
		
		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]*_parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]*_parent._unitPixel;
    	_centerX = this._iniLeft + (this._width/2);
    	_centerY = this._iniTop + (this._height/2);
    	
    	_settings = new PreferencesHandler(_parent._context);
    	
	}
	

	
	private void initPath(){
		
		Typeface font = _parent._defaultFont;
		
		
		
		_textPath = new Path();
		_textPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel);
		_textPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel);
		
		_textPaint = new Paint();
		_textPaint.setAntiAlias(true);
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setLinearText(true);
		_textPaint.setTextAlign(Align.LEFT);
		_textPaint.setAlpha(100);
		_textPaint.setTypeface(font);
		_textPaint.setTextSize(_parent._unitPixel/2);
		
		
		_digiPath = new Path();
		_digiPath.moveTo( _iniLeft,_iniTop+_parent._unitPixel*ELEM_HEIGHT);
		_digiPath.lineTo( _iniLeft+ELEM_WIDTH*_parent._unitPixel, _iniTop+_parent._unitPixel*ELEM_HEIGHT);
		
		_digiPaint = new Paint();
		_digiPaint.setAntiAlias(true);
		_digiPaint.setColor(_parent.GLOBAL_COLOR);
		_digiPaint.setLinearText(true);
		_digiPaint.setTextAlign(Align.LEFT);
		_digiPaint.setTypeface(font);
		_digiPaint.setTextSize((float) (_parent._unitPixel));
		
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		String text = "distance ";
		String distance;
		
		if(_distance<1){
			
			if(_parent.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH){
				text = text + "(meter)";
				distance = String.format("%.0f",_distance*1000) + " m";
			}else{
				text = text + "(yard)";
				distance = String.format("%.0f",_distance*1000*1.09361) + " y";
			}
		}else{
			if(_parent.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH){
				text = text + "(Km)";
				distance = String.format("%.1f",_distance) + " km";
			}else{
				text = text + "(mile)";
				distance = String.format("%.1f",_distance*0.621371192) + " ml";
			}
		}
		
		
		
		_textPaint.setAlpha(100);
		canvas.drawTextOnPath(text, _textPath, 0, 0, _textPaint);
		canvas.drawTextOnPath(distance, _digiPath, 0, 0, _digiPaint);
		invalidate();
	}

	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub

		if(location.getProvider().equals(LocationManager.GPS_PROVIDER)){
			Latlng latlng = new Latlng(location.getLatitude(), location.getLongitude());
			if(_curLocation!= null){
				float[] d = new float[1];
				Location.distanceBetween(_curLocation.getLat(), _curLocation.getLng(), latlng.getLat(), latlng.getLng(), d);
				_distance += d[0]/1000;
				
			}
			
			_curLocation = latlng;
		}
	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		// TODO Auto-generated method stub
		
		_textPaint.setColor(color);
		_digiPaint.setColor(color);
		_textPaint.setAlpha(100);
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

		
	}
	
	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft, int iniTop , int[] pin, Context context){
		
		//paths & paints
		Path textPath;
		Path digiPath;
		
		Paint textPaint;
		Paint digiPaint;
		
		Typeface font;
		try{
			font = Typeface.createFromAsset(context.getAssets(), "fonts/SquadaOne-Regular.ttf");
		}catch(Exception e){
			font = Typeface.DEFAULT;
		}
		
		textPath = new Path();
		textPath.moveTo( iniLeft+pin[0]*uniPixel,iniTop+uniPixel/2+pin[1]*uniPixel);
		textPath.lineTo( iniLeft+ELEM_WIDTH*uniPixel+pin[0]*uniPixel, iniTop+uniPixel/2+pin[1]*uniPixel);
		
		textPaint = new Paint();
		textPaint.setAntiAlias(true);
		textPaint.setColor(Color.CYAN);
		textPaint.setLinearText(true);
		textPaint.setTextAlign(Align.LEFT);
		textPaint.setTypeface(font);
		textPaint.setTextSize(uniPixel/2);
		
		
		digiPath = new Path();
		digiPath.moveTo( iniLeft+pin[0]*uniPixel,iniTop+uniPixel*ELEM_HEIGHT+pin[1]*uniPixel);
		digiPath.lineTo( iniLeft+ELEM_WIDTH*uniPixel+pin[0]*uniPixel, iniTop+uniPixel*ELEM_HEIGHT+pin[1]*uniPixel);
		
		digiPaint = new Paint();
		digiPaint.setAntiAlias(true);
		digiPaint.setColor(Color.CYAN);
		digiPaint.setLinearText(true);
		digiPaint.setTextAlign(Align.LEFT);
		digiPaint.setTypeface(font);
		digiPaint.setTextSize((float) (uniPixel*1.5));
		
		//
		canvas.save();
		canvas.drawTextOnPath("distance ", textPath, 0, 0, textPaint);
		canvas.drawTextOnPath(String.format("%03.1f", Math.abs(2.3))+" km", digiPath, 0, 0, digiPaint);
		
		canvas.restore();
		
		
		
	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub
		savedStates.putDouble(SAVEDSTATE_TOTALDISTANCE_DISTANCE, _distance);
		if(_curLocation!=null){
			double lat = _curLocation.getLat();
			double lng = _curLocation.getLng();
			savedStates.putDouble(SAVEDSTATE_TOTALDISTANCE_PRELAT, lat);
			savedStates.putDouble(SAVEDSTATE_TOTALDISTANCE_PRELNG, lng);
		}
		
	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub
		if(savedStates.containsKey(SAVEDSTATE_TOTALDISTANCE_DISTANCE)) 
			_distance = savedStates.getDouble(SAVEDSTATE_TOTALDISTANCE_DISTANCE);
		
		if(savedStates.containsKey(SAVEDSTATE_TOTALDISTANCE_PRELAT) && savedStates.containsKey(SAVEDSTATE_TOTALDISTANCE_PRELNG)){
			double lat = savedStates.getDouble(SAVEDSTATE_TOTALDISTANCE_PRELAT);
			double lng = savedStates.getDouble(SAVEDSTATE_TOTALDISTANCE_PRELNG);
			_curLocation = new Latlng(lat,lng);
		}
		
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_digiPaint.setColor(_parent.GLOBAL_COLOR);
	}
	
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		ELEM_NAME = name;
	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onIsGPSFix(GpsStatus status,boolean isFix) {
		// TODO Auto-generated method stub
		
	}

}
