package idv.xunqun.navier.v2;

import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.Version;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.http.SyncPlaceTask;
import idv.xunqun.navier.http.SyncPlaceTask.SyncPlaceHandler;
import idv.xunqun.navier.utils.http.NetworkState;
import idv.xunqun.navier.v2.dialog.LisenceNotAllowDialog;
import idv.xunqun.navier.v2.dialog.PlaceNameSettingDialog;
import idv.xunqun.navier.v2.dialog.PlaceNameSettingDialog.OnPlaceNameSettingListener;
import idv.xunqun.navier.v2.dialog.PlaceOperationDialog;
import idv.xunqun.navier.v2.dialog.PlaceOperationDialog.PlaceOperationListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PlacesChooserFragment extends Fragment implements HomeFragment {

	public static final int TYPE_ALL = 0;
	public static final int TYPE_FAV = 1;

	public interface PlacesChooserCallback {

	}

	private PlacesChooserCallback mCallback;

	private ListView mListView;
	private ListAdapter mListAdapter;
	private ArrayList<HashMap<String, Object>> mPlaceList;
	private PreferencesHandler mPref;
	private LinearLayout mLoading;
	LayoutInflater inflater;
	private ImageButton addButton;
	private ImageButton trashButton;
	private RadioButton radioButton0;
	private RadioButton radioButton1;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		mCallback = (PlacesChooserCallback) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.fragment_placechooser, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		mPref = new PreferencesHandler(getActivity());

		inflater = (LayoutInflater) getActivity().getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);

		initViews();

		mPlaceList = new ArrayList<HashMap<String, Object>>();

		// Get Account
		String account = mPref.getPREF_ACCOUNT();

		// do data sync
		// doSyncPlace();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// Get Account

		dataAccess(TYPE_ALL);
		refreshList();

	}

	private void doSyncPlace() {
		// TODO Auto-generated method stub
		// do data sync
		String account = mPref.getPREF_ACCOUNT();
		if (!account.equals(getString(R.string.PREF_ACCOUNT_DEFAULT)) && mPref.getPREF_SYNC()) {

			if (NetworkState.checkNetworkConnected(getActivity())) {
				if (this.isAdded()) {
					mLoading.setVisibility(View.VISIBLE);
					new SyncPlaceTask(getActivity(), mSyncPlaceHandler, account).execute();
				}
			} else {

				Toast.makeText(getActivity(), getString(R.string.naviermap_connection), Toast.LENGTH_LONG).show();
			}
		}
		dataAccess(TYPE_ALL);
		refreshList();
	}

	private void refreshList() {
		// initBeforeSetContentView list adapter
		if (this.isAdded()) {
			mListAdapter = new ListAdapter();
			mListView.setAdapter(mListAdapter);
			mListAdapter.notifyDataSetChanged();
			mListAdapter.notifyDataSetInvalidated();
		}

	}

	private void initViews() {
		// TODO Auto-generated method stub
		mListView = (ListView) getActivity().findViewById(R.id.placelist);

		// loading animation
		mLoading = (LinearLayout) getActivity().findViewById(R.id.loading_place);

		radioButton0 = (RadioButton) getActivity().findViewById(R.id.radio_history);
		radioButton1 = (RadioButton) getActivity().findViewById(R.id.radio_favorite);

		radioButton0.setOnCheckedChangeListener(onCheckedChangeListener);
		radioButton1.setOnCheckedChangeListener(onCheckedChangeListener);

		addButton = (ImageButton) getActivity().findViewById(R.id.btn_add);
		addButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mPref.getPREF_LISENCEDUSER()) {
					if(Version.isLite(getActivity())){
						Intent it = new Intent(getActivity(), PlanActivity.class);
//						Intent it = new Intent(getActivity(), PlanNutiteqActivity.class);
						startActivity(it);
					}else{
						Intent it = new Intent(getActivity(), PlanActivity.class);
						startActivity(it);
					}
				} else {
					new LisenceNotAllowDialog().show(getActivity().getSupportFragmentManager(), "");
				}
			}
		});

		trashButton = (ImageButton) getActivity().findViewById(R.id.btn_trash);
		trashButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle(getString(R.string.navieroperation_deleteall))
						.setIcon(R.drawable.ic_alert)
						.setMessage(R.string.navieroperation_deleteall_desc)
						.setPositiveButton(getString(R.string.naviernaviconfigure_ok), new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								cleanHistories();
							}
						})
						.setNegativeButton(getString(R.string.naviernaviconfigure_cancel), new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub

							}
						});

				builder.create().show();
			}
		});

	}

	private void cleanHistories() {

		SQLiteDatabase db = MyDBOpenHelper.getDb(getActivity());

		String[] columns = { MyDBOpenHelper.COL_ID, MyDBOpenHelper.COL_FINGERPRINT };
		Cursor c = db.query(MyDBOpenHelper.TABLE_NAME, columns, MyDBOpenHelper.COL_FAVERITE + "=?", new String[] { "0" }, "", "", "");
		while (c.moveToNext()) {
			String id = c.getString(c.getColumnIndex(MyDBOpenHelper.COL_ID));
			String fp = c.getString(c.getColumnIndex(MyDBOpenHelper.COL_FINGERPRINT));
			putSyncState(id, MyDBOpenHelper.STATE_KILLED, fp);
		}
		db.delete(MyDBOpenHelper.TABLE_NAME, MyDBOpenHelper.COL_FAVERITE + "=?", new String[] { "0" });
		c.close();

		if (!mPref.getPREF_ACCOUNT().equals(getString(R.string.PREF_ACCOUNT_DEFAULT)) && mPref.getPREF_SYNC()) {
			mLoading.setVisibility(View.VISIBLE);
			new SyncPlaceTask(getActivity(), mSyncPlaceHandler, mPref.getPREF_ACCOUNT()).execute();
		}

	}

	private void putSyncState(String id, String state, String fingerprint) {
		// add to sync state table
		SQLiteDatabase db = MyDBOpenHelper.getDb(getActivity());

		ContentValues values = new ContentValues();
		values.put(MyDBOpenHelper.SYNC_COL_PLACEID, id);
		values.put(MyDBOpenHelper.SYNC_COL_SYNCSTATE, state);
		values.put(MyDBOpenHelper.SYNC_COL_FINGERPRINT, fingerprint);

		db.insert(MyDBOpenHelper.SYNC_TABLE_NAME, null, values);

	}

	private void dataAccess(int type) {

		if (type == TYPE_ALL) {
			radioButton0.setChecked(true);
		} else {
			radioButton1.setChecked(true);
		}

		String[] columns = { MyDBOpenHelper.COL_ID,
				MyDBOpenHelper.COL_PLACE_NAME,
				MyDBOpenHelper.COL_ADDRESS,
				MyDBOpenHelper.COL_DESCRIPTION,
				MyDBOpenHelper.COL_FAVERITE,
				MyDBOpenHelper.COL_LATITUDE,
				MyDBOpenHelper.COL_LONGITUDE,
				MyDBOpenHelper.COL_TIMESTAMP,
				MyDBOpenHelper.COL_FINGERPRINT };

		String selection = type == TYPE_ALL ? "" : MyDBOpenHelper.COL_FAVERITE + "=1";

		String[] selectionArgs = {};
		String groupBy = "";
		String having = "";
		String orderBy = MyDBOpenHelper.COL_TIMESTAMP + " DESC";

		Cursor c = MyDBOpenHelper.getDb(getActivity()).query(MyDBOpenHelper.TABLE_NAME, columns,
				selection, selectionArgs, groupBy, having, orderBy);

		int index_id = c.getColumnIndex(MyDBOpenHelper.COL_ID), index_name = c.getColumnIndex(MyDBOpenHelper.COL_PLACE_NAME), index_address = c.getColumnIndex(MyDBOpenHelper.COL_ADDRESS), index_description = c.getColumnIndex(MyDBOpenHelper.COL_DESCRIPTION), index_faverite = c
				.getColumnIndex(MyDBOpenHelper.COL_FAVERITE), index_latitude = c.getColumnIndex(MyDBOpenHelper.COL_LATITUDE), index_longitude = c.getColumnIndex(MyDBOpenHelper.COL_LONGITUDE), index_timestamp = c.getColumnIndex(MyDBOpenHelper.COL_TIMESTAMP), index_fingerprint = c
				.getColumnIndex(MyDBOpenHelper.COL_FINGERPRINT);

		// c.moveToFirst();
		mPlaceList.clear();

		while (c.moveToNext()) {

			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(MyDBOpenHelper.COL_ID,
					String.valueOf(c.getInt(index_id)));
			map.put(MyDBOpenHelper.COL_PLACE_NAME,
					c.getString(index_name));
			map.put(MyDBOpenHelper.COL_ADDRESS,
					c.getString(index_address));
			map.put(MyDBOpenHelper.COL_FAVERITE,
					String.valueOf(c.getInt(index_faverite)));
			if (c.getInt(index_faverite) == 0) {
				map.put("star", R.drawable.star_small);
			} else {
				map.put("star", R.drawable.star_small_enable);
			}
			map.put(MyDBOpenHelper.COL_LATITUDE,
					String.valueOf(c.getDouble(index_latitude)));
			map.put(MyDBOpenHelper.COL_LONGITUDE,
					String.valueOf(c.getDouble(index_longitude)));
			map.put(MyDBOpenHelper.COL_DESCRIPTION,
					c.getString(index_description));

			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(c.getLong(index_timestamp));

			map.put("date", timeRepresent(calendar));
			map.put(MyDBOpenHelper.COL_TIMESTAMP,
					String.valueOf(c.getLong(index_timestamp)));
			map.put(MyDBOpenHelper.COL_FINGERPRINT,
					c.getString(index_fingerprint));

			mPlaceList.add(map);

		}
		// c.close();

	}

	public static String timeRepresent(Calendar calendar) {

		return calendar.get(Calendar.YEAR) + "-" +
				monthString(calendar.get(Calendar.MONTH)) + "-" +
				calendar.get(Calendar.DATE) + " " +
				calendar.get(Calendar.HOUR_OF_DAY) + ":" +
				calendar.get(Calendar.MINUTE);
	}

	public static String monthString(int i) {
		String s = "";
		switch (i) {
		case 0:
			s = "JAN";
			break;
		case 1:
			s = "FEB";
			break;
		case 2:
			s = "MAR";
			break;
		case 3:
			s = "APR";
			break;
		case 4:
			s = "MAY";
			break;
		case 5:
			s = "JUN";
			break;
		case 6:
			s = "JUL";
			break;
		case 7:
			s = "AUG";
			break;
		case 8:
			s = "SEP";
			break;
		case 9:
			s = "OCT";
			break;
		case 10:
			s = "NOV";
			break;
		case 11:
			s = "DEC";
			break;
		default:
			s = "UNDECIMBER";
		}
		return s;
	}

	private class ListAdapter extends BaseAdapter {

		Drawable startedDrawable, nonStartedDrawable;

		public ListAdapter() {
			startedDrawable = getResources().getDrawable(R.drawable.star_small_enable);
			nonStartedDrawable = getResources().getDrawable(R.drawable.star_small);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mPlaceList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mPlaceList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.placelist_item, null);
			}

			ImageButton star = (ImageButton) convertView.findViewById(R.id.imageButton1);
			ImageButton config = (ImageButton) convertView.findViewById(R.id.config);
			TextView name = (TextView) convertView.findViewById(R.id.name);
			TextView date = (TextView) convertView.findViewById(R.id.date);
			TextView addr = (TextView) convertView.findViewById(R.id.addr);

			star.setImageDrawable(((Integer) mPlaceList.get(position).get("star") == R.drawable.star_small ? nonStartedDrawable : startedDrawable));
			name.setText((String) mPlaceList.get(position).get(MyDBOpenHelper.COL_PLACE_NAME));
			date.setText((String) mPlaceList.get(position).get("date"));
			addr.setText((String) mPlaceList.get(position).get(MyDBOpenHelper.COL_ADDRESS));
			config.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					PlaceOperationDialog dialog = new PlaceOperationDialog();
					dialog.setOnOperationListener(new MyPlaceOperationListener(position), radioButton1.isChecked() ? true : false);
					dialog.show(getFragmentManager(), "");

				}
			});

			star.setOnClickListener(new OnStarClickListener(position));
			convertView.setOnClickListener(new OnPlaceClickListener(mPlaceList.get(position)));
			convertView.setOnLongClickListener(new OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {

					PlaceOperationDialog dialog = new PlaceOperationDialog();
					dialog.setOnOperationListener(new MyPlaceOperationListener(position), radioButton1.isChecked() ? true : false);
					dialog.show(getFragmentManager(), "");
					return true;
				}
			});
			return convertView;
		}

	}

	class MyPlaceOperationListener implements PlaceOperationListener {

		private int mPosition;

		public MyPlaceOperationListener(int position) {
			mPosition = position;
		}

		@Override
		public void onOperationClick(int operation) {
			// TODO Auto-generated method stub
			switch (operation) {
			case PlaceOperationDialog.DELETE:
				deleteDes((String) mPlaceList.get(mPosition).get(MyDBOpenHelper.COL_ID), (String) mPlaceList.get(mPosition).get(MyDBOpenHelper.COL_FINGERPRINT));
				break;
			case PlaceOperationDialog.RENAME:
				PlaceNameSettingDialog dialog = new PlaceNameSettingDialog();
				dialog.setPlace(mPlaceList.get(mPosition));
				dialog.setOnPanelNameSettingListener(new OnPlaceNameSettingListener() {

					@Override
					public void onNameComplete() {
						// TODO Auto-generated method stub
						dataAccess(TYPE_ALL);
						refreshList();

						if (!mPref.getPREF_ACCOUNT().equals(getString(R.string.PREF_ACCOUNT_DEFAULT)) && mPref.getPREF_SYNC()) {
							mLoading.setVisibility(View.VISIBLE);
							new SyncPlaceTask(getActivity(), mSyncPlaceHandler, mPref.getPREF_ACCOUNT()).execute();
						}
					}
				});
				dialog.show(getFragmentManager(), "");
				break;
			case PlaceOperationDialog.USE:
				if (mPref.getPREF_LISENCEDUSER()) {
					if(Version.isLite(getActivity())){
//						Intent it = new Intent(getActivity(), PlanNutiteqActivity.class);
						Intent it = new Intent(getActivity(), PlanActivity.class);
						startActivity(it);
					}else{
						Intent it = new Intent(getActivity(), PlanActivity.class);
						startActivity(it);
					}
				} else {
					new LisenceNotAllowDialog().show(getActivity().getSupportFragmentManager(), "");
				}
				break;
			case PlaceOperationDialog.FAVORITE:
				setFavorit(mPlaceList.get(mPosition));
				break;

			default:
				break;
			}
		}

	};

	private void setFavorit(HashMap p) {

		if (Integer.parseInt((String) p.get(MyDBOpenHelper.COL_FAVERITE)) == 1) {
			return;
		}

		SQLiteDatabase db = MyDBOpenHelper.getDb(getActivity());
		ContentValues values = new ContentValues();
		values.put(MyDBOpenHelper.COL_FAVERITE, 1);
		// String sql = "UPDATE "+ MyPlacesDBOpenHelper.TABLE_NAME + " SET " +
		// MyPlacesDBOpenHelper.COL_FAVERITE + "='" + 1 + "' WHERE " +
		// MyPlacesDBOpenHelper.COL_ID + " = "+ this.mSelectedId;
		db.update(MyDBOpenHelper.TABLE_NAME, values, MyDBOpenHelper.COL_ID + "=?", new String[] { (String) p.get(MyDBOpenHelper.COL_ID) });
		// db.execSQL(sql);

		putSyncState((String) p.get(MyDBOpenHelper.COL_ID), MyDBOpenHelper.STATE_MODIFY, (String) p.get(MyDBOpenHelper.COL_FINGERPRINT));

		dataAccess(TYPE_ALL);
		refreshList();

		if (!mPref.getPREF_ACCOUNT().equals(getString(R.string.PREF_ACCOUNT_DEFAULT)) && mPref.getPREF_SYNC()) {
			mLoading.setVisibility(View.VISIBLE);
			new SyncPlaceTask(getActivity(), mSyncPlaceHandler, mPref.getPREF_ACCOUNT()).execute();
		}

	}

	private void deleteDes(String id, String fingerPrint) throws SQLException {

		SQLiteDatabase db = MyDBOpenHelper.getDb(getActivity());
		// String sql = "DELETE FROM "+ MyPlacesDBOpenHelper.TABLE_NAME +
		// " WHERE " + MyPlacesDBOpenHelper.COL_ID + " = "+ id;
		db.delete(MyDBOpenHelper.TABLE_NAME, MyDBOpenHelper.COL_ID + "=?", new String[] { id });
		putSyncState(id, MyDBOpenHelper.STATE_KILLED, fingerPrint);
		// db.execSQL(sql);
		dataAccess(TYPE_ALL);
		refreshList();
		if (!mPref.getPREF_ACCOUNT().equals(getString(R.string.PREF_ACCOUNT_DEFAULT)) && mPref.getPREF_SYNC()) {
			mLoading.setVisibility(View.VISIBLE);
			new SyncPlaceTask(getActivity(), mSyncPlaceHandler, mPref.getPREF_ACCOUNT()).execute();
		}
	}

	private class OnStarClickListener implements OnClickListener {
		int mPosition;

		public OnStarClickListener(int position) {
			mPosition = position;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (Integer.valueOf((String) mPlaceList.get(mPosition).get(MyDBOpenHelper.COL_FAVERITE)) == 1) {

			} else {

			}
		}
	}

	private class OnPlaceClickListener implements OnClickListener {

		HashMap<String, Object> mPlace;

		public OnPlaceClickListener(HashMap<String, Object> place) {
			mPlace = place;
		}

		@Override
		public void onClick(View v) {

			if (mPref.getPREF_LISENCEDUSER()) {

				Latlng latlng = new Latlng(Double.parseDouble((String) mPlace.get(MyDBOpenHelper.COL_LATITUDE)), Double.parseDouble((String) mPlace.get(MyDBOpenHelper.COL_LONGITUDE)));
				latlng.setName((String) mPlace.get(MyDBOpenHelper.COL_PLACE_NAME) == null ? "" : (String) mPlace.get(MyDBOpenHelper.COL_PLACE_NAME));
				Intent it;
				if(Version.isLite(getActivity())){
//					it = new Intent(getActivity(), PlanNutiteqActivity.class);
					it = new Intent(getActivity(), PlanActivity.class);
					startActivity(it);
				}else{
					it = new Intent(getActivity(), PlanActivity.class);
					startActivity(it);
				}
				
				startActivity(it);
			} else {
				new LisenceNotAllowDialog().show(getActivity().getSupportFragmentManager(), "");
			}
		}

	}

	private SyncPlaceHandler mSyncPlaceHandler = new SyncPlaceHandler() {

		@Override
		public void onSyncPlaceComplete(boolean ok) {
			// TODO Auto-generated method stub

			// initBeforeSetContentView list adapter
			dataAccess(TYPE_ALL);
			refreshList();

			mLoading.setVisibility(View.GONE);
		}
	};

	OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub

			if (isChecked && buttonView.getId() == R.id.radio_history) {
				// all history
				dataAccess(TYPE_ALL);
				trashButton.setVisibility(View.VISIBLE);
			}

			else if (isChecked && buttonView.getId() == R.id.radio_favorite) {
				// only favorite
				dataAccess(TYPE_FAV);
				trashButton.setVisibility(View.GONE);
			}

			refreshList();
		}
	};

	@Override
	public void onRefreschClicked() {
		// TODO Auto-generated method stub
		doSyncPlace();
	}
}
