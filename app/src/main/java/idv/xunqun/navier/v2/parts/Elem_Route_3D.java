package idv.xunqun.navier.v2.parts;

import idv.xunqun.navier.NavigationManager;
import idv.xunqun.navier.R;
import idv.xunqun.navier.StreetMapRequester;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.content.RoutePlan;
import idv.xunqun.navier.utils.Utility;
import idv.xunqun.navier.v2.content.DirectionLeg;
import idv.xunqun.navier.v2.content.DirectionRoute;
import idv.xunqun.navier.v2.content.DirectionStep;
import idv.xunqun.navier.v2.parts.Elem_Route.RoadAngleAnimate;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PathEffect;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.GeomagneticField;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Elem_Route_3D extends NaviParts {

	public static final String TAG = "Elem_Route_3D";
	// Default Value
	public static final int ELEM_HEIGHT = 8;
	public static final int ELEM_WIDTH = 16;
	public static final int ELEM_PART = Parts.ELEM_ROUTE_3D;
	public static String ELEM_NAME = "Direction Route";
	public static final boolean ELEM_ISNAVPART = true;
	public static final int ELEM_THUMBNAIL = R.drawable.part_route_3d;

	public static final int ZOOMIN_MAP_SCALE = 0;
	public static final int NORMAL_MAP_SCALE = 1;
	public static final int ZOOMOUT_MAP_SCALE = 2;

	public static final float ZOOMIN_MAP_SCALE_RATE = 0.005f;
	public static final float NORMAL_MAP_SCALE_RATE = 0.01f;
	public static final float ZOOMOUT_MAP_SCALE_RATE = 0.05f;

	public static final float SLOW_SCALE = 2f;
	public static final float NORMAL_SCALE = 1f;
	public static final float RAPID_SCALE = 1f;

	// property
	private static final float VERTICAL_HEADER_Y = 1f;
	public int _width;
	public int _height;
	public int _iniTop;
	public int _iniLeft;
	private int _centerX;
	private int _centerY;
	
	private boolean _ready = false;
	private long _lastGPStimeStamp = 0;
	private Latlng _preLocation;
	private Latlng _curLocation;
	private Location _location;
	private NavigationManager _NavManager;
	// private RoutePlan _plan;
	private DirectionRoute _directionPlan;
	private ArrayList _routeList = new ArrayList();
	// private ArrayList<OsmWay> _wayList = new ArrayList<OsmWay>(); //The path
	// of street map
	private Latlng _initPos;
	private float _compassValues = 0;
	private int _mapscale = NORMAL_MAP_SCALE;
	private boolean _isArrived;
	private boolean _isRouteInit = false;
	private int _scaningBarWidth;
	private float _curSpeedScale = NORMAL_SCALE;
	private float _scaleAnimate = NORMAL_SCALE;
	NavigationGridBoard _parent;

	// animation maker timer
	// scaner
	private CountDownTimer _loadingAniTimer;
	private CountDownTimer _pointAniTimer;
	private int _loadingAniTimer_loc;
	private boolean _loadingAniForward = true;

	// location animate
	double stepLng;
	double stepLat;
	float stepSpeed;

	// pointer
	int _pointAniSize;
	float stepPointer;

	// path and paint

	private Path _loadingAniPath;
	private Path _loadingText;
	private Path _routePath;
	private Path _turnPath;
	private Path _pointerPath;
	private Path _headPath;
	private Path _pointerAniPath;
	private Path _accurcyRangePath;
	private Path _viewdirectionPath;

	private Paint _loadingAniPaint;
	private Paint _loadingTextPaint;
	private Paint _routePaint;
	private Paint _subRoutePaint;
	private Paint _turnPaint;
	private Paint _pointerPaint;
	private Paint _pointerAniPaint;
	private Paint _accurcyRangePaint;
	private Paint _viewdirectionPaint;
	private Paint _osmPaint;

	private PathEffect _accurcyRangeEffect = new PathEffect();
	private int _phase;

	private Camera _camera;

	private float _speed;
	private Bitmap mBitmap;

	// values
	StreetMapRequester _osmRequester;
	private double _turnAlertDistance;
	private Location _prelocation = null;
	float _targetScale = 0;
	private double _roadAngle = 0;
	private double _preRoadAngle = 0;
	private double _curRoadAngle = 0;
	private Thread _roadAngleAnimThread;
	private boolean _doRoadAngleAnimate = true;
	private boolean _stopMapRetreiving = false;
	private Matrix mMatrix;
	private float mBearing = 0;
	private Canvas mCanvas;
	float[] mVerts;

	@SuppressLint("NewApi")
	public Elem_Route_3D(GridBoard p, int[] pin) {
		super(p, pin);
		// TODO Auto-generated constructor stub
		_parent = (NavigationGridBoard) p;

		this.ELEM_PIN = pin;

		initProperty();
		initPath();
		// initGPSCheck();

		startLoadingAnimation();
		setPivotX(_centerX);
		setPivotY(_height);
		setScaleX(3);
		createBitmap();
		setRotationX(30);

		// float scale = getResources().getDisplayMetrics().density;
		// setCameraDistance(2 * _width);

		/*
		 * mMatrix = new Matrix(); Camera mCamera = new Camera();
		 * 
		 * 
		 * mCamera.rotateX(60); mCamera.getMatrix(mMatrix);
		 */
	}

	private void createBitmap() {
		WindowManager wm = (WindowManager) getContext().getSystemService(
				Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;

		mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		mCanvas = new Canvas(mBitmap);
	}

	private synchronized void drawPath() {
		mCanvas.drawColor(Color.BLACK);
		drawRoute(mCanvas);
		drawDecoration(mCanvas);
	}

	@Override
	protected void onDraw(Canvas canvas) {

		if (_isRouteInit) {
			drawPath();
			canvas.drawBitmap(mBitmap, 0, 0, _routePaint);
			
		}

		invalidate();

	}

	@Override
	public void onPause() {
		
		try {
			_doRoadAngleAnimate = false;
			_stopMapRetreiving = true;
		} catch (Exception e) {

		}
	}

	@Override
	public void onResume() {
		
		_doRoadAngleAnimate = true;
		_roadAngleAnimThread = null;
		_roadAngleAnimThread = new Thread(new RoadAngleAnimate());
		_roadAngleAnimThread.start();
		// doStreetMapRequester();
	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		

	}

	@Override
	public void onLocationChange(Location location) {

		_location = location;
		// _osmRequester.setCurLocation(location);

		if (_isRouteInit) {

			if (isLocationOK(location)) {
				_ready = true;

			} else {
				_ready = false;

				// invalidate();
			}
		}

		if (_preLocation != null) {
			long finish = 700;
			long tick = 100;

			_curLocation = new Latlng(location.getLatitude(),
					location.getLongitude());
			double deltaLng = location.getLongitude() - _preLocation.getLng();
			double deltaLat = location.getLatitude() - _preLocation.getLat();

			stepLng = deltaLng / (finish / tick);
			stepLat = deltaLat / (finish / tick);

			// generate smooth location
			new CountDownTimer(finish, tick) {

				@Override
				public void onTick(long millisUntilFinished) {
					
					_preLocation = new Latlng(_preLocation.getLat() + stepLat,
							_preLocation.getLng() + stepLng);
					
				}

				@Override
				public void onFinish() {
					
					_preLocation = _curLocation;
				}
			}.start();

			// generate smooth speed
			if (location.getProvider().equals(LocationManager.GPS_PROVIDER)
					&& location.getSpeed() != 0) {

				_speed = getSpeed(location, _prelocation);
				genSmoothSpeed(_speed / 1000 * 3600, finish, tick);
			}

			if (location.getProvider().equals(LocationManager.GPS_PROVIDER)
					&& location.getSpeed() == 0 && _prelocation != null) {
				// some device no speed
				_speed = Utility.caculateSpeed(location, _prelocation);
				genSmoothSpeed(_speed / 1000 * 3600, finish, tick);
			}

		} else {

			/*
			 * At initial situation, there are no _currLocation value.
			 */
			_preLocation = new Latlng(location.getLatitude(),
					location.getLongitude());
		}

		if (_prelocation != null
				&& _prelocation.hasAccuracy()
				&& _prelocation.getAccuracy() < _prelocation
						.distanceTo(location))
			mBearing = _prelocation.bearingTo(location);
		_prelocation = location;
		new InitRoutePathTask().execute();

	}

	/*
	 * Should put after initRoutePath(), initProperty()
	 */
	/*
	 * private void doStreetMapRequester(){ _wayList.clear();
	 * 
	 * //_osmRequester = new StreetMapRequester(getContext(), this,_location,
	 * _initPos, _width, NORMAL_MAP_SCALE_RATE); }
	 */
	private float getSpeed(Location currlocation, Location prelocation) {
		if (currlocation.getSpeed() != 0) {
			return (currlocation.getSpeed());
		} else {
			if (prelocation != null) {
				return Utility.caculateSpeed(currlocation, prelocation);
			} else {
				return 0;
			}
		}
	}

	public void genSmoothSpeed(float speed, long finish, long tick) {

		if (speed > 70) {

			_targetScale = RAPID_SCALE;

		} else if (40 < speed && speed <= 70) {

			_targetScale = NORMAL_SCALE;

		} else {

			_targetScale = SLOW_SCALE;
		}

		if (_targetScale != this._curSpeedScale) {

			stepSpeed = (_targetScale - _curSpeedScale) / (finish / tick);
			_curSpeedScale = _targetScale;

			new CountDownTimer(finish, tick) {

				@Override
				public void onTick(long millisUntilFinished) {
					
					_scaleAnimate = _scaleAnimate + stepSpeed;

				}

				@Override
				public void onFinish() {
					
					_scaleAnimate = _targetScale;
				}

			}.start();

		}
	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		
		/*
		 * if (_location != null) { GeomagneticField geoField = new
		 * GeomagneticField( (float) _location.getLatitude(), (float)
		 * _location.getLongitude(), (float) _location.getAltitude(),
		 * System.currentTimeMillis()); _compassValues = event.values[0] +
		 * geoField.getDeclination(); // Log("GeomagneticField Declination: " +
		 * // geoField.getDeclination()); } else { _compassValues =
		 * event.values[0]; }
		 */
	}

	// road orientation
	@Override
	public void onSensorChange(float[] orientation) {
		
		// _compassValues = (float) Math.toDegrees(orientation[0]);
		if (_location != null) {
			GeomagneticField geoField = new GeomagneticField(
					(float) _location.getLatitude(),
					(float) _location.getLongitude(),
					(float) _location.getAltitude(), System.currentTimeMillis());
			_compassValues = orientation[0] + geoField.getDeclination();
			// mValues[1] = (float) Math.toDegrees(orientation[1]);
		} else {

			_compassValues = orientation[0];
		}

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		

	}

	private static void makeEffects(PathEffect e, float phase) {

		e = new DashPathEffect(new float[] { 10, 5, 5, 5 }, phase);

	}

	private boolean isLocationOK(Location location) {

		if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
			return true;
		} else if (location.getAccuracy() <= 20) {
			return true;
		} else {
			return false;
		}

	}

	private void initProperty() {

		_width = _parent._unitPixel * ELEM_WIDTH;
		_height = _parent._unitPixel * ELEM_HEIGHT;
		_iniTop = _parent._screenHeightMargin + ELEM_PIN[PIN_HEIGHT]
				* _parent._unitPixel;
		_iniLeft = _parent._screenWidthMargin + ELEM_PIN[PIN_WIDTH]
				* _parent._unitPixel;
		_centerX = this._iniLeft + (this._width / 2);
		_centerY = this._iniTop + (this._height / 2);

	}

	private void initPath() {

		// scan bar
		_scaningBarWidth = _width / ELEM_WIDTH;

		_loadingAniPath = new Path();
		_loadingAniPaint = new Paint();
		_loadingAniPaint.setAntiAlias(true);
		_loadingAniPaint.setColor((int) (_parent.GLOBAL_COLOR * 0.5));
		_loadingAniPaint.setStyle(Style.STROKE);
		_loadingAniPaint.setStrokeWidth(_scaningBarWidth);

		// scan text

		// route
		// setting Path

		_routePaint = new Paint();
		_subRoutePaint = new Paint();
		_turnPaint = new Paint();
		_osmPaint = new Paint();

		_routePaint.setAntiAlias(true);
		_routePaint.setColor(_parent.GLOBAL_COLOR);
		_routePaint.setStrokeJoin(Join.ROUND);
		_routePaint.setStrokeMiter(10);
		_routePaint.setStyle(Style.STROKE);
		_routePaint.setStrokeJoin(Join.ROUND);
		_routePaint.setStrokeCap(Cap.ROUND);
		_routePaint.setStrokeWidth(_parent._unitPixel * 1f);

		_subRoutePaint.setAntiAlias(true);
		_subRoutePaint.setColor(Color.BLACK);
		_subRoutePaint.setStrokeJoin(Join.ROUND);
		_subRoutePaint.setStrokeMiter(10);
		_subRoutePaint.setStyle(Style.STROKE);
		_subRoutePaint.setStrokeJoin(Join.ROUND);
		_subRoutePaint.setStrokeCap(Cap.ROUND);
		_subRoutePaint.setStrokeWidth(_parent._unitPixel * 0.8f);

		_osmPaint.setColor(Color.WHITE);
		_osmPaint.setStrokeJoin(Join.ROUND);
		_osmPaint.setStrokeMiter(2);
		_osmPaint.setAlpha(50);
		_osmPaint.setAntiAlias(true);
		_osmPaint.setStyle(Style.STROKE);
		_osmPaint.setStrokeCap(Cap.ROUND);
		_osmPaint.setStrokeWidth(3);

		_turnPaint.setColor(_parent.GLOBAL_COLOR);
		_turnPaint.setStyle(Style.STROKE);
		_turnPaint.setStrokeWidth(3);
		_turnPaint.setAntiAlias(true);

		// pointer
		_pointAniSize = _height /24;
		final long finish = 500;
		final long tick = 50;

		_pointerPath = new Path();
		_pointerPath.addRect(new RectF(_centerX - _pointAniSize / 2, _height*VERTICAL_HEADER_Y - _pointAniSize / 2, _centerX
				+ _pointAniSize / 2, _height*VERTICAL_HEADER_Y
				+ _pointAniSize / 2), Direction.CCW);

		_headPath = new Path();
		_headPath.moveTo(_centerX, (float) (_height * VERTICAL_HEADER_Y - _pointAniSize * 0.5));
		_headPath.lineTo(_centerX - _pointAniSize, _height * VERTICAL_HEADER_Y);
		_headPath.lineTo(_centerX, _height * VERTICAL_HEADER_Y - _pointAniSize * 2);
		_headPath.lineTo(_centerX + _pointAniSize, _height * VERTICAL_HEADER_Y);
		_headPath.close();

		_pointerAniPath = new Path();
		stepPointer = _pointAniSize / (finish / tick);

		_pointAniSize = _pointAniSize * 2;

		_pointAniTimer = new CountDownTimer(finish, tick) {

			@Override
			public void onTick(long millisUntilFinished) {
				
				if (_pointAniSize > _width / 24) {
					_pointAniSize -= stepPointer;
				} else {
					_pointAniSize = _width / 24 * 2;
				}
				_pointerAniPath.reset();
				_pointerAniPath.addRect(new RectF(_centerX - _pointAniSize / 2,
						_height*VERTICAL_HEADER_Y - _pointAniSize / 2,
						_centerX + _pointAniSize / 2, _height*VERTICAL_HEADER_Y + _pointAniSize / 2),
						Direction.CCW);



			}

			@Override
			public void onFinish() {
				
				_pointAniTimer.start();
				
			}

		};
		// _pointAniTimer.start();

		_pointerPaint = new Paint();
		_pointerPaint.setAntiAlias(true);
		_pointerPaint.setStyle(Style.FILL);
		_pointerPaint.setColor(_parent.SECONDARY_COLOR);
		_pointerPaint.setStrokeWidth(3);

		_pointerAniPaint = new Paint();
		_pointerAniPaint.setAntiAlias(true);
		_pointerAniPaint.setStyle(Style.STROKE);
		_pointerAniPaint.setColor((int) (_parent.SECONDARY_COLOR * 0.5));
		_pointerAniPaint.setStrokeWidth(3);

		// accuracy range
		_accurcyRangePath = new Path();
		_accurcyRangePath.addCircle(_centerX, _height*VERTICAL_HEADER_Y,
				20, Direction.CCW);

		_accurcyRangePaint = new Paint();
		_accurcyRangePaint.setAntiAlias(true);
		_accurcyRangePaint.setStyle(Style.FILL_AND_STROKE);
		_accurcyRangePaint.setColor((int) (Color.WHITE * 0.5));
		_accurcyRangePaint.setStrokeWidth(2);

		_accurcyRangePaint.setAntiAlias(true);

		// view direction
		_viewdirectionPath = new Path();

		_viewdirectionPath.addArc(new RectF(_centerX - _pointAniSize * 2,
				_height*VERTICAL_HEADER_Y - _pointAniSize * 2, _centerX
						+ _pointAniSize * 2, _height*VERTICAL_HEADER_Y
						+ _pointAniSize * 2), -30, 60);
		_viewdirectionPath.lineTo(_centerX, _height*VERTICAL_HEADER_Y);
		_viewdirectionPath.close();

		_viewdirectionPaint = new Paint();
		_viewdirectionPaint.setStyle(Style.STROKE);
		_viewdirectionPaint.setColor(_parent.SECONDARY_COLOR);
		_viewdirectionPaint.setAlpha(150);
		_viewdirectionPaint.setStrokeWidth(3);
		_viewdirectionPaint.setAntiAlias(true);

		// camera
		/*
		 * _camera = new Camera();
		 * 
		 * _camera.save(); _camera.rotateX(20); _matrix = new Matrix();
		 * _camera.getMatrix(_matrix); _camera.restore();
		 */
	}

	private void startLoadingAnimation() {

		final long finish = 6000;
		final long tick = 50;
		_loadingAniTimer_loc = this._iniLeft;
		final int step = (int) (_width / (finish / tick));

		_ready = false;

		_loadingAniTimer = new CountDownTimer(finish, tick) {

			@Override
			public void onTick(long millisUntilFinished) {
				

				if (_loadingAniTimer_loc + (_scaningBarWidth / 2) + step >= _iniLeft
						+ _width)
					_loadingAniForward = false;
				if (_loadingAniTimer_loc - (_scaningBarWidth / 2) - step <= _iniLeft)
					_loadingAniForward = true;

				if (_loadingAniForward) {

					_loadingAniTimer_loc += step;
				} else {
					_loadingAniTimer_loc -= step;
				}
				// invalidate();
			}

			@Override
			public void onFinish() {
				
				_loadingAniTimer.start();
			}

		};

		_loadingAniTimer.start();
	}

	/*
	 * private void stopLoadingAnimation(){
	 * 
	 * _ready = true; _loadingAniTimer.cancel();
	 * 
	 * }
	 */

	private void drawLoadingAnimation(Canvas canvas) {

		canvas.save();

		_loadingAniPath.reset();
		_loadingAniPath.moveTo(_loadingAniTimer_loc, _iniTop);
		_loadingAniPath.lineTo(_loadingAniTimer_loc, _iniTop + _height);
		canvas.drawPath(_loadingAniPath, _loadingAniPaint);

		canvas.restore();
	}

	private void drawRoute(Canvas canvas) {
		if (canvas == null)
			return;

		if (_preLocation != null) {

			// new TurnPathRegenTask(false, canvas).execute();
			turnPathRegen(false);

			canvas.save();

			// canvas.clipRect(_iniLeft, _iniTop, _iniLeft + _width, _iniTop
			// + _height, Region.Op.INTERSECT);
			Point temp = latlng2Map(_preLocation);

			canvas.translate(_centerX - temp.x, (float) (_height*VERTICAL_HEADER_Y - temp.y));

			// canvas.scale(1, 1, temp.x, temp.y);
			// canvas.translate(mCenter.x, mCenter.y);
			if (_parent._settings.getPREF_LOCK_ROAD()) {

				canvas.rotate((float) -_roadAngle, temp.x, temp.y);
			} else {
				canvas.rotate(getHeading(), temp.x, temp.y);
			}

			_turnPaint.setPathEffect(new DashPathEffect(new float[] { 30, 5,
					30, 5 }, _phase));
			_phase += 1;

			if (_turnPath != null) {
				canvas.drawPath(_turnPath, _turnPaint);
			}
			canvas.drawPath(_routePath, _routePaint);
			canvas.drawPath(_routePath, _subRoutePaint);
			canvas.restore();

		}

	}

	private void drawDecoration(Canvas canvas) {
		if (canvas == null)
			return;
		
		canvas.save();

		if (_parent._settings.getPREF_LOCK_ROAD()) {

			// canvas.rotate((float) -_roadAngle - getHeading() - 90, _centerX,
			// _centerY + (int) (_width * 0.25));
			canvas.rotate(mBearing + (float) -_roadAngle, _centerX, _height*VERTICAL_HEADER_Y);
			// canvas.drawPath(_viewdirectionPath, _viewdirectionPaint);
			canvas.drawPath(_headPath, _pointerPaint);
		}

		if (_location != null && _location.hasBearing() && !_parent._settings.getPREF_LOCK_ROAD()) {

			canvas.rotate(_location.getBearing() + getHeading(), _centerX, _height*VERTICAL_HEADER_Y);
			canvas.drawPath(_headPath, _pointerPaint);
		}

		// canvas.drawPath(_pointerAniPath, _pointerAniPaint);
		// canvas.drawPath(_pointerPath, _pointerPaint);
		canvas.restore();
	}

	private void drawAccuracyRange(Canvas canvas) {
		canvas.save();
		float scale = getScale();
		// canvas.clipRect(_iniLeft, _iniTop, _iniLeft + _width, _iniTop +
		// _height);

		canvas.scale(scale, scale, _centerX, _height*VERTICAL_HEADER_Y);

		canvas.drawPath(getAccuracyRangePath(), _accurcyRangePaint);

		canvas.restore();
	}

	private Path getAccuracyRangePath() {
		if (_location != null) {
			_accurcyRangePath.reset();
			float ratio = (float) distance2Map(_location.getAccuracy() / 1000);
			_accurcyRangePath.addCircle(_centerX, _height*VERTICAL_HEADER_Y, ratio > (_width / 2) ? _width / 2
					: ratio, Direction.CCW);
		}
		return _accurcyRangePath;
	}

	private float getScale() {

		return _scaleAnimate;

	}

	/*
	 * public void involkOsmWayList(ArrayList<OsmWay> wayList){ _wayList =
	 * wayList; }
	 */

	private float getHeading() {

		/*
		 * if(_location.hasSpeed() && (_location.getSpeed()/1000*3600)>20 &&
		 * _location.hasBearing()){ //if( _location.hasBearing()){ return
		 * (-_location.getBearing());
		 * 
		 * }else{ try{ return (-_compassValues[0]-90); }catch(Exception e){
		 * e.printStackTrace(); return 0; }
		 * 
		 * 
		 * }
		 */

		try {

			return (-_compassValues);

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	private void initGPSCheck() {

		long curTime = System.currentTimeMillis();
		new CountDownTimer(30000, 1000) {

			public void onTick(long millisUntilFinished) {

			}

			public void onFinish() {

			}
		}.start();
	}

	public Point latlng2Map(Latlng latlng) {
		double lat = latlng.getLat();
		double lng = latlng.getLng();

		double x = lng - _initPos.getLng();
		double y = lat - _initPos.getLat();

		float flattenRate = 0.003367f;

		// currently just use normal_map_scale

		switch (_mapscale) {
		case NORMAL_MAP_SCALE:

			x = (_width / NORMAL_MAP_SCALE_RATE) * x;
			y = (_width / (NORMAL_MAP_SCALE_RATE)) * y * -1;

			break;

		case ZOOMOUT_MAP_SCALE:

			x = (_width / ZOOMOUT_MAP_SCALE_RATE) * x;
			y = (_width / (ZOOMOUT_MAP_SCALE_RATE)) * y * -1;

			break;

		case ZOOMIN_MAP_SCALE:

			x = (_width / ZOOMIN_MAP_SCALE_RATE) * x;
			y = (_width / (ZOOMIN_MAP_SCALE_RATE)) * y * -1;

			break;

		}

		return new Point((int) x, (int) y);

	}

	public double latLngRate(double lat) {
		return Math.abs(Math.sin((90 - Math.abs(lat)) * Math.PI / 180));
	}

	public double distance2Map(double km) {

		double rate = 0;

		switch (_mapscale) {
		case NORMAL_MAP_SCALE:
			rate = (_width / NORMAL_MAP_SCALE_RATE);

			break;
		case ZOOMOUT_MAP_SCALE:
			rate = (_width / ZOOMOUT_MAP_SCALE_RATE);
			break;
		case ZOOMIN_MAP_SCALE:
			rate = (_width / ZOOMIN_MAP_SCALE_RATE);
			break;
		}

		return (rate / 111.31955) * km;
	}

	public void setRoute() throws Exception {

	}

	public void Log(String msg) {
		Log.d("parts", msg);
	}

	/*
	 * //asyncTask private class HttpRequestTask extends AsyncTask{
	 * 
	 * 
	 * 
	 * 
	 * @Override protected void onPreExecute() { // TODO Auto-generated method
	 * stub
	 * 
	 * 
	 * }
	 * 
	 * 
	 * 
	 * @Override protected void onPostExecute(Object result) { // TODO
	 * Auto-generated method stub
	 * 
	 * }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @Override protected Object doInBackground(Object... curr) { // TODO
	 * Auto-generated method stub
	 * 
	 * NavierPanel context = (NavierPanel)_parent._context; _NavManager = new
	 * NavigationManager(context._destination);
	 * _NavManager.directionRequest((Latlng)curr[0]);
	 * 
	 * if(_NavManager.mRoutePlan!=null){ try{
	 * 
	 * _plan = _NavManager.mRoutePlan; //route properties
	 * 
	 * _routeList = _plan.arr_steps; initRoutePath(); Thread.sleep(1000);
	 * _isRouteInit = true;
	 * 
	 * }catch(Exception e){
	 * 
	 * } }
	 * 
	 * return null;
	 * 
	 * 
	 * 
	 * }
	 * 
	 * }
	 */

	@Override
	public void onArrival() {
		

	}

	@Override
	public void onLeftDistanceNotify(double distance, double alarmDistance) {
		

	}

	@Override
	public void onGlobalColorChange(int color, int sndColor) {
		
		_loadingAniPaint.setColor(color);
		_loadingAniPaint.setAlpha(50);
		_pointerPaint.setColor(sndColor);
		_pointerAniPaint.setColor(sndColor);
		_viewdirectionPaint.setColor(sndColor);
		_viewdirectionPaint.setAlpha(150);
		_routePaint.setColor(color);
		_turnPaint.setColor(color);
		// _viewdirectionPaint.setColor(color);
	}

	public static void drawShadow(Canvas canvas, int uniPixel, int iniLeft,
			int iniTop, int[] pin, Context context) {

		Path _pointerPath;
		Path _headPath;
		Path _pointerAniPath;
		Path _accurcyRangePath;
		Paint _pointerPaint;
		Paint _pointerAniPaint;
		Paint _accurcyRangePaint;

		iniLeft += pin[0] * uniPixel;
		iniLeft += pin[1] * uniPixel;

		int _centerX = iniLeft + ELEM_WIDTH * uniPixel / 2;
		int _centerY = iniTop + ELEM_HEIGHT * uniPixel / 2;
		int _pointAniSize = uniPixel * ELEM_WIDTH / 32;

		// path & paint
		_headPath = new Path();
		_headPath.moveTo(_centerX, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25) - _pointAniSize * 2);
		_headPath.lineTo(_centerX - _pointAniSize / 2, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25) - _pointAniSize * 2);
		_headPath.lineTo(_centerX, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25) - _pointAniSize * 3);
		_headPath.lineTo(_centerX + _pointAniSize / 2, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25) - _pointAniSize * 2);
		_headPath.lineTo(_centerX, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25) - _pointAniSize * 2);

		_pointAniSize = uniPixel * ELEM_WIDTH / 32;
		final long finish = 500;
		final long tick = 50;

		_pointerPath = new Path();
		_pointerPath.addRect(new RectF(_centerX - _pointAniSize / 2, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25) - _pointAniSize / 2,
				_centerX + _pointAniSize / 2, _centerY
						+ (int) (uniPixel * ELEM_WIDTH * 0.25) + _pointAniSize
						/ 2), Direction.CCW);

		_headPath = new Path();
		_headPath.moveTo(_centerX, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25));
		_headPath.lineTo(_centerX - _pointAniSize / 2, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25));
		_headPath.lineTo(_centerX, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25) - _pointAniSize);
		_headPath.lineTo(_centerX + _pointAniSize / 2, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25));
		_headPath.lineTo(_centerX, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25));

		_pointerAniPath = new Path();
		_pointAniSize = _pointAniSize * 2;

		_pointerAniPath.addRect(new RectF(_centerX - _pointAniSize / 2,
				_centerY + (int) (uniPixel * ELEM_WIDTH * 0.25) - _pointAniSize
						/ 2, _centerX + _pointAniSize / 2, _centerY
						+ (int) (uniPixel * ELEM_WIDTH * 0.25) + _pointAniSize
						/ 2), Direction.CCW);

		_accurcyRangePaint = new Paint();
		_accurcyRangePaint.setStyle(Style.FILL_AND_STROKE);
		_accurcyRangePaint.setColor(Color.WHITE);
		_accurcyRangePaint.setStrokeWidth(2);
		_accurcyRangePaint.setAlpha(50);

		_accurcyRangePaint.setAntiAlias(true);

		_accurcyRangePath = new Path();
		_accurcyRangePath.addCircle(_centerX, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25), uniPixel * ELEM_WIDTH
				/ 2, Direction.CCW);

		_pointerPaint = new Paint();
		_pointerPaint.setAntiAlias(true);
		_pointerPaint.setStyle(Style.FILL);
		_pointerPaint.setColor(Color.RED);
		_pointerPaint.setStrokeWidth(3);

		_pointerAniPaint = new Paint();
		_pointerAniPaint.setAntiAlias(true);
		_pointerAniPaint.setStyle(Style.STROKE);
		_pointerAniPaint.setColor(Color.RED);
		_pointerAniPaint.setAlpha(150);
		_pointerAniPaint.setStrokeWidth(3);

		// draw accuracy
		canvas.save();

		canvas.clipRect(iniLeft, iniTop, iniLeft + uniPixel * ELEM_WIDTH,
				iniTop + uniPixel * ELEM_HEIGHT);
		canvas.drawPath(_accurcyRangePath, _accurcyRangePaint);

		canvas.rotate(30, _centerX, _centerY
				+ (int) (uniPixel * ELEM_WIDTH * 0.25));
		canvas.drawPath(_headPath, _pointerPaint);

		canvas.drawPath(_pointerAniPath, _pointerAniPaint);
		canvas.drawPath(_pointerPath, _pointerPaint);

		canvas.restore();
	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		

	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		
		_loadingAniPaint.setColor(_parent.GLOBAL_COLOR);
		_loadingAniPaint.setAlpha(50);
		_pointerPaint.setColor(_parent.SECONDARY_COLOR);
		_pointerAniPaint.setColor(_parent.SECONDARY_COLOR);
		_viewdirectionPaint.setColor(_parent.SECONDARY_COLOR);
		_viewdirectionPaint.setAlpha(150);
		_routePaint.setColor(_parent.GLOBAL_COLOR);
		_turnPaint.setColor(_parent.GLOBAL_COLOR);
	}

	@Override
	public void setName(String name) {
		
		ELEM_NAME = name;
	}

	@Override
	public void onLocationOnRoadChange(Latlng p, double angle) {
		
		Log.d("onLocationOnRoadChange", angle+"");
		_curRoadAngle = angle;

		/*
		 * long tick = 500; long finish = 5000; float delta; _curRoadAngle =
		 * angle;
		 * 
		 * _curRoadAngle = _curRoadAngle < _roadAngle ? _curRoadAngle +360 :
		 * _curRoadAngle; if(Math.abs(_curRoadAngle - _roadAngle) < 180){
		 * 
		 * delta = (float) (_curRoadAngle - _roadAngle); }else{
		 * 
		 * delta = (float) (_roadAngle - _curRoadAngle); } final float step =
		 * delta/ (finish/tick);
		 * 
		 * new CountDownTimer(finish,tick){
		 * 
		 * @Override public void onFinish() { 
		 * _roadAngle = (float) (_roadAngle + step); }
		 * 
		 * @Override public void onTick(long millisUntilFinished) { // TODO
		 * Auto-generated method stub _roadAngle = (float) (_roadAngle + step);
		 * }
		 * 
		 * 
		 * }.start(); _preRoadAngle = _curRoadAngle;
		 */
	}

	private class RoadAngleAnimate implements Runnable {

		float delta;
		double targetAngle = 0;
		double preAngle = 0;
		float step;
		int count = 0;
		int max_count = 10;

		@Override
		public void run() {
			
			while (_doRoadAngleAnimate) {

				try {
					Thread.sleep((long) 50);
					if (preAngle != _curRoadAngle) {
						preAngle = _curRoadAngle;

						targetAngle = _curRoadAngle;

						delta = (float) (targetAngle - _roadAngle);
						delta = delta < 0 ? delta + 360 : delta;

						if (delta < 180) {
							step = delta / max_count;
						} else {

							step = -((360 - delta) / max_count);
						}

						count = 0;

						stepup();
						drawPath();

					} else {
						stepup();
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		private void stepup() {
			if (count < max_count) {
				_roadAngle += step;
				count++;
			}
		}

	}

	@Override
	public void onIsGPSFix(GpsStatus status, boolean isFix) {
		

	}

	@Override
	public void onRouteReplan(RoutePlan route) {
		
		/*
		 * _plan = null; _plan = route;
		 * 
		 * _routeList = null; _routeList = _plan.arr_steps;
		 * 
		 * initRoutePath();
		 * 
		 * _isRouteInit = true;
		 */
	}

	@Override
	public void onDirectionReplan(DirectionRoute directionPlan) {
		
		_directionPlan = directionPlan;
		// initRoutePath();
		new InitRoutePathTask().execute();

	}

	private class InitRoutePathTask extends AsyncTask<Void, Float, Path> {

		public InitRoutePathTask() {

		}

		@Override
		protected void onPreExecute() {
			
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Path result) {
			

			super.onPostExecute(result);
			_routePath = result;
			_isRouteInit = true;
			

		}

		@Override
		protected Path doInBackground(Void... params) {
			Path routePath = new Path();
			_initPos = new Latlng(
					_directionPlan._legList.get(0)._stepList.get(0).startlocation_lat,
					_directionPlan._legList.get(0)._stepList.get(0).startlocation_lng);

			boolean inited = false;
			// for (Latlng latlng : _directionPlan._overview_polyline) {
			// Point temp = latlng2Map(latlng);
			// if (!inited) {
			// routePath.moveTo(temp.x, temp.y);
			// inited = true;
			// } else {
			// routePath.lineTo(temp.x, temp.y);
			// }
			// }

			for (int i = 0; i < _directionPlan._overview_polyline.size() - 1; i++) {
				Latlng latlng = _directionPlan._overview_polyline.get(i);
				Latlng latlng1 = _directionPlan._overview_polyline.get(i + 1);
				Point temp = latlng2Map(latlng);
				Point temp1 = latlng2Map(latlng1);
				Point newP1 = null;
				Point newP2 = null;
				if (_curLocation != null) {
					Point l = latlng2Map(_curLocation);
					if (aabbContainsSegment(temp.x, temp.y, temp1.x, temp1.y,
							l.x - _width * 2, l.y - _height * 2, l.x + _width
									* 2, l.y + _height * 2)) {

						Path p = new Path();
						p.moveTo(temp.x, temp.y);
						p.lineTo(temp1.x, temp1.y);
						routePath.addPath(p);
					}
				}
			}

			return routePath;
		}

		public Point[] getIntersectionPoint(Line2D line, Rect rect) {

			Point[] p = new Point[4];

			// Top line
			p[0] = getIntersectionPoint(line, new Line2D(rect.left, rect.top,
					rect.right, rect.top));
			// Bottom line
			p[1] = getIntersectionPoint(line, new Line2D(rect.left,
					rect.bottom, rect.right, rect.bottom));
			// Left side...
			p[2] = getIntersectionPoint(line, new Line2D(rect.left, rect.top,
					rect.left, rect.bottom));
			// Right side
			p[3] = getIntersectionPoint(line, new Line2D(rect.right, rect.top,
					rect.right, rect.bottom));

			return p;

		}

		public Point getIntersectionPoint(Line2D lineA, Line2D lineB) {

			double x1 = lineA.getX1();
			double y1 = lineA.getY1();
			double x2 = lineA.getX2();
			double y2 = lineA.getY2();

			double x3 = lineB.getX1();
			double y3 = lineB.getY1();
			double x4 = lineB.getX2();
			double y4 = lineB.getY2();

			Point p = null;

			double d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
			if (d != 0) {
				double xi = ((x3 - x4) * (x1 * y2 - y1 * x2) - (x1 - x2)
						* (x3 * y4 - y3 * x4))
						/ d;
				double yi = ((y3 - y4) * (x1 * y2 - y1 * x2) - (y1 - y2)
						* (x3 * y4 - y3 * x4))
						/ d;

				p = new Point((int) xi, (int) yi);

			}
			return p;
		}

	}

	public boolean aabbContainsSegment(float x1, float y1, float x2, float y2,
			float minX, float minY, float maxX, float maxY) {
		// Completely outside.
		if ((x1 <= minX && x2 <= minX) || (y1 <= minY && y2 <= minY)
				|| (x1 >= maxX && x2 >= maxX) || (y1 >= maxY && y2 >= maxY))
			return false;
		// Start or end inside.
		if ((x1 > minX && x1 < maxX && y1 > minY && y1 < maxY)
				|| (x2 > minX && x2 < maxX && y2 > minY && y2 < maxY))
			return true;

		float m = (y2 - y1) / (x2 - x1);

		float y = m * (minX - x1) + y1;
		if (y > minY && y < maxY)
			return true;

		y = m * (maxX - x1) + y1;
		if (y > minY && y < maxY)
			return true;

		float x = (minY - y1) / m + x1;
		if (x > minX && x < maxX)
			return true;

		x = (maxY - y1) / m + x1;
		if (x > minX && x < maxX)
			return true;

		return false;
	}

	private class TurnPathRegenTask extends AsyncTask<Void, Float, Path> {

		private boolean _forceDrawTurnDistance;

		public TurnPathRegenTask(boolean forceDrawTurnDistance) {
			_forceDrawTurnDistance = forceDrawTurnDistance;

		}

		@Override
		protected void onPreExecute() {
			
			super.onPreExecute();
			_initPos = _directionPlan._legList.get(0)._stepList.get(0).polyline_list
					.get(0);

		}

		@Override
		protected void onPostExecute(Path result) {
			
			super.onPostExecute(result);
			
			if (result != null) {
				
			}

		}

		@Override
		protected Path doInBackground(Void... params) {
			

			float ratio = 0;
			if (_location != null) {
				ratio = (float) distance2Map((_parent._naviManager
						.getAlarmDistance(_preLocation, _speed)) / 1000);
			}
			ratio = (float) (ratio == 0 ? distance2Map((70f / 1000f)) : ratio);

			if (ratio != _turnAlertDistance || _forceDrawTurnDistance == true) {

				_turnPath = new Path();
				for (int leg = _directionPlan._current_leg; leg < _directionPlan._legList
						.size(); leg++) {
					DirectionLeg theLeg = _directionPlan._legList.get(leg);

					for (int step = leg == _directionPlan._current_leg ? _directionPlan._current_step
							: 0; step < theLeg._stepList.size(); step++) {

						DirectionStep theStep = theLeg._stepList.get(step);
						Point temp = latlng2Map(theStep.polyline_list.get(0));
						_turnPath.addCircle(temp.x, temp.y, 5, Direction.CCW);
						_turnPath.addCircle(temp.x, temp.y, ratio,
								Direction.CCW);
					}
				}
			}

			return null;
		}

	}

	private void turnPathRegen(boolean forceDrawTurnDistance) {
		// DirectionStep step = (DirectionStep)_routeList.get(0);
		// _initPos = step.polyline_list.get(0);
		_initPos = _directionPlan._legList.get(0)._stepList.get(0).polyline_list
				.get(0);

		// for counting width and height
		float ratio = 0;
		if (_location != null) {
			ratio = (float) distance2Map((_parent._naviManager
					.getAlarmDistance(_preLocation, _speed)) / 1000);
		}
		ratio = (float) (ratio == 0 ? distance2Map((70f / 1000f)) : ratio);

		if (ratio != _turnAlertDistance || forceDrawTurnDistance == true) {

			_turnPath = new Path();
			int count = 0;
			for (int leg = _directionPlan._current_leg; leg < _directionPlan._legList
					.size(); leg++) {
				DirectionLeg theLeg = _directionPlan._legList.get(leg);

				for (int step = leg == _directionPlan._current_leg ? _directionPlan._current_step
						: 0; step < theLeg._stepList.size(); step++) {

					if (count < 2) {
						DirectionStep theStep = theLeg._stepList.get(step);
						Point temp = latlng2Map(theStep.polyline_list.get(0));
						_turnPath.addCircle(temp.x, temp.y, 5, Direction.CCW);
						_turnPath.addCircle(temp.x, temp.y, ratio,
								Direction.CCW);
						count++;
					} else {
						break;
					}
				}

				if (count < 3) {
					break;
				}
			}
		}
		
		
		/*
		 * //for counting width and height float ratio = 0; if(_location!=null
		 * ){ ratio =
		 * (float)distance2Map((_parent._naviManager.getAlarmDistance(
		 * _preLocation, _speed))/1000); } ratio = (float)
		 * (ratio==0?distance2Map((70f/1000f)):ratio);
		 * 
		 * if(ratio!=this._turnAlertDistance || forceDrawTurnDistance==true){
		 * 
		 * try{ _turnPath = new Path(); for(int i=0;i<_routeList.size();i++){
		 * 
		 * step = (DirectionStep)_routeList.get(i); Point temp =
		 * latlng2Map(step.polyline_list.get(0)); _turnPath.addCircle(temp.x,
		 * temp.y, 5, Direction.CCW); _turnPath.addCircle(temp.x, temp.y, ratio,
		 * Direction.CCW); } }catch(Exception e){
		 * 
		 * } _turnAlertDistance = ratio; }
		 */

	}

	class Line2D {
		public Point p1;
		public Point p2;

		public Line2D(int x1, int y1, int x2, int y2) {
			p1 = new Point(x1, y1);
			p2 = new Point(x2, y2);

		}

		public double getY2() {
			
			return p2.y;
		}

		public double getX2() {
			
			return p2.x;
		}

		public double getY1() {
			
			return p1.y;
		}

		public double getX1() {
			
			return p1.x;
		}
	}

}
