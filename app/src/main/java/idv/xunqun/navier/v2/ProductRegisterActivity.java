package idv.xunqun.navier.v2;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.http.ProductRegisterRequester;
import idv.xunqun.navier.utils.Utility;

import org.apache.http.client.protocol.RequestAddCookies;
import org.json.JSONObject;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.AccountPicker;

public class ProductRegisterActivity extends Activity {
	protected static final int REQUEST_ACCOUNT = 0;
	EditText tvProductId;
	EditText tvNavierCode;
	Button btnUlock;
	Button btnGoogleAccount;
	private boolean isAccountSet = false;

	// Value to send
	private String mAccount;
	private String mProductId;
	private String mNavierCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_register);
		initViews();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_ACCOUNT && resultCode == Activity.RESULT_OK) {
			String accountName = data
					.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
			btnGoogleAccount.setText(accountName);
			isAccountSet = true;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onResume() {
		
		super.onResume();
		// Utility.checkLanscapeOrientation(this);
		Utility.requestLocationServiceUpdate(this);
		if(PreferencesHandler.get(this).getPREF_SPACIAL_VERSION() == true){
			findViewById(R.id.unlocked_frame).setVisibility(View.VISIBLE);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Utility.requestLocationServiceStop(this);
	}

	private void initViews() {
		btnUlock = (Button) findViewById(R.id.unlock);
		tvProductId = (EditText) findViewById(R.id.productid);
		tvNavierCode = (EditText) findViewById(R.id.naviercode);
		btnGoogleAccount = (Button) findViewById(R.id.account);
		btnGoogleAccount.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String[] accountTypes = new String[] { "com.google" };
				Intent it = AccountPicker.newChooseAccountIntent(null, null,
						accountTypes, false, null, null, null, null);
				startActivityForResult(it, REQUEST_ACCOUNT);
			}
		});

		btnUlock.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean ok = true;
				if (TextUtils.isEmpty(tvProductId.getText().toString())) {
					ok = false;
					tvProductId.setError(getString(R.string.err_cannot_empty));

				}

				if (TextUtils.isEmpty(tvNavierCode.getText().toString())) {
					ok = false;
					tvNavierCode.setError(getString(R.string.err_cannot_empty));
				}

				if (!isAccountSet) {
					btnGoogleAccount
							.setError(getString(R.string.err_cannot_empty));
				}

				if (ok) {
					// check online
					mProductId = tvProductId.getText().toString();
					mAccount = btnGoogleAccount.getText().toString();
					mNavierCode = tvNavierCode.getText().toString();
					mHttpRequestTask = new RegTask();
					mHttpRequestTask.execute();
				}

			}
		});
	}

	private class RegTask extends AsyncTask<Void, Void, Integer>{

		AlertDialog resultDialog;

		@Override
		protected void onPreExecute() {
			findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
			super.onPreExecute();

		}

		@Override
		protected void onPostExecute(Integer result) {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					ProductRegisterActivity.this);
			if (ProductRegisterActivity.this != null) {
				findViewById(R.id.progressBar).setVisibility(View.GONE);
				
				int stringRes = R.string.error_code_invaliate;
				switch (result) {
				case 0:
					PreferencesHandler.get(ProductRegisterActivity.this)
							.setPREF_SPECIAL_VERSION(true);
					findViewById(R.id.unlocked_frame).setVisibility(View.VISIBLE);
					stringRes = R.string.special_unlocked;
					break;
				case 1:
					PreferencesHandler.get(ProductRegisterActivity.this)
							.setPREF_SPECIAL_VERSION(true);
					findViewById(R.id.unlocked_frame).setVisibility(View.VISIBLE);
					stringRes = R.string.special_unlocked;
					break;
				case 2:
					stringRes = R.string.error_code_invaliate;
					break;
				case 3:
					stringRes = R.string.error_code_invaliate;
					break;

				default:
					break;
				}
				// show dialog
				builder.setMessage(getString(stringRes));
				builder.setPositiveButton(
						getString(R.string.naviernaviconfigure_ok),new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								
								
							}
						});
				builder.create().show();
				
			}
			super.onPostExecute(result);
		}

		@Override
		protected Integer doInBackground(Void... params) {
			ProductRegisterRequester requester = new ProductRegisterRequester(
					mAccount, mProductId, mNavierCode);
			try {
				String result = requester.sentHttpRequest();
				JSONObject jObj = new JSONObject(result);
				int state = jObj.getInt("state");
				return state;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return -1;
		}
		
	}
	private AsyncTask<Void, Void, Integer> mHttpRequestTask;
}
