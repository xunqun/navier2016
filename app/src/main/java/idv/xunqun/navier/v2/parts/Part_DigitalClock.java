package idv.xunqun.navier.v2.parts;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.parts.Parts;

import java.util.Calendar;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Typeface;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.TextPaint;
/*
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Part_DigitalClock extends BasePart {

	private Context context;

	private PreferencesHandler _pref;

	// draw values
	private Path _ampmPath;
	private TextPaint _textPaint;

	private int _centerX;
	private int _centerY;
	private int _TEXTSIZE;

	// ThumbNail
	DigitalClockThumbNail _thumbNail;

	private Path _hourPath;

	private Path _dashPath;

	private Path _minPath;

	private Paint _dashPaint;

	private String _h;

	private String _m;

	private long _timeCounter;

	private String _ampm;

	public class DigitalClockThumbNail extends ThumbNail {
		@Override
		public void draw(Canvas c) {
			// TODO Auto-generated method stub

		}
	}

	public Part_DigitalClock(BaseGridBoard parent, int[] pin) {
		super(parent._context, parent, pin);

		context = parent._context;
		_pref = new PreferencesHandler(context);

		// MUST HAVE properties
		setPartProperties(2, 4, Parts.ELEM_DIGITALCLOCK,
				context.getString(R.string.part_digitalclock), false);

		// MUST HAVE ThumbNail
		_thumbNail = new DigitalClockThumbNail();

		initValue();
		initPath();

	}

	private void initValue() {
		// TODO Auto-generated method stub
		_centerX = _iniLeft + (_width / 2);
		_centerY = _iniTop + (_height / 2);
		_TEXTSIZE = (int) (_parent._unitPixel * 1.6);

	}

	private void initPath() {
		Typeface font;
		try {
			font = Typeface.createFromAsset(_parent._context.getAssets(),
					"fonts/mplus-1m-light.ttf");
		} catch (Exception e) {
			font = Typeface.DEFAULT;
			e.printStackTrace();
		}

		_textPaint = new TextPaint();
		_textPaint.setColor(_parent.GLOBAL_COLOR);
		_textPaint.setTypeface(font);
		_textPaint.setTextAlign(Align.CENTER);

		_dashPaint = new Paint();
		_dashPaint.setColor(_parent.GLOBAL_COLOR);
		_dashPaint.setTypeface(font);
		_dashPaint.setTextSize(_TEXTSIZE);
		_dashPaint.setTextAlign(Align.CENTER);

		_textPaint.setTextSize(_TEXTSIZE / 4);
		_ampmPath = new Path();
		_ampmPath.moveTo(0, (float) (0.5 * _parent._unitPixel));
		_ampmPath.lineTo(_textPaint.measureText("AM"),
				(float) (0.5 * _parent._unitPixel));

		_textPaint.setTextSize(_TEXTSIZE);
		float dash_start = (ELEM_WIDTH) * _parent._unitPixel / 2
				- (_textPaint.measureText(":") / 2);
		float dash_end = (ELEM_WIDTH) * _parent._unitPixel / 2
				+ (_textPaint.measureText(":") / 2);

		_hourPath = new Path();
		_hourPath.moveTo(0, ELEM_HEIGHT * _parent._unitPixel);
		_hourPath.lineTo(dash_start, ELEM_HEIGHT * _parent._unitPixel);

		_dashPath = new Path();
		_dashPath.moveTo(dash_start, ELEM_HEIGHT * _parent._unitPixel);
		_dashPath.lineTo(dash_end, ELEM_HEIGHT * _parent._unitPixel);

		_minPath = new Path();
		_minPath.moveTo(dash_end, ELEM_HEIGHT * _parent._unitPixel);
		_minPath.lineTo((ELEM_WIDTH) * _parent._unitPixel, ELEM_HEIGHT
				* _parent._unitPixel);

	}

	@Override
	public void doUpdateValues(long now, long elapsed) {
		Calendar c = Calendar.getInstance();
		_h = String.format("%02.0f", (float) c.get(Calendar.HOUR_OF_DAY));
		_m = String.format("%02.0f", (float) c.get(Calendar.MINUTE));
		_timeCounter += elapsed;

		if (!_pref.getPREF_24HOUR()) {
			int intH = Integer.valueOf(_h);
			_ampm = "AM";
			if (intH == 0) {
				_h = "12";
			}
			if (intH > 12) {
				intH = intH - 12;
				_h = String.format("%2d", intH);
				_ampm = "PM";
			}

		} else {
			_ampm = "";
		}

		
		if (_timeCounter > 1000) {
			if (_dashPaint.getAlpha() != 0) {
				_dashPaint.setAlpha(0);
			} else {
				_dashPaint.setAlpha(255);
			}
			_timeCounter = 0;
		}

	}

	@Override
	public void doDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		
		canvas.save();
		canvas.translate(_iniLeft, _iniTop);
		_textPaint.setTextSize(_TEXTSIZE);
		
		canvas.drawTextOnPath(_h, _hourPath, 0, 0, _textPaint);
		canvas.drawTextOnPath(_m, _minPath, 0, 0, _textPaint);
		canvas.drawTextOnPath(":", _dashPath, 0, 0, _dashPaint);
		_textPaint.setTextSize(_TEXTSIZE / 4);
		canvas.drawTextOnPath(_ampm, _ampmPath, 0, 0, _textPaint);
		canvas.restore();
	}

	@Override
	public ThumbNail getThumbNail(ThumbNail t) {
		// TODO Auto-generated method stub
		return _thumbNail;
	}

	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onIsGPSFix(GpsStatus status, boolean isFix) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(float[] orientation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGlobalColorChange(int mainColor, int sndColor) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSpeedUnitChange(int speedUnit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceSave(Bundle savedStates) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInstanceRestore(Bundle savedStates) {
		// TODO Auto-generated method stub

	}

}
*/