package idv.xunqun.navier.v2;

/*
 * Play the role as idv.xunqun.navier.NavierPanel
 */
import idv.xunqun.navier.BaseNavierPanelContext;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.RRelativeLayout;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.content.Layout;
import idv.xunqun.navier.content.LayoutItem;
import idv.xunqun.navier.content.Place;
import idv.xunqun.navier.v2.parts.DashGridBoard;
import idv.xunqun.navier.utils.Utility;
import idv.xunqun.navier.v2.content.DirectionRoute;
import idv.xunqun.navier.v2.parts.GridBoard;
import idv.xunqun.navier.v2.parts.GridBoard.GridBoardHandler;
import idv.xunqun.navier.v2.parts.NavigationGridBoard;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class RuntimePanelFragment extends Fragment implements OnInitListener,
		BaseNavierPanelContext {

	public static final String EXTRA_PLACE_DISTINATION = "dest"; // only when
																	// layout is
																	// a
																	// Navigation
																	// Panel
	public static final String EXTRA_PLACE_FROM = "curr";
	public static final String EXTRA_ROUTE = "route";
	public static final String EXTRA_LAYOUT = "layout";
	public String JSONOBJECT_DEFAULT_LAYOUT;
	private final int TTS_CHECK_CODE = 1;
	public static final float MaxStdDeviation = 1f;

	public static final String NOTIFICATION_CLEAR_INTENT = "NOTIFICATION_CLEAR_INTENT";

	//
	// private PreferencesHandler _settings;
	// public SharedPreferences _settings;
	public RtPanelFragmentHandler _context;

	public String _raw_route;
	public JSONObject _gridBoardLayout;
	public GridBoard _gridBoard;
	public DirectionRoute _directionPlan;

	// values
	private ArrayList<HashMap<String, Object>> _colorList = new ArrayList<HashMap<String, Object>>();
	private SimpleAdapter _colorListAdapter;

	public Bundle _tmpInstanceBundle = null;

	// animation settings on gridboard
	public boolean _endAnim = false;
	public boolean _stopAnim = false;

	private boolean _dontTick = false;
	public View _layoutView;

	public interface RtPanelFragmentHandler {
		void doTTS(String text);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		initDefaultLayout();
		setRetainInstance(true);
		log_life("RuntimePanelFragment.onCreate()");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		Log.i("lifecycle", "RuntimePanelFragment.onCreateView()");

		// For navigation panel : Load the intent bundle

		Intent it = getActivity().getIntent();
		if (it.hasExtra(EXTRA_PLACE_DISTINATION)
				&& it.hasExtra(EXTRA_PLACE_FROM) && it.hasExtra(EXTRA_ROUTE)) {
			initDirectionPlan(it);
		}

		initGridview();

		if (savedInstanceState != null) {
			_gridBoard.restoreInstance(savedInstanceState);
		} else if (_tmpInstanceBundle != null) {
			_gridBoard.restoreInstance(_tmpInstanceBundle);
			_tmpInstanceBundle = null;
		}

		return _layoutView;
	}

	private void initGridview() {
		// Initial GridView

		try {
			LayoutItem layout;
			if (getActivity().getIntent().hasExtra(EXTRA_LAYOUT)) {
				layout = (LayoutItem) getActivity().getIntent()
						.getSerializableExtra(EXTRA_LAYOUT);
				_gridBoardLayout = new JSONObject(layout.LAYOUT_JSONSTR);
			} else {
				_gridBoardLayout = new JSONObject(JSONOBJECT_DEFAULT_LAYOUT);
			}

			if (_gridBoardLayout.getJSONObject(Layout.JSON_ROOT).getInt(
					Layout.JSON_TYPE) == Layout.TYPE_NAVIGATION) {

				_gridBoard = new NavigationGridBoard(getActivity(), this,
						_gridBoardLayout, new MyGridViewHandler());

			} else {
				_gridBoard = new DashGridBoard(getActivity(), this,
						_gridBoardLayout, new MyGridViewHandler());
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initDirectionPlan(Intent it) {

		_directionPlan = new DirectionRoute();

		try {
			String destination = it.getStringExtra(EXTRA_PLACE_DISTINATION);
			_directionPlan.setWayPoint(new JSONArray(destination));
			_directionPlan._from = (Latlng) it
					.getSerializableExtra(EXTRA_PLACE_FROM);
			_raw_route = getActivity().getIntent().getStringExtra(EXTRA_ROUTE);
			JSONObject jsonObject = new JSONObject(_raw_route);
			_directionPlan.setHereJson(getActivity(),jsonObject);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		_context = (RtPanelFragmentHandler) activity;
	}

	@Override
	public void onStop() {

		Log.i("lifecycle", "RuntimePanelFragment.onStop()");
		_tmpInstanceBundle = new Bundle();
		try {
			_gridBoard.saveInstance(_tmpInstanceBundle);
		} catch (Exception e) {
			e.printStackTrace();
		}

		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {

		_gridBoard.saveInstance(outState);
		super.onSaveInstanceState(outState);
		Log.d("lifecycle", "RuntimePanelFragment.onSaveInstanceState()");
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
		Log.d("lifecycle", "RuntimePanelFragment.onActivityCreated()");
		if (savedInstanceState != null) {
			_gridBoard.restoreInstance(savedInstanceState);
		}

		setIsEndAnimation(false);
	}

	@Override
	public void onPause() {

		super.onPause();
		Log.d("lifecycle", "RuntimePanelFragment.onPause()");

		try {
			_gridBoard.onPause();
		} catch (Exception e) {
			e.printStackTrace();
		}

		setIsStopAnimation(true);
	}

	@Override
	public void onResume() {

		super.onResume();
		Log.d("lifecycle", "RuntimePanelFragment.onResume()");

		if (_gridBoard != null) {
			_gridBoard.onResume();
		} else {
			initGridview();
		}

		setIsStopAnimation(false);
	}

	@Override
	public void onDestroy() {

		Log.d("lifecycle", "RuntimePanelFragment.onDestroy()");

		// Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,
		// _brightness);

		log_life("RuntimePanelFragment.onDestroy()");
		setIsEndAnimation(true);
		super.onDestroy();
	}

	public void onModeSwitch() {
		int mode = _gridBoard._layout._mode == Layout.MODE_NORMAL ? Layout.MODE_HUD
				: Layout.MODE_NORMAL;
		_gridBoard.modeSwitch(mode);
	}

	public void onSpeedUnitChange() {
		int unit = (_gridBoard.GLOBAL_SPEEDUNIT == PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH) ? PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_MPH
				: PreferencesHandler.VALUE_OF_PREF_SPEED_UNIT_KMH;
		_gridBoard.onSpeedUnitChangeHandler(unit);
		PreferencesHandler.get(getActivity()).setPREF_SPEED_UNIT(unit);

	}

	public void onLeftRightSwitch() {
		_gridBoard.lrModeSwitch();
	}

	public void onColorChange() {
		String[] colors = getResources().getStringArray(R.array.color_prompt);
		_colorListAdapter = new SimpleAdapter(getActivity(), _colorList,
				R.layout.color_list_item, new String[] { "name", "type" },
				new int[] { R.id.colorname, R.id.colortype });

		setColorItem(colors);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getString(R.string.navierpanel_color));
		// builder.setItems(colors, new onColorClick(this,colors));
		builder.setAdapter(_colorListAdapter, new onColorClick(getActivity(),
				colors));
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void onTogleGrid() {
		_gridBoard._showGrid = _gridBoard._showGrid == true ? false : true;
		PreferencesHandler.get(getActivity()).setPREF_SHOW_GRID(
				_gridBoard._showGrid);
	}

	public void initDefaultLayout() {

		JSONOBJECT_DEFAULT_LAYOUT = new Utility(getActivity())
				.getStringFromRaw(R.raw.navigation_classic);
	}

	@Override
	public void onInit(int status) {

		Log.d("lifecycle", "RuntimePanelFragment.onInit()");
	}

	private void setColorItem(String[] colors) {

		_colorList.clear();
		HashMap map;

		// CYAN
		map = new HashMap();
		map.put("name", colors[0]);
		map.put("type", R.drawable.color_cyan);

		_colorList.add(map);

		// Light gray
		map = new HashMap();

		map.put("name", colors[1]);
		map.put("type", R.drawable.color_lightgray);

		_colorList.add(map);

		// WHITE
		map = new HashMap();
		map.put("name", colors[2]);
		map.put("type", R.drawable.color_white);

		_colorList.add(map);

		// yellow
		map = new HashMap();
		map.put("name", colors[3]);
		map.put("type", R.drawable.color_yellow);

		_colorList.add(map);

		// green
		map = new HashMap();
		map.put("name", colors[4]);
		map.put("type", R.drawable.color_green);

		_colorList.add(map);

		// blue
		map = new HashMap();
		map.put("name", colors[5]);
		map.put("type", R.drawable.color_blue);

		_colorList.add(map);

		// orange red
		map = new HashMap();
		map.put("name", colors[6]);
		map.put("type", R.drawable.color_orangered);

		_colorList.add(map);

		// sky blue
		map = new HashMap();
		map.put("name", colors[7]);
		map.put("type", R.drawable.color_skyblue);

		_colorList.add(map);
	}

	private class onColorClick implements DialogInterface.OnClickListener {

		Context context;
		String[] colors;

		public onColorClick(Context c, String[] colors) {
			context = c;
			this.colors = colors;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {

			int rgb = 0xFFFFFF;
			String colorString = PreferencesHandler.VALUE_OF_PREF_COLOR_CYAN;

			switch (which) {
			case 0:
				rgb = Color.CYAN;
				colorString = PreferencesHandler.VALUE_OF_PREF_COLOR_CYAN;
				break;
			case 1:
				rgb = Color.LTGRAY;
				colorString = PreferencesHandler.VALUE_OF_PREF_COLOR_LIGHTGRAY;
				break;
			case 2:
				rgb = Color.WHITE;
				colorString = PreferencesHandler.VALUE_OF_PREF_COLOR_WHITE;
				break;
			case 3:
				rgb = Color.YELLOW;
				colorString = PreferencesHandler.VALUE_OF_PREF_COLOR_YELLOW;
				break;
			case 4:
				rgb = Color.GREEN;
				colorString = PreferencesHandler.VALUE_OF_PREF_COLOR_GREEN;
				break;

			case 5:
				rgb = Color.BLUE;
				colorString = PreferencesHandler.VALUE_OF_PREF_COLOR_BLUE;
				break;

			case 6:
				rgb = 0xffff4500;
				colorString = PreferencesHandler.VALUE_OF_PREF_COLOR_ORANGERED;
				break;
			case 7:
				rgb = 0xff87ceeb;
				colorString = PreferencesHandler.VALUE_OF_PREF_COLOR_SKYBLUE;
				break;
			default:
				rgb = Color.CYAN;
				colorString = PreferencesHandler.VALUE_OF_PREF_COLOR_CYAN;
			}

			_gridBoard.GLOBAL_COLOR = rgb;
			_gridBoard.onGlobalColorChange(rgb);
			PreferencesHandler.get(getActivity()).setPREF_COLOR(colorString);
		}

	}

	private void log_life(String s) {
		Log.d("life", s);
	}

	protected class MyGridViewHandler implements GridBoardHandler {

		@Override
		public void onLayoutInflate(View v) {

			_layoutView = v;
		}

	}

	// Below methods are implement of BaseNavierPanelContext

	@Override
	public boolean getIsEndAnimation() {

		return _endAnim;
	}

	@Override
	public void setIsEndAnimation(boolean b) {

		_endAnim = b;
	}

	@Override
	public boolean getIsStopAnimation() {

		return _stopAnim;
	}

	@Override
	public void setIsStopAnimation(boolean b) {

		_stopAnim = b;
	}

	/**
	 * Use getDirectionPlan()
	 */
	@Override
	@Deprecated
	public Place getDestPlace() {

		Latlng latlng = _directionPlan._wayPointList
				.get(_directionPlan._wayPointList.size() - 1);
		Place place = new Place();
		place.latitude = latlng.getLat();
		place.longitude = latlng.getLng();

		return place;
	}

	/**
	 * Use getDirectionPlan()
	 */
	@Override
	@Deprecated
	public Place getCurrPlace() {

		Place place = new Place();
		place.latitude = _directionPlan._from.getLat();
		place.longitude = _directionPlan._from.getLng();
		return place;
	}

	@Override
	public DirectionRoute getDirectionPlan() {
		return _directionPlan;
	}

	@Override
	public String getRowRoute() {

		return _raw_route;
	}

	@Override
	public void doTTS(String text) {

		_context.doTTS(text);
	}

}
