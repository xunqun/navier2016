package idv.xunqun.navier.v2;

import com.crashlytics.android.Crashlytics;
import idv.xunqun.navier.v2.maps.DownloadPackage;

import io.fabric.sdk.android.Fabric;
import java.util.Map;

import android.app.Application;

public class MyApplication extends Application {

    /**
     * Packages obtained from parsing Maps.xml
     */
    private Map<String, DownloadPackage> packageMap;

    
	@Override
	public void onCreate() {
		try {
			Class.forName("android.os.AsyncTask");
		} catch (Throwable ignore) {
		}
		super.onCreate();
		Fabric.with(this, new Crashlytics());
	}
	public Map<String, DownloadPackage> getPackageMap() {
        return packageMap;
    }
    
    public void setPackageMap(Map<String, DownloadPackage> packageMap) {
        this.packageMap = packageMap;
    }

}
