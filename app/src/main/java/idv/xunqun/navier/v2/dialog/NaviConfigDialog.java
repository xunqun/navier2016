package idv.xunqun.navier.v2.dialog;

import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.Spinner;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class NaviConfigDialog extends DialogFragment {

	private OnNavierConfigListener mListener;
	private PreferencesHandler _settings;
	ImageButton[] buttons = new ImageButton[3];
	String[] sec_options;
	private CheckBox avoidTolls;
	private CheckBox avoidHway;
	private Spinner spinner;
	private CheckBox lockroad;

	public interface OnNavierConfigListener {
		void onConfig();
	}

	public void setOnNavierConfigListener(OnNavierConfigListener listener) {
		mListener = listener;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		View view = inflater.inflate(R.layout.navigation_config, null);
		buttons[0] = (ImageButton) view.findViewById(R.id.driving);
		buttons[1] = (ImageButton) view.findViewById(R.id.bicycleing);
		buttons[2] = (ImageButton) view.findViewById(R.id.walking);
		
		avoidTolls = (CheckBox) view.findViewById(R.id.avoidtolls);
		avoidHway = (CheckBox) view.findViewById(R.id.avoidhway);

		spinner = (Spinner) view.findViewById(R.id.spinner1);
		lockroad = (CheckBox) view.findViewById(R.id.lockroad);
		
		builder.setView(view)
				.setPositiveButton(getString(R.string.naviernaviconfigure_ok), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						mListener.onConfig();
						getDialog().dismiss();
					}
				});
		return builder.create();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		_settings = new PreferencesHandler(getActivity());
		initView();
	}

	private void initView() {

		// Transportation type

		String type = _settings.getPREF_TRANS_TYPE();
		setDefaultButtonImg(buttons, type);

		buttons[0].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buttons[0].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_down));
				buttons[1].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				buttons[2].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				_settings.setPREF_TRANS_TYPE(PreferencesHandler.VALUE_OF_PREF_TRANS_TYPE_DRIVING);
			}

		});

		buttons[1].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buttons[0].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				buttons[1].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_down));
				buttons[2].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				_settings.setPREF_TRANS_TYPE(PreferencesHandler.VALUE_OF_PREF_TRANS_TYPE_BICYCLEING);
			}

		});

		buttons[2].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buttons[0].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				buttons[1].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				buttons[2].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_down));
				_settings.setPREF_TRANS_TYPE(PreferencesHandler.VALUE_OF_PREF_TRANS_TYPE_WALKING);
			}

		});

		// Turn alarm sec

		
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.trunsec_prompt_value, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		spinner.setSelection(getDefaultAlertSecPos(), true);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View view, int pos, long id) {
				// TODO Auto-generated method stub
				_settings.setPREF_TURNALARM_SEC(Integer.parseInt(sec_options[pos]));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});

		// restrictions
		

		avoidTolls.setChecked(_settings.getPREF_AVOID_TOLLS());
		avoidHway.setChecked(_settings.getPREF_AVOID_HIGHWAY());

		avoidTolls.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				_settings.setPREF_AVOID_TOLLS(arg1);
			}

		});

		avoidHway.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				_settings.setPREF_AVOID_HIGHWAY(arg1);
			}

		});

		// lockroad

		lockroad.setChecked(_settings.getPREF_LOCK_ROAD());

		lockroad.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				_settings.setPREF_LOCK_ROAD(arg1);
			}

		});

	}

	private void setDefaultButtonImg(ImageButton[] buttons, String type) {

		if (type.equalsIgnoreCase(PreferencesHandler.VALUE_OF_PREF_TRANS_TYPE_WALKING)) {

			buttons[2].setBackgroundResource(R.drawable.btn_down);
			buttons[0].setBackgroundResource(R.drawable.btn_up);
			buttons[1].setBackgroundResource(R.drawable.btn_up);

		} else if (type.equalsIgnoreCase(PreferencesHandler.VALUE_OF_PREF_TRANS_TYPE_BICYCLEING)) {

			buttons[1].setBackgroundResource(R.drawable.btn_down);
			buttons[0].setBackgroundResource(R.drawable.btn_up);
			buttons[2].setBackgroundResource(R.drawable.btn_up);

		} else {

			buttons[0].setBackgroundResource(R.drawable.btn_down);
			buttons[1].setBackgroundResource(R.drawable.btn_up);
			buttons[2].setBackgroundResource(R.drawable.btn_up);

		}
	}

	private int getDefaultAlertSecPos() {

		int sec = _settings.getPREF_TURNALARM_SEC();
		sec_options = this.getResources().getStringArray(R.array.trunsec_prompt_value);

		for (int i = 0; i < sec_options.length; i++) {
			if (sec == Integer.parseInt(sec_options[i])) {
				return i;
			}
		}

		return 3;
	}

}
