package idv.xunqun.navier.v2;




import idv.xunqun.navier.R;
import idv.xunqun.navier.utils.Utility;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;

public class PlaceConfigureActivity extends Activity {

	public static final String EXTRA_POSITION = "position";
	public static final String EXTRA_PLACENAME = "placename";
	public static final String EXTRA_ACTIONSUM = "action_sum";

	public static final int ACTION_DELETE = 1;
	public static final int ACTION_RENAME = 2;
	public static final int ACTION_SAVEASFAVOR = 4;

	String _placeNameString = "";
	int _position;
	private EditText _et_name;
	private Button _btn_ok;
	private Button _btn_saveasfavor;
	private Button _btn_delete;

	private int _actionSum = 0;
	private Bundle _replyBundle = new Bundle();

	private OnClickListener BtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.equals(_btn_ok)) {
				if (!_et_name.getText().toString().trim()
						.equals(_placeNameString)) {
					_actionSum += ACTION_RENAME;
					_placeNameString = _et_name.getText().toString();
					_replyBundle.putString(EXTRA_PLACENAME, _placeNameString);

				}

				_replyBundle.putInt(EXTRA_ACTIONSUM, _actionSum);
				_replyBundle.putInt(EXTRA_POSITION, _position);

				Intent it = new Intent();
				it.putExtras(_replyBundle);
				setResult(RESULT_OK, it);
				finish();
			}

			else if (v.equals(_btn_delete)) {
				_actionSum += ACTION_DELETE;

				// _replyBundle.putSerializable(EXTRA_PLACE, _place); //no
				// needed
				_replyBundle.putInt(EXTRA_ACTIONSUM, _actionSum);
				_replyBundle.putInt(EXTRA_POSITION, _position);

				Intent it = new Intent();
				it.putExtras(_replyBundle);
				setResult(RESULT_OK, it);
				finish();
			}

			else if (v.equals(_btn_saveasfavor)) {

			}

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_place_configure);
		/*
		 * LayoutParams params = getWindow().getAttributes(); params.height =
		 * LayoutParams.MATCH_PARENT; params.width = LayoutParams.MATCH_PARENT;
		 * getWindow().setAttributes((android.view.WindowManager.LayoutParams)
		 * params);
		 */

		try {
			Bundle bundle = getIntent().getExtras();
			_placeNameString = bundle.getString(EXTRA_PLACENAME);
			_position = bundle.getInt(EXTRA_POSITION);
		}catch (Exception e){
			e.printStackTrace();
		}

		initViews();

	}

	@Override
	protected void onDestroy() {
		
		super.onDestroy();
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Utility.checkLanscapeOrientation(this);
		Utility.requestLocationServiceUpdate(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Utility.requestLocationServiceStop(this);
	}

	private void initViews() {
		// TODO Auto-generated method stub
		_et_name = (EditText) findViewById(R.id.name);
		_et_name.setText(_placeNameString);

		_btn_ok = (Button) findViewById(R.id.ok);
		_btn_saveasfavor = (Button) findViewById(R.id.saveasfavor);
		_btn_delete = (Button) findViewById(R.id.delete);

		_btn_ok.setOnClickListener(BtnClickListener);
		_btn_delete.setOnClickListener(BtnClickListener);
		_btn_saveasfavor.setOnClickListener(BtnClickListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_place_editor, menu);
		return true;
	}

}
