package idv.xunqun.navier.v2.dialog;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.http.DirectionRequester;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;

@SuppressLint("ValidFragment")
public class SimpleNaviConfigDialog extends DialogFragment {

	private Context mContext;
	private LayoutInflater mInflater;
	private NaviConfigDialogListener mListener;
	private PreferencesHandler mPref;

	public interface NaviConfigDialogListener{
		void onOk(int mode);
	}
	
	public SimpleNaviConfigDialog(Context context, NaviConfigDialogListener listener) {
		mPref = new PreferencesHandler(context);
		mContext = context;
		mListener = listener;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		View view = mInflater.inflate(R.layout.dialog_navi_config, null);
		final CheckBox avoidTolls = (CheckBox) view.findViewById(R.id.avoid_tolls);
//		final CheckBox avoidHighway = (CheckBox) view.findViewById(R.id.avoid_highway);
		
		//initBeforeSetContentView chechBox
		avoidTolls.setChecked(mPref.getPREF_AVOID_TOLLS());
//		avoidHighway.setChecked(mPref.getPREF_AVOID_HIGHWAY());
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(view);
		builder.setPositiveButton(getString(R.string.naviernaviconfigure_ok), new OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				int value = 0;
				if(avoidTolls.isChecked()){
					value += DirectionRequester.AVOID_TOLLS;
				}
				
//				if(avoidHighway.isChecked()){
//					value += DirectionRequester.AVOID_HIGHWAY;
//				}
				
				mPref.setPREF_AVOID_TOLLS(avoidTolls.isChecked());
//				mPref.setPREF_AVOID_HIGHWAY(avoidHighway.isChecked());
				
				mListener.onOk(value);
			}
		});
		
		return builder.create();
	}
}
