package idv.xunqun.navier.v2.parts;

import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.content.RoutePlan;
import idv.xunqun.navier.v2.parts.GridBoard;
import idv.xunqun.navier.v2.content.DirectionRoute;
import android.hardware.SensorEvent;
import android.location.Location;
import android.os.Bundle;

/*
 *		Parts that need the re-plan inform 
 */

public abstract class NaviParts extends Parts {

	public NaviParts(GridBoard parent, int[] pin) {
		super(parent, pin);
		// TODO Auto-generated constructor stub
	}

	@Deprecated
	public abstract void onRouteReplan(RoutePlan route);	
	public abstract void onDirectionReplan(DirectionRoute directionPlan);
	public abstract void onArrival();
	public abstract void onLeftDistanceNotify(double distance, double alarmDistance);
	public abstract void onLocationOnRoadChange(Latlng p, double d);
	
	@Override
	public void onLocationChange(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationStatusChange(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub

	}
	

	@Override
	public void onLocationProviderDisable(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChange(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setELEM_PIN(int[] pin) {
		// TODO Auto-generated method stub

	}

}
