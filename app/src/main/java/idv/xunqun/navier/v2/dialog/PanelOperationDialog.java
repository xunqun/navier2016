package idv.xunqun.navier.v2.dialog;

import idv.xunqun.navier.R;
import idv.xunqun.navier.v2.content.Panel;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.GpsStatus.Listener;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PanelOperationDialog extends DialogFragment {

	public static final int USE_PANEL = 0;
	public static final int RENAME = 1;
	public static final int EDIT = 2;
	public static final int DELETE = 3;
	public static final int AUTORUN = 4;
	

	public interface PanelOperationListener {
		void onOperationClick(int operation);
	}

	private PanelOperationListener mListener;
	private int mType;

	public void setOnOperationListener(PanelOperationListener listener, int panelType) {
		mListener = listener;
		mType = panelType;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the Builder class for convenient dialog construction
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		View view = inflater.inflate(R.layout.dialog_paneloperation, null);
		
		Button useBtn = (Button) view.findViewById(R.id.use);
		useBtn.setOnClickListener(onClickListener);
		
		Button renameBtn = (Button) view.findViewById(R.id.rename);
		renameBtn.setOnClickListener(onClickListener);
		
		
		Button editBtn = (Button) view.findViewById(R.id.edit);
		editBtn.setOnClickListener(onClickListener);
		
		Button deleteBtn = (Button) view.findViewById(R.id.delete);
		deleteBtn.setOnClickListener(onClickListener);		
		
		Button autorunBtn = (Button) view.findViewById(R.id.autorun);
		autorunBtn.setOnClickListener(onClickListener);		

		builder.setView(view);
		// Create the AlertDialog object and return it
		return builder.create();
	}

	OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.use) {
				mListener.onOperationClick(USE_PANEL);
			}

			else if (v.getId() == R.id.rename) {
				mListener.onOperationClick(RENAME);
			}
			
			else if (v.getId() == R.id.edit) {
				mListener.onOperationClick(EDIT);
			}

			else if (v.getId() == R.id.delete) {
				mListener.onOperationClick(DELETE);
			}
			
			else if (v.getId() == R.id.autorun) {
				mListener.onOperationClick(AUTORUN);
			}
			
			getDialog().dismiss();
		}
	};

}
