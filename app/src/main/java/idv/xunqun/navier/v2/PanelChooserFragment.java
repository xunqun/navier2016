package idv.xunqun.navier.v2;

import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.NavierPanelDesigner;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.Version;
import idv.xunqun.navier.content.LayoutItem;
import idv.xunqun.navier.http.PullPanelTask;
import idv.xunqun.navier.http.PullPanelTask.OnPullPanelListener;
import idv.xunqun.navier.http.PushPanelTask;
import idv.xunqun.navier.http.PushPanelTask.OnPushPanelTaskListener;
import idv.xunqun.navier.utils.Utility;
import idv.xunqun.navier.utils.http.NetworkState;
import idv.xunqun.navier.v2.content.Panel;
import idv.xunqun.navier.v2.content.PanelStore;
import idv.xunqun.navier.v2.dialog.LisenceNotAllowDialog;
import idv.xunqun.navier.v2.dialog.PanelChooserDialog_Support;
import idv.xunqun.navier.v2.dialog.PanelChooserDialog_Support.OnPanelChooserListener;
import idv.xunqun.navier.v2.dialog.PanelNameSettingDialog;
import idv.xunqun.navier.v2.dialog.PanelNameSettingDialog.OnPanelNameSettingListener;
import idv.xunqun.navier.v2.dialog.PanelOperationDialog;
import idv.xunqun.navier.v2.dialog.PanelOperationDialog.PanelOperationListener;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONException;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.model.LatLng;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PanelChooserFragment extends Fragment implements HomeFragment {

	public static final String ARG_SECTION_NUMBER = "section_number";

	private ImageButton mAddBtn;

	RadioButton radioButton0;
	RadioButton radioButton1;
	RadioButton radioButton2;

	private OnNavierMenuHandler mHandler;

	private LinearLayout mPanelsWrapper;

	private PreferencesHandler mPref;

	private LinearLayout mLoading;

	private ArrayList<LayoutItem> mAvailableLayouts;

	private LayoutInflater inflater;
	private boolean initialing = false;
	private RelativeLayout mAutoPanel;
	private TextView mAutoPanelTextView;
	private boolean mFirstRun = true;
	private AdView mAdView;

	public interface OnNavierMenuHandler {
		public void onAddClick();
	}

	public void setHandler(OnNavierMenuHandler handler) {
		mHandler = handler;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_panelchooser, null);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
		inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mPref = new PreferencesHandler(getActivity());

		initViews();
		addAdView();

		// doSyncLayout();
	}

	private void addAdView() {
		Calendar calendar = Calendar.getInstance();
		if (Version.isLite(getActivity())) {
			getView().findViewById(R.id.adwrapper).setVisibility(View.VISIBLE);

			// Create the adView
			mAdView = (AdView) getView().findViewById(R.id.ad);

			// Initiate a generic request to load it with an ad
			AdRequest adRequest = new AdRequest.Builder().build();
			mAdView.loadAd(adRequest);

			// more app
			getView().findViewById(R.id.moreApp).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri
							.parse(getString(R.string.more_apps_link)));
					startActivity(myIntent);

				}
			});
		} else {
			getView().findViewById(R.id.adwrapper).setVisibility(View.GONE);
		}
	}

	private void destroyAdView() {
		if (mAdView != null) {
			mAdView.destroy();
		}

	}

	@Override
	public void onDestroy() {

		destroyAdView();
		super.onDestroy();
	}

	private void goToAutoPanel() throws JSONException {

		if (!mPref.getPREF_AUTO_RUN_PANEL().equalsIgnoreCase("none")) {

			for (final Panel p : PanelStore.getPanelStore(getActivity())) {
				if (p.LAYOUT_FINGERPRINT.equals(mPref.getPREF_AUTO_RUN_PANEL())) {
					LayoutItem item = new LayoutItem();

					item.LAYOUT_FINGERPRINT = p.LAYOUT_FINGERPRINT;
					item.LAYOUT_ID = p.LAYOUT_ID;
					item.LAYOUT_JSONSTR = p.LAYOUT_JSONSTR;
					item.LAYOUT_NAME = Utility.getLayoutName(item.LAYOUT_JSONSTR);
					item.LAYOUT_TYPE = Utility.getLayoutType(item.LAYOUT_JSONSTR);
					item._isEditable = true;
					item._isStored = true;
					usePanel(item, null);
					break;
				}
			}
		}
	}

	@Override
	public void onResume() {

		super.onResume();

		initPanels(PanelStore.FILTER_ALL);

		// Go to auto run panel
		if (mFirstRun == true) {
			try {
				mFirstRun = false;
				goToAutoPanel();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		resetRadioButtonState();
	}

	public void doSyncLayout() {

		// Get Account
		String account = mPref.getPREF_ACCOUNT();

		if (account.equals(getString(R.string.PREF_ACCOUNT_DEFAULT))) {

			initPanels(PanelStore.FILTER_ALL);
		} else {
			if (NetworkState.checkNetworkConnected(getActivity())) {

				// Check is the fragment attached on activity before Http task
				if (isAdded()
						&& mPref.getPREF_SYNC()
						&& !mPref.getPREF_ACCOUNT()
								.equals(getString(R.string.PREF_ACCOUNT_DEFAULT))) {

					// do push sync data befor pull
					new PushPanelTask(getActivity(), new OnPushPanelTaskListener() {

						@Override
						public void onSyncPanelComplete(boolean ok) {
						}

						@Override
						public void onPreExecute() {

							mLoading.setVisibility(View.VISIBLE);
						}

						@Override
						public void onPostExecute() {

							mLoading.setVisibility(View.GONE);

						}
					}, mPref.getPREF_ACCOUNT()).execute();

					PullPanelTask task = new PullPanelTask(getActivity(), account);
					task.setOnPullPanelListener(new OnPullPanelListener() {

						@Override
						public void onPostExecute(int result) {

							PanelStore.notifyLayoutSetChange(getActivity());
							initPanels(PanelStore.FILTER_ALL);
							mLoading.setVisibility(View.GONE);
						}

						@Override
						public void onPreExecute() {

							mLoading.setVisibility(View.VISIBLE);
						}
					});
					task.execute();

				}
			} else {
				initPanels(PanelStore.FILTER_ALL);
				Toast.makeText(getActivity(), getString(R.string.naviermap_connection),
						Toast.LENGTH_LONG).show();
			}

		}
	}

	private void initPanels(int require) {

		if (!isAdded() || initialing) {

			return;
		}

		initialing = true;

		mPanelsWrapper.removeAllViews();

		PanelStore.notifyLayoutSetChange(getActivity());

		View view;

		for (final Panel p : PanelStore.getPanelStore(getActivity())) {

			if (require == PanelStore.FILTER_NAVIGATION && p.getType() == Panel.TYPE_NORMAL) {
				continue;
			} else if (require == PanelStore.FILTER_NORMAL && p.getType() == Panel.TYPE_NAVI) {
				continue;
			}

			if (p.getType() == Panel.TYPE_NAVI) {
				view = inflater.inflate(R.layout.panellist_item_nav, null);
			} else {
				view = inflater.inflate(R.layout.panellist_item_nor, null);
			}

			final LayoutItem item = new LayoutItem();
			item.LAYOUT_FINGERPRINT = p.LAYOUT_FINGERPRINT;
			item.LAYOUT_ID = p.LAYOUT_ID;
			item.LAYOUT_JSONSTR = p.LAYOUT_JSONSTR;
			item.LAYOUT_NAME = p.LAYOUT_NAME;
			item.LAYOUT_TYPE = p.LAYOUT_TYPE;
			item._isEditable = true;
			item._isStored = true;

			TextView name = (TextView) view.findViewById(R.id.name);
			final TextView place = (TextView) view.findViewById(R.id.place);
			ImageButton config = (ImageButton) view.findViewById(R.id.config);
			config.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					PanelOperationDialog dialogOperation = new PanelOperationDialog();
					dialogOperation.setOnOperationListener(new PanelOperationListener() {

						@Override
						public void onOperationClick(int operation) {

							Intent it;

							switch (operation) {
							case PanelOperationDialog.USE_PANEL:
								usePanel(item, null);
								break;

							case PanelOperationDialog.RENAME:
								PanelNameSettingDialog dialog = new PanelNameSettingDialog();
								dialog.setPanel(p);
								dialog.setOnPanelNameSettingListener(new OnPanelNameSettingListener() {

									@Override
									public void onNameComplete() {

										initPanels(PanelStore.FILTER_ALL);
									}
								});
								dialog.show(getFragmentManager(), "");

								break;
							case PanelOperationDialog.EDIT:

								editPanel(item);
								break;
							case PanelOperationDialog.DELETE:
								PanelStore.deletePanel(getActivity(), p);
								initPanels(PanelStore.FILTER_ALL);

								if (!mPref.getPREF_ACCOUNT().equals(
										getString(R.string.PREF_ACCOUNT_DEFAULT))
										&& mPref.getPREF_SYNC()) {
									mLoading.setVisibility(View.VISIBLE);
									new PushPanelTask(getActivity(), mSyncPanelHandler, mPref
											.getPREF_ACCOUNT()).execute();
								}
								break;

							case PanelOperationDialog.AUTORUN:
								setAsAutoRun(p);
								break;

							default:
								break;
							}
						}

					}, p.LAYOUT_TYPE);

					dialogOperation.show(getFragmentManager(), "");
				}
			});

			name.setText(p.LAYOUT_NAME);

			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(getResources()
					.getDimensionPixelSize(R.dimen.panel_width), getResources()
					.getDimensionPixelSize(R.dimen.panel_height));
			layoutParams.setMargins(24, 0, 24, 0);

			view.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					usePanel(item, null);
				}
			});

			view.setOnLongClickListener(new OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {

					PanelOperationDialog dialogOperation = new PanelOperationDialog();
					dialogOperation.setOnOperationListener(new PanelOperationListener() {

						@Override
						public void onOperationClick(int operation) {

							Intent it;

							switch (operation) {
							case PanelOperationDialog.USE_PANEL:
								usePanel(item, null);
								break;

							case PanelOperationDialog.RENAME:
								PanelNameSettingDialog dialog = new PanelNameSettingDialog();
								dialog.setPanel(p);
								dialog.setOnPanelNameSettingListener(new OnPanelNameSettingListener() {

									@Override
									public void onNameComplete() {

										initPanels(PanelStore.FILTER_ALL);
									}
								});
								dialog.show(getFragmentManager(), "");

								break;
							case PanelOperationDialog.EDIT:
								editPanel(item);
								break;
							case PanelOperationDialog.DELETE:
								PanelStore.deletePanel(getActivity(), p);
								initPanels(PanelStore.FILTER_ALL);

								if (!mPref.getPREF_ACCOUNT().equals(
										getString(R.string.PREF_ACCOUNT_DEFAULT))
										&& mPref.getPREF_SYNC()) {
									mLoading.setVisibility(View.VISIBLE);
									new PushPanelTask(getActivity(), mSyncPanelHandler, mPref
											.getPREF_ACCOUNT()).execute();
								}
								break;

							case PanelOperationDialog.AUTORUN:
								setAsAutoRun(p);
								break;

							default:
								break;
							}
						}

					}, p.LAYOUT_TYPE);

					dialogOperation.show(getFragmentManager(), "");

					return true;
				}
			});

			mPanelsWrapper.addView(view, layoutParams);
		}
		mPanelsWrapper.invalidate();
		initialing = false;
	}

	private void setAsAutoRun(Panel p) {

		mPref.setPREF_AUTO_RUN_PANEL(p.LAYOUT_FINGERPRINT);
		mAutoPanel.setVisibility(View.VISIBLE);
		mAutoPanelTextView.setText(p.LAYOUT_NAME);
	}

	private void usePanel(LayoutItem item, LatLng defaultPlace) {
		Intent it;

		if (mPref.getPREF_LISENCEDUSER()) {
			if (item.LAYOUT_TYPE == Panel.TYPE_NAVI) {
				if (Version.isLite(getActivity())) {

//					it = new Intent(getActivity(), PlanNutiteqActivity.class);
					it = new Intent(getActivity(), PlanActivity.class);
				} else {
					it = new Intent(getActivity(), PlanActivity.class);
				}
				it.putExtra(PlanActivity.EXTRA_LAYOUT, item);

				if (defaultPlace != null) {
					it.putExtra(PlanActivity.EXTRA_DESTINATION, defaultPlace);
				}
				startActivity(it);
			} else {
				it = new Intent(getActivity(), RunTimeActivity.class);
				it.putExtra(RuntimePanelFragment.EXTRA_LAYOUT, item);
				startActivity(it);
			}
		} else {
			new LisenceNotAllowDialog().show(getActivity().getSupportFragmentManager(), "");
		}

	}

	private void editPanel(LayoutItem layout) {

		Intent it = new Intent(getActivity(), NavierPanelDesigner.class);
		it.putExtra(NavierPanelDesigner.EXTRA_INT_ACTION, NavierPanelDesigner.EXTRA_ACTION_EDIT);
		it.putExtra(NavierPanelDesigner.EXTRA_LAYOUTITEM, layout);
		startActivity(it);
	}

	private void initViews() {

		// initial action bar
		ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.filled_box));

		mPanelsWrapper = (LinearLayout) getActivity().findViewById(R.id.panels_wrapper);
		mLoading = (LinearLayout) getActivity().findViewById(R.id.loading_panel);

		radioButton0 = (RadioButton) getActivity().findViewById(R.id.radio0);
		radioButton1 = (RadioButton) getActivity().findViewById(R.id.radio1);
		radioButton2 = (RadioButton) getActivity().findViewById(R.id.radio2);

		radioButton0.setOnCheckedChangeListener(onCheckedChangeListener);
		radioButton1.setOnCheckedChangeListener(onCheckedChangeListener);
		radioButton2.setOnCheckedChangeListener(onCheckedChangeListener);

		ImageButton addButton = (ImageButton) getActivity().findViewById(R.id.imageButton1);
		addButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent it = new Intent(getActivity(), NavierPanelDesigner.class);
				it.putExtra(NavierPanelDesigner.EXTRA_INT_ACTION,
						NavierPanelDesigner.EXTRA_ACTION_NEW);

				startActivity(it);
			}
		});

		mAutoPanelTextView = (TextView) getActivity().findViewById(R.id.autorunpanel);

		mAutoPanel = (RelativeLayout) getActivity().findViewById(R.id.autopanelwraper);
		mAutoPanel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				PanelChooserDialog_Support dialog = new PanelChooserDialog_Support();
				dialog.setOnPanelChooserListener(new OnPanelChooserListener() {

					@Override
					public void onPanelChoosed(LayoutItem panelLayout) {

						mAutoPanelTextView.setText(panelLayout.LAYOUT_NAME);
						mAutoPanel.setVisibility(View.VISIBLE);
						mPref.setPREF_AUTO_RUN_PANEL(panelLayout.LAYOUT_FINGERPRINT);
					}

					@Override
					public void onSetToNoAuto() {

						mAutoPanel.setVisibility(View.GONE);
						mPref.setPREF_AUTO_RUN_PANEL("none");
					}
				});
				dialog.setFilter(PanelStore.FILTER_ALL);

				dialog.show(getFragmentManager(), "");
			}
		});

		if (mPref.getPREF_AUTO_RUN_PANEL().equals("none")) {
			mAutoPanel.setVisibility(View.GONE);
		} else {
			mAutoPanel.setVisibility(View.VISIBLE);

			String name;
			try {
				name = getPanelName(mPref.getPREF_AUTO_RUN_PANEL());
				if (name == null) {
					mAutoPanel.setVisibility(View.GONE);
				} else {
					mAutoPanel.setVisibility(View.VISIBLE);
					mAutoPanelTextView.setText(name);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				mAutoPanel.setVisibility(View.GONE);
				e.printStackTrace();
			}

		}

	}

	private String getPanelName(String pref_AUTO_RUN_PANEL) throws JSONException {

		SQLiteDatabase db = MyDBOpenHelper.getDb(getActivity());
		Cursor c = db.query(MyDBOpenHelper.LAYOUT_TABLE_NAME,
				new String[] { MyDBOpenHelper.LAYOUT_COL_LAYOUT },
				MyDBOpenHelper.LAYOUT_COL_FINGERPRINT + "=?", new String[] { pref_AUTO_RUN_PANEL },
				"", "", "");

		if (c.getCount() > 0) {
			c.moveToNext();

			return Utility.getLayoutName(c.getString(c
					.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_LAYOUT)));
		} else {
			return null;
		}

	}

	private void resetRadioButtonState() {
		radioButton0.setChecked(true);
	}

	OnPushPanelTaskListener mSyncPanelHandler = new OnPushPanelTaskListener() {

		@Override
		public void onSyncPanelComplete(boolean ok) {

			if (!ok) {
				try {
					Toast.makeText(getActivity(), getString(R.string.naviermap_connection),
							Toast.LENGTH_SHORT);
				} catch (Exception e) {

				}
			} else {
				initPanels(PanelStore.FILTER_ALL);
			}

			mLoading.setVisibility(View.GONE);
		}

		@Override
		public void onPreExecute() {

		}

		@Override
		public void onPostExecute() {

		}
	};

	OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

			if (isChecked && buttonView.equals(radioButton0)) {
				initPanels(PanelStore.FILTER_ALL);
			}

			else if (isChecked && buttonView.equals(radioButton1)) {
				initPanels(PanelStore.FILTER_NORMAL);
			}

			else if (isChecked && buttonView.equals(radioButton2)) {
				initPanels(PanelStore.FILTER_NAVIGATION);
			}
		}
	};

	@Override
	public void onRefreschClicked() {
		doSyncLayout();

	}
}
