package idv.xunqun.navier.v2.content;

import idv.xunqun.navier.v2.parts.Parts;

import java.util.ArrayList;

public class Layout {
	// JSON tree
			public static final String JSON_ROOT = "layout";
			public static final String JSON_NAME = "name";
			public static final String JSON_MODE = "mode";
			public static final String JSON_TYPE = "type";
			public static final String JSON_PARTS = "parts";

			public static final String JSON_PARTS_PIN = "pin";
			public static final String JSON_PARTS_PART = "part";

			// Properties options
			public static final int MODE_NORMAL = 1;
			public static final int MODE_HUD = 2;

			public static final int TYPE_DASHBOARD = 1;
			public static final int TYPE_NAVIGATION = 2;

			// Layout properties
			public int _mode = MODE_NORMAL;
			public String _name = "Default Layout";
			public int _type = TYPE_NAVIGATION;

			public ArrayList<Parts> _parts_list = new ArrayList();
}
