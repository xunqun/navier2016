package idv.xunqun.navier.v2;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.speech.RecognizerIntent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.maps.model.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import idv.xunqun.navier.LocationService;
import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.NavigationManager;
import idv.xunqun.navier.PreferencesHandler;
import idv.xunqun.navier.R;
import idv.xunqun.navier.Version;
import idv.xunqun.navier.content.Latlng;
import idv.xunqun.navier.content.LayoutItem;
import idv.xunqun.navier.http.HereMapDirectionTask;
import idv.xunqun.navier.http.HereMapDirectionTask.HereMapDirectionTaskListener;
import idv.xunqun.navier.http.PlaceSearchTask;
import idv.xunqun.navier.http.PlaceSearchTask.OnPlaceSearchListener;
import idv.xunqun.navier.http.SyncPlaceTask;
import idv.xunqun.navier.http.SyncPlaceTask.SyncPlaceHandler;
import idv.xunqun.navier.utils.Utility;
import idv.xunqun.navier.v2.content.DirectionRoute;
import idv.xunqun.navier.v2.content.PanelStore;
import idv.xunqun.navier.v2.content.Place;
import idv.xunqun.navier.v2.dialog.IntroListDialog;
import idv.xunqun.navier.v2.dialog.PanelChooserDialog;
import idv.xunqun.navier.v2.dialog.PanelChooserDialog.OnPanelChooserListener;
import idv.xunqun.navier.v2.dialog.PlaceChooserDialog;
import idv.xunqun.navier.v2.dialog.PlaceChooserDialog.OnPlaceChoosedListener;
import idv.xunqun.navier.v2.dialog.SimpleNaviConfigDialog;
import idv.xunqun.navier.v2.dialog.SimpleNaviConfigDialog.NaviConfigDialogListener;
import idv.xunqun.navier.v2.maps.MapSdk;
import idv.xunqun.navier.v2.maps.MapboxMapSdk;


public class PlanActivity extends AppCompatActivity{

	public static final String TAG = "PlanActivity";

	public static final String EXTRA_LAYOUT = "layout";
	public static final String EXTRA_DESTINATION = "destination";

	private final int _AnimationTime = 500;
	private final int VOICE_RECOGNITION_REQUEST_CODE = 0;
	private final int ZOOM_STREET_LEVEL = 14;

	@BindView(R.id.myloc)
	protected ImageButton vMyLoc;

	@BindView(R.id.search_text)
	protected EditText vSearchStr;

	@BindView(R.id.search_btn)
	protected ImageButton vSearchBtn;

	@BindView(R.id.search_progress)
	protected ProgressBar vSearchProgress;

	@BindView(R.id.mic)
	protected ImageButton vMicBtn;

	@BindView(R.id.places)
	protected ImageButton vPlaceBtn;

	@BindView(R.id.pager)
	protected ViewPager vPager;

	@BindView(R.id.ad)
	protected AdView mAdView;

	@BindView(R.id.plan_frame)
	protected LinearLayout vPlanFrame;

	@BindView(R.id.from)
	protected TextView vFrom;

	@BindView(R.id.to)
	protected TextView vTo;

	@BindView(R.id.from_v)
	protected TextView vFromV;

	@BindView(R.id.to_v)
	protected TextView vToV;

	@BindView(R.id.routeList)
	protected ListView vRouteList;

	@BindView(R.id.help)
	protected ImageButton vHelpBtn;

	@BindView(R.id.view_help)
	protected ScrollView vHelpFrame;

	@BindView(R.id.mode1)
	protected ImageButton vMode1;

	@BindView(R.id.mode2)
	protected ImageButton vMode2;

	@BindView(R.id.mode3)
	protected ImageButton vMode3;

	@BindView(R.id.show_route)
	protected ImageButton vShowRoute;

	@BindView(R.id.mapview)
	protected MapView vMapView;

	protected ArrayList<DirectionRoute> mRouteList = new ArrayList<DirectionRoute>();
	private RouteListAdapter mRouteListAdapter;
	public int mSelectedRoute;
	protected Place mChoosedPlace;
	private boolean mIsMapSetup = false;


	private SlidePagerAdapter mPagerAdapter;
	private LocationListener locationListener;
	protected Location mCurrentLocation;
	protected List<Place> mSearchResults = new ArrayList<Place>();
	private int mWidth;
	private int mHeight;
	private PreferencesHandler mPref;
	private Place mSelectedDest;
	// map sdk
	MapSdk mapSdk;
	private boolean isNew = true;
	private InterstitialAd interstitial;

	// find location
	private final int MaxFilterCount = 6;
	private Location mPrelocation;
	private int mFilterCount = 0;

	private BroadcastReceiver _MessageReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.getAction().equals(LocationService.EXTRA_ONLOCATIONCHANGE)) {

				Location location = intent.getParcelableExtra("location");
				mCurrentLocation = findBeterLocation(location);
				Log.d(TAG, location.toString());
				if (mapSdk.isInitialized() && !mIsMapSetup) {
					mapSdk.setZoom(13);
					mapSdk.moveToLocation(new LatLng(mCurrentLocation.getLatitude(),
							mCurrentLocation.getLongitude()));
					mIsMapSetup = true;
				}
				mapSdk.setMyLocation(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation
						.getLongitude()));

			}

			else if (intent.getAction().equals(LocationService.EXTRA_ONPROVIDERDISABLEED)) {

			}

			else if (intent.getAction().equals(LocationService.EXTRA_ONPROVIDERENABLED)) {

			}

			else if (intent.getAction().equals(LocationService.EXTRA_ONSTATUSCHANGED)) {

			}

		}

	};

	private Location findBeterLocation(Location location) {
		if (mPrelocation != null && location.distanceTo(mPrelocation) < location.getAccuracy()
				&& mFilterCount < MaxFilterCount) {
			mFilterCount++;
			return mCurrentLocation;
		} else {
			mFilterCount = 0;
			mPrelocation = location;
		}

		if (NavigationManager.isBetterLocation(location, mCurrentLocation)) {
			return location;
		} else {
			return mCurrentLocation;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mapSdk = new MapboxMapSdk();
		mapSdk.initBeforeSetContentView(this);
		setContentView(R.layout.activity_plan);
		ButterKnife.bind(this);
		((MapboxMapSdk)mapSdk).onViewCreate(this, vMapView, savedInstanceState);

		mPref = new PreferencesHandler(this);
		bindMap();
		bindView();
		addAdView();
		try {
			if (getIntent().getExtras().containsKey(EXTRA_DESTINATION)) {
				mIsMapSetup = true; // to avoid map animate to my location
				Latlng latlng = (Latlng) getIntent().getExtras().getSerializable(EXTRA_DESTINATION);

				Place place = new Place();
				place.name = latlng.getName();
				place.latitude = latlng.getLat();
				place.longitude = latlng.getLng();
				place.address = "";
				if (mSearchResults != null) {
					mSearchResults.clear();
				} else {
					mSearchResults = new ArrayList<Place>();
				}
				mSearchResults.add(place);
				mPagerAdapter.notifyDataSetChanged();
				showResultToMap();
				showPager();
				vPager.setCurrentItem(0);
				mapSdk.animateToLocation(new LatLng(place.latitude, place.longitude));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Initializes the SKMaps framework
	 */
	private void initializeLibrary() {

	}


	@Override
	protected void onDestroy() {
		mapSdk.destory();
		if (mCurrentLocation != null) {
			mPref.setPREF_LATEST_LAT((float) mCurrentLocation.getLatitude());
			mPref.setPREF_LATEST_LNG((float) mCurrentLocation.getLongitude());
		}
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mapSdk.onResume();

		registerServiceEvent();
		Utility.requestLocationServiceUpdate(this);

		// Used to specify is Activity is returned
		if (Version.isLite(this)) {
			if (!isNew) {
				displayInterstitial();
			} else {
				isNew = false;
			}
		}

	}

	private void registerServiceEvent() {
		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver,
				new IntentFilter(LocationService.EXTRA_ONLOCATIONCHANGE));
		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver,
				new IntentFilter(LocationService.EXTRA_ONPROVIDERDISABLEED));
		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver,
				new IntentFilter(LocationService.EXTRA_ONPROVIDERENABLED));
		LocalBroadcastManager.getInstance(this).registerReceiver(_MessageReceiver,
				new IntentFilter(LocationService.EXTRA_ONSTATUSCHANGED));

	}

	@Override
	protected void onPause() {
		super.onPause();
		Utility.requestLocationServiceStop(this);
		unregisterServiceEvent();
		MyDBOpenHelper.closeDb();
		mapSdk.onPause();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mapSdk.onSaveInstanceState(outState);
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mapSdk.onLowMemory();
	}

	private void unregisterServiceEvent() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(_MessageReceiver);

	}

	private void bindView() {

		hidePlanFrame();
		mRouteListAdapter = new RouteListAdapter();
		vRouteList.setAdapter(mRouteListAdapter);

		((ImageButton) findViewById(R.id.config)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SimpleNaviConfigDialog configDialog = new SimpleNaviConfigDialog(
						PlanActivity.this, new NaviConfigDialogListener() {
							@Override
							public void onOk(int mode) {
								showPlanFrame(mChoosedPlace);
							}
						});
				configDialog.show(getSupportFragmentManager(), "");
			}
		});

		initMode();


		vShowRoute.setVisibility(View.GONE);
		vShowRoute.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showPlanFrame();

			}
		});

		vMyLoc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (mCurrentLocation != null) {
					mapSdk.animateToLocation(new LatLng(mCurrentLocation.getLatitude(),
							mCurrentLocation.getLongitude()));

				}

			}
		});

		vSearchStr.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_GO
						|| actionId == EditorInfo.IME_ACTION_NEXT
						|| actionId == EditorInfo.IME_ACTION_SEND
						|| actionId == EditorInfo.IME_ACTION_SEARCH
						|| actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
					doSearch();

				}
				return false;
			}
		});

		vSearchBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				doSearch();
			}
		});
		vSearchProgress.setVisibility(View.GONE);
		vMicBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startVoiceRecognition();

			}
		});
		vPlaceBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				PlaceChooserDialog dialog = new PlaceChooserDialog();
				dialog.setOnPlaceChoosedListener(new OnPlaceChoosedListener() {

					@Override
					public void OnClick(String name, double lat, double lng, String addr) {
						Place place = new Place();
						place.name = name;
						place.latitude = lat;
						place.longitude = lng;
						place.address = addr;
						if (mSearchResults != null) {
							mSearchResults.clear();
						} else {
							mSearchResults = new ArrayList<Place>();
						}
						mSearchResults.add(place);
						mPagerAdapter.notifyDataSetChanged();
						showResultToMap();
						showPager();
						vPager.setCurrentItem(0);
						mapSdk.animateToLocation(new LatLng(lat, lng));
					}
				});
				dialog.show(getSupportFragmentManager(), "");

			}
		});
		mPagerAdapter = new SlidePagerAdapter();
		vPager.setAdapter(mPagerAdapter);
		vPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int index) {
				moveToMarker(index);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});
		hidePager();

		vHelpBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (vHelpFrame.getVisibility() == View.GONE) {
					vHelpFrame.setVisibility(View.VISIBLE);

				} else {
					vHelpFrame.setVisibility(View.GONE);

				}
			}
		});
	}

	private void initMode() {
		// fix bugs

		if (!mPref.getPREF_TRANS_TYPE().equals(PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_FASTEST)
				&& !mPref.getPREF_TRANS_TYPE().equals(
						PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_BICYCLE)
				&& !mPref.getPREF_TRANS_TYPE().equals(
						PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_PEDESTRIAN)) {
			mPref.setPREF_TRANS_TYPE(PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_FASTEST);
		}

		vMode1.setBackgroundColor(Color.WHITE);
		vMode2.setBackgroundColor(Color.WHITE);
		vMode3.setBackgroundColor(Color.WHITE);
		if (mPref.getPREF_TRANS_TYPE().equals(PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_FASTEST)) {
			vMode1.setBackgroundColor(0xffe0e0e0);
		} else if (mPref.getPREF_TRANS_TYPE().equals(
				PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_BICYCLE)) {
			vMode2.setBackgroundColor(0xffe0e0e0);
		} else {
			vMode3.setBackgroundColor(0xffe0e0e0);
		}

		OnClickListener listener = new OnClickListener() {

			@Override
			public void onClick(View view) {
				if (view.equals(vMode1)
						&& !mPref.getPREF_TRANS_TYPE().equals(
						PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_FASTEST)) {

					mPref.setPREF_TRANS_TYPE(PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_FASTEST);
					showPlanFrame(mChoosedPlace);
				} else if (view.equals(vMode2)
						&& !mPref.getPREF_TRANS_TYPE().equals(
						PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_BICYCLE)) {

					mPref.setPREF_TRANS_TYPE(PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_BICYCLE);
					showPlanFrame(mChoosedPlace);
				} else if (view.equals(vMode3)
						&& !mPref.getPREF_TRANS_TYPE().equals(
						PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_PEDESTRIAN)) {

					mPref.setPREF_TRANS_TYPE(PreferencesHandler.VALUE_OF_PREF_ROUTE_TYPE_PEDESTRIAN);
					showPlanFrame(mChoosedPlace);
				}

				initMode();

			}

		};

		vMode1.setOnClickListener(listener);
		vMode2.setOnClickListener(listener);
		vMode3.setOnClickListener(listener);
	}

	private void bindMap() {
		MapboxMapSdk.MapboxMapSdkListener listener = new MapboxMapSdk.MapboxMapSdkListener() {

			@Override
			public void onLongPress(LatLng latLng) {
				Place place = new Place();
				place.latitude = latLng.latitude;
				place.longitude = latLng.longitude;
				place.name = getString(R.string.naviermap_placename);
				place.address = "";
				if (mSearchResults != null) {
					mSearchResults.clear();
				} else {
					mSearchResults = new ArrayList<Place>();
				}
				mSearchResults.add(place);
				mPagerAdapter.notifyDataSetChanged();
				showResultToMap();
				showPager();
				vPager.setCurrentItem(0);
			}

			@Override
			public void onAnnotationSelected(int index) {

				vPager.setCurrentItem(index);
			}

			@Override
			public void onInitComplete() {
				if (!mIsMapSetup) {
					mapSdk.setZoom(13);
					mapSdk.moveToLocation(new LatLng(mCurrentLocation.getLatitude(),
							mCurrentLocation.getLongitude()));
					mIsMapSetup = true;
				}
			}
		};
		((MapboxMapSdk) mapSdk).setListener(listener);

	}

	private void startVoiceRecognition() {
		try {
			Intent it = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
			it.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass().getPackage().getName());
			it.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.naviervrecognition_vc));
			it.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
					RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			it.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
			startActivityForResult(it, VOICE_RECOGNITION_REQUEST_CODE);
		} catch (Exception e) {

		}
	}

	public void addMarker(LatLng location, String label, Place userdata) {
		mapSdk.addMarker(location, label, userdata.description);
	}

	protected void doSearch() {

		final String name = vSearchStr.getText().toString();
		if (!TextUtils.isEmpty(name)) {

			if (mCurrentLocation == null) {
				Toast.makeText(this, getString(R.string.naviersearchresult_positionfail),
						Toast.LENGTH_LONG).show();

			} else {
				// LatLng mapLocation = mapSdk.getScreenLocation(mWidth / 2,
				// mHeight
				// / 2);
				vSearchProgress.setVisibility(View.VISIBLE);
				vSearchBtn.setVisibility(View.GONE);
				PlaceSearchTask task = new PlaceSearchTask(this, name, new LatLng(
						mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()),
						new OnPlaceSearchListener() {

							@Override
							public void onPostExecute(List result) {

								if (result != null) {

									mSearchResults = result;
									mPagerAdapter.notifyDataSetChanged();

									if (result.size() > 0) {
										vSearchStr.setText("");

										showPager();

										vPager.setCurrentItem(0, true);
										showResultToMap();
										moveToMarker(0);
									} else {
										Toast.makeText(
												PlanActivity.this,
												getString(R.string.naviermap_nplacesfound).replace(
														"{N}", "0"), Toast.LENGTH_LONG).show();
									}
									vSearchProgress.setVisibility(View.GONE);
									vSearchBtn.setVisibility(View.VISIBLE);
								} else {

									try {
										mSearchResults.clear();
										mPagerAdapter.notifyDataSetChanged();
										Toast.makeText(PlanActivity.this,
												getString(R.string.naviersearchresult_searchfail),
												Toast.LENGTH_LONG).show();
									} catch (Exception exception) {

									} finally {
										vSearchProgress.setVisibility(View.GONE);
										vSearchBtn.setVisibility(View.GONE);
									}
								}

							}
						});
				task.execute();
			}
		}
	}

	protected void showResultToMap() {
		mapSdk.cleanMarkers();
		mapSdk.cleanPolyline();

		for (Place place : mSearchResults) {
			mapSdk.addMarker(new LatLng(place.latitude, place.longitude), place.name, place.address);
		}

	}

	protected void moveToMarker(double lat, double lng) {
		mapSdk.animateToLocation(new LatLng(lat, lng));
		mapSdk.setZoom(ZOOM_STREET_LEVEL);
	}

	protected void moveToMarker(int index) {

		if (mSearchResults.size() > index) {
			mapSdk.animateToLocation(new LatLng(mSearchResults.get(index).latitude, mSearchResults
					.get(index).longitude));
			mapSdk.setZoom(ZOOM_STREET_LEVEL);
		}

	}

	public void getScreenDimension() {
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		mWidth = size.x;
		mHeight = size.y;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
			// Fill the list view with the strings the recognizer thought it
			// could have heard
			ArrayList<String> matches = data
					.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

			if (matches != null && matches.size() > 0) {
				vSearchStr.setText(matches.get(0));
				doSearch();
			} else {
				Toast.makeText(this, getString(R.string.naviermap_destnotset), Toast.LENGTH_SHORT)
						.show();
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private void hidePager() {
		showAdView();
		mapSdk.cleanMarkers();
		mSearchResults.clear();
		vPager.setVisibility(View.GONE);
	}

	private void showPager() {
		hideAdView();
		vPager.setVisibility(View.VISIBLE);
	}

	private void showAdView() {
		if (mAdView != null & Version.isLite(this)) {
			findViewById(R.id.adwrapper).setVisibility(View.VISIBLE);
		}
	}

	private void hideAdView() {
		if (mAdView != null) {
			findViewById(R.id.adwrapper).setVisibility(View.GONE);
		}
	}

	private void addAdView() {

		if (Version.isLite(this)) {
			// Create the adView
			findViewById(R.id.adwrapper).setVisibility(View.VISIBLE);
			mAdView = (AdView) findViewById(R.id.ad);

			// Initiate a generic request to load it with an ad

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					AdRequest adRequest = new AdRequest.Builder().build();
					mAdView.loadAd(adRequest);

				}
			}, 5000);

			// more app
			findViewById(R.id.moreApp).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri
							.parse(getString(R.string.more_apps_link)));
					startActivity(myIntent);

				}
			});

			// fullscreen ads
			// 建立插頁式廣告。
			interstitial = new InterstitialAd(this);
			interstitial.setAdUnitId("ca-app-pub-2264979852880694/6279699220");

			// 建立廣告請求。
			AdRequest adRequest = new AdRequest.Builder().build();

			// 開始載入您的插頁式廣告。
			interstitial.loadAd(adRequest);

		} else {
			findViewById(R.id.adwrapper).setVisibility(View.GONE);
		}

	}

	// 當您準備好顯示插頁式廣告時，叫用 displayInterstitial()。
	public void displayInterstitial() {
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}



	@Override
	protected void onStop() {
		super.onStop();
		mapSdk.destory();
	}


	private void hidePlanFrame() {

		vPlanFrame.setVisibility(View.INVISIBLE);
	}

	private void showPlanFrame() {
		vPlanFrame.setVisibility(View.VISIBLE);
		Animation anim = AnimationUtils.loadAnimation(this, R.anim.push_up_in);
		vPlanFrame.startAnimation(anim);
	}

	protected void showPlanFrame(Place dest) {
		if (vPlanFrame.getVisibility() != View.VISIBLE) {
			Animation anim = AnimationUtils.loadAnimation(this, R.anim.push_up_in);
			vPlanFrame.startAnimation(anim);
			vPlanFrame.setVisibility(View.VISIBLE);
		}

		if (mRouteList != null) {
			mRouteList.clear();
			mRouteListAdapter.notifyDataSetChanged();
		}

		((ProgressBar) findViewById(R.id.routeProgressBar)).setVisibility(View.VISIBLE);
		mSelectedDest = dest;

		vTo.setText(dest.name);
		vToV.setText(dest.address);

		Location myLocation = mCurrentLocation;
		if (myLocation != null) {

			HereMapDirectionTask task = new HereMapDirectionTask(this,
					new HereMapDirectionTaskListener() {

						@Override
						public void onDirectionTaskComplete(Boolean ok, String jsonResult) {
							mapSdk.cleanPolyline();
							if (ok) {
								((ProgressBar) findViewById(R.id.routeProgressBar))
										.setVisibility(View.GONE);
								try {
									JSONObject jsonObject = new JSONObject(jsonResult);
									JSONArray routeArray = jsonObject.getJSONObject("response")
											.getJSONArray("route");
									mRouteList = new ArrayList<DirectionRoute>();
									for (int i = 0; i < routeArray.length(); i++) {
										DirectionRoute route = new DirectionRoute();
										route.setHereJson(PlanActivity.this,
												routeArray.getJSONObject(i));

										mRouteList.add(route);
										addLineToRoute(route, 0xA0666666);
									}
									mRouteListAdapter.notifyDataSetChanged();

								} catch (JSONException e) {
									Toast.makeText(PlanActivity.this,
											getString(R.string.navierdesinfo_routenotfound),
											Toast.LENGTH_LONG).show();
									e.printStackTrace();
								}
							} else {
								Toast.makeText(PlanActivity.this,
										getString(R.string.navierdesinfo_routenotfound),
										Toast.LENGTH_LONG).show();
							}
						}

					}, new Latlng(myLocation.getLatitude(), myLocation.getLongitude()), new Latlng(
							dest.latitude, dest.longitude));
			task.execute();

		} else {
			// position not fix
			Animation anim = AnimationUtils.loadAnimation(this, R.anim.push_up_out);
			vPlanFrame.startAnimation(anim);
			vPlanFrame.setVisibility(View.GONE);
			Toast.makeText(this, getString(R.string.naviersearchresult_positionfail),
					Toast.LENGTH_LONG).show();
		}
	}

	private void setFavorit(HashMap p) {

		SQLiteDatabase db = MyDBOpenHelper.getDb(PlanActivity.this);
		ContentValues values = new ContentValues();

		String time = String.valueOf(System.currentTimeMillis());
		values.put(MyDBOpenHelper.COL_ADDRESS, (String) p.get(MyDBOpenHelper.COL_ADDRESS));
		values.put(MyDBOpenHelper.COL_DESCRIPTION, (String) p.get(MyDBOpenHelper.COL_DESCRIPTION));
		values.put(
				MyDBOpenHelper.COL_FINGERPRINT,
				time + String.valueOf(p.get(MyDBOpenHelper.COL_LATITUDE))
						+ String.valueOf(p.get(MyDBOpenHelper.COL_LONGITUDE)));
		values.put(MyDBOpenHelper.COL_LATITUDE, (String) p.get(MyDBOpenHelper.COL_LATITUDE));
		values.put(MyDBOpenHelper.COL_LONGITUDE, (String) p.get(MyDBOpenHelper.COL_LONGITUDE));
		values.put(MyDBOpenHelper.COL_PLACE_NAME, (String) p.get(MyDBOpenHelper.COL_PLACE_NAME));
		values.put(MyDBOpenHelper.COL_TIMESTAMP, time);
		values.put(MyDBOpenHelper.COL_FAVERITE, 1);

		String id = String.valueOf(db.insert(MyDBOpenHelper.TABLE_NAME, null, values));
		// db.execSQL(sql);

		putSyncState(id, MyDBOpenHelper.STATE_NEW,
				(String) values.get(MyDBOpenHelper.COL_FINGERPRINT));

		if (!mPref.getPREF_ACCOUNT().equals(getString(R.string.PREF_ACCOUNT_DEFAULT))
				&& mPref.getPREF_SYNC()) {
			new SyncPlaceTask(PlanActivity.this, new SyncPlaceHandler() {

				@Override
				public void onSyncPlaceComplete(boolean ok) {

				}
			}, mPref.getPREF_ACCOUNT()).execute();
		}

	}

	private void putSyncState(String id, String state, String fingerprint) {
		// add to sync state table
		SQLiteDatabase db = MyDBOpenHelper.getDb(PlanActivity.this);

		ContentValues values = new ContentValues();
		values.put(MyDBOpenHelper.SYNC_COL_PLACEID, id);
		values.put(MyDBOpenHelper.SYNC_COL_SYNCSTATE, state);
		values.put(MyDBOpenHelper.SYNC_COL_FINGERPRINT, fingerprint);

		db.insert(MyDBOpenHelper.SYNC_TABLE_NAME, null, values);

	}

	private void store2Db(Place place) {

		ContentValues values = new ContentValues();
		Long time = System.currentTimeMillis();

		String fingerprint = String.valueOf(time) + String.valueOf(place.latitude)
				+ String.valueOf(place.longitude);

		values.put(MyDBOpenHelper.COL_PLACE_NAME, place.name);
		values.put(MyDBOpenHelper.COL_ADDRESS, place.address);
		values.put(MyDBOpenHelper.COL_DESCRIPTION, place.description);
		values.put(MyDBOpenHelper.COL_LATITUDE, place.latitude);
		values.put(MyDBOpenHelper.COL_LONGITUDE, place.longitude);
		values.put(MyDBOpenHelper.COL_FAVERITE, 0);
		values.put(MyDBOpenHelper.COL_TIMESTAMP, String.valueOf(time));
		values.put(MyDBOpenHelper.COL_FINGERPRINT, fingerprint);

		MyDBOpenHelper.getDb(this).insert(MyDBOpenHelper.TABLE_NAME, null, values);
	}

	@Override
	public void onBackPressed() {

		if (vHelpFrame.getVisibility() == View.VISIBLE) {

			hidePager();
			vHelpFrame.setVisibility(View.GONE);
		} else if (vPlanFrame.getVisibility() == View.VISIBLE) {

			hidePlanFrame();
		} else if (vPager.getVisibility() == View.VISIBLE) {

			hidePager();
		} else {
			super.onBackPressed();
		}

	}

	private void startNavigation(final String plan, LatLng from, final String dst_list)
			throws JSONException {

		if (!getIntent().hasExtra(EXTRA_LAYOUT)) {

			PanelChooserDialog dialog = new PanelChooserDialog();
			dialog.setOnPanelChooserListener(new OnPanelChooserListener() {

				@Override
				public void onPanelChoosed(LayoutItem panelLayout) {

					try {

						Intent it = new Intent(PlanActivity.this, RunTimeActivity.class);

						Bundle bundle = new Bundle();

						bundle.putSerializable(RuntimePanelFragment.EXTRA_LAYOUT, panelLayout);
						bundle.putString(RuntimePanelFragment.EXTRA_PLACE_DISTINATION, dst_list);
						bundle.putSerializable(RuntimePanelFragment.EXTRA_PLACE_FROM, new Latlng(
								mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));

						// In old version direction plan should be only one
						// leg in the
						// plan, this version the direction may have several
						// legs
						// depended on how much waypoint

						bundle.putString(RuntimePanelFragment.EXTRA_ROUTE, plan);

						it.putExtras(bundle);
						startActivity(it);

					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			});

			dialog.setFilter(PanelStore.FILTER_NAVIGATION);
			dialog.show(getFragmentManager(), "");

		} else {

			if (mCurrentLocation == null) {

				Toast.makeText(this, getString(R.string.naviersearchresult_positionfail),
						Toast.LENGTH_LONG).show();
			} else {

				Latlng myLocationLatlng;
				if (mCurrentLocation == null) {
					myLocationLatlng = new Latlng(mCurrentLocation.getLatitude(),
							mCurrentLocation.getLongitude());

				} else {
					myLocationLatlng = new Latlng(mCurrentLocation.getLatitude(),
							mCurrentLocation.getLongitude());
				}

				try {
					Intent it = new Intent(PlanActivity.this, RunTimeActivity.class);
					Bundle bundle = new Bundle();

					// The layout parameter same with the old version

					bundle.putSerializable(RuntimePanelFragment.EXTRA_LAYOUT, getIntent()
							.getSerializableExtra(EXTRA_LAYOUT));

					bundle.putString(RuntimePanelFragment.EXTRA_PLACE_DISTINATION, dst_list);

					// In old version it should be a Place object, this version
					// changing to a Latlng object

					bundle.putSerializable(RuntimePanelFragment.EXTRA_PLACE_FROM, myLocationLatlng);

					// In old version direction plan should be only one leg in
					// the
					// plan, this version the direction may have several legs
					// depended on how much waypoint

					bundle.putString(RuntimePanelFragment.EXTRA_ROUTE, plan);

					it.putExtras(bundle);
					startActivity(it);

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
	}

	private class SlidePagerAdapter extends PagerAdapter {

		@Override
		public int getCount() {

			return mSearchResults == null ? 0 : mSearchResults.size();
		}

		@Override
		public int getItemPosition(Object object) {

			return POSITION_NONE;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {

			return arg0 == ((View) arg1);
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void destroyItem(View collection, int position, Object view) {
			((ViewPager) collection).removeView((View) view);
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int index) {
			Location location = mCurrentLocation;

			// Inflate and create the view
			LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.fragment_seach_result_page, null);
			TextView vName = (TextView) view.findViewById(R.id.name);
			TextView vAddr = (TextView) view.findViewById(R.id.addr);
			ImageButton vGo = (ImageButton) view.findViewById(R.id.go);
			ImageButton vFavor = (ImageButton) view.findViewById(R.id.fav);

			vGo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mChoosedPlace = mSearchResults.get(index);
					showPlanFrame(mChoosedPlace);

				}

			});

			// LatLng currLatLng = new LatLng(location.getLatitude(),
			// location.getLongitude());
			final Place place = mSearchResults.get(index);

			vFavor.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(MyDBOpenHelper.COL_ADDRESS, place.address);
					map.put(MyDBOpenHelper.COL_DESCRIPTION, place.address);
					map.put(MyDBOpenHelper.COL_LATITUDE, String.valueOf(place.latitude));
					map.put(MyDBOpenHelper.COL_LONGITUDE, String.valueOf(place.longitude));
					map.put(MyDBOpenHelper.COL_PLACE_NAME, place.name);
					setFavorit(map);
					Toast.makeText(PlanActivity.this,
							place.name + getString(R.string.naviersearchresult_setfav),
							Toast.LENGTH_LONG).show();
				}
			});

			vName.setText(place.name);
			vAddr.setText(place.address);
			float[] results = new float[3];
			((ViewPager) container).addView(view, 0);
			return view;

		}

	}

	private void addLineToRoute(DirectionRoute directionRoute, int color) {

		ArrayList<LatLng> pointList = new ArrayList<LatLng>();
		for (Latlng latLng : directionRoute._overview_polyline) {
			pointList.add(latLng.toLatLng());
		}
		mapSdk.addPolyline(pointList, color);
	}

	private class RouteListAdapter extends BaseAdapter {

		private LayoutInflater inflater;

		public RouteListAdapter() {
			inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {

			return mRouteList == null ? 0 : mRouteList.size();
		}

		@Override
		public Object getItem(int position) {

			return null;
		}

		@Override
		public long getItemId(int position) {

			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.view_navi_plan_item, null);
			}
			TextView duration = (TextView) convertView.findViewById(R.id.duration);
			TextView distance = (TextView) convertView.findViewById(R.id.distance);
			TextView summary = (TextView) convertView.findViewById(R.id.summary);
			ImageButton showlist = (ImageButton) convertView.findViewById(R.id.showlist);

			Button start = (Button) convertView.findViewById(R.id.start_navi);
			ImageButton vSelected = (ImageButton) convertView.findViewById(R.id.selected);

			((ImageButton) convertView.findViewById(R.id.show_line))
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							mSelectedRoute = position;
							addLineToRoute(mRouteList.get(position), 0xffff0000);
							hidePlanFrame();
						}
					});
			if ((mSelectedRoute == -1 && position == 0) || mSelectedRoute == position) {
				vSelected.setVisibility(View.VISIBLE);
			} else {
				vSelected.setVisibility(View.INVISIBLE);
			}
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mSelectedRoute = position;
					notifyDataSetInvalidated();
				}
			});

			start.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {

						// gen waypoint list
						JSONArray wayarray = new JSONArray();
						JSONObject wayobj = new JSONObject();
						wayobj.put("lat", mSelectedDest.latitude);
						wayobj.put("lng", mSelectedDest.longitude);
						wayarray.put(wayobj);

						// Place place = new Place();
						// place.address = vToV.getText().toString();
						// place.description = vToV.getText().toString();
						// place.name = vTo.getText().toString();
						// place.latitude = mSelectedDest.latitude;
						// place.longitude = mSelectedDest.longitude;
						store2Db(mSelectedDest);
						startNavigation(mRouteList.get(position)._oriHereJsonString, new LatLng(
								mCurrentLocation.getLatitude(), mCurrentLocation.getLatitude()),
								wayarray.toString());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			showlist.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					IntroListDialog dialog = new IntroListDialog(PlanActivity.this,
							mRouteList.get(position));
					dialog.show(getFragmentManager(), "");
				}
			});

			DirectionRoute route = mRouteList.get(position);

			duration.setText(route._legList.get(0)._durationString);
			distance.setText(route._legList.get(0)._distanString);

			String sumString = (route._fromString.length() > 0 ? route._fromString
					: getString(R.string.naviermap_mylocation))
					+ " <---> "
					+ (route._toString.length() > 0 ? route._toString : mChoosedPlace.name);
			summary.setText(sumString);
			return convertView;
		}
	}
}
