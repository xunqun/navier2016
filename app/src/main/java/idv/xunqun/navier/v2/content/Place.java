package idv.xunqun.navier.v2.content;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.google.android.gms.maps.model.Marker;

public class Place implements Serializable {
	
	public double latitude;
	public double longitude;
	public String name;
	public String description;
	public String address;
	public int isFaverite = 0; // 0 or 1  -> false or true
	public boolean isStroed = false;
	public double timestamp;
	public Long id;
	public Date date;
	public String fingerprint;
	public ArrayList<PhoneNumber> phoneNumbers = new ArrayList<PhoneNumber>();
	public Marker marker;
	public String iconUri;
	public String legDistance;

	public Place(){
		
	}
	
	/**
	 * 
	 * @param n name of place
	 * @param des description of place
	 */
	
	public Place(String n,String des){
		//this.location = point;
		this.name=n;
		this.description = des;
	}
	public void setTimeStamp(long t){
		timestamp = t;
		date.setTime(t);
	}
	public static class PhoneNumber implements Serializable{
		public String type;
		public String number;
	}

}
