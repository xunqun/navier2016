package idv.xunqun.navier.v2;

import idv.xunqun.navier.v2.content.Panel;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

public class PreviewPanelView extends View {



	public PreviewPanelView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	private Panel mPanel;


	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		canvas.drawColor(Color.LTGRAY);
		super.onDraw(canvas);
		
	}
	
	/**
	 * Sets a panel to the view
	 * @param p
	 */
	public void init(Panel p){
		mPanel = p;
	}

}
