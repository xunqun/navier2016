package idv.xunqun.navier.v2.parts;

import idv.xunqun.navier.BaseNavierPanelContext;
import idv.xunqun.navier.R;
import idv.xunqun.navier.parts.GridBoard.GridBoardHandler;
import idv.xunqun.navier.utils.Utility;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.hardware.SensorEvent;
import android.location.GpsStatus;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.AttributeSet;

public class DashGridBoard extends GridBoard{
	
	

	private Context _context;
	public Location _currentBestLocation;
	
	private MediaPlayer speedAlarmPlayer;
	private boolean _isSpeedAlarmDuration = false;
	private long _speedAlarmDurationSec = 10000;
	private boolean _overspeed;

	/**
	 * @param context  Should be activity
	 * @param basepanel May a activity or fragment that implements the BaseNavierPanel
	 * @param jlayout
	 * @param handler
	 */
	
	public DashGridBoard(Context context, BaseNavierPanelContext basepanel , JSONObject layout, GridBoardHandler handler) {
		super(context, basepanel, layout, handler);
		// TODO Auto-generated constructor stub
		_context = context;
		initProperties();
	}

	
	private void initProperties() {
		// TODO Auto-generated method stub
		
		speedAlarmPlayer = MediaPlayer.create(_context, R.raw.cowbell);

         
	}
	
	private void playSpeedAlarm(Location location){
		
		_overspeed = Utility.meters2kmh(location.getSpeed()) > _settings.getPREF_ALERT_SPEED();
		
		if(!_isSpeedAlarmDuration && _overspeed ){
		
			try{
				if(_settings.getPREF_NOTIFY_SOUND()){
					_isSpeedAlarmDuration = true;
					speedAlarmPlayer.start();
					new CountDownTimer(_speedAlarmDurationSec, _speedAlarmDurationSec){

						@Override
						public void onFinish() {
							// TODO Auto-generated method stub
							_isSpeedAlarmDuration = false;
						}

						@Override
						public void onTick(long millisUntilFinished) {
							// TODO Auto-generated method stub
							
						}
						
					}.start();
				}
			}catch(Exception e){
				
			}
		}
		

	}

	@Override
	public void onSensorChangeHandler(SensorEvent event) {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onSensorChange(event);
		}
	}

	@Override
	public void onSensorChangeHandler(float[] orientation) {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onSensorChange(orientation);
		}
		
	}

	@Override
	public void onLocationChangeHandler(Location location) {
		// TODO Auto-generated method stub
		//if(NavigationManager.isBetterLocation(location, _currentBestLocation)){
			_currentBestLocation = location;
			for(int i=0;i<_layout._parts_list.size();i++){
				Parts p = _layout._parts_list.get(i);
				p.onLocationChange(location);
			}
			
			playSpeedAlarm(location);
			
		//}
	}

	@Override
	public void onLocationStatusChangeHandle(String provider, int status,
			Bundle extras) {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onLocationStatusChange(provider, status, extras);
		}
	}

	@Override
	public void onLocationProviderDisableHandle(String provider) {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onLocationProviderDisable(provider);
		}
	}

	@Override
	public void onSpeedUnitChangeHandler(int sunit) {
		// TODO Auto-generated method stub
		this.GLOBAL_SPEEDUNIT = sunit;
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onSpeedUnitChange(sunit);
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onGlobalColorChange(int color) {
		// TODO Auto-generated method stub
		
		_gridPaint.setColor(color);
		_gridPaint.setAlpha(0x50);
		
		if(GLOBAL_COLOR == 0xffff4500){
			SECONDARY_COLOR = Color.WHITE;
		}else{
			SECONDARY_COLOR = Color.RED;
		}
		
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			
			p.onGlobalColorChange(color,SECONDARY_COLOR);
		}
	}

	@Override
	public void onIsGPSFix(GpsStatus status,Boolean isfix) {
		// TODO Auto-generated method stub
		for(int i=0;i<_layout._parts_list.size();i++){
			Parts p = _layout._parts_list.get(i);
			p.onIsGPSFix(status,isfix);
		}
	}


}
