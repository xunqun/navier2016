package idv.xunqun.navier.v2.dialog;

import idv.xunqun.navier.R;
import android.R.integer;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.GpsStatus.Listener;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PlaceOperationDialog extends DialogFragment {

	public static final int USE = 0;
	public static final int RENAME = 1;
	public static final int FAVORITE = 2;
	public static final int DELETE = 3;

	public interface PlaceOperationListener {
		void onOperationClick(int operation);
	}

	private PlaceOperationListener mListener;
	private boolean mHideFavoriteBtn = false;

	public void setOnOperationListener(PlaceOperationListener listener) {
		mListener = listener;
	}
	
	public void setOnOperationListener(PlaceOperationListener listener, boolean hideFavoriteBtn) {
		mListener = listener;
		mHideFavoriteBtn  = hideFavoriteBtn;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the Builder class for convenient dialog construction
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		View view = inflater.inflate(R.layout.dialog_placeoperation, null);
		
		Button useBtn = (Button) view.findViewById(R.id.use);
		useBtn.setOnClickListener(onClickListener);
		
		Button renameBtn = (Button) view.findViewById(R.id.rename);
		renameBtn.setOnClickListener(onClickListener);
		

		Button favoriteBtn = (Button) view.findViewById(R.id.favorite);
		if(mHideFavoriteBtn){
			favoriteBtn.setVisibility(View.INVISIBLE);
		}else{
			favoriteBtn.setVisibility(View.VISIBLE);
			favoriteBtn.setOnClickListener(onClickListener);
		}
		
		Button deleteBtn = (Button) view.findViewById(R.id.delete);
		deleteBtn.setOnClickListener(onClickListener);		
		

		builder.setView(view);
		// Create the AlertDialog object and return it
		return builder.create();
	}

	OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.use) {
				mListener.onOperationClick(USE);
			}

			else if (v.getId() == R.id.rename) {
				mListener.onOperationClick(RENAME);
			}
			
			else if(v.getId() == R.id.favorite){
				mListener.onOperationClick(FAVORITE);
			}

			else if (v.getId() == R.id.delete) {
				mListener.onOperationClick(DELETE);
			}
			getDialog().dismiss();
		}
	};

}
