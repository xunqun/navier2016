package idv.xunqun.navier.v2.dialog;

import idv.xunqun.navier.R;
import idv.xunqun.navier.content.LayoutItem;
import idv.xunqun.navier.v2.content.Panel;
import idv.xunqun.navier.v2.content.PanelStore;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PanelChooserDialog_Support extends DialogFragment {

	private LayoutInflater inflater;
	private OnPanelChooserListener mListener;
	private RadioButton[] mRadioButton = new RadioButton[3];
	private ListView mListView;
	private int mFilter = PanelStore.FILTER_ALL;
	private Button mNoAutoRunBtn;

	public interface OnPanelChooserListener {
		void onPanelChoosed(LayoutItem panelLayout);

		void onSetToNoAuto();

	}

	public void setOnPanelChooserListener(OnPanelChooserListener listener) {
		mListener = listener;
	}

	public void setFilter(int filter) {
		mFilter = filter;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
		
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		inflater = (LayoutInflater) getActivity().getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		View view = inflater.inflate(R.layout.dialog_panelchooser_support, null);
		initViews(view);
		builder.setView(view);
		Dialog d = builder.create();

		

		return d;
	}

	private void initViews(View view) {
		// TODO Auto-generated method stub
		mRadioButton[0] = (RadioButton) view.findViewById(R.id.radio0);
		mRadioButton[1] = (RadioButton) view.findViewById(R.id.radio1);
		mRadioButton[2] = (RadioButton) view.findViewById(R.id.radio2);

		mRadioButton[0].setOnCheckedChangeListener(mOnCheckedChangeListener);
		mRadioButton[1].setOnCheckedChangeListener(mOnCheckedChangeListener);
		mRadioButton[2].setOnCheckedChangeListener(mOnCheckedChangeListener);

		mListView = (ListView) view.findViewById(R.id.dialog_panellist);

		// set filter
		if (mFilter == PanelStore.FILTER_NORMAL) {
			mRadioButton[0].setVisibility(View.GONE);
			mRadioButton[1].setChecked(true);
			mRadioButton[2].setVisibility(View.GONE);
		}

		if (mFilter == PanelStore.FILTER_NAVIGATION) {
			mRadioButton[0].setVisibility(View.GONE);
			mRadioButton[1].setVisibility(View.GONE);
			mRadioButton[2].setChecked(true);
		}

		mListView.setAdapter(new PanelAdapter(mFilter));

		mNoAutoRunBtn = (Button) view.findViewById(R.id.no_auto);
		mNoAutoRunBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mListener.onSetToNoAuto();
				getDialog().dismiss();
			}
		});

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}

	public class PanelAdapter extends BaseAdapter {

		private ArrayList<Panel> mList;

		public PanelAdapter(int type) {

			mList = new ArrayList<Panel>();
			ArrayList<Panel> list = PanelStore.getPanelStore(getActivity());

			switch (type) {
			case PanelStore.FILTER_ALL:
				mList = list;
				break;
			case PanelStore.FILTER_NORMAL:
				for (Panel panel : list) {
					if (panel.LAYOUT_TYPE == Panel.TYPE_NORMAL) {
						mList.add(panel);
					}
				}
				break;
			case PanelStore.FILTER_NAVIGATION:
				for (Panel panel : list) {
					if (panel.LAYOUT_TYPE == Panel.TYPE_NAVI) {
						mList.add(panel);
					}
				}
				break;
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.menu_list_item, null);
			}

			TextView name = (TextView) convertView.findViewById(R.id.name);
			ImageView type = (ImageView) convertView.findViewById(R.id.type);
			name.setText(mList.get(position).LAYOUT_NAME);
			type.setImageResource(mList.get(position).LAYOUT_TYPE == Panel.TYPE_NAVI ? R.drawable.nav_panel : R.drawable.nor_panel);

			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					final LayoutItem item = new LayoutItem();
					item.LAYOUT_FINGERPRINT = mList.get(position).LAYOUT_FINGERPRINT;
					item.LAYOUT_ID = mList.get(position).LAYOUT_ID;
					item.LAYOUT_JSONSTR = mList.get(position).LAYOUT_JSONSTR;
					item.LAYOUT_NAME = mList.get(position).LAYOUT_NAME;
					item.LAYOUT_TYPE = mList.get(position).LAYOUT_TYPE;
					item._isEditable = true;
					item._isStored = true;

					mListener.onPanelChoosed(item);
					PanelChooserDialog_Support.this.getDialog().dismiss();
				}
			});

			return convertView;
		}

	}

	private OnCheckedChangeListener mOnCheckedChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			if (isChecked && buttonView.equals(mRadioButton[0])) {
				mListView.setAdapter(new PanelAdapter(PanelStore.FILTER_ALL));
			}

			else if (isChecked && buttonView.equals(mRadioButton[1])) {
				mListView.setAdapter(new PanelAdapter(PanelStore.FILTER_NORMAL));
			}

			else if (isChecked && buttonView.equals(mRadioButton[2])) {
				mListView.setAdapter(new PanelAdapter(PanelStore.FILTER_NAVIGATION));
			}
		}
	};

}
