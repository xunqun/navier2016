package idv.xunqun.navier.v2.content;

import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.content.LayoutItem;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

public class Panel extends LayoutItem{
	public static final int FAIL = -1;
	public static final int TYPE_NORMAL= 1;
	public static final int TYPE_NAVI = 2;
	
	/*
	public String mNameString;
	public String mFingerPrintString;
	public int mId;
	*/
	public long mTimeStamp;
	
	public JSONObject mLayoutJsonObject;
	//public String mJsonString;
	
	public Panel(JSONObject json){
		
		LAYOUT_JSONSTR = json.toString();
		if(json.has("layout")){
			
			try{
				mLayoutJsonObject = (JSONObject) json.get("layout");
				//LAYOUT_JSONSTR = json.getString("layout");
				LAYOUT_TYPE = mLayoutJsonObject.getInt("type");
				LAYOUT_NAME = mLayoutJsonObject.getString("name");
				LAYOUT_FINGERPRINT = json.getString("fingerprint");
				LAYOUT_ID = json.getInt("id");
				
				
				mTimeStamp = System.currentTimeMillis();
				
			}catch(Exception e){
				
			}
		}
	}
	
	public int getType(){
		try {
			return mLayoutJsonObject.getInt("type");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return FAIL;
	}
	
	public long storeToDb(SQLiteDatabase db, String fingerprint){
		
		ContentValues values = new ContentValues();
		values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, LAYOUT_JSONSTR);
		values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, mTimeStamp);
		values.put(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT, fingerprint);
		long id = db.insert(MyDBOpenHelper.LAYOUT_TABLE_NAME, null, values);
		
		return id;
	}
	

}
