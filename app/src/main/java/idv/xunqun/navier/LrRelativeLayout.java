package idv.xunqun.navier;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class LrRelativeLayout extends RelativeLayout {

	public LrRelativeLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	public LrRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}


	@Override
	protected void dispatchDraw(Canvas canvas) {
		
		canvas.translate(getWidth(), 0);
		canvas.scale(-1, 1, 0,0);
		super.dispatchDraw(canvas);
	}
	
	

}
