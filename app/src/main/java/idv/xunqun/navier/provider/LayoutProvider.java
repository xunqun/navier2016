package idv.xunqun.navier.provider;

import idv.xunqun.navier.MyDBOpenHelper;
import idv.xunqun.navier.provider.LayoutProviderMetadata.LayoutTableMetadata;

import java.util.HashMap;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class LayoutProvider extends ContentProvider {

	private static HashMap<String, String> layoutsProjectionMap;
	static{
		layoutsProjectionMap = new HashMap<String, String>();
		layoutsProjectionMap.put(LayoutTableMetadata.LAYOUT_ID, LayoutTableMetadata.LAYOUT_ID);
		layoutsProjectionMap.put(LayoutTableMetadata.LAYOUT_LAYOUT, LayoutTableMetadata.LAYOUT_LAYOUT);
		layoutsProjectionMap.put(LayoutTableMetadata.LAYOUT_FINGERPRINT, LayoutTableMetadata.LAYOUT_FINGERPRINT);
		layoutsProjectionMap.put(LayoutTableMetadata.LAYOUT_TIMESTAMP, LayoutTableMetadata.LAYOUT_TIMESTAMP);
		
	}
	
	private static final UriMatcher _uriMatcher;
	private static final int INCOMING_LAYOUT_COLLECTION_URI_INDICATOR =1;
	private static final int INCOMING_SINGLE_LAYOUT_URI_INDICATOR =2;
	static{
		_uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		_uriMatcher.addURI(LayoutProviderMetadata.AUTHORITY, "layouts", INCOMING_LAYOUT_COLLECTION_URI_INDICATOR);
		_uriMatcher.addURI(LayoutProviderMetadata.AUTHORITY, "layouts/#", INCOMING_SINGLE_LAYOUT_URI_INDICATOR);
		
	}
	
	private MyDBOpenHelper _openHelper;
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		SQLiteDatabase db = _openHelper.getWritableDatabase();
		int count;
		switch(_uriMatcher.match(uri)){
		case INCOMING_LAYOUT_COLLECTION_URI_INDICATOR:
			count = db.delete(LayoutTableMetadata.TABLE_NAME, selection, selectionArgs);
			break;
		case INCOMING_SINGLE_LAYOUT_URI_INDICATOR:
			String rowId = uri.getPathSegments().get(1);
			count = db.delete(LayoutTableMetadata.TABLE_NAME, LayoutTableMetadata.LAYOUT_ID + "=" + rowId + (!TextUtils.isEmpty(selection)?" AND ("+ selection + ")" : ""), selectionArgs);
			break;
		
		default:
			throw new IllegalArgumentException("Unknow URI "+uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		switch(_uriMatcher.match(uri)){
		case INCOMING_LAYOUT_COLLECTION_URI_INDICATOR:
			
			return LayoutTableMetadata.CONTENT_TYPE;
		case INCOMING_SINGLE_LAYOUT_URI_INDICATOR:
			
			return LayoutTableMetadata.CONTENT_ITEM_TYPE;
		default:
			throw new IllegalArgumentException("Unknow URI "+uri);
		}
		
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		if(_uriMatcher.match(uri)!=INCOMING_LAYOUT_COLLECTION_URI_INDICATOR){
			throw new IllegalArgumentException("Unknow URI "+uri);
		}
		
		Long now = Long.valueOf(System.currentTimeMillis());
		if(!values.containsKey(LayoutTableMetadata.LAYOUT_LAYOUT)){
			throw new SQLException("No LAYOUT_LAYOUT parameter.");
		}
		
		if(!values.containsKey(LayoutTableMetadata.LAYOUT_FINGERPRINT)){
			values.put(LayoutTableMetadata.LAYOUT_FINGERPRINT, now);
		}
		
		if(!values.containsKey(LayoutTableMetadata.LAYOUT_TIMESTAMP)){
			values.put(LayoutTableMetadata.LAYOUT_FINGERPRINT, now);
		}
		
		SQLiteDatabase db = _openHelper.getWritableDatabase();
		long rowId = db.insert(LayoutTableMetadata.TABLE_NAME, null, values);
		
		if(rowId>0){
			Uri layoutUri = ContentUris.withAppendedId(LayoutTableMetadata.CONTENT_URI, rowId);
			getContext().getContentResolver().notifyChange(layoutUri, null);
			return layoutUri;
		}
		
		
		throw new SQLException("Failed to insert row into "+uri);
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		_openHelper = new MyDBOpenHelper(this.getContext(),null);
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		
		switch(_uriMatcher.match(uri)){
		
		case INCOMING_LAYOUT_COLLECTION_URI_INDICATOR:
			qb.setTables(LayoutTableMetadata.TABLE_NAME);
			qb.setProjectionMap(layoutsProjectionMap);
			break;
		case INCOMING_SINGLE_LAYOUT_URI_INDICATOR:
			qb.setTables(LayoutTableMetadata.TABLE_NAME);
			qb.setProjectionMap(layoutsProjectionMap);
			qb.appendWhere(LayoutTableMetadata.LAYOUT_ID + "=" + uri.getPathSegments().get(1));
			break;
		
		default:
			throw new IllegalArgumentException("Unknow URI "+uri );
		}
		
		String orderBy;
		if(TextUtils.isEmpty(sortOrder)){
			orderBy = LayoutTableMetadata.DEFAULT_SORT_ORDER;
		}else{
			orderBy = sortOrder;
		}
		
		SQLiteDatabase db = _openHelper.getReadableDatabase();
		Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
		int i = c.getCount();
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		SQLiteDatabase db = _openHelper.getWritableDatabase();
		int count;
		
		switch(_uriMatcher.match(uri)){
		case INCOMING_LAYOUT_COLLECTION_URI_INDICATOR:
			count = db.update(LayoutTableMetadata.TABLE_NAME, values, selection, selectionArgs);
			break;
			
		case INCOMING_SINGLE_LAYOUT_URI_INDICATOR:
			String rowId = uri.getPathSegments().get(1);
			count = db.update(LayoutTableMetadata.TABLE_NAME, values, LayoutTableMetadata.LAYOUT_FINGERPRINT + "=" +rowId +(!TextUtils.isEmpty(selection)?" AND ("+ selection +")":"") , selectionArgs);
			break;
			
		default:
			throw new IllegalArgumentException("Unknow URI "+uri );	
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

}
