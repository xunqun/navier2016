package idv.xunqun.navier.provider;

import idv.xunqun.navier.MyDBOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;

public class LayoutProviderMetadata {
	public static final String AUTHORITY = "idv.navier.provider.layoutprovider";
	
	public static final String DATABASE_NAME = MyDBOpenHelper.DATABASE_NAME;
	public static final int DATABASE_VERSION = MyDBOpenHelper.DATABASE_VERSION;
	public static final String LAYOUT_TABLE_NAME = MyDBOpenHelper.LAYOUT_TABLE_NAME;
	
	public static final class LayoutTableMetadata implements BaseColumns{
		
		private LayoutTableMetadata(){}
		public static final String TABLE_NAME = MyDBOpenHelper.LAYOUT_TABLE_NAME;
		
		public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/layouts");
		public static final String CONTENT_TYPE = "vnd.navier.cursor.dir/vnd.navier.layout";
		public static final String CONTENT_ITEM_TYPE = "vnd.navier.cursor.item/vnd.navier.layout";
		public static final String DEFAULT_SORT_ORDER = "fingerprint DESC";
		
		public static final String LAYOUT_ID = MyDBOpenHelper.LAYOUT_COL_ID;
		public static final String LAYOUT_LAYOUT = MyDBOpenHelper.LAYOUT_COL_LAYOUT;
		public static final String LAYOUT_FINGERPRINT = MyDBOpenHelper.LAYOUT_COL_FINGERPRINT;
		public static final String LAYOUT_TIMESTAMP = MyDBOpenHelper.LAYOUT_COL_TIMESTAMP;
	}
	
}
