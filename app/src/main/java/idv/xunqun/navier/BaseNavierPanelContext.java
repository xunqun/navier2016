package idv.xunqun.navier;

import idv.xunqun.navier.content.Place;
import idv.xunqun.navier.v2.content.DirectionRoute;

public interface BaseNavierPanelContext {
	boolean getIsEndAnimation();
	void setIsEndAnimation(boolean b);
	
	boolean getIsStopAnimation();
	void setIsStopAnimation(boolean b);
	
	
	
	// below are for navigation panel
	
	/**
	 * Return the destination place in Navigation Panel. In new version, There may having several waypoint in the plan. So this's replaced by getDirectionPlan();
	 * @return Place
	 */
	@Deprecated
	Place getDestPlace();
	
	/**
	 * Return user's departure place in Navigation Panel
	 * @return Place
	 */
	Place getCurrPlace();
	
	/**
	 * Return a planed route if exist 
	 * @return String
	 */
	String getRowRoute();
	
	/**
	 * Return a DirectionPlan object
	 * @return DirectionPlan
	 */
	DirectionRoute getDirectionPlan();
	
	/**
	 * Request a TTS 
	 * @param text
	 */
	void doTTS(String text);
}
