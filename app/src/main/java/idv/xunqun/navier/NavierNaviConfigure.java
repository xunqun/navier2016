package idv.xunqun.navier;

import idv.xunqun.navier.utils.Utility;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.Spinner;

public class NavierNaviConfigure extends Activity {
	
	public static final String EXTRA_CONFIG_TRANSTYPE = "transtype";
	public static final String EXTRA_CONFIG_ALARMSEC = "alarmsec";
	public static final String EXTRA_CONFIG_AVOIDHWAY = "avoidhway";
	public static final String EXTRA_CONFIG_AVOIDTOLLS = "avoidtolls";

	private PreferencesHandler _settings;
	ImageButton[] buttons = new ImageButton[3]; 
	
	String[] sec_options;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.navigation_config);
		_settings = new PreferencesHandler(this);
		
		initView();
	}

	private void initView(){
		
		//Transportation type
		
		buttons[0] = (ImageButton) findViewById(R.id.driving);
		buttons[1] = (ImageButton)findViewById(R.id.bicycleing);
		buttons[2] = (ImageButton)findViewById(R.id.walking);
		
		String type = _settings.getPREF_TRANS_TYPE();
		setDefaultButtonImg(buttons, type);
		
		buttons[0].setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buttons[0].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_down));
				buttons[1].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				buttons[2].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				_settings.setPREF_TRANS_TYPE(PreferencesHandler.VALUE_OF_PREF_TRANS_TYPE_DRIVING);
			}
			
		});
		
		buttons[1].setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buttons[0].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				buttons[1].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_down));
				buttons[2].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				_settings.setPREF_TRANS_TYPE(PreferencesHandler.VALUE_OF_PREF_TRANS_TYPE_BICYCLEING);
			}
			
		});
		
		buttons[2].setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buttons[0].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				buttons[1].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_up));
				buttons[2].setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_down));
				_settings.setPREF_TRANS_TYPE(PreferencesHandler.VALUE_OF_PREF_TRANS_TYPE_WALKING);
			}
			
		});
		
		//Turn alarm sec
		
		Spinner spinner = (Spinner) findViewById(R.id.spinner1);
	    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
	            this, R.array.trunsec_prompt_value, android.R.layout.simple_spinner_item);
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    spinner.setAdapter(adapter);
	    spinner.setSelection(getDefaultAlertSecPos(),true);
	    spinner.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View view, int pos, long id) {
				// TODO Auto-generated method stub
				_settings.setPREF_TURNALARM_SEC(Integer.parseInt(sec_options[pos]));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
	    	
	    });
	    
	    //restrictions
	    CheckBox avoidTolls = (CheckBox) findViewById(R.id.avoidtolls);
	    CheckBox avoidHway = (CheckBox) findViewById(R.id.avoidhway);
	    
	    avoidTolls.setChecked(_settings.getPREF_AVOID_TOLLS());
	    avoidHway.setChecked(_settings.getPREF_AVOID_HIGHWAY());
	    
	    avoidTolls.setOnCheckedChangeListener(new OnCheckedChangeListener(){


			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				_settings.setPREF_AVOID_TOLLS(arg1);
			}
	    	
	    });
	    
	    avoidHway.setOnCheckedChangeListener(new OnCheckedChangeListener(){


			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				_settings.setPREF_AVOID_HIGHWAY(arg1);
			}
	    	
	    });
	    

	    
	    //lockroad
	    CheckBox lockroad = (CheckBox) findViewById(R.id.lockroad);
	    lockroad.setChecked(_settings.getPREF_LOCK_ROAD());
	    
	    lockroad.setOnCheckedChangeListener(new OnCheckedChangeListener(){


			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				_settings.setPREF_LOCK_ROAD(arg1);
			}
	    	
	    });
	    
	    

	}
	
	private void setDefaultButtonImg(ImageButton[] buttons, String type){
		
		if(type.equalsIgnoreCase(PreferencesHandler.VALUE_OF_PREF_TRANS_TYPE_WALKING)){
			
			buttons[2].setBackgroundDrawable(this.getResources().getDrawable(R.drawable.btn_down));
			buttons[0].setBackgroundDrawable(this.getResources().getDrawable(R.drawable.btn_up));
			buttons[1].setBackgroundDrawable(this.getResources().getDrawable(R.drawable.btn_up));
			
		}else if(type.equalsIgnoreCase(PreferencesHandler.VALUE_OF_PREF_TRANS_TYPE_BICYCLEING)){
			
			buttons[1].setBackgroundDrawable(this.getResources().getDrawable(R.drawable.btn_down));
			buttons[0].setBackgroundDrawable(this.getResources().getDrawable(R.drawable.btn_up));
			buttons[2].setBackgroundDrawable(this.getResources().getDrawable(R.drawable.btn_up));
			
		}else{
			
			buttons[0].setBackgroundDrawable(this.getResources().getDrawable(R.drawable.btn_down));
			buttons[1].setBackgroundDrawable(this.getResources().getDrawable(R.drawable.btn_up));
			buttons[2].setBackgroundDrawable(this.getResources().getDrawable(R.drawable.btn_up));
			
		}
	}
	
	private int getDefaultAlertSecPos(){
		
		int sec = _settings.getPREF_TURNALARM_SEC();
		sec_options = this.getResources().getStringArray(R.array.trunsec_prompt_value);
		
		for(int i = 0 ; i < sec_options.length ; i++){
			if(sec == Integer.parseInt(sec_options[i])){
				return i;
			}
		}
		
		return 3;
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Utility.requestLocationServiceStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//Utility.checkLanscapeOrientation(this);
		Utility.requestLocationServiceUpdate(this);
	}
	

}
