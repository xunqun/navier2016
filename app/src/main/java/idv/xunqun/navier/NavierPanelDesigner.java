package idv.xunqun.navier;

import idv.xunqun.navier.content.Layout;
import idv.xunqun.navier.content.LayoutItem;
import idv.xunqun.navier.http.PushPanelTask;
import idv.xunqun.navier.utils.Utility;
import idv.xunqun.navier.v2.DownloadActivity;
import idv.xunqun.navier.v2.content.PanelStore;
import idv.xunqun.navier.v2.parts.Elem_AccDigital;
import idv.xunqun.navier.v2.parts.Elem_Acceleration_Chart;
import idv.xunqun.navier.v2.parts.Elem_AverageSpeed;
import idv.xunqun.navier.v2.parts.Elem_Battery;
import idv.xunqun.navier.v2.parts.Elem_Compass;
import idv.xunqun.navier.v2.parts.Elem_CompassWheel;
import idv.xunqun.navier.v2.parts.Elem_DigitalClock;
import idv.xunqun.navier.v2.parts.Elem_DistanceLeft;
import idv.xunqun.navier.v2.parts.Elem_DistanceToTurn;
import idv.xunqun.navier.v2.parts.Elem_GPSChart;
import idv.xunqun.navier.v2.parts.Elem_GPSState;
import idv.xunqun.navier.v2.parts.Elem_GpsChart_Large;
import idv.xunqun.navier.v2.parts.Elem_MaxSpeed;
import idv.xunqun.navier.v2.parts.Elem_NextTurn;
import idv.xunqun.navier.v2.parts.Elem_Road_Name;
import idv.xunqun.navier.v2.parts.Elem_Route;
import idv.xunqun.navier.v2.parts.Elem_RouteProgress;
import idv.xunqun.navier.v2.parts.Elem_Route_3D;
import idv.xunqun.navier.v2.parts.Elem_Route_Wide;
import idv.xunqun.navier.v2.parts.Elem_SimpleCompass_Tiny;
import idv.xunqun.navier.v2.parts.Elem_Speed;
import idv.xunqun.navier.v2.parts.Elem_SpeedChart;
import idv.xunqun.navier.v2.parts.Elem_SpeedChart_Large;
import idv.xunqun.navier.v2.parts.Elem_SpeedDigital;
import idv.xunqun.navier.v2.parts.Elem_SpeedDigital_Large;
import idv.xunqun.navier.v2.parts.Elem_TotalDistance;
import idv.xunqun.navier.v2.parts.Elem_TotalTime;
import idv.xunqun.navier.v2.parts.Parts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Telephony.Mms.Part;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;


public class NavierPanelDesigner extends Activity implements OnTouchListener {

	public static final String EXTRA_INT_ACTION = "action";
	public static final String EXTRA_INT_TYPE = "type";
	public static final String EXTRA_LAYOUTITEM = "layoutitem";

	public static final int EXTRA_ACTION_NEW = 1;
	public static final int EXTRA_ACTION_EDIT = 2;

	public static final String LAYOUT_NAME = "name";
	public static final String LAYOUT_JSONSTRING = "jsonstring";
	public static final String LAYOUT_TYPE = "type";
	public static final boolean LAYOUT_ISSTORED = true;

	public static final int NEW_LAYOUT = -1;

	// dialog
	public final static int DIALOG_PANEL_CHOOSER = 1;
	public final static int DIALOG_LAYOUT_NAME = 2;
	public final static int DIALOG_LAYOUT_TYPE = 3;
	private EditText etName;

	// state
	public final static int STATE_NEW = 0;
	public final static int STATE_EDIT = 1;
	public final static int STATE_READY = 2;
	public int _currentState = STATE_NEW;

	// values
	private PreferencesHandler _settings;
	SimpleAdapter _listAdapter;
	private ArrayList<HashMap<String, Object>> _list = new ArrayList<HashMap<String, Object>>();

	LayoutPanel _layoutPanel;
	LinearLayout _okcancel;
	SlidingDrawer _slidingDrawer;
	ImageButton _btn_ok;
	ImageButton _btn_cancel;

	ImageButton _btn_new;
	ImageButton _btn_load;
	ImageButton _btn_save;
	ListView _lvLayout;

	int screenWidth;

	ArrayList<LayoutItem> _availableLayouts;
	HashMap<Integer, PartNail> _partMap = new HashMap();
	int _currentEdit = NEW_LAYOUT;
	int _currentType = Layout.TYPE_DASHBOARD;

	// Special and Premium part list
	HashSet<Integer> mPremiumParts;

	//
	private CharSequence[] labels;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.paneldesigner);

		initViews();
		initPremiumPartsSet();

		_settings = new PreferencesHandler(this);
		Intent it = this.getIntent();

		int action = it.getIntExtra(EXTRA_INT_ACTION, EXTRA_ACTION_NEW);

		if (action == EXTRA_ACTION_EDIT) {

			JSONObject json;
			try {

				LayoutItem layout = (LayoutItem) it
						.getSerializableExtra(EXTRA_LAYOUTITEM);
				this.getAvailableLayouts();
				loadLayout(layout);
				this.setState(STATE_READY);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {

			int type = it.getIntExtra(EXTRA_INT_TYPE, Layout.TYPE_DASHBOARD);

			JSONObject json = new JSONObject();
			JSONObject layout = new JSONObject();
			JSONArray parts = new JSONArray();
			try {

				// add name
				layout.put(Layout.JSON_NAME, "Unnamed Panel");

				// add type
				if (type == Layout.TYPE_DASHBOARD)
					layout.put(Layout.JSON_TYPE, Layout.TYPE_DASHBOARD);
				else
					layout.put(Layout.JSON_TYPE, Layout.TYPE_NAVIGATION);

				// add mode
				layout.put(Layout.JSON_MODE, Layout.MODE_NORMAL);
				layout.put(Layout.JSON_PARTS, parts);

				json.put(Layout.JSON_ROOT, layout);
				this.setState(STATE_NEW);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private void initPremiumPartsSet() {
		mPremiumParts = new HashSet<Integer>();
		mPremiumParts.add(Parts.ELEM_ROUTE_WIDE);
		mPremiumParts.add(Parts.ELEM_ROAD_NAME);
		mPremiumParts.add(Parts.ELEM_DISTANCE_TO_TURN);
		mPremiumParts.add(Parts.ELEM_ROUTE_3D);

	}

	@Override
	public void onStart() {
		super.onStart();

	}

	@Override
	protected void onResume() {

		super.onResume();
		// Utility.checkLanscapeOrientation(this);
		Utility.requestLocationServiceUpdate(this);
	}

	@Override
	protected void onPause() {
		Utility.requestLocationServiceStop(this);
		super.onPause();
		MyDBOpenHelper.closeDb();
	}

	@Override
	protected void onStop() {
		super.onStop();

	}

	public void initViews() {

		Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
				.getDefaultDisplay();
		screenWidth = display.getWidth();

		_layoutPanel = (LayoutPanel) findViewById(R.id.layoutPanel);
		_layoutPanel.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				_layoutPanel.onLongClick();
				return false;
			}

		});

		_slidingDrawer = (SlidingDrawer) findViewById(R.id.slidingDrawer1);
		_okcancel = (LinearLayout) findViewById(R.id.okcancel);

		GridView norGridView = (GridView) this.findViewById(R.id.norcontent);
		norGridView.setAdapter(new NorPartAdapter());

		GridView navGridView = (GridView) this.findViewById(R.id.navcontent);
		navGridView.setAdapter(new NavPartAdapter());

		// ok button

		_btn_ok = (ImageButton) findViewById(R.id.ok);
		_btn_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				_layoutPanel.stickCurrentPartNail();
				setState(STATE_READY);
			}

		});

		// cancel button
		_btn_cancel = (ImageButton) findViewById(R.id.cancel);
		_btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				_layoutPanel.cancelCurrentPartNail();
				setState(STATE_READY);
			}

		});

		// new button
		_btn_new = (ImageButton) findViewById(R.id.new_btn);
		_btn_new.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				_currentEdit = NEW_LAYOUT;
				// showDialog(DIALOG_LAYOUT_TYPE);
				_layoutPanel.handleNewButton();
			}

		});

		// load button
		/*
		 * _btn_load = (ImageButton) findViewById(R.id.load_btn);
		 * _btn_load.setOnClickListener(new OnClickListener(){
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub
		 * 
		 * showDialog(DIALOG_PANEL_CHOOSER); }
		 * 
		 * });
		 */
		// save button
		_btn_save = (ImageButton) findViewById(R.id.save_btn);
		_btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// saveCurrLayout();

				showDialog(DIALOG_LAYOUT_NAME);

			}

		});

	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		// TODO Auto-generated method stub
		switch (id) {
		case DIALOG_PANEL_CHOOSER:
			getAvailableLayouts();
			_listAdapter.notifyDataSetChanged();
			// _lvLayout.
			/*
			 * labels = new CharSequence[_availableLayouts.size()];
			 * 
			 * for(int i=0 ; i<_availableLayouts.size() ; i++){ LayoutItem obj =
			 * _availableLayouts.get(i); labels[i] = "["+ new
			 * Utility(this).typeToString((Integer) obj.LAYOUT_TYPE) +"] " +
			 * (CharSequence) obj.LAYOUT_NAME;
			 * 
			 * }
			 */
			break;

		}

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		Dialog dialog = null;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		switch (id) {
		/*
		 * case DIALOG_PANEL_CHOOSER:
		 * 
		 * getAvailableLayouts();
		 * 
		 * labels = new CharSequence[_availableLayouts.size()];
		 * 
		 * for(int i=0 ; i<_availableLayouts.size() ; i++){ LayoutItem obj =
		 * _availableLayouts.get(i); labels[i] = "["+ new
		 * Utility(this).typeToString((Integer) obj.LAYOUT_TYPE) +"] " +
		 * (CharSequence) obj.LAYOUT_NAME; }
		 * 
		 * 
		 * 
		 * AlertDialog.Builder builder = new AlertDialog.Builder(this);
		 * LayoutInflater inflater =
		 * (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 * View l = inflater.inflate(R.layout.layout_chooser, null); _lvLayout =
		 * (ListView) l.findViewById(R.id.layoutlist);
		 * _lvLayout.setOnItemClickListener(new OnItemClickListener(){
		 * 
		 * @Override public void onItemClick(AdapterView<?> parent, View view,
		 * int position, long id) { // TODO Auto-generated method stub
		 * 
		 * LayoutItem layout = _availableLayouts.get(position); _currentEdit =
		 * position; try { loadLayout(layout); setState(STATE_READY);
		 * dismissDialog(DIALOG_PANEL_CHOOSER); } catch (JSONException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 * 
		 * 
		 * }
		 * 
		 * });
		 * 
		 * 
		 * 
		 * _listAdapter = new
		 * SimpleAdapter(this,_list,R.layout.menu_list_item,new
		 * String[]{"name","type"},new int[]{R.id.name,R.id.type});
		 * _lvLayout.setAdapter(_listAdapter);
		 * 
		 * 
		 * 
		 * builder.setView(l);
		 * 
		 * 
		 * 
		 * dialog = builder.create();
		 * 
		 * break;
		 */
		case DIALOG_LAYOUT_NAME:

			Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
					.getDefaultDisplay();
			int width = display.getWidth();

			dialog = new Dialog(this);

			dialog.setContentView(R.layout.namesetting);
			dialog.setTitle(getString(R.string.navieroperation_setpanelname));

			etName = (EditText) dialog.findViewById(R.id.editText1);

			if (_currentEdit != NEW_LAYOUT) {
				etName.setText(this._availableLayouts.get(_currentEdit).LAYOUT_NAME);
			}

			Button ok = (Button) dialog.findViewById(R.id.ok);
			ok.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						saveCurrLayout(etName.getText().toString());
						_layoutPanel.handleNewButton();
						_currentEdit = NEW_LAYOUT;
						// doSync();
						Toast.makeText(NavierPanelDesigner.this,
								getString(R.string.panel_created),
								Toast.LENGTH_LONG).show();
						finish();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						dismissDialog(DIALOG_LAYOUT_NAME);
					}

				}

			});

			Button cancel = (Button) dialog.findViewById(R.id.cancel);
			cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dismissDialog(DIALOG_LAYOUT_NAME);
				}

			});

			break;

		case DIALOG_LAYOUT_TYPE:

			labels = new CharSequence[] { "Normal Panel", "Navigation Panel" };

			builder = new AlertDialog.Builder(this);
			builder.setTitle("Panel type");
			builder.setItems(labels, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					try {
						if (which == 0) {
							_currentType = Layout.TYPE_DASHBOARD;
						} else {
							_currentType = Layout.TYPE_NAVIGATION;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			});

			dialog = builder.create();
			break;

		}

		return dialog;
	}

	private void doSync() {
		if (_settings == null)
			_settings = new PreferencesHandler(this);
		Intent intent = new Intent(this, SyncManager.class);
		intent.putExtra(SyncManager.EXTRA_ACCOUNT, _settings.getPREF_ACCOUNT());
		startService(intent);
	}

	private void saveCurrLayout() throws JSONException {
		saveCurrLayout(this
				.getString(R.string.navierpaneldesigner_defaulelayoutname));
	}

	private void saveCurrLayout(String name) throws JSONException {

		// int type = checkLayoutType();

		// encode partnails to json
		JSONObject json = new JSONObject();
		JSONObject root = new JSONObject();

		root.put(Layout.JSON_NAME, name);
		root.put(Layout.JSON_MODE, Layout.MODE_HUD);
		// root.put(Layout.JSON_TYPE, type);

		JSONArray parts = new JSONArray();
		boolean isNav = false;

		for (int i = 0; i < _layoutPanel._allPartNails.size(); i++) {

			PartNail p = _layoutPanel._allPartNails.get(i);

			if (p.ELEM_ISNAVPART == true) {
				isNav = true;
			}

			JSONObject part = new JSONObject();
			part.put(Layout.JSON_PARTS_PART, p.ELEM_ID);

			JSONObject pin = new JSONObject();
			pin.put("x", p.ELEM_PIN[0]);
			pin.put("y", p.ELEM_PIN[1]);

			part.put(Layout.JSON_PARTS_PIN, pin);

			parts.put(part);

		}
		root.put(Layout.JSON_TYPE, isNav ? Layout.TYPE_NAVIGATION
				: Layout.TYPE_DASHBOARD);

		root.put(Layout.JSON_PARTS, parts);
		json.put(Layout.JSON_ROOT, root);

		if (_currentEdit == NEW_LAYOUT) {
			insert2Db(json);
		} else {
			LayoutItem item = this._availableLayouts.get(_currentEdit);
			item.LAYOUT_NAME = name;
			item.LAYOUT_JSONSTR = json.toString();
			update2Db(item);
		}

		_layoutPanel._allPartNails.clear();
		this.setState(STATE_NEW);

	}

	private void insert2Db(JSONObject json) {

		long currTimeMillis = System.currentTimeMillis();
		long id = PanelStore.addDbPanel(this, json.toString(),
				String.valueOf(currTimeMillis));
		// ContentValues values = new ContentValues();
		// values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, json.toString() );
		// values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP, currTimeMillis);
		// values.put(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT, currTimeMillis);
		//
		// long id = db.insert(MyDBOpenHelper.LAYOUT_TABLE_NAME, null, values);
		// Uri uri =
		// this.getContentResolver().insert(LayoutTableMetadata.CONTENT_URI,
		// values);

		// insert to sync layout
		// values = new ContentValues();
		// values.put(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE,
		// MyDBOpenHelper.STATE_NEW);
		// values.put(MyDBOpenHelper.SYNC_LAYOUT_LAYOUTID, id);
		// values.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT, currTimeMillis);
		// // initBeforeSetContentView layout + currenttimemillis
		//
		// db.insert(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME,null,values);
		PanelStore.addDbSyncPanel(this, id, MyDBOpenHelper.STATE_NEW,
				String.valueOf(currTimeMillis));

		new PushPanelTask(this, null, _settings.getPREF_ACCOUNT()).execute();
		Log.d("layout: ", json.toString());
	}

	private void update2Db(LayoutItem item) {

		String json = item.LAYOUT_JSONSTR;
		PanelStore.updateDbPanel(this, json, item.LAYOUT_FINGERPRINT);
		// ContentValues values = new ContentValues();
		// values.put(MyDBOpenHelper.LAYOUT_COL_LAYOUT, json.toString() );
		// values.put(MyDBOpenHelper.LAYOUT_COL_TIMESTAMP,
		// System.currentTimeMillis());
		//
		// String whereClause = MyDBOpenHelper.LAYOUT_COL_FINGERPRINT + "=?";
		// String[] whereArgs = new
		// String[]{String.valueOf(item.LAYOUT_FINGERPRINT)};
		//
		// db.update(MyDBOpenHelper.LAYOUT_TABLE_NAME, values, whereClause,
		// whereArgs);

		// this.getContentResolver().update(Uri.withAppendedPath(LayoutTableMetadata.CONTENT_URI,
		// item.LAYOUT_FINGERPRINT), values, null, null);

		// insert to sync layout
		// values = new ContentValues();
		// values.put(MyDBOpenHelper.SYNC_LAYOUT_SYNCSTATE,
		// MyDBOpenHelper.STATE_MODIFY);
		// values.put(MyDBOpenHelper.SYNC_LAYOUT_LAYOUTID, item.LAYOUT_ID);
		// values.put(MyDBOpenHelper.SYNC_LAYOUT_FINGERPRINT,
		// item.LAYOUT_FINGERPRINT);
		// db.insert(MyDBOpenHelper.SYNC_LAYOUT_TABLE_NAME,null,values);
		PanelStore.addDbSyncPanel(this, item.LAYOUT_ID,
				MyDBOpenHelper.STATE_MODIFY, item.LAYOUT_FINGERPRINT);
		new PushPanelTask(this, null, _settings.getPREF_ACCOUNT()).execute();
	}

	private int checkLayoutType() {

		for (int i = 0; i < _layoutPanel._allPartNails.size(); i++) {
			PartNail p = _layoutPanel._allPartNails.get(i);
			if (p.ELEM_ISNAVPART) {

				return Layout.TYPE_NAVIGATION;
			}
		}
		return Layout.TYPE_DASHBOARD;

	}

	private void loadLayout(LayoutItem layout) throws JSONException {

		for (int i = 0; i < _availableLayouts.size(); i++) {
			LayoutItem obj = _availableLayouts.get(i);
			if (layout.LAYOUT_FINGERPRINT
					.equalsIgnoreCase(obj.LAYOUT_FINGERPRINT)) {
				_currentEdit = i;
				loadLayout(i);
				break;
			}
		}
	}

	private void loadLayout(int n) throws JSONException {

		Utility utility = new Utility(this);
		LayoutItem obj = _availableLayouts.get(n);
		JSONObject json = new JSONObject((String) obj.LAYOUT_JSONSTR);
		JSONObject root = ((JSONObject) json.get(Layout.JSON_ROOT));
		JSONArray array = root.getJSONArray(Layout.JSON_PARTS);

		ArrayList<PartNail> partlist = new ArrayList<PartNail>();

		for (int i = 0; i < array.length(); i++) {
			JSONObject part = array.getJSONObject(i);
			int id = part.optInt(Layout.JSON_PARTS_PART);
			/*
			 * PartNail nail = new PartNail(utility.getPartName(id), id,
			 * utility.getPartDimension(id), utility.getPartThumbnail(id));
			 * nail.ELEM_PIN = new int[]{pin.optInt("x"), pin.optInt("y")};
			 */
			JSONObject pin = part.getJSONObject(Layout.JSON_PARTS_PIN);
			PartNail p = _partMap.get(id);
			p.setPin(new int[] { pin.optInt("x"), pin.optInt("y") });
			partlist.add(_partMap.get(id));
		}

		_layoutPanel.handleLoadLayout(partlist);

	}

	private void getAvailableLayouts() {

		_availableLayouts = new ArrayList<LayoutItem>();
		_list.clear();

		Utility util = new Utility(this);

		_availableLayouts = new ArrayList<LayoutItem>();

		try {

			// get from row
			/*
			 * HashMap map = new HashMap(); String jsonstring =
			 * util.getStringFromRaw(R.raw.navigation_classic);
			 * map.put(LAYOUT_JSONSTRING, jsonstring); map.put(LAYOUT_NAME,
			 * util.getLayoutName(jsonstring)); map.put(LAYOUT_TYPE,
			 * util.getLayoutType(jsonstring)); map.put(, value)
			 */

			/*
			 * String jsonstring =
			 * util.getStringFromRaw(R.raw.navigation_classic);
			 * 
			 * LayoutItem obj = new LayoutItem(); obj._isStored = true;
			 * obj._isEditable = false; obj.LAYOUT_JSONSTR = jsonstring;
			 * obj.LAYOUT_NAME = util.getLayoutName(jsonstring); obj.LAYOUT_TYPE
			 * = util.getLayoutType(jsonstring);
			 * 
			 * 
			 * _availableLayouts.add(obj);
			 * 
			 * 
			 * HashMap map = new HashMap(); map.put("name", obj.LAYOUT_NAME);
			 * map.put("type",
			 * obj.LAYOUT_TYPE==Layout.TYPE_DASHBOARD?R.drawable.
			 * nor_panel:R.drawable.nav_panel);
			 * 
			 * _list.add(map);
			 */

			LayoutItem obj = new LayoutItem();
			HashMap map = new HashMap();

			// get from db
			String[] columns = { MyDBOpenHelper.LAYOUT_COL_ID,
					MyDBOpenHelper.LAYOUT_COL_LAYOUT,
					MyDBOpenHelper.LAYOUT_COL_TIMESTAMP,
					MyDBOpenHelper.LAYOUT_COL_FINGERPRINT };
			String selection = null; // if press faverite button
			String[] selectionArgs = {};
			String groupBy = "";
			String having = "";
			String orderBy = "";

			Cursor c = MyDBOpenHelper.getDb(this).query(
					MyDBOpenHelper.LAYOUT_TABLE_NAME, columns, selection,
					selectionArgs, groupBy, having, orderBy);

			while (c.moveToNext()) {
				obj = new LayoutItem();
				obj._isStored = true;
				obj._isEditable = true;
				obj.LAYOUT_ID = c.getInt(c
						.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_ID));
				obj.LAYOUT_JSONSTR = c.getString(c
						.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_LAYOUT));
				obj.LAYOUT_NAME = util.getLayoutName(obj.LAYOUT_JSONSTR);
				obj.LAYOUT_TYPE = util.getLayoutType(obj.LAYOUT_JSONSTR);
				obj.LAYOUT_FINGERPRINT = c.getString(c
						.getColumnIndex(MyDBOpenHelper.LAYOUT_COL_FINGERPRINT));

				_availableLayouts.add(obj);

				map = new HashMap();
				map.put("name", obj.LAYOUT_NAME);
				map.put("type",
						obj.LAYOUT_TYPE == Layout.TYPE_DASHBOARD ? R.drawable.nor_panel
								: R.drawable.nav_panel);

				_list.add(map);
			}
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void handlePartnailClick(PartNail p) {
		Toast.makeText(this, p.ELEM_NAME, Toast.LENGTH_SHORT).show();
		_slidingDrawer.animateClose();
		_layoutPanel.addEditablePartNail(p);
		_layoutPanel.setOnTouchListener(this);
		setState(STATE_EDIT);
	}

	public void setState(int state) {
		switch (state) {
		case STATE_EDIT:

			_currentState = STATE_EDIT;
			_okcancel.setVisibility(View.VISIBLE);

			break;
		case STATE_NEW:

			_currentState = STATE_NEW;
			_okcancel.setVisibility(View.INVISIBLE);

			break;
		case STATE_READY:

			_currentState = STATE_READY;
			_okcancel.setVisibility(View.INVISIBLE);
			break;
		}
	}

	public class NorPartAdapter extends BaseAdapter {

		ArrayList<PartNail> _partNails = new ArrayList<PartNail>();
		LayoutInflater inflater;

		public NorPartAdapter() {
			inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			initParts();
		}

		private void initParts() {
			// _partNails = new ArrayList<PartNail>();
			// _partMap = new HashMap();

			// SIMPLE COMPASS
			PartNail part = new PartNail(getResources().getString(
					R.string.part_compass), Elem_Compass.ELEM_PART, new int[] {
					Elem_Compass.ELEM_WIDTH, Elem_Compass.ELEM_HEIGHT },
					Elem_Compass.ELEM_THUMBNAIL, Elem_Compass.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_Compass.ELEM_PART, part);

			// SIMPLE COMPASS TINY
			part = new PartNail(getResources().getString(
					R.string.part_simplecompass_tiny),
					Elem_SimpleCompass_Tiny.ELEM_PART, new int[] {
							Elem_SimpleCompass_Tiny.ELEM_WIDTH,
							Elem_SimpleCompass_Tiny.ELEM_HEIGHT },
					Elem_SimpleCompass_Tiny.ELEM_THUMBNAIL,
					Elem_SimpleCompass_Tiny.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_SimpleCompass_Tiny.ELEM_PART, part);

			// COMPASS_WHEEL
			part = new PartNail(getResources().getString(
					R.string.part_compasswheel), Elem_CompassWheel.ELEM_PART,
					new int[] { Elem_CompassWheel.ELEM_WIDTH,
							Elem_CompassWheel.ELEM_HEIGHT },
					Elem_CompassWheel.ELEM_THUMBNAIL,
					Elem_CompassWheel.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_CompassWheel.ELEM_PART, part);

			// SPEEDOMETER
			part = new PartNail(getResources().getString(R.string.part_speed),
					Elem_Speed.ELEM_PART, new int[] { Elem_Speed.ELEM_WIDTH,
							Elem_Speed.ELEM_HEIGHT },
					Elem_Speed.ELEM_THUMBNAIL, Elem_Speed.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_Speed.ELEM_PART, part);

			// DIGITAL SPEED
			part = new PartNail(getResources().getString(
					R.string.part_speeddigital), Elem_SpeedDigital.ELEM_PART,
					new int[] { Elem_SpeedDigital.ELEM_WIDTH,
							Elem_SpeedDigital.ELEM_HEIGHT },
					Elem_SpeedDigital.ELEM_THUMBNAIL,
					Elem_SpeedDigital.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_SpeedDigital.ELEM_PART, part);

			// DIGITAL SPEED LARGE
			part = new PartNail(getResources().getString(
					R.string.part_speeddigital_large),
					Elem_SpeedDigital_Large.ELEM_PART, new int[] {
							Elem_SpeedDigital_Large.ELEM_WIDTH,
							Elem_SpeedDigital_Large.ELEM_HEIGHT },
					Elem_SpeedDigital_Large.ELEM_THUMBNAIL,
					Elem_SpeedDigital_Large.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_SpeedDigital_Large.ELEM_PART, part);

			// SPEED CHART
			part = new PartNail(getResources().getString(
					R.string.part_speedchart), Elem_SpeedChart.ELEM_PART,
					new int[] { Elem_SpeedChart.ELEM_WIDTH,
							Elem_SpeedChart.ELEM_HEIGHT },
					Elem_SpeedChart.ELEM_THUMBNAIL,
					Elem_SpeedChart.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_SpeedChart.ELEM_PART, part);

			// SPEED CHART LARGE
			part = new PartNail(getResources().getString(
					R.string.part_speedchart_large),
					Elem_SpeedChart_Large.ELEM_PART, new int[] {
							Elem_SpeedChart_Large.ELEM_WIDTH,
							Elem_SpeedChart_Large.ELEM_HEIGHT },
					Elem_SpeedChart_Large.ELEM_THUMBNAIL,
					Elem_SpeedChart_Large.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_SpeedChart_Large.ELEM_PART, part);

			// ACCELERATION CHART
			part = new PartNail(getResources()
					.getString(R.string.part_accchart),
					Elem_Acceleration_Chart.ELEM_PART, new int[] {
							Elem_Acceleration_Chart.ELEM_WIDTH,
							Elem_Acceleration_Chart.ELEM_HEIGHT },
					Elem_Acceleration_Chart.ELEM_THUMBNAIL,
					Elem_Acceleration_Chart.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_Acceleration_Chart.ELEM_PART, part);

			// MAX SPEED
			part = new PartNail(getResources()
					.getString(R.string.part_maxspeed),
					Elem_MaxSpeed.ELEM_PART,
					new int[] { Elem_MaxSpeed.ELEM_WIDTH,
							Elem_MaxSpeed.ELEM_HEIGHT },
					Elem_MaxSpeed.ELEM_THUMBNAIL, Elem_MaxSpeed.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_MaxSpeed.ELEM_PART, part);

			// ACCELERATION
			part = new PartNail(getResources().getString(
					R.string.part_accdigital), Elem_AccDigital.ELEM_PART,
					new int[] { Elem_AccDigital.ELEM_WIDTH,
							Elem_AccDigital.ELEM_HEIGHT },
					Elem_AccDigital.ELEM_THUMBNAIL,
					Elem_AccDigital.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_AccDigital.ELEM_PART, part);

			// DIGITAL CLOCK
			part = new PartNail(getResources().getString(
					R.string.part_digitalclock), Elem_DigitalClock.ELEM_PART,
					new int[] { Elem_DigitalClock.ELEM_WIDTH,
							Elem_DigitalClock.ELEM_HEIGHT },
					Elem_DigitalClock.ELEM_THUMBNAIL,
					Elem_DigitalClock.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_DigitalClock.ELEM_PART, part);

			// AVERAGE SPEED
			part = new PartNail(getResources().getString(
					R.string.part_averagespeed), Elem_AverageSpeed.ELEM_PART,
					new int[] { Elem_AverageSpeed.ELEM_WIDTH,
							Elem_AverageSpeed.ELEM_HEIGHT },
					Elem_AverageSpeed.ELEM_THUMBNAIL,
					Elem_AverageSpeed.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_AverageSpeed.ELEM_PART, part);

			// TOTAL DISTANCE
			part = new PartNail(getResources()
					.getString(R.string.part_distance),
					Elem_TotalDistance.ELEM_PART, new int[] {
							Elem_TotalDistance.ELEM_WIDTH,
							Elem_TotalDistance.ELEM_HEIGHT },
					Elem_TotalDistance.ELEM_THUMBNAIL,
					Elem_TotalDistance.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_TotalDistance.ELEM_PART, part);

			// TOTAL TIME
			part = new PartNail(getResources().getString(
					R.string.part_totaltime), Elem_TotalTime.ELEM_PART,
					new int[] { Elem_TotalTime.ELEM_WIDTH,
							Elem_TotalTime.ELEM_HEIGHT },
					Elem_TotalTime.ELEM_THUMBNAIL,
					Elem_TotalTime.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_TotalTime.ELEM_PART, part);

			// GPS STATE
			part = new PartNail(getResources()
					.getString(R.string.part_gpsstate),
					Elem_GPSState.ELEM_PART,
					new int[] { Elem_GPSState.ELEM_WIDTH,
							Elem_GPSState.ELEM_HEIGHT },
					Elem_GPSState.ELEM_THUMBNAIL, Elem_GPSState.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_GPSState.ELEM_PART, part);

			// GPS CHART
			part = new PartNail(getResources()
					.getString(R.string.part_gpschart),
					Elem_GPSChart.ELEM_PART,
					new int[] { Elem_GPSChart.ELEM_WIDTH,
							Elem_GPSChart.ELEM_HEIGHT },
					Elem_GPSChart.ELEM_THUMBNAIL, Elem_GPSChart.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_GPSChart.ELEM_PART, part);

			// GPS CHART LARGE
			part = new PartNail(getResources().getString(
					R.string.part_gpschart_large),
					Elem_GpsChart_Large.ELEM_PART, new int[] {
							Elem_GpsChart_Large.ELEM_WIDTH,
							Elem_GpsChart_Large.ELEM_HEIGHT },
					Elem_GpsChart_Large.ELEM_THUMBNAIL,
					Elem_GpsChart_Large.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_GpsChart_Large.ELEM_PART, part);

			// Battery
			part = new PartNail(
					getResources().getString(R.string.part_battery),
					Elem_Battery.ELEM_PART,
					new int[] { Elem_Battery.ELEM_WIDTH,
							Elem_Battery.ELEM_HEIGHT },
					Elem_Battery.ELEM_THUMBNAIL, Elem_Battery.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_Battery.ELEM_PART, part);

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return _partNails.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			PartNail n = _partNails.get(position);
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.partnail, null);
			}

			LinearLayout root = (LinearLayout) convertView
					.findViewById(R.id.root);
			if (mPremiumParts.contains(n.ELEM_ID)) {
				root.setBackgroundResource(R.drawable.shadow_bg_premium);
			}

			String intro = n.ELEM_NAME + " \n(" + n.ELEM_DIMENSION[0] + " X "
					+ n.ELEM_DIMENSION[1] + ")";

			ImageView image = (ImageView) convertView
					.findViewById(R.id.imageView1);
			TextView text = (TextView) convertView.findViewById(R.id.textView1);

			text.setText(intro);

			Drawable d = getResources().getDrawable(n.ELEM_DRAWABLE);
			image.setImageDrawable(d);

			convertView.setOnClickListener(new onPartChoosed(n));

			return convertView;
		}

	}

	public class NavPartAdapter extends BaseAdapter {

		LayoutInflater inflater;
		ArrayList<PartNail> _partNails = new ArrayList<PartNail>();;

		public NavPartAdapter() {
			inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			initParts();
		}

		private void initParts() {
			// _partNails = new ArrayList<PartNail>();
			// _partMap = new HashMap();

			// 3d route
			PartNail part = new PartNail(getResources().getString(
					R.string.part_route_3d), Elem_Route_3D.ELEM_PART,
					new int[] { Elem_Route_3D.ELEM_WIDTH,
							Elem_Route_3D.ELEM_HEIGHT },
					Elem_Route_3D.ELEM_THUMBNAIL, Elem_Route_3D.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_Route_3D.ELEM_PART, part);

			// ROAD NAME
			part = new PartNail(getResources().getString(
					R.string.part_road_name), Elem_Road_Name.ELEM_PART,
					new int[] { Elem_Road_Name.ELEM_WIDTH,
							Elem_Road_Name.ELEM_HEIGHT },
					Elem_Road_Name.ELEM_THUMBNAIL,
					Elem_Road_Name.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_Road_Name.ELEM_PART, part);

			// Wide route

			part = new PartNail(getResources().getString(
					R.string.part_route_wide), Elem_Route_Wide.ELEM_PART,
					new int[] { Elem_Route_Wide.ELEM_WIDTH,
							Elem_Route_Wide.ELEM_HEIGHT },
					Elem_Route_Wide.ELEM_THUMBNAIL,
					Elem_Route_Wide.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_Route_Wide.ELEM_PART, part);

			// Distance to turn

			part = new PartNail(getResources().getString(
					R.string.part_distance_to_turn),
					Elem_DistanceToTurn.ELEM_PART, new int[] {
							Elem_DistanceToTurn.ELEM_WIDTH,
							Elem_DistanceToTurn.ELEM_HEIGHT },
					Elem_DistanceToTurn.ELEM_THUMBNAIL,
					Elem_DistanceToTurn.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_DistanceToTurn.ELEM_PART, part);

			// DIRECTION_ROUTE
			part = new PartNail(getResources().getString(R.string.part_route),
					Elem_Route.ELEM_PART, new int[] { Elem_Route.ELEM_WIDTH,
							Elem_Route.ELEM_HEIGHT },
					Elem_Route.ELEM_THUMBNAIL, Elem_Route.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_Route.ELEM_PART, part);

			// NEXT TURN
			part = new PartNail(getResources()
					.getString(R.string.part_nextturn),
					Elem_NextTurn.ELEM_PART,
					new int[] { Elem_NextTurn.ELEM_WIDTH,
							Elem_NextTurn.ELEM_HEIGHT },
					Elem_NextTurn.ELEM_THUMBNAIL, Elem_NextTurn.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_NextTurn.ELEM_PART, part);

			// ROUTE PROGRESS
			part = new PartNail(getResources().getString(
					R.string.part_routeprogress), Elem_RouteProgress.ELEM_PART,
					new int[] { Elem_RouteProgress.ELEM_WIDTH,
							Elem_RouteProgress.ELEM_HEIGHT },
					Elem_RouteProgress.ELEM_THUMBNAIL,
					Elem_RouteProgress.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_RouteProgress.ELEM_PART, part);

			// DISTANCE LEFT
			part = new PartNail(getResources().getString(
					R.string.part_distanceleft), Elem_DistanceLeft.ELEM_PART,
					new int[] { Elem_DistanceLeft.ELEM_WIDTH,
							Elem_DistanceLeft.ELEM_HEIGHT },
					Elem_DistanceLeft.ELEM_THUMBNAIL,
					Elem_DistanceLeft.ELEM_ISNAVPART);
			_partNails.add(part);
			_partMap.put(Elem_DistanceLeft.ELEM_PART, part);

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return _partNails.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			PartNail n = _partNails.get(position);
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.partnail, null);
			}
			LinearLayout root = (LinearLayout) convertView
					.findViewById(R.id.root);
			if (mPremiumParts.contains(n.ELEM_ID)) {
				root.setBackgroundResource(R.drawable.shadow_bg_premium);
			} else {
				root.setBackgroundResource(R.drawable.shadow_bg);
			}

			String intro = n.ELEM_NAME + " \n(" + n.ELEM_DIMENSION[0] + " X "
					+ n.ELEM_DIMENSION[1] + ")";

			ImageView image = (ImageView) convertView
					.findViewById(R.id.imageView1);
			TextView text = (TextView) convertView.findViewById(R.id.textView1);

			text.setText(intro);

			Drawable d = getResources().getDrawable(n.ELEM_DRAWABLE);
			image.setImageDrawable(d);

			convertView.setOnClickListener(new onPartChoosed(n));

			return convertView;
		}

	}

	public class onPartChoosed implements OnClickListener {

		PartNail _p;

		public onPartChoosed(PartNail p) {
			_p = p;
		}

		@Override
		public void onClick(View v) {
			String message = "";

			if (mPremiumParts.contains(_p.ELEM_ID)
					&& Version.isLite(NavierPanelDesigner.this)) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						NavierPanelDesigner.this);
				if (mPremiumParts.contains(_p.ELEM_ID)){
					builder.setMessage(R.string.premium_only);
				}
					
				builder.setPositiveButton(R.string.navieraccount_upgradebtn, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent it = new Intent(NavierPanelDesigner.this, DownloadActivity.class);
						it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(it);
					}
				});
				builder.setNegativeButton(R.string.naviernaviconfigure_ok, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				});
				
				builder.show();
			} else {
				handlePartnailClick(_p);
				return;

			}
		}

	}

	public class PartNail {

		public String ELEM_NAME;
		public int ELEM_ID;
		public int[] ELEM_DIMENSION = new int[2];
		public int[] ELEM_PIN = new int[2];
		public int ELEM_DRAWABLE;
		public boolean ELEM_ISNAVPART;
		public int[] ELEM_RECT = new int[4]; // current left, top, right, bottom

		public PartNail(String name, int id, int[] dimension, int drawable,
				boolean isnavipart) {

			ELEM_NAME = name;
			ELEM_ID = id;
			ELEM_DIMENSION = dimension;
			ELEM_DRAWABLE = drawable;
			ELEM_ISNAVPART = isnavipart;

		}

		public void drawNail(Canvas canvas, int wMargin, int hMargin,
				int unitPixel) {

			Rect r = new Rect(wMargin + ELEM_PIN[0] * unitPixel, hMargin
					+ ELEM_PIN[1] * unitPixel, wMargin + ELEM_DIMENSION[0]
					* unitPixel + ELEM_PIN[0] * unitPixel, hMargin
					+ ELEM_DIMENSION[1] * unitPixel + ELEM_PIN[1] * unitPixel);
			Paint paint = new Paint();
			paint.setStyle(Style.STROKE);
			paint.setStrokeWidth(5);
			paint.setColor(Color.WHITE);
			canvas.drawRect(r, paint);

		}

		public void setPin(int[] pin) {
			ELEM_PIN = pin;
			setRect();
		}

		public void setRect() {
			int left = ELEM_PIN[0] * _layoutPanel._unitPixel
					+ _layoutPanel._screenWidthMargin;
			int top = ELEM_PIN[1] * _layoutPanel._unitPixel
					+ _layoutPanel._screenHeightMargin;
			int right = left + ELEM_DIMENSION[0] * _layoutPanel._unitPixel;
			int bottom = top + ELEM_DIMENSION[1] * _layoutPanel._unitPixel;

			ELEM_RECT = new int[] { left, top, right, bottom };
		}

		public void drawShadow(Canvas canvas, int uniPixel, int iniLeft,
				int iniTop, int[] pin) {
			switch (ELEM_ID) {
			case Elem_Compass.ELEM_PART:
				Elem_Compass.drawShadow(canvas, uniPixel, iniLeft, iniTop, pin,
						NavierPanelDesigner.this);
				break;

			case Elem_SimpleCompass_Tiny.ELEM_PART:
				Elem_SimpleCompass_Tiny.drawShadow(canvas, uniPixel, iniLeft,
						iniTop, pin, NavierPanelDesigner.this);
				break;

			case Elem_CompassWheel.ELEM_PART:
				Elem_CompassWheel.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;

			case Elem_Route.ELEM_PART:
				Elem_Route.drawShadow(canvas, uniPixel, iniLeft, iniTop, pin,
						NavierPanelDesigner.this);
				break;

			case Elem_Route_3D.ELEM_PART:
				Elem_Route_3D.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;
			case Elem_Route_Wide.ELEM_PART:
				Elem_Route_Wide.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;
			case Elem_Speed.ELEM_PART:
				Elem_Speed.drawShadow(canvas, uniPixel, iniLeft, iniTop, pin,
						NavierPanelDesigner.this);
				break;

			case Elem_SpeedDigital.ELEM_PART:
				Elem_SpeedDigital.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;

			case Elem_SpeedDigital_Large.ELEM_PART:
				Elem_SpeedDigital_Large.drawShadow(canvas, uniPixel, iniLeft,
						iniTop, pin, NavierPanelDesigner.this);
				break;

			case Elem_SpeedChart.ELEM_PART:
				Elem_SpeedChart.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;

			case Elem_SpeedChart_Large.ELEM_PART:
				Elem_SpeedChart_Large.drawShadow(canvas, uniPixel, iniLeft,
						iniTop, pin, NavierPanelDesigner.this);
				break;

			case Elem_Acceleration_Chart.ELEM_PART:
				Elem_Acceleration_Chart.drawShadow(canvas, uniPixel, iniLeft,
						iniTop, pin, NavierPanelDesigner.this);
				break;

			case Elem_MaxSpeed.ELEM_PART:
				Elem_MaxSpeed.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;

			case Elem_AccDigital.ELEM_PART:
				Elem_AccDigital.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;

			case Elem_DigitalClock.ELEM_PART:
				Elem_DigitalClock.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;

			case Elem_AverageSpeed.ELEM_PART:
				Elem_AverageSpeed.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;

			case Elem_TotalDistance.ELEM_PART:
				Elem_TotalDistance.drawShadow(canvas, uniPixel, iniLeft,
						iniTop, pin, NavierPanelDesigner.this);
				break;

			case Elem_DistanceLeft.ELEM_PART:
				Elem_DistanceLeft.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;

			case Elem_TotalTime.ELEM_PART:
				Elem_TotalTime.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;

			case Elem_GPSState.ELEM_PART:
				Elem_GPSState.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;

			case Elem_GPSChart.ELEM_PART:
				Elem_GPSChart.drawShadow(canvas, uniPixel, iniLeft, iniTop,
						pin, NavierPanelDesigner.this);
				break;

			case Elem_GpsChart_Large.ELEM_PART:
				Elem_GpsChart_Large.drawShadow(canvas, uniPixel, iniLeft,
						iniTop, pin, NavierPanelDesigner.this);
				break;

			case Elem_Battery.ELEM_PART:
				Elem_Battery.drawShadow(canvas, uniPixel, iniLeft, iniTop, pin,
						NavierPanelDesigner.this);
				break;

			case Elem_NextTurn.ELEM_PART:
				Elem_NextTurn.drawShadow(canvas, uniPixel, iniLeft + pin[0],
						iniTop + pin[1], pin, NavierPanelDesigner.this);
				break;

			case Elem_RouteProgress.ELEM_PART:
				Elem_RouteProgress.drawShadow(canvas, uniPixel, iniLeft
						+ pin[0], iniTop + pin[1], pin,
						NavierPanelDesigner.this);
				break;
			case Elem_Road_Name.ELEM_PART:
				Elem_Road_Name.drawShadow(canvas, uniPixel, iniLeft + pin[0],
						iniTop + pin[1], pin, NavierPanelDesigner.this);
				break;

			case Elem_DistanceToTurn.ELEM_PART:
				Elem_DistanceToTurn.drawShadow(canvas, uniPixel, iniLeft
						+ pin[0], iniTop + pin[1], pin,
						NavierPanelDesigner.this);
				break;

			}

		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		Log.d("mine", "ACTION: " + event.getAction());
		return false;
	}

}
