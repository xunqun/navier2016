package idv.xunqun.navier;

import idv.xunqun.navier.utils.Utility;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class NavierAbout extends Activity {

	//views
	private EditText et_comment;
	private ProgressDialog dialog;
	private WebView mWebView;
	//values
	private PreferencesHandler _settings;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.about);
		initViews();
	}

	private void initViews(){
		_settings = new PreferencesHandler(this);
		
		et_comment = (EditText) this.findViewById(R.id.suggestion);
		
		
		Button submit = (Button) this.findViewById(R.id.submit);
		submit.setText(getString(R.string.naviernaviconfigure_ok));
		submit.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(et_comment.getText().toString().trim()!=""){
					/*
					dialog = ProgressDialog.show(NavierAbout.this, "", getString(R.string.naviernaviconfigure_plzwait), true);
					new SubmitTask(et_comment.getText().toString(), _settings.getPREF_ACCOUNT(), NavierAbout.this).execute(new Object());
					*/
					
					Intent emailIntent = new Intent(Intent.ACTION_SEND);
					emailIntent.setType("plain/text");
					emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{ "lu.shangchiun@gmail.com"});
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Navier HUD Feedback");
					emailIntent.putExtra(Intent.EXTRA_TEXT, et_comment.getText());
					startActivity(Intent.createChooser(emailIntent, "Give me a feedback..."));
				}
			}
		});
		
		TextView language = (TextView) this.findViewById(R.id.language);
		language.setText("Localization is supported by " + getString(R.string.language_support));
		
		LinearLayout market = (LinearLayout) this.findViewById(R.id.market);
		market.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent it = new Intent(Intent.ACTION_VIEW); 
				if(Version.isLite(NavierAbout.this)){
	                it.setData(Uri.parse("market://details?id=idv.xunqun.navier"));					
				}else{
					it.setData(Uri.parse("market://details?id=idv.xunqun.navier.premium"));
				}
				startActivity(it);
			}
			
		});
		
	    mWebView = (WebView) findViewById(R.id.webView1);
	    try{
	    	mWebView.loadUrl("http://asnippets.blogspot.tw/2012/06/whats-new-in-navier-hud.html?m=1");
	    }catch(Exception e){
	    	
	    }
	    
	    TextView tv_version = (TextView) this.findViewById(R.id.textView1);
	    
	    String strVersion="";
	    try{
		    if(Version.isLite(this)){
		    	strVersion = this.getPackageManager().getPackageInfo("idv.xunqun.navier", PackageManager.GET_ACTIVITIES).versionName;
		    }else{
		    	strVersion = this.getPackageManager().getPackageInfo("idv.xunqun.navier.premium", PackageManager.GET_ACTIVITIES).versionName;
		    }
	    }catch(Exception e){
	    	
	    }
	    tv_version.setText(strVersion);
	}
	

	
	private class SubmitTask extends AsyncTask{

		private DefaultHttpClient client;
		private String SUBMIT_URL = "http://navierlab.appspot.com/suggestcomment";
		
		private String COMMENT = "comment";
		private String USER = "user";
		
		//values
		private String _comment;
		private String _user;
		private Context _context;
		
		boolean _result;
		
		public SubmitTask(String comment, String user, Context context){
			_comment = comment;
			_user = user;
			_context = context;
		}
		
		@Override
		protected void onPostExecute(Object result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			if(!_result){
				Toast.makeText(NavierAbout.this, "Data transmission fail, please try again later.", Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(NavierAbout.this, "Thanks your suggestion!.", Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected Object doInBackground(Object... params) {
			// TODO Auto-generated method stub
			
			_result = sendHttpPost();
			
			
			return null;
		}
		
		private boolean sendHttpPost(){
			
			List<NameValuePair> paramList = new ArrayList<NameValuePair>();
			paramList.add(new BasicNameValuePair(COMMENT, _comment)); 
			paramList.add(new BasicNameValuePair(USER,_user));
			
			
			
			
			client = new DefaultHttpClient();
			HttpPost request = new HttpPost(SUBMIT_URL);
			try {
				request.setEntity(new UrlEncodedFormEntity(paramList,HTTP.UTF_8));
				
				HttpResponse response = client.execute(request);
				
				if(response.getStatusLine().getStatusCode() ==200){
					
					return true;
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			return false;
			
			
		}
		
	}



	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Utility.requestLocationServiceStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Utility.requestLocationServiceUpdate(this);
	}
	
}
