package idv.xunqun.navier;

import android.app.Application;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

public class NavierApplication extends Application {
	
	public static LocationManager locationManager;
	
	public static final String EXTRA_ONLOCATIONCHANGE = "EXTRA_ONLOCATIONCHANGE";
	public static final String EXTRA_ONPROVIDERDISABLEED = "EXTRA_ONPROVIDERDISABLEED";
	public static final String EXTRA_ONPROVIDERENABLED = "EXTRA_ONPROVIDERENABLED";
	public static final String EXTRA_ONSTATUSCHANGED = "EXTRA_ONSTATUSCHANGED";
	
	
	
	private LocationListener locationListener = new LocationListener(){

		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			Intent it = new Intent("EXTRA_ONLOCATIONCHANGE");
			it.putExtra("location", location);
			LocalBroadcastManager.getInstance(NavierApplication.this).sendBroadcast(it);
			
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			Intent it = new Intent("EXTRA_ONPROVIDERDISABLEED");
			it.putExtra("provider", provider);
			LocalBroadcastManager.getInstance(NavierApplication.this).sendBroadcast(it);
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			Intent it = new Intent("EXTRA_ONPROVIDERENABLED");
			it.putExtra("provider", provider);
			LocalBroadcastManager.getInstance(NavierApplication.this).sendBroadcast(it);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			Intent it = new Intent("EXTRA_ONSTATUSCHANGED");
			it.putExtra("provider", provider);
			it.putExtra("status", status);
			it.putExtra("extra", extras);
			LocalBroadcastManager.getInstance(NavierApplication.this).sendBroadcast(it);
		}
		
	};
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		/*
		locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		
		try{
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0, 0, locationListener);
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0, 0, locationListener);
		}catch(Exception e){
			e.printStackTrace();
		}
		*/
	}

	@Override
	public void onTerminate() {
		// TODO Auto-generated method stub
		super.onTerminate();
		
		/*
		try{
			locationManager.removeUpdates(locationListener);
			locationManager.removeUpdates(locationListener);
		}catch(Exception e){
			e.printStackTrace();
		}
		*/
	}
	
	
}
